<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
	if(Auth::user()){
		return redirect('/dashboard');
	}
	else{
		return redirect('/login');
	}
});

	Route::group(['middleware'=>'auth'], function ($router)
	{

			Route::get('dashboard', 'DashboardController@index');
			Route::get('/home', 'HomeController@index')->name('home');
			Route::get('role/create','RoleController@createRole');
			Route::get('role/list','RoleController@getRoleList');
			Route::post('role/store','RoleController@storeRole');
			Route::post('role/get-permissions','RoleController@getAccountPermissions');
			Route::post('role/get-edit-permissions','RoleController@getEditAccountPermissions');
			Route::get('role/delete/{id}','RoleController@deleteRole');
			Route::get('role/view/{id}','RoleController@viewRole');
			Route::get('role/edit/{id}','RoleController@editRole');
			Route::post('role/update','RoleController@updateRole');
			Route::post('role/data-list','RoleController@roleDataTable')->name('roleDataTable');
			//.........................User Related Routes..............................
			Route::get('user/create','RegisterController@createUser');
			Route::post('user/store','RegisterController@storeUser');
			Route::get('user/list','RegisterController@viewUserList');

			Route::get('user/delete/{id}','RegisterController@deleteUser');

			Route::get('user/view/{id}','RegisterController@viewUser');

			Route::get('user/edit/{id}','RegisterController@editUser');

			Route::post('user/update','RegisterController@updateUser');

			Route::get('user/update-status/{id}','RegisterController@updateUserStatus');

			Route::post('user/data-list','RegisterController@getList')->name('getList');
			
			Route::post('user/update/session','RegisterController@updateSession');

			Route::get('user/edit-profile','RegisterController@editUserProfile');
			Route::post('user/update-password','RegisterController@updateUserPassword');
		//------------Accounts Routes--------------------------------
			Route::get('account/create','AccountController@createAccount');
			Route::get('account/list','AccountController@viewAccountList');
			Route::get('account/view/{id}','AccountController@viewAccount');
			Route::get('account/edit/{id}','AccountController@editAccount');

			Route::post('account/store','ApiControllers\AccountsController@storeAccount');
			Route::post('account/data-list','ApiControllers\AccountsController@getAccountsList')->name('getList');
			Route::post('account/update','ApiControllers\AccountsController@updteAccount')->name('updteAccount');

		//------------Settings Routes--------------------------------
			Route::get('corpusFund/create','SettingsController@createCorpusFund');
			Route::post('corpusFund/store','SettingsController@storeCorpusFund');
			Route::get('corpusFund/edit/{id}','SettingsController@editCorpusFund');
			Route::post('corpusFund/update','SettingsController@updateCorpusFund');
			Route::get('corpusFund/list','SettingsController@viewCorpusFundList');
			Route::post('corpusFund/data-list','SettingsController@getCorpusFundData');

			Route::get('fcSetting/view','SettingsController@viewFCSetting');
			Route::get('fcSetting/edit','SettingsController@editFCSetting');
			Route::post('fcSetting/update','ApiControllers\SettingController@updateFCSetting');

		//------------Member Management Routes--------------------------------
			Route::get('member/create','MemberController@createMember');
			Route::post('member/store','ApiControllers\MemberController@storeMember');
			Route::get('member/edit/{id}','MemberController@editMember');
			Route::get('member/view/{id}','MemberController@viewMember');

			Route::post('member/checkDuplicates','MemberController@checkDuplicateValues');

			Route::post('member/update/perosnalDetails','ApiControllers\MemberController@updatePerosnalDetails');

			Route::post('member/update/medicalDetails','ApiControllers\MemberController@updateMedicalDetails');

			Route::post('member/update/memberDetails','ApiControllers\MemberController@updateMemberDetails');

			Route::post('member/update/nomineeDetails','ApiControllers\MemberController@updateNomineeDetails');
			Route::get('member/delete/nomineeDetails/{id}','ApiControllers\MemberController@deleteNomineeDetails');

			Route::post('member/update/paymentDetails','ApiControllers\MemberController@updatePaymentDetails');
			Route::post('member/update/MemberStatus','ApiControllers\MemberController@updateMemberStatus');

			Route::get('member/list','MemberController@viewMemberList');
			Route::post('member/data-list','MemberController@getMembersData');
			Route::post('member/getCorpusAmount','MemberController@getCorpusAmount');
			Route::post('member/getFCAmount','MemberController@getFCAmount');
			Route::post('member/getPendingDue','MemberController@getPendingDue');

			Route::get('member/expired/list','MemberController@viewExpiredMemberList');
			Route::post('member/expired/data-list','MemberController@getExpiredMembersData');

			Route::get('member/inactive/list','MemberController@viewInactiveMemberList');
			Route::post('member/inactive/data-list','MemberController@getInactiveMembersData');
			
			Route::get('member/active/list','MemberController@viewActiveMemberList');
			Route::post('member/active/data-list','MemberController@getActiveMembersData');

			Route::get('member/probationary/list','MemberController@viewProbationaryMemberList');
			Route::post('member/probationary/data-list','MemberController@getProbationaryMembersData');

			Route::get('getStatesofCountry/{id}','MemberController@getStatesofCountry');
			Route::get('getCitiesofState/{id}','MemberController@getCitiesofState');

			Route::get('payments/pending/list','PaymentsController@viewPendingPayments');

			Route::post('payments/pending/data-list','PaymentsController@getPendingPaymentsData');
			Route::get('payments/pending/view/{id}','PaymentsController@viewPaymentById');
			Route::post('payments/pending/get-data','PaymentsController@getPaymentsData');
			
			Route::get('payments/received/list','PaymentsController@viewReceivedPayments');
			Route::post('payments/received/data-list','PaymentsController@getReceivedPaymentsData');
			
			Route::post('payment/storePayment','ApiControllers\PaymentController@storePayment');
			Route::post('payment/storePenalty','ApiControllers\PaymentController@storePenalty');
			Route::post('member/getDueAmount','PaymentsController@getDueAmount');
			Route::post('member/getMembers','PaymentsController@getMembers');
			Route::get('member/profile','MemberController@getMemberProfile');
			Route::get('member/pendingPayment','PaymentsController@getMemberPendingPayment');
			Route::get('member/clearedPayment','PaymentsController@getMemberClearedPayment');
			Route::post('member/invoice/data-list','PaymentsController@getMemberInvoiceList');

			Route::post('payments/cleared/data-list','PaymentsController@getMemberClearedPaymentsData');

			Route::get('payments/penalties/list','PaymentsController@viewPenalties');
			Route::post('payments/penalties/data-list','PaymentsController@getPenaltiesData');

			Route::get('insurance/list','PaymentsController@viewInsuranceList');
			Route::post('insurance/data-list','PaymentsController@getInsuranceListData');
			Route::post('insurance/member/data-list','PaymentsController@getMemberInsuranceData');
			Route::post('insurance/update','ApiControllers\PaymentController@updateInsurance');

			Route::get('critical_defaulters/list','PaymentsController@viewCriticalDefaultersList');
			Route::post('critical_defaulters/data-list','PaymentsController@getCriticalDefaultersData');

			Route::get('reports/fc/list','ReportsController@viewTotalFCsReleased');
			Route::post('reports/fc/data-list','ReportsController@getFcReleasedData');

			Route::get('reports/advance/list','ReportsController@viewTotalAdvancePayments');
			Route::post('reports/advance/data-list','ReportsController@getTotalAdvancePaymentsData');

			Route::get('reports/corpusFund/list','ReportsController@viewTotalCorpusFunds');
			Route::post('reports/corpusFund/data-list','ReportsController@getTotalCorpusFundsData');

	});
	
	Route::get('/logout','Auth\LoginController@logout');