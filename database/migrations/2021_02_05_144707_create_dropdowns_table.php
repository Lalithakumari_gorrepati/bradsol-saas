<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDropdownsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dropdowns', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('dropdown_id')->nullable();
            $table->foreign('dropdown_id')->references('id')->on('dropdown_types')->onDelete('cascade');
            $table->string('dropdown_value');
            $table->enum('status',['Active','InActive'])->default('Active');
            $table->timestamps();
            $table->string('created_by');
            $table->string('updated_by')->nullable();
            $table->enum('is_deleted',['Y','N'])->default('N');
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dropdowns');
    }
}
