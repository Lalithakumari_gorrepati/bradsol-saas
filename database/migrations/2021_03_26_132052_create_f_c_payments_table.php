<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFCPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fc_payments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('member_id');
            $table->unsignedBigInteger('payment_against_member')->nullable();
            $table->enum('payment_type',['advance','fc_amount','due_penalty']);
            $table->string('amount_to_pay')->nullable();
            $table->string('admin_expense')->nullable();
            $table->string('total_payment');
            $table->string('paid_amount')->nullable();
            $table->date('bill_date');
            $table->date('due_date');
            $table->unsignedBigInteger('invoice_id')->nullable();
            $table->enum('has_paid',['Y','N'])->default('N');
            $table->timestamps();
            $table->string('created_by');
            $table->string('updated_by')->nullable();
        });

        Schema::table('fc_payments', function($table) {
            $table->foreign('member_id')->references('id')->on('users');
        });
        Schema::table('fc_payments', function($table) {
            $table->foreign('payment_against_member')->references('id')->on('users');
        });

        Schema::table('fc_payments', function($table) {
            $table->foreign('invoice_id')->references('id')->on('invoices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fc_payments');
    }
}
