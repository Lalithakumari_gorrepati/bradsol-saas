<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembershipDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('membership_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('corpus_amount');
            $table->date('join_date');
            $table->integer('age');
            $table->enum('status', ['Active', 'Inactive','Expired','Probationary'])->default('Active');;
            $table->date('confirm_date')->nullable();
            $table->string('fc_amount')->nullable();
            $table->string('released_fc_amount')->nullable();
            $table->text('note')->nullable();
            $table->timestamps();
            $table->string('created_by');
            $table->string('updated_by')->nullable();
        });
         Schema::table('membership_details', function($table) {
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('membership_details');
    }
}
