<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profiles', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('member_id')->unique();
            $table->string('alternative_email')->nullable();
            $table->string('alternative_phone')->nullable();
            $table->string('martial_status')->nullable();
            $table->string('country')->nullable();
            $table->string('state')->nullable();
            $table->string('city')->nullable();
            $table->text('id_proof')->nullable();
            $table->text('age_proof')->nullable();
            $table->enum('physical_disability',['Y', 'N'])->default('N');;
            $table->string('physical_disability_type')->nullable();
            $table->enum('previous_illness',['Y', 'N'])->default('N');;
            $table->string('illness_type')->nullable();
            $table->enum('health_declaration',['Y', 'N'])->default('N');;
            $table->text('scanned_application')->nullable();
            $table->text('reports')->nullable();
            $table->text('address')->nullable();
            $table->text('image')->nullable();
            $table->timestamps();
            $table->string('created_by');
            $table->string('updated_by')->nullable();
        });

         Schema::table('user_profiles', function($table) {
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profiles');
    }
}
