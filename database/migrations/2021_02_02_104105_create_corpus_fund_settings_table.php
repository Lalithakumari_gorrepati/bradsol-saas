<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCorpusFundSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('corpus_fund_settings', function (Blueprint $table) {
            $table->id();
            $table->string('age_from');
            $table->string('age_to');
            $table->string('corpus_fund');
            $table->enum('is_deleted', ['Y', 'N'])->default('N');
            $table->timestamps();
            $table->string('created_by');
            $table->string('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('corpus_fund_settings');
    }
}
