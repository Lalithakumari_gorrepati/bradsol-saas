<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
           $table->id();
           $table->string('code')->nullable();
           $table->string('name');
           $table->enum('status',['Active','InActive'])->default('Active');
           $table->integer('phonecode')->nullable();
           $table->timestamps();
           $table->string('created_by')->default('Admin');
           $table->string('updated_by')->nullable();
           $table->enum('is_deleted',['Y','N'])->default('N');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
