<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->enum('status',['Active','InActive'])->default('Active');
            $table->unsignedBigInteger('state_id');      
            $table->timestamps();
            $table->string('created_by')->default('Admin');
            $table->string('updated_by')->nullable();
            $table->enum('is_deleted',['Y','N'])->default('N');
        });

        Schema::table('cities', function($table) {
               $table->foreign('state_id')->references('id')->on('states');
           });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
