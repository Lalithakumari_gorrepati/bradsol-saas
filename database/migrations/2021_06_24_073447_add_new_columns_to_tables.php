<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewColumnsToTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!Schema::hasColumn('nominees','fc_percentage')){
            Schema::table('nominees', function (Blueprint $table) {
                $table->string('fc_percentage')->nullable()->after('nominee_relationship');
            });
        }

        if (!Schema::hasColumn('user_profiles','approved_status')){
            Schema::table('user_profiles', function (Blueprint $table) {
                $table->enum('approved_status',['Approved','NotApproved'])->default('Approved')->after('image');
            });
        }

        if (!Schema::hasColumn('user_profiles','interested_in_other_scheme')){
            Schema::table('user_profiles', function (Blueprint $table) {
                $table->enum('interested_in_other_scheme',['Y','N'])->nullable()->after('image');
            });
        }

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
