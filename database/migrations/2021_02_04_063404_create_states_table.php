<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('states', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedBigInteger('country_id');
            $table->enum('status',['Active','InActive'])->default('Active');
            $table->timestamps();
            $table->string('created_by')->default('Admin');
            $table->string('updated_by')->nullable();
            $table->enum('is_deleted',['Y','N'])->default('N');
        });
        Schema::table('states', function($table) {
                $table->foreign('country_id')->references('id')->on('countries');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('states');
    }
}
