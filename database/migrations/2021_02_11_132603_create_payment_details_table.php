<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('payment_id');
            $table->string('transaction_id')->nullable();
            $table->unsignedBigInteger('payment_against_member')->nullable();
            $table->enum('payment_type',['advance','corpus_amount','fc_amount','due_penalty']);
            $table->timestamps();
            $table->string('created_by');
            $table->string('updated_by')->nullable();
        });

        Schema::table('payment_details', function($table) {
            $table->foreign('payment_id')->references('id')->on('payments');
        });

        Schema::table('payment_details', function($table) {
            $table->foreign('payment_against_member')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_details');
    }
}
