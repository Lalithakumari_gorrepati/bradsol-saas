<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//dashboard
        $permission = Permission::updateOrCreate(['name' => 'Total Accounts','screen'=>'dashboard','account_id'=>'2']);
        $permission = Permission::updateOrCreate(['name' => 'Total Active Accounts','screen'=>'dashboard','account_id'=>'2']);
        $permission = Permission::updateOrCreate(['name' => 'Total InActive Accounts','screen'=>'dashboard','account_id'=>'2']);
        $permission = Permission::updateOrCreate(['name' => 'Total Users','screen'=>'dashboard','account_id'=>'2']);
        $permission = Permission::updateOrCreate(['name' => 'Total Active Users','screen'=>'dashboard','account_id'=>'2']);
        $permission = Permission::updateOrCreate(['name' => 'Total InActive Users','screen'=>'dashboard','account_id'=>'2']);
        $permission = Permission::updateOrCreate(['name' => 'Total Registration','screen'=>'dashboard','account_id'=>'2']);
        $permission = Permission::updateOrCreate(['name' => 'Total Active Members','screen'=>'dashboard','account_id'=>'2']);
        $permission = Permission::updateOrCreate(['name' => 'Total Inactive Members','screen'=>'dashboard','account_id'=>'2']);
        $permission = Permission::updateOrCreate(['name' => 'Total Expired Members','screen'=>'dashboard','account_id'=>'2']);
        $permission = Permission::updateOrCreate(['name' => 'Total Probationary Members','screen'=>'dashboard','account_id'=>'2']);
        $permission = Permission::updateOrCreate(['name' => 'Total Pending Payments','screen'=>'dashboard','account_id'=>'2']);
        $permission = Permission::updateOrCreate(['name' => 'Total Received Payments','screen'=>'dashboard','account_id'=>'2']);
        $permission = Permission::updateOrCreate(['name' => 'Total Accidental Insurance','screen'=>'dashboard','account_id'=>'2']);
        $permission = Permission::updateOrCreate(['name' => 'Total Critical Defaulters','screen'=>'dashboard','account_id'=>'2']);
        
    	//User and role
        $permission = Permission::updateOrCreate(['name' => 'Admin Dashboard','screen'=>'user and role']);
        $permission = Permission::updateOrCreate(['name' => 'User & Role Management','screen'=>'user and role']);
    		//role
        $permission = Permission::updateOrCreate(['name' => 'Role Management','screen'=>'user and role']);
        $permission = Permission::updateOrCreate(['name' => 'Add Roles','screen'=>'user and role']);
        $permission = Permission::updateOrCreate(['name' => 'View Roles','screen'=>'user and role']);
        $permission = Permission::updateOrCreate(['name' => 'Edit Roles','screen'=>'user and role']);
        $permission = Permission::updateOrCreate(['name' => 'Update Profile & Password','screen'=>'user and role']);
    		
    		//user
        $permission = Permission::updateOrCreate(['name' => 'User Management','screen'=>'user and role']);
        $permission = Permission::updateOrCreate(['name' => 'Add Users','screen'=>'user and role']);
        $permission = Permission::updateOrCreate(['name' => 'Edit Users','screen'=>'user and role']);
        $permission = Permission::updateOrCreate(['name' => 'View Users','screen'=>'user and role']);
        $permission = Permission::updateOrCreate(['name' => 'Accounts Management Main Menu','screen'=>'user and role']);
            //Accounts Screen
            
        $permission = Permission::updateOrCreate(['name' => 'Accounts Management','screen'=>'accounts management']);
        $permission = Permission::updateOrCreate(['name' => 'View Accounts List','screen'=>'accounts management']);
        $permission = Permission::updateOrCreate(['name' => 'Add Account','screen'=>'accounts management']);
        $permission = Permission::updateOrCreate(['name' => 'Edit Account','screen'=>'accounts management']);
        $permission = Permission::updateOrCreate(['name' => 'View Account','screen'=>'accounts management']);


            //Corpus fund Screen
            $permission = Permission::updateOrCreate(['name' => 'Configurations Setting','screen'=>'corpus fund','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'Corpus Fund Setting','screen'=>'corpus fund','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'Add Corpus Fund','screen'=>'corpus fund','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'Edit Corpus Fund','screen'=>'corpus fund','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'View Corpus Fund','screen'=>'corpus fund','account_id'=>'2']);
            
            //Fraternity Contribution Screen
            $permission = Permission::updateOrCreate(['name' => 'Fraternity Contribution Setting','screen'=>'FC Setting','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'View FC Setting','screen'=>'FC Setting','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'Edit FC Setting','screen'=>'FC Setting','account_id'=>'2']);

            //Member management
            $permission = Permission::updateOrCreate(['name' => 'Members Management','screen'=>'member management','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'Total Registrations','screen'=>'member management','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'Active members','screen'=>'member management','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'Add Member','screen'=>'member management','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'Edit Member','screen'=>'member management','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'View Member','screen'=>'member management','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'Change Member Status','screen'=>'member management','account_id'=>'2']);
            
            $permission = Permission::updateOrCreate(['name' => 'Inactive members','screen'=>'member management','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'Expired members','screen'=>'member management','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'Probationary members','screen'=>'member management','account_id'=>'2']);

            //Payments Screen
            $permission = Permission::updateOrCreate(['name' => 'Payments Management','screen'=>'payments management','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'Pending Payments','screen'=>'payments management','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'Received Payments','screen'=>'payments management','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'Add New Payment','screen'=>'payments management','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'View Payment','screen'=>'payments management','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'View Penalties','screen'=>'payments management','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'Accidental Insurance List','screen'=>'payments management','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'View Insurance','screen'=>'payments management','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'Edit Insurance','screen'=>'payments management','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'Critical Defaulters','screen'=>'payments management','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'Download Invoice','screen'=>'payments management','account_id'=>'2']);

            //Reports Screen
            $permission = Permission::updateOrCreate(['name' => 'Reports Management','screen'=>'reports','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'FC Released Reports','screen'=>'reports','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'Total Advance Payments Reports','screen'=>'reports','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'Corpus Fund Reports','screen'=>'reports','account_id'=>'2']);

            //Member Dashboard Screen
            $permission = Permission::updateOrCreate(['name' => 'Member Dashboard','screen'=>'member dashboard','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'My Profile','screen'=>'member dashboard','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'Member Pending Payments','screen'=>'member dashboard','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'Member Cleared Payments','screen'=>'member dashboard','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'View Pending Payment','screen'=>'member dashboard','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'View Cleared Payment','screen'=>'member dashboard','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'Member General Setting','screen'=>'member dashboard','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'About Scheme','screen'=>'member dashboard','account_id'=>'2']);
            $permission = Permission::updateOrCreate(['name' => 'Download Bill','screen'=>'member dashboard','account_id'=>'2']);
    }
}