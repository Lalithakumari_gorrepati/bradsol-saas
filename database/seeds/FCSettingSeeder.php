<?php

use Illuminate\Database\Seeder;
use App\Models\Setting;
use App\Models\Account;
use Carbon\Carbon;
class FCSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $account_id=Account::where('account_name','MF-FSS')->value('id');
        if (!empty($account_id)) {
            
            Setting::firstOrCreate([
        		'name'=>'Scheme Name',
        		'value'=>'Mahesh Foundation- Family Security Scheme',
        		'setting_type'=>'FC_Setting',
        		'account_id'=> $account_id,
        		'created_by' => 'superadmin@gmail.com',
    		]);
                
    		Setting::firstOrCreate([
    			'name'=>'Fraternity Amount',
        		'value'=>'1000000',
        		'setting_type'=>'FC_Setting',
        		'account_id'=> $account_id,
        		'created_by' => 'superadmin@gmail.com',
    		]);

    		Setting::firstOrCreate([
    			'name'=>'Eligibility Age From',
        		'value'=>'21',
        		'setting_type'=>'FC_Setting',
        		'account_id'=> $account_id,
        		'created_by' => 'superadmin@gmail.com',
    		]);

    		Setting::firstOrCreate([
    			'name'=>'Eligibility Age To',
        		'value'=>'55',
        		'setting_type'=>'FC_Setting',
        		'account_id'=> $account_id,
        		'created_by' => 'superadmin@gmail.com',
    		]);

    		Setting::firstOrCreate([
    			'name'=>'FC First Year',
        		'value'=>'Not Eligible',
        		'setting_type'=>'FC_Setting',
        		'account_id'=> $account_id,
        		'created_by' => 'superadmin@gmail.com',
    		]);

    		Setting::firstOrCreate([
    			'name'=>'FC Second Year',
        		'value'=>'500000',
        		'setting_type'=>'FC_Setting',
        		'account_id'=> $account_id,
        		'created_by' => 'superadmin@gmail.com',
    		]);

    		Setting::firstOrCreate([
    			'name'=>'Advance Amount Half Yearly',
        		'value'=>'4000',
        		'setting_type'=>'FC_Setting',
        		'account_id'=> $account_id,
        		'created_by' => 'superadmin@gmail.com',
    		]);

    		Setting::firstOrCreate([
    			'name'=>'Terms and Conditions',
        		'value'=>'Terms and Conditions',
        		'setting_type'=>'FC_Setting',
        		'account_id'=> $account_id,
        		'created_by' => 'superadmin@gmail.com',
    		]);

            Setting::firstOrCreate([
                'name'=>'Due Penalty Per Month',
                'value'=>'100',
                'setting_type'=>'FC_Setting',
                'account_id'=> $account_id,
                'created_by' => 'superadmin@gmail.com',
            ]);

            Setting::firstOrCreate([
                'name'=>'All By Laws',
                'value'=>'The Corpus Fundreceived towardsfraternity contribution isto be exclusively used for the security scheme.<br> 
                   i)The Fraternity contributionshall be Rs. 10,00,000/-(Rupees Tenlakh only) ondeath of the member.Additional insurance cover of Rs.5.00 lacs shall betaken for members upto the age of 30years with any insuring agency and the premium shall be paidby the MF-FSS Scheme.<br>
                   ii) The Corpus Fundshall be invested indeposit with banks I investments incorporate Bonds/Government Securities etc. Or as decided bythe governingbody.<br>
                   iii) The Fraternity contributionwillbe collected equally from amongst the members.<br>
                   iv)  The interest on Corpus Fundwillbeavailable to beutilized for other objectives of Mahesh Foundation.<br>
                   v)   Rs.100/-will becollected per member perdeath towards theAdministrative Expenses to meet the expenses under the scheme.<br>
                   vi)  The billing cyclewill beon a halfyearly basisand bills shall be raised on 1stApril coveringthe period (October-March) and on 1stOctober coveringthe period (April-September).<br>
                   a)   The bill isto bepaidwithin 30days from thedateof issue of billwhichwill be 1stApriland 1st October.<br>
                   b)   Advance of Rs.4,000/- will becollectedfrom member ineach half yearly billingcycle inthe 1styear only to meet the fraternity contribution liability that may arise on the death of themember<br>
                   c)   Inthe event of nonpayment bythe member penalty of Rs.100/-per monthwillbecharged subject to a maximumof Rs.250/- besides interest at 15%p.a shallbeseparately levied for the period under default.<br>
                   d).lncase default exceeds 12 months from the billdate membershipwould stand automatically terminated and as a consequence thereof his/her nominee,will/shall not beeligible to claimI beentitled to the benefit under the Scheme.For eg:AprilBillpayment willhave to becleared by 30thof April(onemonth) but not later than March 31st inthe following year with arrears ;interest and penalty.<br>
                   e) Failure toclear the billwillresult intermination of the member from the scheme andadvance paid bythe member will be adjusted andaccount closed.<br>
                   vii) Notices not received bythe member due to lapse of postal authorities oron account of any other reasons, the member
                   shall obtaindetailsof the billamount payable by correspondingwiththe MF-FSS Officeand is to effect the payment.<br>
                  ',
                'setting_type'=>'FC_Setting',
                'account_id'=> $account_id,
                'created_by' => 'superadmin@gmail.com',
            ]);
        }
    }
}
