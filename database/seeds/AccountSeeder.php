<?php

use Illuminate\Database\Seeder;
use App\Models\Account;
use Carbon\Carbon;
class AccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $account_exist=Account::where('contact_email','manager@bradsol.com')->first();
       if($account_exist === null)
       {
        $account_id = Account::insertGetId([
            'account_name' => 'bradsol',
            'account_domain' => 'bradsol.com',
            'contact_name' => 'Bradsol Manager',
            'contact_email' => 'manager@bradsol.com',
            'contact_phone'=>'9030782213',
            'additional_info'=>'Test Info',
            'logo'=>null,
            'is_active'=>'Y',
            'is_deleted'=>'N',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        }

        $account_exist=Account::where('contact_email','manager@mffss.com')->first();
       if($account_exist === null)
       {
         $account_id = Account::insertGetId([
            'account_name'  =>'MF-FSS',
            'account_domain'=>'mffss.com',
            'contact_name' => 'MF-FSS Manager',
            'contact_email' => 'manager@mffss.com',
            'contact_phone'=>'9030782213',
            'additional_info'=>'Testing Account for Mahesh Foundation',
            'logo'=>null,
            'is_active'=>'Y',
            'is_deleted'=>'N',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
           ]);
        }
    }
}
