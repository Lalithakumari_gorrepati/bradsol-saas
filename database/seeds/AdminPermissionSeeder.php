<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class AdminPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

          $permission= Permission::all();
         
			$role=Role::where('name','SuperAdmin')->first();
            
            $role->syncPermissions($permission);
           
    }
}
