<?php

use Illuminate\Database\Seeder;
use App\Models\DropdownType;
use App\Models\Dropdown;

class DropdownValuesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = date('Y-m-d H:i:s'); 
        $data = DropdownType::where(['dropdown_name'=>'Marital Status'])->first();
        $id = $data->id;
        $items = [
    		['dropdown_id'=>$id,'dropdown_value'=>'Never Married','status'=>'Active','created_at'=> $date,'created_by'=>'Admin','is_deleted'=>'N'],
    		['dropdown_id'=>$id,'dropdown_value'=>'Awaiting Divorce','status'=>'Active','created_at'=> $date,'created_by'=>'Admin','is_deleted'=>'N'],
    		['dropdown_id'=>$id,'dropdown_value'=>'Divorced','status'=>'Active','created_at'=> $date,'created_by'=>'Admin','is_deleted'=>'N'],
    		['dropdown_id'=>$id,'dropdown_value'=>'Widowed','status'=>'Active','created_at'=> $date,'created_by'=>'Admin','is_deleted'=>'N'],
    		['dropdown_id'=>$id,'dropdown_value'=>'Annulled','status'=>'Active','created_at'=> $date,'created_by'=>'Admin','is_deleted'=>'N'] ];

        foreach ($items as $item) {
            Dropdown::updateOrCreate(['dropdown_id' => $item['dropdown_id'],'dropdown_value'=> $item['dropdown_value']], $item);
        }

        $data = DropdownType::where(['dropdown_name'=>'Customer Id Type'])->first();
        
        if(!empty($data->id)){
            $id = $data->id;
            $items = [
                ['dropdown_id'=>$id,'dropdown_value'=>'Passport','status'=>'Active','created_at'=> $date,'created_by'=>'Admin','is_deleted'=>'N'],
                ['dropdown_id'=>$id,'dropdown_value'=>'Pan','status'=>'Active','created_at'=> $date,'created_by'=>'Admin','is_deleted'=>'N'],
                ['dropdown_id'=>$id,'dropdown_value'=>'Voter','status'=>'Active','created_at'=> $date,'created_by'=>'Admin','is_deleted'=>'N'],
                ['dropdown_id'=>$id,'dropdown_value'=>'Driving','status'=>'Active','created_at'=> $date,'created_by'=>'Admin','is_deleted'=>'N'] ,
                ['dropdown_id'=>$id,'dropdown_value'=>'Aadhar','status'=>'Active','created_at'=> $date,'created_by'=>'Admin','is_deleted'=>'N'] ];
            
            foreach ($items as $item) {
                Dropdown::updateOrCreate(['dropdown_id' => $item['dropdown_id'],'dropdown_value'=> $item['dropdown_value']], $item);
            }
        }

        $data = DropdownType::where(['dropdown_name'=>'Illness Type'])
        ->first();
        
        if(!empty($data->id)){
            $id = $data->id;
            $items = [
                ['dropdown_id'=>$id,'dropdown_value'=>'Cancer','status'=>'Active','created_at'=> $date,'created_by'=>'Admin','is_deleted'=>'N'],
                ['dropdown_id'=>$id,'dropdown_value'=>'AIDS','status'=>'Active','created_at'=> $date,'created_by'=>'Admin','is_deleted'=>'N'],
                ['dropdown_id'=>$id,'dropdown_value'=>'Serious Heart Disease','status'=>'Active','created_at'=> $date,'created_by'=>'Admin','is_deleted'=>'N'],
                ['dropdown_id'=>$id,'dropdown_value'=>'Degenerative Brain & other Disease','status'=>'Active','created_at'=> $date,'created_by'=>'Admin','is_deleted'=>'N'] ,
                ['dropdown_id'=>$id,'dropdown_value'=>'Liver Kideney Failure','status'=>'Active','created_at'=> $date,'created_by'=>'Admin','is_deleted'=>'N'],
                ['dropdown_id'=>$id,'dropdown_value'=>'Valve Replacement','status'=>'Active','created_at'=> $date,'created_by'=>'Admin','is_deleted'=>'N'],
                ['dropdown_id'=>$id,'dropdown_value'=>'Bypass','status'=>'Active','created_at'=> $date,'created_by'=>'Admin','is_deleted'=>'N'],
                ['dropdown_id'=>$id,'dropdown_value'=>'Major Surgery','status'=>'Active','created_at'=> $date,'created_by'=>'Admin','is_deleted'=>'N'],
                ['dropdown_id'=>$id,'dropdown_value'=>'Heart Attack','status'=>'Active','created_at'=> $date,'created_by'=>'Admin','is_deleted'=>'N'],
                ['dropdown_id'=>$id,'dropdown_value'=>'Organ Transplantation','status'=>'Active','created_at'=> $date,'created_by'=>'Admin','is_deleted'=>'N'],
                ['dropdown_id'=>$id,'dropdown_value'=>'Paralysis','status'=>'Active','created_at'=> $date,'created_by'=>'Admin','is_deleted'=>'N'],
                ['dropdown_id'=>$id,'dropdown_value'=>'Diseases related to Arota','status'=>'Active','created_at'=> $date,'created_by'=>'Admin','is_deleted'=>'N'],
              ];
            
            foreach ($items as $item) {
                Dropdown::updateOrCreate(['dropdown_id' => $item['dropdown_id'],'dropdown_value'=> $item['dropdown_value']], $item);
            }
        }

        $date = date('Y-m-d H:i:s');
        $data = DropdownType::where(['dropdown_name'=>'Payment Mode'])->first();
        $id = $data->id;
        $items = [
    		['dropdown_id'=>$id,'dropdown_value'=>'Cash','status'=>'Active','created_at'=> $date,'created_by'=>'Admin','is_deleted'=>'N'],
    		['dropdown_id'=>$id,'dropdown_value'=>'Bank','status'=>'Active','created_at'=> $date,'created_by'=>'Admin','is_deleted'=>'N'],
            ['dropdown_id'=>$id,'dropdown_value'=>'Cheque','status'=>'Active','created_at'=> $date,'created_by'=>'Admin','is_deleted'=>'N'] ];

        foreach ($items as $item) {
            Dropdown::updateOrCreate(['dropdown_id' => $item['dropdown_id'],'dropdown_value'=> $item['dropdown_value']], $item);
        }
    }
}
