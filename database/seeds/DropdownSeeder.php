<?php

use Illuminate\Database\Seeder;
use App\Models\DropdownType;
class DropdownSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DropdownType::firstOrCreate(
    		['dropdown_name'=>'Marital Status','dependent_dropdown'=>null]);
        DropdownType::firstOrCreate(['dropdown_name'=>'Customer Id Type','dependent_dropdown'=>null]);
        DropdownType::firstOrCreate(['dropdown_name'=>'Illness Type','dependent_dropdown'=>null]);
        DropdownType::firstOrCreate(
            ['dropdown_name'=>'Payment Mode','dependent_dropdown'=>null]);
    }
}
