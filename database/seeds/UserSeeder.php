<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;
use App\Models\UserAccount;
use Carbon\Carbon;
use App\Models\Account;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_exist=User::where('email','superadmin@gmail.com')->first();
       if($user_exist === null)
       {
         $permission= Permission::all();
         $user_id = User::insertGetId([

            'name' => 'superadmin',
            'email' => 'superadmin@gmail.com',
            'mobile_number'=>'9030782213',
            'role'=>'1',
            'gender'=>'Female',
            'dob'=>'1990/01/01',
            'password' => \Hash::make('password'),
            'is_deleted'=>'N',
            'is_active'=>'Y',
            'created_by' => 'superadmin@gmail.com',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
           ]);
        
         $user=User::where('id',$user_id)->first();
         
         $user->assignRole('SuperAdmin');
         
         $role=Role::where('id',1)->first();
            
            foreach ($permission as $value) 
            {
                $role->givePermissionTo($value);
            }
        }

        $user_exist=User::where('email','admin@mffss.com')->first();
        if($user_exist === null)
        {
            $role_id=Role::where('name','Admin')->value('id');
            $account_id=Account::where('account_name','MF-FSS')->value('id');
            $user_id = User::insertGetId([
                'name' =>'MF-FSS Admin',
                'email' =>'admin@mffss.com',
                'mobile_number'=>'1234567890',
                'role'=>$role_id,
                'gender'=>'Male',
                'dob'=>'1990/01/01',
                'password' => \Hash::make('password'),
                'is_deleted'=>'N',
                'is_active'=>'Y',
                'created_by' => 'superadmin@gmail.com',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);

            if (!empty($user_id) && !empty($account_id)) {
                $user=User::where('id',$user_id)->first();
                $user->assignRole('Admin');

                $account_data=[];
                $account_data=array( 
                    'user_id'     => $user_id,
                    'account_id'  => $account_id,
                    'created_at'  => date('Y-m-d h:i:s'),
                    'created_by'  => 'superadmin@gmail.com',
                );
                if (!empty($account_data)) {
                    $user=UserAccount::create($account_data); 
                }
            }
        } 
    }
}