<?php

use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $countries = array(
			array('code' => 'IN','name' => "India",'phonecode' => 91),
		);
		DB::table('countries')->insert($countries);
    }
}
