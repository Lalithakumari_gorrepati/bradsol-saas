<?php

use Illuminate\Database\Seeder;
use App\Models\CorpusFundSetting;
use Carbon\Carbon;
class CorpusFundSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CorpusFundSetting::updateOrCreate(
            [
            'age_from'=>'21',
            'age_to'=>'27',
            'corpus_fund'=>'2000',
             'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' =>null,
            'created_by' => 'superadmin@gmail.com',
            'updated_by' => ' ',
            'is_deleted'=>'N'
        ]);
        CorpusFundSetting::updateOrCreate(
            [
            'age_from'=>'28',
            'age_to'=>'28',
            'corpus_fund'=>'5000',
             'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' =>null,
            'created_by' => 'superadmin@gmail.com',
            'updated_by' => ' ',
            'is_deleted'=>'N'
        ]);CorpusFundSetting::updateOrCreate(
            [
            'age_from'=>'29',
            'age_to'=>'29',
            'corpus_fund'=>'3500',
             'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' =>null,
            'created_by' => 'superadmin@gmail.com',
            'updated_by' => ' ',
            'is_deleted'=>'N'
        ]);CorpusFundSetting::updateOrCreate(
            [
            'age_from'=>'30',
            'age_to'=>'30',
            'corpus_fund'=>'4000',
             'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' =>null,
            'created_by' => 'superadmin@gmail.com',
            'updated_by' => ' ',
            'is_deleted'=>'N'
        ]);CorpusFundSetting::updateOrCreate(
            [
            'age_from'=>'31',
            'age_to'=>'31',
            'corpus_fund'=>'5000',
             'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' =>null,
            'created_by' => 'superadmin@gmail.com',
            'updated_by' => ' ',
            'is_deleted'=>'N'
        ]);CorpusFundSetting::updateOrCreate(
            [
            'age_from'=>'32',
            'age_to'=>'32',
            'corpus_fund'=>'6000',
             'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' =>null,
            'created_by' => 'superadmin@gmail.com',
            'updated_by' => ' ',
            'is_deleted'=>'N'
        ]);CorpusFundSetting::updateOrCreate(
            [
            'age_from'=>'33',
            'age_to'=>'33',
            'corpus_fund'=>'7000',
             'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' =>null,
            'created_by' => 'superadmin@gmail.com',
            'updated_by' => ' ',
            'is_deleted'=>'N'
        ]);CorpusFundSetting::updateOrCreate(
            [
            'age_from'=>'34',
            'age_to'=>'34',
            'corpus_fund'=>'7500',
             'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' =>null,
            'created_by' => 'superadmin@gmail.com',
            'updated_by' => ' ',
            'is_deleted'=>'N'
        ]);CorpusFundSetting::updateOrCreate(
            [
            'age_from'=>'35',
            'age_to'=>'35',
            'corpus_fund'=>'8000',
             'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' =>null,
            'created_by' => 'superadmin@gmail.com',
            'updated_by' => ' ',
            'is_deleted'=>'N'
        ]);CorpusFundSetting::updateOrCreate(
            [
            'age_from'=>'36',
            'age_to'=>'36',
            'corpus_fund'=>'9000',
             'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' =>null,
            'created_by' => 'superadmin@gmail.com',
            'updated_by' => ' ',
            'is_deleted'=>'N'
        ]);CorpusFundSetting::updateOrCreate(
            [
            'age_from'=>'37',
            'age_to'=>'37',
            'corpus_fund'=>'10000',
             'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' =>null,
            'created_by' => 'superadmin@gmail.com',
            'updated_by' => ' ',
            'is_deleted'=>'N'
        ]);CorpusFundSetting::updateOrCreate(
            [
            'age_from'=>'38',
            'age_to'=>'38',
            'corpus_fund'=>'11000',
             'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' =>null,
            'created_by' => 'superadmin@gmail.com',
            'updated_by' => ' ',
            'is_deleted'=>'N'
        ]);CorpusFundSetting::updateOrCreate(
            [
            'age_from'=>'39',
            'age_to'=>'39',
            'corpus_fund'=>'12000',
             'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' =>null,
            'created_by' => 'superadmin@gmail.com',
            'updated_by' => ' ',
            'is_deleted'=>'N'
        ]);CorpusFundSetting::updateOrCreate(
            [
            'age_from'=>'40',
            'age_to'=>'40',
            'corpus_fund'=>'13000',
             'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' =>null,
            'created_by' => 'superadmin@gmail.com',
            'updated_by' => ' ',
            'is_deleted'=>'N'
        ]);CorpusFundSetting::updateOrCreate(
            [
            'age_from'=>'41',
            'age_to'=>'41',
            'corpus_fund'=>'14000',
             'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' =>null,
            'created_by' => 'superadmin@gmail.com',
            'updated_by' => ' ',
            'is_deleted'=>'N'
        ]);CorpusFundSetting::updateOrCreate(
            [
            'age_from'=>'42',
            'age_to'=>'42',
            'corpus_fund'=>'15000',
             'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' =>null,
            'created_by' => 'superadmin@gmail.com',
            'updated_by' => ' ',
            'is_deleted'=>'N'
        ]);CorpusFundSetting::updateOrCreate(
            [
            'age_from'=>'43',
            'age_to'=>'45',
            'corpus_fund'=>'18000',
             'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' =>null,
            'created_by' => 'superadmin@gmail.com',
            'updated_by' => ' ',
            'is_deleted'=>'N'
        ]);CorpusFundSetting::updateOrCreate(
            [
            'age_from'=>'46',
            'age_to'=>'47',
            'corpus_fund'=>'20000',
             'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' =>null,
            'created_by' => 'superadmin@gmail.com',
            'updated_by' => ' ',
            'is_deleted'=>'N'
        ]);CorpusFundSetting::updateOrCreate(
            [
            'age_from'=>'48',
            'age_to'=>'50',
            'corpus_fund'=>'23000',
             'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' =>null,
            'created_by' => 'superadmin@gmail.com',
            'updated_by' => ' ',
            'is_deleted'=>'N'
        ]);CorpusFundSetting::updateOrCreate(
            [
            'age_from'=>'51',
            'age_to'=>'53',
            'corpus_fund'=>'25000',
             'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' =>null,
            'created_by' => 'superadmin@gmail.com',
            'updated_by' => ' ',
            'is_deleted'=>'N'
        ]);CorpusFundSetting::updateOrCreate(
            [
            'age_from'=>'54',
            'age_to'=>'55',
            'corpus_fund'=>'27000',
             'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' =>null,
            'created_by' => 'superadmin@gmail.com',
            'updated_by' => ' ',
            'is_deleted'=>'N'
        ]);
    }
}
