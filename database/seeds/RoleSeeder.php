<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     	$superAdmin = Role::firstOrCreate(['name'=>'SuperAdmin','description'=>'test superadmin','account_id'=>'1','created_by'=>'1']);
     	$Admin = Role::firstOrCreate(['name'=>'Admin','description'=>'test admin','account_id'=>'1','created_by'=>'1']);
     	$Member = Role::firstOrCreate(['name'=>'Member','description'=>'test member','account_id'=>'1','created_by'=>'1']);
    }
}
