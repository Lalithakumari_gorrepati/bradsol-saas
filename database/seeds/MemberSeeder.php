<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\User;
use App\Models\UserAccount;
use App\Models\Account;
use App\Models\UserProfile;
use App\Models\Nominee;
use App\Models\Payment;
use App\Models\FCPayments;
use App\Models\PaymentDetails;
use App\Models\MembershipDetails;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class MemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	
        $faker = Faker\Factory::create();
        $role=Role::where('name','Member')->first();
        $account_id=Account::where('account_name','MF-FSS')->value('id');

        DB::beginTransaction();
        
        foreach (range(1,50) as $index) {
        	$date=$faker->dateTimeBetween($startDate = '-40 years', $endDate = '-21 years');
        	$dob=date('Y-m-d', strtotime(Carbon::parse($date)));
         	$user_data=array(
	            'name' => $faker->firstNameMale,
	            'email' => $faker->unique()->safeEmail,
	            'role'  => $role->id,
	            'mobile_number' =>'6'.rand(666666666,999999999),
	            'gender' => $faker->randomElement($array = array ('Male','Female')),
	            'dob'    => $dob,
	            'password' => Hash::make('password'),
	            'additional_info' => ' ',
	            'is_active' => 'Y',
	            'created_at' => date('Y-m-d h:i:s'),
	            'created_by' => 'superadmin@gmail.com',
            );
            
            $newuser=User::create($user_data);  
            $AssignRole=$newuser->assignRole($role);
			$user_id=$newuser->id;

			$account_data=[];
                 
            $account_data=array( 
                'user_id'     => $user_id,
                'account_id'  => $account_id,
                'created_at'  => date('Y-m-d h:i:s'),
                'created_by'  => 'superadmin@gmail.com',
            );
            
            $account=UserAccount::insert($account_data);

            $profile_data=[];
            $profile_data=array( 
                'user_id'     => $user_id,
                'member_id'   => Str::random(6),
                'alternative_email'  =>'',
                'alternative_phone'  =>'',
                'martial_status'  =>$faker->randomElement($array = array ('Married','Single')),
                'country'  =>'India',
                'state'  =>'Telangana',
                'city'  =>'Hyderabad',
                'id_proof'  =>null,
                'age_proof'  =>'',
                'physical_disability'  => 'N',
                'physical_disability_type'  => '',
                'previous_illness'  => 'N',
                'illness_type'  =>'',
                'health_declaration'  =>'Y',
                'reports'  =>null,
                'address'  =>'Begumpet',
                'image'  => '',
                'created_at'  => date('Y-m-d h:i:s'),
                'created_by'  =>'superadmin@gmail.com',
            );
        
            $profile=UserProfile::insert($profile_data);

            $date=$faker->dateTimeBetween($startDate = '-60 years', $endDate = '-50 years');
        	$nominee_dob=date('Y-m-d', strtotime(Carbon::parse($date)));

            $nominee_data=[];
            $nominee_data=array( 
                'member_id' => $user_id,
                'name'      => $faker->name,
                'email'     => $faker->unique()->safeEmail,
                'phone'     => '7'.rand(666666666,999999999),
                'image'     => '',
                'address'   => $faker->streetAddress,
                'dob'       => $nominee_dob,
                'nominee_relationship'=>$faker->randomElement($array = array ('Mother','Father')),
                'created_at'  => date('Y-m-d h:i:s'),
                'created_by'  =>'superadmin@gmail.com',
            );
        
            $nominee=Nominee::insert($nominee_data);

            $payment_date=$faker->dateTimeBetween($startDate = '-3years', $endDate = '-1 months');
        	$payment_date=date('Y-m-d', strtotime(Carbon::parse($payment_date)));

            $payment_data=[];
            $payment_data=array( 
                'member_id'     => $user_id,
                'account_id'    => $account_id,
                'amount'        => $faker->randomElement($array = array ('2400','3500','4000','5000','6000','7000','7500','8000')),
                'remarks'       =>'',
                'mode_of_payment'=> $faker->randomElement($array = array ('Bank','Cash','Cheque')),
                'payment_date'  => $payment_date,
                'created_at'  => date('Y-m-d h:i:s'),
                'created_by'  =>'superadmin@gmail.com',
            );
            $payment_id=Payment::insertGetId($payment_data);
            if($payment_id) {
                $payment_details=[];
               $payment_details=array( 
                'payment_id'    => $payment_id,
                'payment_against_member'=> null,
                'payment_type'  =>'corpus_amount',
                'created_at'  => date('Y-m-d h:i:s'),
                'created_by'  =>'superadmin@gmail.com',
                ); 
                if (!empty($payment_details)) {
                    $insert=PaymentDetails::insert($payment_details);  
                }                       
            }
            $corpus_amount=Payment::where('member_id',$user_id)->value('amount');

            $join_date=$faker->dateTimeBetween($startDate = '-3years', $endDate = '-1 months');
        	$join_date=date('Y-m-d', strtotime(Carbon::parse($join_date)));

            $membership_details=[];
            $membership_details=array( 
                'user_id'       => $user_id,
                'corpus_amount' => $corpus_amount,
                'join_date'     => $join_date,
                'age'           => $faker->numberBetween(21,40),
                'created_at'    => date('Y-m-d h:i:s'),
                'created_by'    =>'superadmin@gmail.com',
            );
            $membership=MembershipDetails::insert($membership_details);
        }
        DB::commit(); 
    }
}
