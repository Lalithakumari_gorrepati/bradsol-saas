<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AccountSeeder::class);
        $this->call(PermissionsSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(AdminPermissionSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(CorpusFundSeeder::class);
        $this->call(DropdownSeeder::class);
        $this->call(DropdownValuesSeeder::class);
        $this->call(CountrySeeder::class);
        $this->call(StatesSeeder::class);
        $this->call(CitiesSeeder::class);
        $this->call(FCSettingSeeder::class);
        $this->call(MemberSeeder::class);
    }
}
