<?php
namespace App\Repository;

use Illuminate\Support\Facades\Log;
use App\Models\WebNotifications;
use App\Models\WebNotificationCustomer;
use Spatie\Permission\Models\Role;
use App\User;
use Carbon\Carbon;
use Auth;
use App\Customer;


class WebNotificationRepository
{
	/**
	 *  Create Web Notifications for the user.
	 *
	 * @access  public
	 * @param   Illuminate\Http\Request $request
	 * @return  Response
	 * @author  BRAD Contech Solutions Pvt Ltd.
	 */
	static function create($data) {
		try {
			Log::info('WebNotification Repo: Create Web Notifications::Start');
			WebNotifications::insert($data);
        	Log::info('WebNotification Repo: Create Web Notifications::End');
		} catch(Exception $e) {
            Log::error('WebNotification Repo: Create Web Notifications::Error'.$e->getMessage());
            return $e;
        }
	}
	
	
	static function createNotifictnSingleUser($message,$notificationtype,$id) {
		try {
			Log::info('WebNotification Repo: Create Web Notifications::Start');
			
			$user=Auth::user();
			$user_email=$user['email'];
			$customer1 = User::where('id',$id)->first();
			$name='Dear '.$customer1->name. ' ';
			$data['message'] = $name . $message;
			$data['notification_type'] = $notificationtype;
			$data['created_for'] = $id;
			$data['created_by'] = $user_email;
			$data['created_at'] = Carbon::now()->format('Y-m-d H:i:s');
			$data['updated_at'] = Carbon::now()->format('Y-m-d H:i:s');
			
			WebNotifications::insert($data);
			Log::info('WebNotification Repo: Create Web Notifications::End');
		} catch(Exception $e) {
			Log::error('WebNotification Repo: Create Web Notifications::Error'.$e->getMessage());
			return $e;
		}
	}
	
	static function createNotifictnAdmins($message,$notificationtype) {
		try {
			Log::info('WebNotification Repo: Create Web Notifications::Start');
				
			$user=Auth::user();
			$user_email=$user['email'];
				
			$AdminRole =Role::where('name','Admin')->select('id')->first();
			 
			$admins = User::where('role',$AdminRole->id)->select('id','name')->get();
			
			$i = 0;
			foreach ($admins as $admin){
				$name='Dear '.$admin->name . ' ';
				$adminData[$i]['message'] = $name . $message;
				$adminData[$i]['notification_type'] = $notificationtype;
				$adminData[$i]['created_for'] = $admin->id;
				$adminData[$i]['created_by'] = $user_email;
				$adminData[$i]['created_at'] = Carbon::now()->format('Y-m-d H:i:s');
				$adminData[$i]['updated_at'] = Carbon::now()->format('Y-m-d H:i:s');
				$i++;
			}
			
				
			WebNotifications::insert($adminData);
			Log::info('WebNotification Repo: Create Web Notifications::End');
		} catch(Exception $e) {
			Log::error('WebNotification Repo: Create Web Notifications::Error'.$e->getMessage());
			return $e;
		}
	}
	
	
	static function createNotifictnDataAnalyst($message,$notificationtype) {
		try {
			Log::info('WebNotification Repo: Create Web Notifications::Start');
	
			$user=Auth::user();
			$user_email=$user['email'];
			$Data = array();
	
			$DataAnalystRole =Role::where('name','Data Analyst')->select('id')->first();
	
			$dataAnalysts = User::where('role',$DataAnalystRole->id)->select('id','name')->get();
				
			$i = 0;
			foreach ($dataAnalysts as $dataAnalyst){
				$name='Dear '. $dataAnalyst->name. ' ';
				$Data[$i]['message'] =$name. $message;
				$Data[$i]['notification_type'] = $notificationtype;
				$Data[$i]['created_for'] = $dataAnalyst->id;
				$Data[$i]['created_by'] = $user_email;
				$Data[$i]['created_at'] = Carbon::now()->format('Y-m-d H:i:s');
				$Data[$i]['updated_at'] = Carbon::now()->format('Y-m-d H:i:s');
				$i++;
			}
				
			if(!empty($Data)){
					WebNotifications::insert($Data);
			}
			Log::info('WebNotification Repo: Create Web Notifications::End');
		} catch(Exception $e) {
			Log::error('WebNotification Repo: Create Web Notifications::Error'.$e->getMessage());
			return $e;
		}
	}
	
	/**
	 *  Create Web Notifications for the customer.
	 *
	 * @access  public
	 * @param   $message,$notificationtype,$user_email
	 * @author  BRAD Contech Solutions Pvt Ltd.
	 */
	
	static function createNotificationForCustomer($message,$notificationtype,$user_email) {
		try {
			Log::info('WebNotification Repo: Create Web Notifications for Customers ::Start');	
			$customer_id =Customer::where('email',$user_email)->value('id');

			$notificationData['message'] = $message;
			$notificationData['notification_type'] = $notificationtype;
			$notificationData['created_for'] = $customer_id;
			$notificationData['created_by'] = $user_email;
			$notificationData['created_at'] = Carbon::now()->format('Y-m-d H:i:s');
			$notificationData['updated_at'] = Carbon::now()->format('Y-m-d H:i:s');
				
			WebNotificationCustomer::insert($notificationData);
			Log::info('WebNotification Repo: Create Web Notifications for Customers::End');
		} catch(Exception $e) {
			Log::error('WebNotification Repo: Create Web Notifications for Customers::Error'.$e->getMessage());
		}
	}


	static function createNotifictnEmployee($message,$notificationtype) {
		try {
			Log::info('WebNotification Repo: Create Web Notifications for Employee::Start');
				
			$user=Auth::user();
			$user_email=$user['email'];
				
			$EmployeeRole =Role::where('name','Employee')->select('id')->first();
			 
			$employees = User::where('role',$EmployeeRole->id)->select('id','name')->get();
			
			$i = 0;
			foreach ($employees as $employee){
				$name='Dear '.$employee->name . ' ';
				$employeeData[$i]['message'] = $name . $message;
				$employeeData[$i]['notification_type'] = $notificationtype;
				$employeeData[$i]['created_for'] = $employee->id;
				$employeeData[$i]['created_by'] = $user_email;
				$employeeData[$i]['created_at'] = Carbon::now()->format('Y-m-d H:i:s');
				$employeeData[$i]['updated_at'] = Carbon::now()->format('Y-m-d H:i:s');
				$i++;
			}
				
			WebNotifications::insert($employeeData);
			Log::info('WebNotification Repo: Create Web Notifications for Employee::End');
		} catch(Exception $e) {
			Log::error('WebNotification Repo: Create Web Notifications for Employee::Error'.$e->getMessage());
			return $e;
		}
	}
	
	
	public function webNotificationsListData($params)
	{
		try
		{
			Log::info('WebNotification Repo: get list of Web Notifications::Start');
	
				$data = WebNotifications::where('created_for',Auth::user()->id)->orderBy('created_at','desc')->get();
				
				return $data;
					
			Log::info('WebNotification Repo: get list of Web Notifications::End');
		}
		catch (QueryException $e)
		{
			Log::error('WebNotification Repo: get list of Web Notifications::Error'.$e->getMessage());
			throw $e;
		}
	}
	
	public function updateViewStatus()
	{
		try
		{
			Log::info('WebNotification Repo: update view status Web Notifications::Start');
	
			WebNotifications::where('view_status','Unviewed')->where('created_for',Auth::user()->id)->update(['view_status' => 'viewed']);
					
			Log::info('WebNotification Repo: update view status Web Notifications::End');
		}
		catch (QueryException $e)
		{
			Log::error('WebNotification Repo: update view status Web Notifications::Error'.$e->getMessage());
			throw $e;
		}
	}
	
	public static function getTopFivewebNotifications()
	{
		try
		{
			Log::info('Web notification Controller: getTopFivewebNotifications::Start');
	
			$data = WebNotifications::where('created_for',Auth::user()->id)->where('view_status','Unviewed')->orderBy('created_at','desc')->limit(5)->select('id','message')->get();
			Log::info('Web notification Controller: getTopFivewebNotifications::End');
			return $data;
		}
		catch (QueryException $e)
		{
			Log::error('Web notification Controller: getTopFivewebNotifications::Error'.$e->getMessage());
			return view('errors.custom');
		}
	}
	
	
	public static function countUnviewedNotifications()
	{
		$data = 0;
		try
		{
			Log::info('Web notification Controller: countUnviewedNotifications::Start');
	
			$data = WebNotifications::where('created_for',Auth::user()->id)->where('view_status','Unviewed')->count();
			 
			Log::info('Web notification Controller: countUnviewedNotifications::End');
			
		}
		catch (QueryException $e)
		{
			Log::error('Web notification Controller: countUnviewedNotifications::Error'.$e->getMessage());
		}
		return $data;
	}
	
	

}