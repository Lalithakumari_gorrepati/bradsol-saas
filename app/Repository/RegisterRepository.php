<?php
namespace App\Repository;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\QueryException;
use Spatie\Permission\Models\Role;
use Maklad\Permission\Traits\HasRoles;
use App\Helpers;
use App\User;
use App\Models\Account;
use App\Models\UserAccount;
use Auth;
use DataTables;
use Session;

    /**
     * ------------------------------------------------------------------------
     *   RegisterRepository class
     * ------------------------------------------------------------------------
     * Class having methods to handle requests,
     * details. This class consists of
     * methods to show request form, show request details. 
     *
     *
     * @package  App\Http\Controllers
     * @version  6.2
     * @author   BRAD Contech Solutions Pvt Ltd.
     */

class RegisterRepository{

    /**
     *  To store the User  
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function storeUser($data){
        $res=false;
        try{
            Log::info('Register Repo: Create User::Start');
            $store = [];

            DB::beginTransaction();
            if (!empty($data['dob'])) {
                $dob=date("Y-m-d", strtotime($data['dob']));
            }
            
            $store =array( 
                'name'          => $data['name'],
                'email'         => $data['email'],
                'role'          => $data['role'],
                'mobile_number' => $data['mobile_number'],
                'dob'           => !empty($dob)?$dob:null,
                'gender'        => !empty($data['gender'])?$data['gender']:null,
                'password'      => \Hash::make($data['password']),
                'additional_info' =>!empty($data['additional_info'])?$data['additional_info']:'',
                'is_active'     =>'Y',
                'created_at'    => date('Y-m-d h:i:s'),
                'created_by'    => Auth::user()->email,
                
            );

             $role=Role::where('id',$data['role'])->get(); 
             if (!empty($store)) {
                $newuser=User::create($store);  

                $AssignRole=$newuser->assignRole($role); 
             
                $account_data=[];
                if (!empty($newuser)) {
                    if (!empty($data['account'])) {
                        $user_accounts=[];
                        foreach ($data['account'] as $key => $value) {
                            $account_data=array( 
                                'user_id'     => $newuser->id,
                                'account_id'  => $value,
                                'created_at'  => date('Y-m-d h:i:s'),
                                'created_by'  => Auth::user()->email,
                            );
                            array_push($user_accounts, $account_data);
                        }
                        if (!empty($user_accounts)) {
                            $user=UserAccount::insert($user_accounts);
                        }
                    }
                }
            }
            DB::commit(); 
            $res=true;
            Log::info('Register Repo: create User::End');
        }catch(QueryException $e){
            DB::rollBack();
            Log::error('Register Repo: create User::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }
//-----------------------------------------------------------------------------

    /**
     *  To view the User list  
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function viewUserlist()
    {   
        try{
            Log::info('Register Repo: view user list::Start');
            $data=User::where(['is_deleted'=>'N'])->get();
            Log::info('Register Repo: view user list::End');
            return $userId;
        }catch(QueryException $e){
            Log::error('Register Repo: view user list::Error'.$e->getMessage());
            throw $e;
        }
    }  
//-----------------------------------------------------------------------------

    /**
     *  To view the User list  
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getList($params)
    {     
        try{
            Log::info('Register Repo: Get user list::Start');

//                 $account_id=Session::get('account_id');
                    $account_id=Helpers::getCurrentUserAccountId();
                $role_name=Auth::user()->roles->first()->name;
                
                $query=User::leftJoin('roles', 'users.role', '=', 'roles.id')
                    ->leftJoin('user_accounts', 'users.id', '=', 'user_accounts.user_id')
                    ->select('users.*','roles.name as role_name',
                        DB::raw('DATE_FORMAT(users.created_at, "%d-%m-%Y") as created_date'))
                    ->selectRaw("(CASE is_active WHEN 'N' THEN 'InActive' WHEN 'Y' THEN 'Active' END) AS status"); 
                
                if(!empty($params['account']))
                {
                    $text = trim($params['account']);
                    $query->where('user_accounts.account_id', $text);
                }
                if(!empty($params['role']))
                {
                    $text = trim($params['role']);
                    $query->where('users.role', $text);
                }
                if (!empty($account_id) && $role_name!='SuperAdmin') {
                    $query->where('user_accounts.account_id',$account_id);
                }
                $data = $query->distinct('id')->orderBy('users.id','ASC')->get();
                
                Log::info('Register Repo: Get user list::End');
               
                return $data;
            }
            catch(QueryException $e){
                Log::error('Register Repo: Get user list::Error'.$e->getMessage());
                    throw $e;
            }
    }
//-----------------------------------------------------------------------------


    /**
     *  Get User list  
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getUserList()
    {   
        try{
            $var=array();
            Log::info('Register Repo: Get user list::Start');
            
            $var['data'] = User::leftJoin('roles', 'users.role', '=', 'roles.id')
                ->select('users.*','roles.name as role_name')
                ->get();
                if(Helpers::getCurrentUserAccountId() > 1){
                    $var['role']=Role::where('account_id',Helpers::getCurrentUserAccountId())->get();
                }else{
                    $var['role']=Role::get();
                }
                
                
            Log::info('Register Repo: Get user list::End');
            return $var;
        }
        catch(QueryException $e){
            Log::error('Register Repo: Get user list::Error'.$e->getMessage());
            throw $e;
        }
    }
    //----------------------------------------------------------------------------

    /**
     *  To view the single User  
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function viewUser($id)
    {   
        try{
            Log::info('Register Repo: Get view user::Start');
            $var['data'] = User::leftJoin('roles', 'users.role', '=', 'roles.id')
                           ->leftJoin('user_accounts', 'users.id', '=', 'user_accounts.user_id')
                           ->select('users.*','roles.name as role_name','user_accounts.*')
                           ->selectRaw("(CASE is_active WHEN 'N' THEN 'InActive' WHEN 'Y' THEN 'Active' END) AS status")
                           ->where('users.id',$id)->first();
            $var['user_accounts']=UserAccount::where('user_id',$id)->pluck('account_id');
            
            $var['role']=Role::where(['is_deleted'=>'N'])->get();
              
            Log::info('Register Repo: Get role list::End');
            return $var;
        }
        catch(QueryException $e){
            Log::error('Register Repo: Get view user::Error'.$e->getMessage());
            throw $e;
        }
    }
//---------------------------------------------------------------------------


    /**
     *  To delete the User
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function deleteUser($id)
    {   
        try{
            Log::info('Register Repo: delete user::Start');
            $role  = User::find($id);
            $role->status  = 'Inactive';
            $role->save();
            Log::info('Register Repo: delete user::End');
        }
        catch(QueryException $e){
            Log::error('Register Repo: delete user::Error'.$e->getMessage());
            throw $e;
        }
    }
//----------------------------------------------------------------------------

    /**
     *  To get role  
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getRole()
    {   
        try{
            Log::info('Register Repo: Get role list::Start');
            $data=Role::where(['is_deleted'=>'N'])->get();
            
            Log::info('Register Repo: Get role list::End');
            return $data;
        }catch(QueryException $e){
            Log::error('Register Repo: Get role list::Error'.$e->getMessage());
            throw $e;
        }
    }
    //-------------------------------------------------------------------------


    /**
     *  To get accounts data  
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getAccounts()
    {   
        try{
            Log::info('Register Repo: Get accounts list::Start');
            
            $data=Account::where(['is_deleted'=>'N'])->get();

            Log::info('Register Repo: Get accounts list::End');
            return $data;
        }catch(QueryException $e){
            Log::error('Register Repo: Get accounts list::Error'.$e->getMessage());
            throw $e;
        }
    }
    //-------------------------------------------------------------------------

    /**
     *  To update the User  
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function updateUser($data){
        $res=false;
        try{
            Log::info('Register Repo: Update User::Start');
            $userId = '';
            DB::beginTransaction();
            
             if (!empty($data['dob'])) {
                $dob=date("Y-m-d", strtotime($data['dob']));
            }
               $userData =array(
                'name'          => $data['name'],
                'email'         => $data['email'],
                'mobile_number' => $data['mobile_number'],
                'role'          => $data['role'],
                'gender'        => !empty($data['gender'])?$data['gender']:null,
                'dob'           => !empty($dob)?$dob:null,
                'additional_info'=> $data['additional_info'], 	
                'is_active'     => $data['is_active'],  
                'updated_at'    => date('Y-m-d h:i:s'),
                'updated_by'    => Auth::user()->email,
            );
            
            $role=Role::where('id',$data['role'])->get(); 
            $userId = User::where('id',$data['id'])->update($userData);
            $user = User::where('id',$data['id'])->first();
         	$AssignRole=$user->syncRoles($role); 

            if(!empty($data['account_id'])) {
                Log::info('Register Repo:updateUser::update accounts:start');

                $old_accounts=UserAccount::where('user_id',$data['id'])->pluck('account_id')->toArray();
                $removed_account=array_diff($old_accounts,$data['account_id']);
                if (!empty($removed_account)) {
                    foreach ($removed_account as $key => $value) {
                        $delete=UserAccount::where('user_id',$data['id'])->where('account_id',$value)->delete();
                    } 
                }  
                $user_accounts=[];
                if(empty($old_accounts)) {

                    foreach ($data['account_id'] as $key => $value) {
                        $account_data=array( 
                            'user_id'     => $data['id'],
                            'account_id'  => $value,
                            'created_at'  => date('Y-m-d h:i:s'),
                            'created_by'  => Auth::user()->email,
                        );
                        array_push($user_accounts, $account_data);
                    }
                    if (!empty($user_accounts)) {
                        $update=UserAccount::insert($user_accounts);
                    } 
                }
                else{
                    foreach ($old_accounts as $key => $account) {
                        foreach ($data['account_id'] as $key => $value) {
                            if (!in_array($value, $old_accounts)) {
                                $account_data=array( 
                                    'user_id'     => $data['id'],
                                    'account_id'  => $value,
                                    'created_at'  => date('Y-m-d h:i:s'),
                                    'created_by'  => Auth::user()->email,
                                );
                            array_push($user_accounts, $account_data);
                            array_push($old_accounts,$value);
                           }
                        }
                    }
                    if (!empty($user_accounts)) {
                        $update=UserAccount::insert($user_accounts);
                    }
                }
                Log::info('Register Repo:updateUser::update accounts:end');    
            }
            DB::commit();
             $res=true;
            Log::info('Register Repo: Update User::End');
        }catch(QueryException $e){
            DB::rollBack();
            Log::error('Register Repo: create User::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }
    //------------------------------------------------------------------------------

}