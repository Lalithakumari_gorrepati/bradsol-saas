<?php
namespace App\Repository;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\QueryException;
use App\User;
use App\Models\UserAccount;
use App\Models\Account;
use App\Models\UserProfile;
use App\Models\Payment;
use App\Models\FCPayments;
use App\Models\PaymentDetails;
use App\Models\MembershipDetails;
use App\Models\AccidentalInsurance;
use App\Models\Invoice;
use App\Models\InvoiceFCs;
use Auth;
use DataTables;
use App\Http\Helper\Utilities;
use Session;
use Carbon\Carbon;
use App\Http\Helper\StringConstants;
use Illuminate\Support\Facades\Storage;
use PDF;
use App\Http\Helper\EmailUtilities;
use App\Http\Helper\SMSUtilities;
class PaymentsRepository{

    /**
     *  Return pending payments data
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getPendingPaymentsData($params)
    {             
        try{
            Log::info('PaymentsRepository: return pending payments data::Start');

                $search = !empty($params['search']['value'])?$params['search']['value']:'';  
                $pageSize = !empty($params['length']) ? $params['length'] : 10;
                $start = isset($params['start']) ? $params['start'] : 0;
                $count_filter = 0;
                $data  = array();
                $account_id=Session::get('account_id');
                $role_name=Auth::user()->roles->first()->name;
                
                $query =  Invoice::leftJoin('users', 'invoices.member_id', '=', 'users.id')
                    ->leftJoin('user_profiles','invoices.member_id','=','user_profiles.user_id')
                    ->leftJoin('user_accounts', 'invoices.member_id', '=', 'user_accounts.user_id')
                    ->leftJoin('invoice_has_fcs', 'invoices.id', '=','invoice_has_fcs.invoice_id')
                    ->leftJoin('membership_details', 'invoices.member_id', '=', 'membership_details.user_id')
                    ->leftJoin('fc_payments', function($join)
                         {
                            $join->on('invoice_has_fcs.fc_payment_id', '=', 'fc_payments.id')
                             ->where('fc_payments.payment_type','=',"fc_amount")
                             ->where('fc_payments.has_paid','=',"N");
                             $join->on('fc_payments.member_id','=','invoices.member_id');
                         })
                    ->leftJoin('fc_payments As fc_payments2', function($join)
                         {
                             $join->on('invoices.id', '=', 'fc_payments2.invoice_id')
                             ->where('fc_payments2.payment_type', '=', "due_penalty")
                             ->where('fc_payments2.has_paid','=',"N");
                             $join->on('fc_payments2.member_id','=','invoices.member_id');
                         })
                    ->select('users.name','users.email','users.mobile_number','users.id as user_id','user_profiles.member_id','invoices.invoice','invoices.total_fc As total_fcs','invoices.total_due As bill_amount','invoices.id As invoice_id',
                        DB::raw('DATE_FORMAT(invoices.bill_date, "%d-%m-%Y") as bill_date'),
                    DB::raw('DATE_FORMAT(invoices.due_date, "%d-%m-%Y") as bill_due_date'))
                    ->selectRaw("(CASE WHEN (invoices.total_due + fc_payments2.total_payment) IS NULL THEN invoices.total_due else (invoices.total_due + fc_payments2.total_payment) END) AS total_due_amount")
                    ->selectRaw("ROUND(SUM(fc_payments.total_payment)) AS 'pending_amount'")
                    ->selectRaw("(CASE WHEN fc_payments2.total_payment IS NULL THEN '0' else fc_payments2.total_payment END) AS penalty_amount")
                    ->selectRaw("invoices.total_due AS total_due")
                    ->selectRaw("TRIM('_Invoice.pdf' FROM  invoices.invoice) AS invoice_no")
                    ->selectRaw("(DATEDIFF(CURRENT_DATE,DATE(invoices.due_date))) AS overdue")
                     ->where(DB::raw("(SELECT round(SUM(total_payment)) FROM fc_payments WHERE fc_payments.member_id = invoices.member_id AND fc_payments.invoice_id = invoices.id AND fc_payments.has_paid='N') "),'>','0')
                    ->groupBy('invoice_has_fcs.invoice_id');
                
                if (!empty($account_id) && $role_name!='SuperAdmin') {
                    $query->where('user_accounts.account_id',$account_id);
                }
                if(!empty($params['from']) && !empty($params['to']))  {
                    $from = trim($params['from']);
                    $to = trim($params['to']);
                    $from = date('Y-m-d', strtotime($from));
                    $to = date('Y-m-d', strtotime($to));
                
                    $query->where(function ($q) use ($from,$to) {
                        $q->where('invoices.due_date', '>=', $from.' 00:00:00');
                        $q->where('invoices.due_date', '<=', $to.' 23:59:59');
                    });
                }
                if(!empty($params['age_from']) && !empty($params['age_to']))  {
                    $from = trim($params['age_from']);
                    $to = trim($params['age_to']);
                
                    $query->where(function ($q) use ($from,$to) {
                        $q->where('membership_details.age', '>=', $from);
                        $q->where('membership_details.age', '<=', $to);
                    });
                }
                if(!empty($params['register_id'])) {
                    $query->where('user_profiles.member_id', 'LIKE', '%'.$params['register_id'].'%');
                }
                if(!empty($params['expired_id'])) {
                    $query->where('fc_payments.payment_against_member',$params['expired_id']);
                }
                if(!empty($params['member_id'])) {
                    $query->where('users.id',$params['member_id']);
                }
                if(!empty($params['invoice_no'])) {
                    $query->where('invoices.invoice','LIKE', '%'.$params['invoice_no'].'%');
                }
                $data = $query->get();
                $count_total=count($data);

                if($search != ''){

                    $query->Where(function ($query) use ($search) {
                        $query->where('users.name','LIKE' , '%'.$search.'%')
                        ->orWhere('users.email','LIKE' , '%'.$search.'%')
                        ->orWhere('users.mobile_number','LIKE' , '%'.$search.'%')
                        ->orWhere('user_profiles.member_id','LIKE' , '%'.$search.'%')
                        ->orWhere('invoices.bill_date','LIKE' , '%'.$search.'%')
                        ->orWhere('invoices.total_due','LIKE' , '%'.$search.'%')
                        ->orWhere('invoices.invoice','LIKE' , '%'.$search.'%');
                    });
                    $filter_data = $query->get();
                    $count_filter=count($filter_data);
                }
                
                $qData=$query->orderBy('invoices.created_at','DESC')->skip($start)->take($pageSize)->get();
                
                if($count_filter == 0 && $search == ''){
                    $count_filter = $count_total;
                }
                
                foreach ($qData as $key => $value) {
                    if($value->total_fcs>0){
                        if ($value->overdue<0) {
                            $value->overdue='0';
                        }
                        if ($value->penalty_amount>0) {
                            $value->pending_amount=$value->pending_amount+$value->penalty_amount;
                        }
                    }
                }
                $data = DataTables::of($qData)->addColumn('action', function($data){
                    $button ='';
                    if(auth()->user()->hasPermissionTo('View Payment'))      
                    {
                        if ($data->total_fcs>0 ) {
                            $button .="<a class='btn mr-2 mb-1 btn-rounded btn-primary viewPayment' data-value='' data-date='".$data->bill_date."' href='".url('/')."/payments/pending/view/".$data->user_id."_".$data->invoice_id."'>View</a>";
                        }
                    }
                    if(auth()->user()->hasPermissionTo('Download Invoice'))      
                    {
                    $button .="<a class='btn mr-2 mb-1 btn-rounded btn-primary viewPayment' data-value='' href='".url('/')."/storage/invoices/".$data->invoice."' target='_blank'>Download Invoice</a>";
                    }
                    return $button;
                })->with([
                    "recordsTotal" => $count_total,
                    "recordsFiltered" => $count_filter,
                ])
                ->skipPaging()
                ->rawColumns(['action'])
                ->make(true);  
                Log::info('PaymentsRepository: return pending payments data::End');
                return $data;
            }
            catch(QueryException $e){
                Log::error('PaymentsRepository: return pending payments data::Error'.$e->getMessage());
                throw $e;
            }
    }
    //-------------------------------------------------------------------------

    /**
     *  Return payments data based on member id
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getPaymentsData($params)
    {             
        try{
            Log::info('PaymentsRepository:getPaymentsData::Start');
               
                $param=explode("_",$params['id']);

                if (count($param)>1) {
                   $id=$param[0];
                    $invoice_id=$param[1];
                }else{
                    $id=$params['id'];
                    $invoice_id='';
                }
                
                $search = !empty($params['search']['value'])?$params['search']['value']:'';  
                $pageSize = !empty($params['length']) ? $params['length'] : 10;
                $start = isset($params['start']) ? $params['start'] : 0;
                $count_filter = 0;
                $data  = array();
                $account_id=Session::get('account_id');
                $role_name=Auth::user()->roles->first()->name;
                
                $query =  FCPayments::leftJoin('users as expired_member','fc_payments.payment_against_member','=','expired_member.id')
                    ->leftJoin('membership_details', 'expired_member.id', '=', 'membership_details.user_id')
                    ->leftJoin('user_accounts', 'fc_payments.member_id', '=', 'user_accounts.user_id')
                    ->select('fc_payments.*','expired_member.name as expired_member_name','expired_member.name as expired_member_name','membership_details.fc_amount As fc_released',
                        DB::raw('(SELECT COUNT(*) FROM fc_payments WHERE fc_payments.payment_against_member = expired_member.id AND fc_payments.payment_type ="fc_amount") AS total_members'),
                        DB::raw('DATE_FORMAT(membership_details.confirm_date, "%d-%m-%Y") as expired_date'),
                        DB::raw('DATE_FORMAT(fc_payments.bill_date, "%d-%m-%Y") as bill_date'),
                        DB::raw('DATE_FORMAT(fc_payments.due_date, "%d-%m-%Y") as due_date')
                    )
                    ->selectRaw("(CASE payment_type WHEN 'advance' THEN 'Advance amount' WHEN 'fc_amount' THEN 'FC amount' END) AS payment_type")
                    ->selectRaw("(CASE has_paid WHEN 'N' THEN 'Unpaid' WHEN 'Y' THEN 'Paid' END) AS has_paid")
                    ->selectRaw("round(amount_to_pay) AS amount_to_pay")
                    ->selectRaw("round(total_payment) AS total_payment")
                    ->selectRaw("(DATEDIFF(CURRENT_DATE,DATE(due_date))) AS overdue")
                    ->where('fc_payments.payment_type','fc_amount')
                    ->where('fc_payments.member_id',$id);

                if (!empty($invoice_id)) {
                    $query->leftJoin('invoice_has_fcs', 'fc_payments.id', '=', 'invoice_has_fcs.fc_payment_id')
                    ->where('invoice_has_fcs.invoice_id',$invoice_id);
                }

                if (empty($invoice_id)) {
                    $query->where('fc_payments.has_paid',"N");
                }
                if (!empty($account_id) && $role_name!='SuperAdmin') {
                    $query->where('user_accounts.account_id',$account_id);
                }

                $count_total = $query->count();
                if($search != ''){
                    $query->Where(function ($query) use ($search) {
                        $query->orWhere('expired_member.name','LIKE' ,'%'.$search.'%')
                        ->orWhere('membership_details.fc_amount','LIKE' , '%'.$search.'%')
                        ->orWhere('fc_payments.total_payment','LIKE' , '%'.$search.'%')
                        ->orWhere('fc_payments.bill_date','LIKE' , '%'.$search.'%')
                        ->orWhere('fc_payments.amount_to_pay','LIKE' , '%'.$search.'%');
                    });
                    $count_filter = $query->count();
                }
                $query->skip($start)->take($pageSize);
                if($count_filter == 0 && $search == ''){
                    $count_filter = $count_total;
                }
                $qData=$query->orderBy('fc_payments.created_at','DESC')->get();
                 foreach ($qData as $key => $value) {
                    if ($value->overdue<0) {
                        $value->overdue='0';
                    }
                }
                $data = DataTables::of($qData)->addColumn('action', function($data){
                    $button ='';
                    if(auth()->user()->hasAnyPermission(['View Payment','View Pending Payment']))      
                    {
                        if ($data->payment_type=='FC amount') {
                       
                        $button .= "<a class='btn mr-2 mb-1 btn-rounded btn-primary viewPayment' data-value='' href='#' id='user_".$data->id."' 
                        onclick='showViewForm(".$data->id.")' 
                        data-id='".$data->id."'
                        data-totalFC = '".$data->fc_released."' 
                        data-totalMembers = '".$data->total_members."' 
                        data-fcDue = '".$data->amount_to_pay."'
                        data-adminExpense = '".$data->admin_expense."' 
                        data-fyAdvance = '".$data->advance."'
                        data-totalDue =  '".$data->total_payment."'
                        >View</a>";
                    } 
                }
                    return $button;
                })->with([
                    "recordsTotal" => $count_total,
                    "recordsFiltered" => $count_filter,
                ])
                ->skipPaging()
                ->rawColumns(['action'])
                ->make(true);  
                Log::info('PaymentsRepository:getPaymentsData::End');
                return $data;
            }
            catch(QueryException $e){
                Log::error('PaymentsRepository:getPaymentsData::Error'.$e->getMessage());
                throw $e;
            }
    }
    //------------------------------------------------------------------------------


    /**
     *  Return received payments data
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getReceivedPaymentsData($params)
    {             
        try{
            Log::info('PaymentsRepository: return pending payments data::Start');

                $search = !empty($params['search']['value'])?$params['search']['value']:'';  
                $pageSize = !empty($params['length']) ? $params['length'] : 10;
                $start = isset($params['start']) ? $params['start'] : 0;
                $count_filter = 0;
                $data  = array();
                $account_id=Session::get('account_id');
                $role_name=Auth::user()->roles->first()->name;
                
                $query =  Payment::leftJoin('users as member', 'payments.member_id', '=', 'member.id')
                    ->leftJoin('payment_details','payments.id','=','payment_details.payment_id')
                   ->leftJoin('users as expired_member','payment_details.payment_against_member','=','expired_member.id')
                   ->leftJoin('membership_details', 'member.id', '=', 'membership_details.user_id')
                    ->leftJoin('user_profiles','payments.member_id','=','user_profiles.user_id')
                    ->leftJoin('user_accounts','member.id','=','user_accounts.user_id')
                    ->select('member.*','user_profiles.member_id','payment_details.transaction_id','payments.amount','payments.mode_of_payment','payment_details.payment_type','payment_details.payment_against_member','expired_member.name As expired_name','payments.id as payment_id',
                        DB::raw('DATE_FORMAT(payment_date, "%d-%m-%Y") as payment_date'))
                    ->selectRaw("(CASE payment_type WHEN 'due_penalty' THEN 'Due Penalty' WHEN 'fc_amount' THEN 'FC amount' END) AS payment_type");

                    $query->Where(function ($query) use ($search) {
                        $query->where('payment_details.payment_type','fc_amount')
                        ->orWhere('payment_details.payment_type','due_penalty');
                    });

                if (!empty($account_id) && $role_name!='SuperAdmin') {
                    $query->where('user_accounts.account_id',$account_id);
                }
                if(!empty($params['from']) && !empty($params['to']))  {
                    $from = trim($params['from']);
                    $to = trim($params['to']);
                    $from = date('Y-m-d', strtotime($from));
                    $to = date('Y-m-d', strtotime($to));
                
                    $query->where(function ($q) use ($from,$to) {
                        $q->where('payments.payment_date', '>=', $from.' 00:00:00');
                        $q->where('payments.payment_date', '<=', $to.' 23:59:59');
                    });
                }
                if(!empty($params['age_from']) && !empty($params['age_to']))  {
                    $from = trim($params['age_from']);
                    $to = trim($params['age_to']);
                
                    $query->where(function ($q) use ($from,$to) {
                        $q->where('membership_details.age', '>=', $from);
                        $q->where('membership_details.age', '<=', $to);
                    });
                }
                if(!empty($params['register_id'])) {
                    $query->where('user_profiles.member_id', 'LIKE', '%'.$params['register_id'].'%');
                }
                if(!empty($params['expired_id'])) {
                    $query->where('payment_details.payment_against_member',$params['expired_id']);
                }
                if(!empty($params['member_id'])) {
                    $query->where('member.id',$params['member_id']);
                }
                if(!empty($params['payment_type'])) {
                    $query->where('payment_details.payment_type','LIKE', '%'.$params['payment_type'].'%');
                }

                $count_total = $query->count();
                if($search != ''){
                    $query->Where(function ($query) use ($search) {
                        $query->orWhere('expired_member.name','LIKE' ,'%'.$search.'%')
                        ->orWhere('member.name','LIKE' , '%'.$search.'%')
                        ->orWhere('member.email','LIKE' , '%'.$search.'%')
                        ->orWhere('member.mobile_number','LIKE' , '%'.$search.'%')
                        ->orWhere('payment_details.transaction_id','LIKE' , '%'.$search.'%')
                        ->orWhere('payments.mode_of_payment','LIKE' , '%'.$search.'%')
                        ->orWhere('payments.payment_date','LIKE' , '%'.$search.'%')
                        ->orWhere('payments.amount','LIKE' , '%'.$search.'%')
                        ->orWhere('user_profiles.member_id', 'LIKE' , '%'.$search.'%');
                    });
                    $count_filter = $query->count();
                }
                $query->skip($start)->take($pageSize);
                if($count_filter == 0 && $search == ''){
                    $count_filter = $count_total;
                }
                $qData=$query->orderBy('payment_details.created_at','DESC')->get();
                
                $data = DataTables::of($qData)->addColumn('action', function($data){
                    
                })->with([
                    "recordsTotal" => $count_total,
                    "recordsFiltered" => $count_filter,
                ])
                ->skipPaging()
                ->make(true);  
                Log::info('PaymentsRepository: return pending payments data::End');
                return $data;
            }
            catch(QueryException $e){
                Log::error('PaymentsRepository: return pending payments data::Error'.$e->getMessage());
                throw $e;
            }
    }
    //-------------------------------------------------------------------------

    /**
     *  Return pending advance amount or pending fc amount of particular member
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getDueAmount($params)
    {             
        try{
            Log::info('PaymentsRepository: return DueAmount::Start');
                
                $amount  = 0;
                if (empty($params['expired_id'])) {
                    $amount_to_pay =FCPayments::where('member_id',$params['id'])
                            ->where('payment_type','advance')
                            ->where('has_paid','N')
                            ->selectRaw("round(total_payment) AS amount_to_pay")
                            ->first(); 
                }elseif(!empty($params['expired_id'])) {
                    $amount_to_pay =FCPayments::where('member_id',$params['id'])
                             ->where('payment_type','fc_amount')
                             ->where('payment_against_member',$params['expired_id'])
                             ->where('has_paid','N')
                             ->selectRaw("round(total_payment) AS amount_to_pay")
                             ->first();
                }

                if (!empty($amount_to_pay)) {
                   $amount=$amount_to_pay->amount_to_pay;
                }

                Log::info('PaymentsRepository: return DueAmount::End');
                return $amount;
            }
            catch(QueryException $e){
                Log::error('PaymentsRepository: return DueAmount::Error'.$e->getMessage());
                throw $e;
            }
    }
    //-------------------------------------------------------------------------

    /**
     *  Return pending fc due members list against expired member  
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getMembers($params)
    {             
        try{
            Log::info('PaymentsRepository: return members list::Start');
                
                $members  = array();
                if(!empty($params['expired_id'])) {
                    $data=FCPayments::leftJoin('users','fc_payments.member_id','=','users.id')
                        ->select('users.name','users.id As user_id')
                        ->where('payment_type','fc_amount')
                        ->where('payment_against_member',$params['expired_id'])
                        ->where('has_paid','N')
                        ->get();
                }
                if (!empty($data) && count($data)>0) {
                    $members=$data;
                }
                
                Log::info('PaymentsRepository: return members list::End');
                return $members;
            }
            catch(QueryException $e){
                Log::error('PaymentsRepository: return members list::Error'.$e->getMessage());
                throw $e;
            }
    }
    //-------------------------------------------------------------------------

    /**
     *  Function to store payment
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function storePayment($params)
    {   
        $res=false;          
        try{
            Log::info('PaymentsRepository: Store Payment::Start');
            DB::beginTransaction(); 
            
            Log::info('PaymentsRepository:StorePayment::Member payment details insering started');   

            $payment_data=[];
            if (!empty($params['payment_date'])) {
                $payment_date=date("Y-m-d", strtotime($params['payment_date']));
            }
            $payment_type=$params['type_of_payment'];
            if ($payment_type=='advance') {
                $member_id=$params['advance_member_name'];
            }
            elseif($payment_type=='fc_amount'){
                $member_id=$params['member_name'];
            }
            $transaction_id='';

            $last_payment_id=PaymentDetails::where('payment_type',$payment_type)->orderBy('id','desc')->first('transaction_id');
            
            if(!empty($last_payment_id)){
                $last_id=$last_payment_id->transaction_id;
                $initail=substr($last_id, 0,3);
                
                $id=substr($last_id, 3);
                $latest_id=(int)$id+1;
                $transaction_id=$initail.$latest_id;
            }
            else{
                if($payment_type=='advance'){
                    $transaction_id='AD_10001';
                }
                elseif($payment_type=='fc_amount'){
                   $transaction_id='FC_10001';
                }
            }
            
            $payment_data=array( 
                'member_id'     => $member_id,
                'account_id'    => $params['account_id'],
                'amount'        => $params['amount'],
                'remarks'       => !empty($params['payment_note'])?$params['payment_note']:'',
                'mode_of_payment'=> $params['payment_mode'],
                'payment_date'  => !empty($payment_date)?$payment_date:null,
                'created_at'  => date('Y-m-d h:i:s'),
                'created_by'  => Auth::user()->email,
            );
            
            $payment_id=Payment::insertGetId($payment_data);
                if($payment_id) {
                $payment_details=[];
                $payment_details=array( 
                    'payment_id'     => $payment_id,
                    'transaction_id' => $transaction_id,
                    'payment_against_member'=> !empty($params['expired_member'])?$params['expired_member']:null,
                    'payment_type'  =>$payment_type,
                    'created_at'  => date('Y-m-d h:i:s'),
                    'created_by'  => Auth::user()->email,
                ); 
                if (!empty($payment_details)) {
                    $insert=PaymentDetails::insert($payment_details);  
                }
                $update=array('paid_amount'=>$params['amount'],'has_paid'=>'Y');
                if ($payment_type=='advance') {
                    $fc_payment=FCPayments::where('payment_type',$payment_type)
                                ->where('member_id',$member_id)->update($update); 

                }
                elseif ($payment_type=='fc_amount') {
                    $fc_payment=FCPayments::where('payment_type',$payment_type)
                                ->where('member_id',$member_id)
                                ->where('payment_against_member',$params['expired_member'])
                                ->update($update);
                }
            Log::info('PaymentsRepository:StorePayment::Member payment details insering end');                              
            }
            DB::commit(); 
            $res=true;
            Log::info('PaymentsRepository: Store Payment::End');
        }
        catch(QueryException $e){
            DB::rollBack();
            Log::error('PaymentsRepository: Store Payment::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }
    //-------------------------------------------------------------------------

    /**
     *  Return clared payments data of particular member
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getClearedPaymentsData($params)
    {             
        try{
            Log::info('PaymentsRepository: getClearedPaymentsData::Start');

                $search = !empty($params['search']['value'])?$params['search']['value']:'';  
                $pageSize = !empty($params['length']) ? $params['length'] : 10;
                $start = isset($params['start']) ? $params['start'] : 0;
                $count_filter = 0;
                $data  = array();
                $id=$params['id'];

                $query =  Payment::leftJoin('payment_details','payments.id','=','payment_details.payment_id')
                    ->leftJoin('users as member', 'payments.member_id', '=', 'member.id')
                   ->leftJoin('users as expired_member','payment_details.payment_against_member','=','expired_member.id')
                   ->leftJoin('membership_details', 'payment_details.payment_against_member', '=', 'membership_details.user_id')
                    ->select('member.*','payment_details.transaction_id',
                        'payments.amount','payments.mode_of_payment',
                        'payment_details.payment_type',
                        'payment_details.payment_against_member',
                        'expired_member.name As expired_name',
                        'membership_details.fc_amount As fc_released',
                        DB::raw('DATE_FORMAT(payment_date, "%d-%m-%Y") as payment_date'),
                        DB::raw('DATE_FORMAT(membership_details.confirm_date, "%d-%m-%Y") as expired_date'),
                        DB::raw('(SELECT COUNT(*) FROM fc_payments WHERE fc_payments.payment_against_member = expired_member.id AND fc_payments.payment_type ="fc_amount") AS total_members'))
                    ->selectRaw("(CASE payment_details.payment_type WHEN 'due_penalty' THEN 'Due Penalty' WHEN 'fc_amount' THEN 'FC amount' END) AS payment_type")
                    ->selectRaw("round(amount) AS paid_amount")
                    ->where('payments.member_id',$id);

                    $query->Where(function ($query) use ($search) {
                        $query->where('payment_details.payment_type','fc_amount')
                        ->orWhere('payment_details.payment_type','due_penalty');
                    });

                if(!empty($params['from']) && !empty($params['to']))  {
                    $from = trim($params['from']);
                    $to = trim($params['to']);
                    $from = date('Y-m-d', strtotime($from));
                    $to = date('Y-m-d', strtotime($to));
                
                    $query->where(function ($q) use ($from,$to) {
                        $q->where('payments.payment_date', '>=', $from.' 00:00:00');
                        $q->where('payments.payment_date', '<=', $to.' 23:59:59');
                    });
                }
                if(!empty($params['expired_id'])) {
                    $query->where('payment_details.payment_against_member',$params['expired_id']);
                }
                if(!empty($params['transaction_id'])) {
                    $query->where('payment_details.transaction_id','LIKE', '%'.$params['transaction_id'].'%');
                }

                $count_total = $query->count();
                if($search != ''){
                    $query->Where(function ($query) use ($search) {
                        $query->Where('expired_member.name', 'LIKE' , '%'.$search.'%')
                        ->orWhere('membership_details.fc_amount', 'LIKE' , '%'.$search.'%')
                        ->orWhere('payments.mode_of_payment', 'LIKE' , '%'.$search.'%')
                        ->orWhere('payment_details.transaction_id', 'LIKE' , '%'.$search.'%');
                    });
                    $count_filter = $query->count();
                }
                $query->skip($start)->take($pageSize);
                if($count_filter == 0 && $search == ''){
                    $count_filter = $count_total;
                }
                $qData=$query->orderBy('payment_details.created_at','desc')->distinct('expired_name')->get();
                
                $data = DataTables::of($qData)->addColumn('action', function($data1){
                    $button ='';
                    if(auth()->user()->hasPermissionTo('View Cleared Payment'))      
                    {
                        if ($data1->payment_type=='FC amount') {
                       
                        $button .= "<a class='btn mr-2 mb-1 btn-rounded btn-primary viewPayment' data-value='' href='#' id='user_".$data1->id."' 
                        onclick='showViewForm(".$data1->id.")' 
                        data-id='".$data1->id."'
                        data-totalFC = '".$data1->fc_released."' 
                        data-totalMembers = '".$data1->total_members."' 
                        data-fcDue = '".$data1->amount_to_pay."'
                        data-adminExpense = '".$data1->admin_expense."' 
                        data-dueInterest = '".$data1->due_interest."'
                        data-duePenalty = '".$data1->due_penalty."'
                        data-fyAdvance = '".$data1->advance."'
                        data-totalDue =  '".$data1->paid_amount."'
                        >View</a>";
                    }
                    }
                    return $button;
                })->with([
                    "recordsTotal" => $count_total,
                    "recordsFiltered" => $count_filter,
                ])
                ->skipPaging()
                ->rawColumns(['action'])
                ->make(true);  
                Log::info('PaymentsRepository: getClearedPaymentsData::End');
                return $data;
            }
            catch(QueryException $e){
                Log::error('PaymentsRepository: getClearedPaymentsData::Error'.$e->getMessage());
                throw $e;
            }
    }
    //-------------------------------------------------------------------------

    /**
     * Function to calculate and add penalty on pending payment after due date
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function calculateUpdatePenalty()
    {       
        $res=false;      
        try{
            Log::info('PaymentsRepository: calculateUpdatePenalty::Start');
                $date = Carbon::today();
                $setting=Utilities::getFCSetting();
                $penalty=$setting['penalty'];

                Log::info('PaymentsRepository: calculateUpdatePenalty::getPendingPayments:start');

                $pendingBills=Invoice::whereDate('due_date','<',$date)
                                 ->select('id as invoice_id','member_id','total_fc','bill_date','due_date',
                                    DB::raw('(SELECT COUNT(*) FROM fc_payments WHERE fc_payments.member_id = invoices.member_id AND fc_payments.payment_type ="fc_amount" AND fc_payments.has_paid ="N" AND fc_payments.invoice_id = invoices.id) AS pending_fc'))
                                 ->selectRaw("(DATEDIFF(CURRENT_DATE,DATE(due_date))) AS overdue")
                                 ->get();                            
                                 
                Log::info('PaymentsRepository: calculateUpdatePenalty::getPendingPayments:end');
                DB::beginTransaction();
                if (count($pendingBills)>0) {
                Log::info('PaymentsRepository: calculateUpdatePenalty::calculatePenalty:start');
                    $updatePenalty=[];                 
                    foreach ($pendingBills as $key => $value){
                        $penalty_data=[];
                        if($value->overdue>=1 && $value->pending_fc>0) {
                            $old_penalty=FCPayments::where('payment_type','due_penalty')->where('invoice_id',$value->invoice_id)->first();

                            if (empty($old_penalty)) {
                                $penalty_data=array(
                                            'member_id'=>$value->member_id,
                                            'payment_against_member'=>null,
                                            'payment_type'=>'due_penalty',
                                            'amount_to_pay'=>$penalty,
                                            'admin_expense'=>'0',
                                            'total_payment'=>$penalty,
                                            'bill_date'=>$value->bill_date,
                                            'due_date'=>$value->due_date,
                                            'invoice_id'=>$value->invoice_id,
                                            'has_paid'=>'N',
                                            'created_at'=>date('Y-m-d h:i:s'),
                                            'created_by'=>'job',
                                         );
                                if (!empty($penalty_data)) {
                                    $insert=FCPayments::insert($penalty_data);
                                }
                            }else{
                                $new_penalty=$old_penalty->amount_to_pay+$penalty;
                                $updatePenalty=array(
                                        'amount_to_pay'=>$new_penalty,
                                        'total_payment'=>$new_penalty,
                                );
                                $update=FCPayments::where('id',$old_penalty->id)
                                   ->update($updatePenalty);
                            }                 
                            $res=true;
                            Log::info('PaymentsRepository: calculateUpdatePenalty::calculatePenalty:end');
                        }
                    }
                }
                DB::commit();
            Log::info('PaymentsRepository: calculateUpdatePenalty::End');
            }
            catch(QueryException $e){
                DB::rollBack();
                Log::error('PaymentsRepository: calculateUpdatePenalty::Error'.$e->getMessage());
                throw $e;
            }
            return $res;
    }
    //-------------------------------------------------------------------------

    /**
     *  Return penalties data 
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getPenaltiesData($params)
    {             
        try{
            Log::info('PaymentsRepository: getPenaltiesData::Start');

                $search = !empty($params['search']['value'])?$params['search']['value']:'';  
                $pageSize = !empty($params['length']) ? $params['length'] : 10;
                $start = isset($params['start']) ? $params['start'] : 0;
                $count_filter = 0;
                $data  = array();

                $query =FCPayments::leftJoin('users','fc_payments.member_id',
                    '=','users.id')
                        ->leftJoin('user_profiles','fc_payments.member_id','=','user_profiles.user_id')
                        ->leftJoin('invoices','fc_payments.invoice_id','=','invoices.id')
                        ->select('fc_payments.id as penalty_id','fc_payments.member_id','invoices.id as invoices_id','fc_payments.amount_to_pay','users.name','user_profiles.member_id As register_id','invoices.invoice',DB::raw('DATE_FORMAT(invoices.due_date, "%d-%m-%Y") as due_date'))
                        ->selectRaw("(DATEDIFF(CURRENT_DATE,DATE(invoices.due_date))) AS overdue")
                         ->selectRaw("TRIM('_Invoice.pdf' FROM  invoices.invoice) AS invoice_no")
                         ->where('fc_payments.has_paid','N')
                         ->where('fc_payments.payment_type','due_penalty');

                $count_total = $query->count();
                if($search != ''){
                    $query->Where(function ($query) use ($search) {
                        $query->orWhere('users.name','LIKE' , '%'.$search.'%')
                        ->orWhere('fc_payments.total_payment', 'LIKE' , '%'.$search.'%')
                        ->orWhere('invoices.invoice', 'LIKE' , '%'.$search.'%')
                        ->orWhere('user_profiles.member_id', 'LIKE' , '%'.$search.'%');
                    });
                    $count_filter = $query->count();
                }
                $query->skip($start)->take($pageSize);
                if($count_filter == 0 && $search == ''){
                    $count_filter = $count_total;
                }
                $qData=$query->orderBy('fc_payments.updated_at','desc')->get();
                $data = DataTables::of($qData)->addColumn('action', function($qData){
                    $button ='';
                    
                    $button .= "<a class='btn mr-2 mb-1 btn-rounded btn-primary viewPayment' data-value='' href='#' id='user_".$qData->invoices_id."' data-memberId ='".$qData->member_id." 'data-penaltyid ='".$qData->penalty_id."'
                        data-duePenalty ='".$qData->amount_to_pay."'  onclick='showPaymentForm(".$qData->invoices_id.")'
                        >Pay</a>";
                    return $button;
                })->with([
                    "recordsTotal" => $count_total,
                    "recordsFiltered" => $count_filter,
                ])
                ->skipPaging()
                ->rawColumns(['action'])
                ->make(true);  
                Log::info('PaymentsRepository: getPenaltiesData::End');
                return $data;
            }
            catch(QueryException $e){
                Log::error('PaymentsRepository: getPenaltiesData::Error'.$e->getMessage());
                throw $e;
            }
    }
    //-------------------------------------------------------------------------


    /**
     *  Return list of all members whose AGE is 30 or below 30
     * 
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getInsuranceListData($params)
    {             
        try{
            Log::info('PaymentsRepository: getInsuranceListData::Start');

                $search = !empty($params['search']['value'])?$params['search']['value']:'';  
                $pageSize = !empty($params['length']) ? $params['length'] : 10;
                $start = isset($params['start']) ? $params['start'] : 0;
                $count_filter = 0;
                $data  = array();

                $query =User::leftJoin('membership_details', 'users.id', '=', 'membership_details.user_id')
                ->leftJoin('user_profiles','users.id','=','user_profiles.user_id')
                ->leftJoin('roles', 'users.role', '=', 'roles.id')
                ->leftJoin('user_accounts', 'users.id', '=', 'user_accounts.user_id')
                ->leftJoin('accidental_insurances', 'users.id', '=', 'accidental_insurances.member_id')
                ->select('users.*','user_profiles.member_id','membership_details.status','user_accounts.account_id','membership_details.join_date','membership_details.status as memeber_status',
                    DB::raw('DATE_FORMAT(join_date, "%d-%m-%Y") as join_date'),
                    DB::raw('DATE_FORMAT(users.created_at,"%d-%m-%Y") as created_date'))
                    ->selectRaw("(TIMESTAMPDIFF(YEAR, DATE(users.dob), CURRENT_DATE)) AS member_age")
                    ->where(DB::raw("(TIMESTAMPDIFF(YEAR, DATE(users.dob), CURRENT_DATE))"),'<=','30')
                ->where('accidental_insurances.year',date('Y'))
                ->where('roles.name','Member')
                ->where('membership_details.status','!=','Expired');

                if(!empty($params['from']) && !empty($params['to']))  {
                    $from = trim($params['from']);
                    $to = trim($params['to']);
                    $from = date('Y-m-d', strtotime($from));
                    $to = date('Y-m-d', strtotime($to));
                
                    $query->where(function ($q) use ($from,$to) {
                        $q->where('users.created_at', '>=', $from.' 00:00:00');
                        $q->where('users.created_at', '<=', $to.' 23:59:59');
                    });
                }
                
                if(!empty($params['register_id'])) {
                    $query->where('user_profiles.member_id', 'LIKE', '%'.$params['register_id'].'%');
                }
                if(!empty($params['member_name'])) {
                    $query->where('users.name','LIKE', '%'.$params['member_name'].'%');
                }

                if(!empty($params['member_status'])) {
                    $query->where('membership_details.status',$params['member_status']);
                }

                if(!empty($params['payment_status'])) {
                    $query->where('accidental_insurances.status',$params['payment_status']);
                }
                if (!empty($account_id) && $role_name!='SuperAdmin') {
                    $query->where('user_accounts.account_id',$account_id);
                }

                $count_total = $query->count();

                if($search != ''){
                    $query->Where(function ($query) use ($search) {
                        $query->orWhere('users.name','LIKE' , '%'.$search.'%')
                        ->orWhere('users.email','LIKE' , '%'.$search.'%')
                        ->orWhere('users.mobile_number','LIKE' , '%'.$search.'%')
                        ->orWhere('membership_details.status','LIKE' , '%'.$search.'%')
                        ->orWhere('membership_details.join_date','LIKE' , '%'.$search.'%')
                        ->orWhere('membership_details.age','LIKE' , '%'.$search.'%')
                        ->orWhere('users.created_at','LIKE' , '%'.$search.'%')
                        ->orWhere('user_profiles.member_id', 'LIKE' , '%'.$search.'%');
                    });
                    $count_filter = $query->count();
                }

                $query->skip($start)->take($pageSize);
                if($count_filter == 0 && $search == ''){
                    $count_filter = $count_total;
                }
                $qData=$query->orderBy('users.created_at','desc')->get();
                
                $data = DataTables::of($qData)->addColumn('action', function($data){
                    $button ='';
                    if(auth()->user()->hasPermissionTo('View Insurance'))      
                    {
                        $button .= ' <button data-customer_id="'.$data->id.'" data-status="Y" class="btn btn-primary btn-rounded mr-2 mb-1"  class="text-primary mr-2" data-toggle="modal" onclick="return showInsurance('.$data->id.')">View</button> ';
                    }
                    return $button;
                })->with([
                    "recordsTotal" => $count_total,
                    "recordsFiltered" => $count_filter,
                ])
                ->skipPaging()
                ->rawColumns(['action'])
                ->make(true);  
                Log::info('PaymentsRepository: getInsuranceListData::End');
                return $data;
            }
            catch(QueryException $e){
                Log::error('PaymentsRepository: getInsuranceListData::Error'.$e->getMessage());
                throw $e;
            }
    }
    //-------------------------------------------------------------------------

    /**
     *  Return particular member's yearly accidental insurance data based on id
     * 
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getMemberInsuranceData($params)
    {             
        try{
            Log::info('PaymentsRepository: getMemberInsuranceData::Start');
                $search = !empty($params['search']['value'])?$params['search']['value']:'';  
                $pageSize = !empty($params['length']) ? $params['length'] : 10;
                $start = isset($params['start']) ? $params['start'] : 0;
                $count_filter = 0;
                $data  = array();

                $query = AccidentalInsurance::select('id','year','updated_by','status',DB::raw('DATE_FORMAT(payment_date, "%d-%m-%Y") as payment_date'))->selectRaw("(CASE status WHEN 'N' THEN 'Unpaid' WHEN 'Y' THEN 'Paid' END) AS payment_status")->where('member_id',$params['user_id']);

                $count_total = $query->count();
                $query->skip($start)->take($pageSize);
                if($count_filter == 0 && $search == ''){
                    $count_filter = $count_total;
                }
                $qData=$query->orderBy('created_at','desc')->get();
                
                $data = DataTables::of($qData)->addColumn('action', function($data){
                    $button ='';
                    if(auth()->user()->hasPermissionTo('Edit Insurance'))      
                    {
                        if ($data->year<=date('Y')) {
                            $button .= ' <button data-id="'.$data->id.'" data-year="'.$data->year.'" data-status="'.$data->status.'" data-date="'.$data->payment_date.'" class="btn btn-primary btn-rounded mr-2 mb-1 change_status"  class="text-primary mr-2" data-toggle="modal">Edit</button>'; 
                        }
                       
                    }
                    return $button;
                })->with([
                    "recordsTotal" => $count_total,
                    "recordsFiltered" => $count_filter,
                ])
                ->skipPaging()
                ->rawColumns(['action'])
                ->make(true);  
                Log::info('PaymentsRepository: getMemberInsuranceData::End');
                return $data;
            }
            catch(QueryException $e){
                Log::error('PaymentsRepository: getMemberInsuranceData::Error'.$e->getMessage());
                throw $e;
            }
    }
    //-------------------------------------------------------------------------

    /**
     *  Function to generate and store FC bill
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function generateFcBill(){
        $res=false;
        try{
            Log::info('Payments Repo: generateFcBill::Start');
            
            $date = Carbon::today();
            $month = date('F', strtotime($date));    
            $dates=Utilities::billDate($date,'current_date');
            $current_year = date('Y', strtotime($date));
            $last_year=$current_year-1;
            //get all active members
            Log::info('Payments Repo: generateFcBill::get All Active members:Start'); 
                $active_members=MembershipDetails::where('status','Active')->where('join_date',"<", Carbon::now()->subYear())->get();
                $active_count=count($active_members);
                
            Log::info('Payments Repo: generateFcBill::get All Active members:End'); 

            Log::info('Payments Repo: generateFcBill:: Calculate BillDate & DueDate:Start');
            $query='';  
            $expired_count=0;            
            $admin_expense=StringConstants::$ADMINISTRATIVE_EXPENSE;
            // if ($month=='March' ||$month=='April'){
            //     $start_date=$last_year.'-10-01';
            //     $end_date=$current_year.'-03-31';
            //     $query=MembershipDetails::where('status','Expired')->whereBetween('confirm_date', [$start_date, $end_date]);
            // }
            // elseif($month=='September' ||$month=='October'){
            //     $start_date=$current_year.'-04-01';
            //     $end_date=$current_year.'-09-30';
            //     $query=MembershipDetails::where('status','Expired')->whereBetween('confirm_date', [$start_date, $end_date]);
            // }

            $query=MembershipDetails::where('status','Expired')->where('confirm_date',">", Carbon::now()->subMonths(6));
            Log::info('Payments Repo: generateFcBill:: Calculate BillDate & DueDate:End');

            if (!empty($query)) {
                $expired_count=$query->count(); 
            }
            
            $offset = 0;
            $limit = 100;
            Log::info('Payments Repo: generateFcBill:: Total Expired Members - '.$expired_count);
            while($offset<$expired_count){
                $expired_members=$query->skip($offset)->take($limit)->get();

                Log::info('Payments Repo: generateFcBill:: from - '.$offset.' - to - '.($offset+$limit).' - Start');
                    foreach ($expired_members as $key => $value) {
                       
                        if (!empty($value) && is_numeric($value->fc_amount)) {
                            Log::info('Payments Repo:generateFcBill for expired member:: Start-'.$value->user_id);
                                $insert=$this->storeFcBill($value,'total_expired_members');    
                            Log::info('Payments Repo:generateFcBill for expired member:: End - '.$value->user_id);
                        }
                    }
                Log::info('Payments Repo: generateFcBill:: from - '.$offset.' - to - '.($offset+$limit).' - End');
                unset($expired_members);
                $offset += $limit;
            }
            $res=true;
            unset($query);
            unset($expired_count);
            unset($offset);
            unset($limit);
            Log::info('Payments Repo: generateFcBill:End');
        }catch(QueryException $e){
            Log::error('Payments Repo: generateFcBill::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }
    //-------------------------------------------------------------------------

    /**
     *  Function to generate and store first year advance amount bill
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function generateAdvanceAmountBill(){
        $res=false;
        try{
            Log::info('Payments Repo: generateAdvanceAmountBill::Start');
            DB::beginTransaction();
            $setting=Utilities::getFCSetting();
            $fy_advance=$setting['advance_amount'];
            $current_date = date("Y-m-d");

            $prev_date = Carbon::today()->subYear();
            $last_year_date=date("Y-m-d", strtotime($prev_date));
            
            $dates=Utilities::billDate($current_date,'current_date');
            
            if (!empty($dates)) {
                $bill_date=$dates['bill_date'];
                $final_due_date=$dates['final_due_date'];
            }

            $advance_bill_data=[];
            $count=0;
            $query=MembershipDetails::where('status','Active')->whereBetween('join_date', [$last_year_date,$current_date]);
            
            if (!empty($query)) {
                $count=$query->count(); 
            }
            
            $offset = 0;
            $limit = 100;

            Log::info('Payments Repo: generateAdvanceAmountBill:: New Active Members - '.$count);
                while($offset<$count){
                    $active_members=$query->skip($offset)->take($limit)->get();

                    Log::info('Payments Repo: generateAdvanceAmountBill:: from - '.$offset.' - to - '.($offset+$limit).' - Start');
                    foreach ($active_members as $key => $value) {
                        if (!empty($value)) {
                            Log::info('Payments Repo:generateAdvanceAmountBill for single member:: Start-'.$value->user_id);
                            $old_advance=FCPayments::where('member_id',$value->user_id)->where('payment_type','advance')->where('bill_date','=',$bill_date)->first();

                            $advace_amount=[];
                            $advace_amount=array( 
                                'member_id'     => $value->user_id,
                                'payment_against_member'=> null,
                                'payment_type'  => 'advance',
                                'amount_to_pay' => $fy_advance,
                                'due_penalty'   => '0',
                                'due_interest'  => '0',
                                'admin_expense' => '0',
                                'total_payment' => $fy_advance,
                                'paid_amount'   => null,
                                'bill_date'     => !empty($bill_date)?$bill_date:null,
                                'due_date'      => !empty($final_due_date)?$final_due_date:
                                                    null,
                                'has_paid'      => 'N',
                                'created_at'    => date('Y-m-d h:i:s'),
                                'created_by'    => $value->created_by,
                            );
                            if (empty($old_advance)&&!empty($advace_amount)) {
                                array_push($advance_bill_data, $advace_amount);
                            }
                            Log::info('Payments Repo:generateAdvanceAmountBill for single member:: End-'.$value->user_id);
                        }
                        unset($old_advance);
                    }
                    if (!empty($advance_bill_data)) {
                        $insert=FCPayments::insert($advance_bill_data);
                        Log::info('Payments Repo:generateAdvanceAmountBill:: Data stored');
                        unset($advance_bill_data);
                        unset($advace_amount);
                    }
                    unset($active_members);
                    $offset += $limit;
                }
                unset($count);
                unset($offset);
                unset($limit);
            DB::commit(); 
            $res=true;
            Log::info('Payments Repo: generateAdvanceAmountBill:End');
        }catch(QueryException $e){
            DB::rollBack();
            Log::error('Payments Repo: generateAdvanceAmountBill::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }
    //-------------------------------------------------------------------------

    /**
    *  Store Fc due details data
    *
    * @access  public
    * @param   Illuminate\Http\Request $request
    * @return  Response
    * @author  BRAD Contech Solutions Pvt Ltd.
    */
   public function storeFcBill($data,$type){
        
        $res=false;
        try{
           Log::info('Payments Repo: storeFcBill::Start');
           DB::beginTransaction();
           if (!empty($data)) {

               $setting=Utilities::getFCSetting();
                if ($type=='total_expired_members') {
                    $date = Carbon::today();
                    $dates=Utilities::billDate($date,'current_date');
                    $expired_member_id=$data['user_id'];
                }
                elseif($type=='single_expired_member'){
                    $dates=Utilities::billDate($data['confirm_date'],'expired_date');
                    $expired_member_id=$data['id'];
                }

               //get all active members completed one year membership
               Log::info('Payments Repo: storeFcBill::get All Active members:Start'); 
                    $active_members=MembershipDetails::where('status','Active')->where('join_date',"<", Carbon::now()->subYear())->get();

                    $count=count($active_members);
               Log::info('Payments Repo: storeFcBill::get All Active members:End'); 

               Log::info('Payments Repo: storeFcBill:: Total Members -'.$count);
               if ($count>0 && !empty($data['fc_amount'])) {
                    $fc_due=0;
                    //fc due calculation
                    $fc_due=$data['fc_amount']/$count;
                    $admin_expense=StringConstants::$ADMINISTRATIVE_EXPENSE;
                    $final_due_date='';
                    $bill_date=''; 
                    if (!empty($dates)) {
                        $bill_date=$dates['bill_date'];
                        $final_due_date=$dates['final_due_date'];
                    }

                   $fc_bill_data=[];
                   foreach ($active_members as $key => $value) {
                       $fc_bill=array();
                       if (!empty($value)) {
                           Log::info('Payments Repo:storeFcBill::FcDue Details preparing started - '.$value->user_id);
                           $old_payment=FCPayments::where('member_id',$value->user_id)->where('payment_against_member',$expired_member_id)->first();
                           //total due calculation
                           $total_due=0;
                           if (!empty($fc_due) && !empty($admin_expense)) {
                               $total_due=$fc_due+$admin_expense;
                           }
                           $fc_bill=array(
                               'member_id'   => $value->user_id,
                               'payment_against_member'=> $expired_member_id,
                               'payment_type' => 'fc_amount',
                               'amount_to_pay'=> $fc_due,
                               'admin_expense'=> $admin_expense,
                               'total_payment'=> $total_due,
                               'paid_amount'  => '',
                               'bill_date'    => $bill_date,
                               'due_date'     => $final_due_date,
                               'has_paid'     => 'N',
                               'created_at'   => date('Y-m-d h:i:s'),
                               'created_by'   => $value->created_by,
                            );
                            if (empty($old_payment)) {
                                array_push($fc_bill_data, $fc_bill);
                            }
                            Log::info('Payments Repo:storeFcBill::FC Due Details preparing ended '.$value->user_id);
                       }
                    }
                    if (!empty($fc_bill_data)) {
                        $insert=FCPayments::insert($fc_bill_data);
                        Log::info('Payments Repo:storeFcBill:: Data stored-'.$value->user_id);
                        unset($fc_bill_data);
                        unset($total_due);
                        unset($fc_bill);
                    }
               }
           }
           DB::commit();
           $res=true;
           Log::info('Payments Repo: storeFcBill:End');
       }catch(QueryException $e){
           DB::rollBack();
           Log::error('Payments Repo: storeFcBill::Error'.$e->getMessage());
           throw $e;
       }
       return $res;
   }
   //-------------------------------------------------------------------------

   /**
     *  Return list of all active and probationary members
     * 
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getCriticalDefaultersData($params)
    {             
        try{
            Log::info('PaymentsRepository: getCriticalDefaultersData::Start');

                $search = !empty($params['search']['value'])?$params['search']['value']:'';  
                $pageSize = !empty($params['length']) ? $params['length'] : 10;
                $start = isset($params['start']) ? $params['start'] : 0;
                $count_filter = 0;
                $data  = array();
                $setting=Utilities::getFCSetting();
                $advance=$setting['advance_amount'];
                $critical_default=StringConstants::$CRITICAL_DEFAULTER;

                $query =User::leftJoin('user_profiles','users.id','=','user_profiles.user_id')
                    ->leftJoin('membership_details', 'users.id', '=', 'membership_details.user_id')
                    ->leftJoin('user_accounts', 'users.id', '=', 'user_accounts.user_id')
                    ->select('users.*','users.id as user_id','user_profiles.member_id','membership_details.status','membership_details.age','membership_details.fc_amount',
                    DB::raw('DATE_FORMAT(membership_details.join_date, "%d-%m-%Y") as join_date'),
                    DB::raw('DATE_FORMAT(membership_details.created_at, "%d-%m-%Y") as created_date'),
                    DB::raw('(SELECT COUNT(*) FROM fc_payments WHERE fc_payments.member_id = users.id AND fc_payments.payment_type ="fc_amount" AND fc_payments.has_paid="N") AS total_fc'),
                    DB::raw('(SELECT round(SUM(total_payment)) FROM fc_payments WHERE fc_payments.member_id = users.id AND fc_payments.has_paid="N") AS total_due'))
                    ->where(DB::raw("(SELECT round(SUM(total_payment)) FROM fc_payments WHERE fc_payments.member_id = users.id AND fc_payments.has_paid='N') "),'>','0');
                    
                $query->where(function ($q) {
                    $q->where('membership_details.status', 'Active')
                    ->orWhere('membership_details.status', 'Probationary');
                });
                    

                if(!empty($params['from']) && !empty($params['to']))  {
                    $from = trim($params['from']);
                    $to = trim($params['to']);
                    $from = date('Y-m-d', strtotime($from));
                    $to = date('Y-m-d', strtotime($to));
                
                    $query->where(function ($q) use ($from,$to) {
                        $q->where('users.created_at', '>=', $from.' 00:00:00');
                        $q->where('users.created_at', '<=', $to.' 23:59:59');
                    });
                }
                
                if(!empty($params['register_id'])) {
                    $query->where('user_profiles.member_id',$params['register_id']);
                }
                if(!empty($params['member_name'])) {
                    $query->where('users.name','LIKE', '%'.$params['member_name'].'%');
                }

                if(!empty($params['member_status'])) {
                    $query->where('membership_details.status',$params['member_status']);
                }
                if (!empty($account_id) && $role_name!='SuperAdmin') {
                    $query->where('user_accounts.account_id',$account_id);
                }

                $count_total = $query->count();

                if($search != ''){
                    $query->Where(function ($query) use ($search) {
                        $query->where('users.name','LIKE' , '%'.$search.'%')
                        ->orWhere('users.email','LIKE' , '%'.$search.'%')
                        ->orWhere('users.mobile_number','LIKE' , '%'.$search.'%')
                        ->orWhere('membership_details.status','LIKE' , '%'.$search.'%')
                        ->orWhere('membership_details.join_date','LIKE' , '%'.$search.'%')
                        ->orWhere('users.created_at','LIKE' , '%'.$search.'%')
                        ->orWhere('user_profiles.member_id','LIKE' , '%'.$search.'%');
                    });
                    $count_filter = $query->count();
                }
                $qData=$query->orderBy('users.created_at','desc')->skip($start)->take($pageSize)->get();
                if($count_filter == 0 && $search == ''){
                    $count_filter = $count_total;
                }
                $due=0;

                foreach ($qData as $key => $value) {
                    $balance_due=$advance-$value->total_due;
                    if ($balance_due <=$critical_default) {
                        if ($balance_due>0) {
                            $value->due_amount=$balance_due;
                        }
                        else{
                             $value->due_amount=$due;
                        }
                    }else{
                        $value->total_due=$due;
                        $value->due_amount=$advance;
                    }
                    unset($balance_due);
                }

                $data = DataTables::of($qData)->addColumn('action', function($data){
                    $button ='';
                    if(auth()->user()->hasPermissionTo('View Member'))      
                    {
                        $button .= '<a href="'.url('/')."/member/view/".$data->id.'"class="text-primary"><i class="fa fa-eye"  data-toggle="tooltip" title="View Member" id="'.$data->id.'"></i></a>';
                    }
                    return $button;
                })->with([
                    "recordsTotal" => $count_total,
                    "recordsFiltered" => $count_filter,
                ])
                ->skipPaging()
                ->rawColumns(['action'])
                ->make(true);  
                Log::info('PaymentsRepository: getCriticalDefaultersData::End');
                return $data;
            }
            catch(QueryException $e){
                Log::error('PaymentsRepository: getCriticalDefaultersData::Error'.$e->getMessage());
                throw $e;
            }
    }
    //-------------------------------------------------------------------------

    /**
     *  Update member yearly accidental insurance status with payment date
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function updateInsurance($data){
        $res=false;
        try{
            Log::info('PaymentsRepository: Update Insurance status::Start');
            DB::beginTransaction();
            $insurance_details=[];
            
            if (!empty($data['payment_date'])) {
                $date=date("Y-m-d", strtotime($data['payment_date']));
            }

            $insurance_details =array( 
                'status'        => $data['status'],
                'payment_date'  => !empty($date)?$date:'',
                'updated_at'    => date('Y-m-d h:i:s'),
                'updated_by'    => Auth::user()->email,
            );
            
            if (!empty($insurance_details)) {
                $update=AccidentalInsurance::where('id',$data['insurance_id'])->update($insurance_details);
            }
            
            DB::commit(); 
            $res=true;
            Log::info('PaymentsRepository: update Insurance status:End');
        }catch(QueryException $e){
            DB::rollBack();
            Log::error('PaymentsRepository: update Insurance status::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }
    //------------------------------------------------------------------------------


    /**
     *  Function To download Invoice/Bill pdf 
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */
    public function generateInvoicePdf()
    {
        $res=false;
        try{
            Log::info('PaymentsRepository:generateInvoicePdf::Start');

            //get all active members completed one year membership
            $query=MembershipDetails::leftJoin('users', 'membership_details.user_id', '=', 'users.id')->select('membership_details.*','users.email','users.name')->where('status','Active')->where('join_date',"<", Carbon::now()->subYear());

            $count=$query->count();

            Log::info('PaymentsRepository: generateInvoicePdf:: Total Active Members -'.$count);
            $offset = 0;
            $limit = 10;

            while ( $offset < $count ) {
                $active_members = $query->skip($offset)->take($limit)->get();
                Log::info('PaymentsRepository: generateInvoicePdf:: from - '.$offset.' - to - '.($offset+$limit).' - Start');

                foreach ($active_members as $member) {
                    
                    $invoice_data=[];
                    if (!empty($member)) {
                        $id=$member->user_id;
                        Log::info('PaymentsRepository: generateInvoicePdf::Preparing Invoice for member id:'.$id.' Start');
                        
                        $data=$this->getPaymentsDataForInvoice($id);
                        $expired_members=[];
                        foreach ($data as $key => $value) {
                            
                            if (gettype($value) =='object'){
                                $member_amount=$value->expired_member_name.' - '.$value->total_payment;
                                array_push($expired_members, $member_amount);
                            }
                        }
                        $expired_members_names=implode(',', $expired_members);
                        $expired_members_names=nl2br(str_replace(",","\n",$expired_members_names));
                        if (!empty($data)&& count($data)>0) {
                            $invoice_no=time() . $id;
                            $pdf_name=$invoice_no.'_Invoice.pdf';

                            $pdf = PDF::loadView('payments.bill', compact('data','invoice_no'), 
                            [
                                'watermark_font'       => 'canilari',
                                'show_watermark'       => true,
                                'format'               => 'A4',
                                'margin_left'          => 0,
                                'margin_right'         => 0,
                                'margin_top'           => 10,
                            ]);
                            
                            $content = $pdf->output();

                            $store=Storage::put('public/invoices/'.$pdf_name, $content);

                            if($store){
                                $invoice_data=array( 
                                    'member_id'     => $id,
                                    'invoice'    => $pdf_name,
                                    'total_fc'    => $data['total_fc'],
                                    'bill_date'    => $data['old_bill_date'],
                                    'due_date'    => $data['due_date'],
                                    'total_due'    => $data['amount'],
                                    'created_at'  => date('Y-m-d h:i:s'),
                                    'created_by'  => 'Job',
                                );
                                $invoice_id=Invoice::insertGetId($invoice_data);
                                Log::info('PaymentsRepository: generateInvoicePdf::Invoice Stored');
                                $invoice=array('invoice_id'=>$invoice_id);
                                
                                $update=FCPayments::where('member_id',$id)->whereNull('invoice_id')->update($invoice);

                                $invoice_fcs_data=[];
                                foreach ($data as $key => $value) {
                                    if (gettype($value) =='object'){

                                        $invoice_fc=[];
                                        
                                        $invoice_fc=array(
                                            'invoice_id'=>$invoice_id,
                                            'fc_payment_id'=>$value->id
                                        );
                                        if (!empty($invoice_fc)) {
                                            array_push($invoice_fcs_data, $invoice_fc);
                                        }   
                                    }
                                }
                                if (!empty($invoice_fcs_data)) {
                                    $insert=InvoiceFCs::insert($invoice_fcs_data);
                                }
                            }
                            
                            Log::info('PaymentsRepository: generateInvoicePdf::Send Email -Start');
                                $billtype='';
                                if (!empty($data['bill_number'])) {
                                    if ($data['bill_number']=='1') {
                                        $billtype='First';
                                    }else{
                                        $billtype='Second';
                                    }
                                }
                                $file = array();
                                $file['file'] = $content;
                                $file['file_name'] = $pdf_name;
                                $file['file_mime_type'] = 'application/pdf';
                                $emailContent=[];
                                $name=$member->name;
                                $sendTo=$member->email;
                                $emailContent['sendTo']=$sendTo;
                                $emailContent['subject']='Invoice Email';
                                $emailContent['body']='Dear '.$name.',<br><br>
                                                    Jai Mahesh!!<br><br>
                                                      Please find attached the invoice for the '.$billtype.' half of the current financial year for your perusal.<br><br>
                                                      We have had the following deaths in the last 6 months for which the invoice is raised along with previous dues, if any:<br><br>Name - Amount<br>'.$expired_members_names.'<br><br>
                                                      Request you to make the payment at the earliest via cheque in favour of “Mahesh Foundation” or via RTGS in the following bank account: <br><br>
                                                      Bank Name:    AP MAHESH CO-OPERATIVE URBAN BANK LTD.<br>
                                                      Account :     008001200029882<br>
                                                      Branch:   Himayath Nagar Branch<br>
                                                      IFSC Code:    APMC0000008 <br><br>
                                                      Do provide the snapshot of the payment made online to the back office of Mahesh Foundation via email to maheshfoundation@yahoo.in with the subject line as your FSS membership Number and the reference to the invoice number.<br><br>
                                                      Please do reach out to us via email for any concerns or clarifications.<br><br>
                                                      Regards,<br>
                                                      Team Mahesh Foundation';
                                
                                if (!empty($emailContent) && !empty($sendTo)) {
                                    $emailId=EmailUtilities::sendEmailNotification($emailContent,$sendTo,$file);
                                }
                            Log::info('PaymentsRepository: generateInvoicePdf::Send Email -End');
                        }
                        Log::info('PaymentsRepository: generateInvoicePdf::Preparing Invoice for member id:'.$id.' End');

                        Log::info('PaymentsRepository: generateInvoicePdf::Send Invoice SMS -Start -' .$id);

                            $smsContent=[];
                            $name=$data['name'];
                            $sendTo=!empty($data['mobile_number'])?$data['mobile_number']:'';
                            $smsContent['sendTo']=$sendTo;
                            $smsContent['body']='Dear '.$name.'ji 
                                                  Jai Mahesh!!
                                                  An invoice has been emailed towards the premium for the FSS scheme due to death of '.$data['total_fc'].' members in the past 6 months. Request you to make the payment at the earliest. You can also view more details on the member portal.';
                            
                            if (!empty($smsContent) && !empty($sendTo)) {
                                // $smsId=SmsUtilities::sendSmsNotification($smsContent,$sendTo);
                            }
                           
                         Log::info('PaymentsRepository: generateInvoicePdf::Send Invoice SMS End '.$id);
                    }
                    unset($invoice_data);
                    unset($data);
                }
                Log::info('PaymentsRepository: generateInvoicePdf:: from - '.$offset.' - to - '.($offset+$limit).' - END');
                unset ($active_members);
                $offset += $limit;
            }
            $res=true;
        } catch(QueryException $e) {
            Log::error('PaymentsRepository:Invoice pdf download::Error'.$e->getMessage());
            return view('errors.custom');
        }
        return $res;
    }
    //---------------------------------------------------------------------------------------

    /**
     *  Return particular payments data based on payment id
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getPaymentsDataForInvoice($id)
    {             
        try{
            Log::info('PaymentsRepository:getPaymentsDataForInvoice::Start');
            $data=[];
                if ($id) {
                    
                    $data=FCPayments::leftJoin('users as expired_member','fc_payments.payment_against_member','=','expired_member.id')
                    ->leftJoin('membership_details', 'expired_member.id', '=', 'membership_details.user_id')
                    ->leftJoin('user_profiles as expired_profile','fc_payments.payment_against_member','=','expired_profile.user_id')
                    ->select('fc_payments.*','expired_member.name as expired_member_name','expired_member.name as expired_member_name','expired_profile.member_id As expired_member_id','membership_details.status','membership_details.age','membership_details.fc_amount','membership_details.fc_amount As fc_released','fc_payments.bill_date as original_bill_date',
                        DB::raw('DATE_FORMAT(membership_details.confirm_date, "%d-%m-%Y") as expired_date'),
                        DB::raw('DATE_FORMAT(bill_date, "%d-%m-%Y") as bill_date'),
                        DB::raw('(SELECT COUNT(*) FROM fc_payments WHERE fc_payments.payment_against_member = expired_member.id AND fc_payments.payment_type ="fc_amount") AS total_members')
                    )
                    ->selectRaw("round(amount_to_pay) AS amount_to_pay")
                    ->selectRaw("round(total_payment) AS total_payment")
                    ->where('fc_payments.has_paid','N')
                    ->where('fc_payments.payment_type','fc_amount')
                    ->where('fc_payments.member_id',$id)
                    ->orderBy('fc_payments.created_at','DESC')->get();
                    
                    $bill_data=FCPayments::select('admin_expense',DB::raw('round(SUM(total_payment)) AS total_due'),
                        DB::raw('COUNT(*) AS total_fc'))
                    ->where('payment_type' ,"fc_amount")
                    ->where('member_id',$id)
                    ->where('has_paid','N')
                    ->groupBy('admin_expense')
                    ->first();

                    $penalty=FCPayments::leftJoin('invoices','fc_payments.invoice_id','=','invoices.id')
                        ->select('total_payment')
                        ->selectRaw("TRIM('_Invoice.pdf' FROM  invoices.invoice) AS invoice_no")
                        ->where('payment_type' ,"due_penalty")
                        ->where('fc_payments.member_id',$id)
                        ->where('has_paid','N')
                        ->orderBy('invoices.created_at','DESC')
                        ->first();
                     
                    $user_data=User::leftJoin('user_profiles','users.id','=','user_profiles.user_id')->where('users.id',$id)->first();
                    
                    if (!empty($data) && count($data)>0) {
                       $bill_date=$data[0]['bill_date'];
                       $data['amount_in_word']=Utilities::amountInWords($bill_data['total_due']);
                       $data['amount']=$bill_data['total_due'];
                       $data['member_id']=$user_data['member_id'];
                       $data['address']=$user_data['address'];
                       $data['mobile_number']=$user_data['mobile_number'];
                       $data['name']=$user_data['name'];
                       $data['total_penalty']=$penalty['total_payment'];
                       $data['old_invoice_no']=$penalty['invoice_no'];
                       $data['total_fc']=$bill_data['total_fc'];
                       $data['total_admin_expense']=$bill_data['total_fc']*$bill_data['admin_expense'];
                       $data['due_date']=$data[0]['due_date'];
                       $data['old_bill_date']=$data[0]['original_bill_date'];
                       $data['bill_date']=date('dS F Y', strtotime($bill_date));
                       $data['contribution_date1']=$bill_date;
                       $data['contribution_date2']=Carbon::parse($bill_date)->addMonths(6)->format('d-m-Y');
                       $bill_month=date('m', strtotime($bill_date));
                       if ($bill_month<=6) {
                           $data['bill_number']='1';
                       }else{
                            $data['bill_number']='2';
                       }
                       Log::info('PaymentsRepository:getPaymentsDataForInvoice::End');
                    }
                    else{
                         Log::info('PaymentsRepository:getPaymentsDataForInvoice::Data not found');   
                    }
                    
                }
            }
            catch(QueryException $e){
                Log::error('PaymentsRepository:getPaymentsDataForInvoice::Error'.$e->getMessage());
                throw $e;
            }
            return $data;
    }
    //------------------------------------------------------------------------------


    /**
     *  Function To retuen total fc bills generated for member 
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getMemberInvoiceList($params)
    {             
        try{
            Log::info('PaymentsRepository:getMemberInvoiceList::Start');
            $search = !empty($params['search']['value'])?$params['search']['value']:'';  
            $pageSize = !empty($params['length']) ? $params['length'] : 10;
            $start = isset($params['start']) ? $params['start'] : 0;
            $count_filter = 0;
            $data  = array();

            if ($params) {
                $query=Invoice::where('member_id',$params['user_id'])
                ->select('invoices.*',DB::raw('DATE_FORMAT(bill_date, "%d-%m-%Y") as bill_date'));

                $count_total = $query->count();
                
                if($count_filter == 0 && $search == ''){
                    $count_filter = $count_total;
                }
                $qData=$query->skip($start)->take($pageSize)->get();

                $data = DataTables::of($qData)->addColumn('action', function($data){
                    $button ='';
                    if(auth()->user()->hasPermissionTo('Download Bill'))      
                    {
                        $button .="<a class='btn mr-2 mb-1 btn-rounded btn-primary viewPayment' data-value='' href='".url('/')."/storage/invoices/".$data->invoice."' target='_blank'>Download Invoice</a>";
                    }
                    return $button;
                })->with([
                    "recordsTotal" => $count_total,
                    "recordsFiltered" => $count_filter,
                ])
                ->skipPaging()
                ->rawColumns(['action'])
                ->make(true);
            }
        }
        catch(QueryException $e){
            Log::error('PaymentsRepository:getMemberInvoiceList::Error'.$e->getMessage());
            throw $e;
        }
        return $data;
    }
    //------------------------------------------------------------------------------


    /**
     *  Function to send reminder email to members every 15 days after bill generation
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function sendReminderEmail(){
        $res=false;
        try{
            Log::info('PaymentsRepository: sendReminderEmail::Start');

            $query='';  
            $unpaid_members_count=0;            

            $query=FCPayments::leftJoin('users', 'fc_payments.member_id', '=', 'users.id')
                ->leftJoin('user_profiles','fc_payments.member_id','=','user_profiles.user_id')
                ->leftJoin('membership_details', 'fc_payments.member_id', '=', 'membership_details.user_id')
                ->leftJoin('invoices','fc_payments.member_id', '=', 'invoices.member_id')
                ->select('users.*','fc_payments.member_id as user_id','user_profiles.member_id','membership_details.status','invoices.bill_date',
                DB::raw('(SELECT COUNT(*) FROM fc_payments WHERE fc_payments.member_id = users.id AND fc_payments.payment_type ="fc_amount" AND fc_payments.has_paid="N") AS total_fc'),
                DB::raw('(SELECT round(SUM(total_payment)) FROM fc_payments WHERE fc_payments.member_id = users.id AND fc_payments.has_paid="N") AS total_due'),
                DB::raw('DATE_FORMAT(invoices.bill_date, "%d-%m-%Y") as bill_date'))
                ->selectRaw("TRIM('_Invoice.pdf' FROM  invoices.invoice) AS invoice_no")
                ->where('membership_details.status','!=','Expired')
                ->where('fc_payments.has_paid','N')
                ->distinct('membership_details.user_id');   

            if (!empty($query)) {
                $unpaid_members_count=$query->count(); 
            }
            
            $offset = 0;
            $limit = 100;
            Log::info('PaymentsRepository: sendReminderEmail:: Total Unpaid Members - '.$unpaid_members_count);
            $uniqueIds=[];
            while($offset<$unpaid_members_count){
                $unpaid_members=$query->orderBy('invoices.created_at','DESC')->skip($offset)->take($limit)->get();
                
                Log::info('PaymentsRepository: sendReminderEmail:: from - '.$offset.' - to - '.($offset+$limit).' - Start');
                    foreach ($unpaid_members as $key => $value) {
                        if (!empty($value) && !empty($value->invoice_no) && $value->total_fc>0 && !in_array($value->user_id,$uniqueIds)) {
                            
                            array_push($uniqueIds, $value->user_id); 
                            Log::info('PaymentsRepository:sendReminderEmail::send email to member:: Start-'.$value->member_id);
                                $emailContent=[];
                                $name=$value->name;
                                $sendTo=$value->email;
                                $emailContent['sendTo']=$sendTo;
                                $emailContent['subject']='Reminder Email';
                                $emailContent['body']='Dear '.$name.',<br><br>
                                                    Jai Mahesh!!<br><br>
                                                      This is in reference to the invoice - '.$value->invoice_no.' that was raised on '.$value->bill_date.' and emailed to you. We are yet to receive the payment for the same. <br><br>We request you to make the payment at the earliest.You prompt payments help us to disburse the amounts to the family of the bereaved in time and help in the larger cause.<br><br>We hope you will understand and empathize with us and will do the needful at the earliest.
                                                         Request you to make the payment at the earliest via cheque in favour of “Mahesh Foundation” or via RTGS in the following bank account:<br><br>
                                                         Bank Name:    AP MAHESH CO-OPERATIVE URBAN BANK LTD.<br>
                                                      Account :     008001200029882<br>
                                                      Branch:   Himayath Nagar Branch<br>
                                                      IFSC Code:    APMC0000008 <br><br>
                                                      Do provide the snapshot of the payment made online to the back office of Mahesh Foundation via email to maheshfoundation@yahoo.in with the subject line as your FSS membership Number and the reference to the invoice '.$value->invoice_no.'. <br><br>
                                                      In case you have made the payments in the recent past then please do ignore this email and also send the payment details to the back office via email.<br><br>
                                                      Please do reach out to us via email for any concerns or clarifications.<br><br>
                                                      Regards,<br>
                                                      Team Mahesh Foundation.';
                                                 
                            if (!empty($emailContent) && !empty($sendTo)) {
                                $emailId=EmailUtilities::sendEmailNotification($emailContent,$sendTo);
                            }
                            Log::info('PaymentsRepository:sendReminderEmail::send email to member:: End - '.$value->member_id);

                            Log::info('PaymentsRepository: sendReminderEmail::Send reminder SMS Start -'.$value->member_id);

                            $smsContent=[];
                            $name=$value->name;
                            $sendTo=!empty($value->mobile_number)?$value->mobile_number:'';
                            $smsContent['sendTo']=$sendTo;
                            $smsContent['body']='Dear '.$name.'ji 
                                                  Jai Mahesh!!
                                                  Reminder to make your premium payment for the invoice raised on '.$value->bill_date.'. Request you to make the payment at the earliest. You can also view more details on the member portal.';
                            if (!empty($smsContent) && !empty($sendTo)) {
                                // $smsId=SmsUtilities::sendSmsNotification($smsContent,$sendTo);
                            }
                         Log::info('PaymentsRepository: sendReminderEmail::Send reminder SMS End-'.$value->member_id);
                        }
                    }
                Log::info('PaymentsRepository: sendReminderEmail:: from - '.$offset.' - to - '.($offset+$limit).' - End');
                unset($expired_members);
                $offset += $limit;
                
            }
            $res=true;
            unset($query);
            unset($unpaid_members_count);
            unset($offset);
            unset($limit);
            Log::info('PaymentsRepository: sendReminderEmail:End');
        }catch(QueryException $e){
            Log::error('PaymentsRepository: sendReminderEmail::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }
    //-------------------------------------------------------------------------


    /**
     *  Function to update penalty amount status
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function storePenalty($params)
    {   
        $res=false;          
        try{
            Log::info('PaymentsRepository: storePenalty::Start');
            DB::beginTransaction(); 
            
            Log::info('PaymentsRepository:storePenalty::Penalty payment details inserting started');   
            $account_id=Session::get('account_id');
            $payment_data=[];
            if (!empty($params['payment_date'])) {
                $payment_date=date("Y-m-d", strtotime($params['payment_date']));
            }
              
            $payment_data=array( 
                'member_id'     => $params['member_id'],
                'account_id'    => $account_id,
                'amount'        => $params['amount'],
                'remarks'       => '',
                'mode_of_payment'=> $params['payment_mode'],
                'payment_date'  => !empty($payment_date)?$payment_date:null,
                'created_at'  => date('Y-m-d h:i:s'),
                'created_by'  => Auth::user()->email,
            );
            
            $payment_id=Payment::insertGetId($payment_data);
            if($payment_id) {
                $payment_details=[];
                $payment_details=array( 
                    'payment_id'     => $payment_id,
                    'transaction_id' => '',
                    'payment_against_member'=> null,
                    'payment_type'  =>'due_penalty',
                    'created_at'  => date('Y-m-d h:i:s'),
                    'created_by'  => Auth::user()->email,
                ); 
                if (!empty($payment_details)) {
                    $insert=PaymentDetails::insert($payment_details);  
                }
                $update=array('paid_amount'=>$params['amount'],'has_paid'=>'Y');
                  
                $fc_payment=FCPayments::where('payment_type','due_penalty')
                                ->where('id',$params['penalty_id'])->update($update); 

            Log::info('PaymentsRepository:storePenalty::Penalty payment details inserting end');                              
            }
            DB::commit(); 
            $res=true;
            Log::info('PaymentsRepository: storePenalty::End');
        }
        catch(QueryException $e){
            DB::rollBack();
            Log::error('PaymentsRepository: storePenalty::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }
    //-------------------------------------------------------------------------

}