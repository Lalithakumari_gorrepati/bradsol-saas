<?php
namespace App\Repository;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\QueryException;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Helpers;
use App\User;
use Auth;
use DataTables;

    /**
     * ------------------------------------------------------------------------
     *   RoleRepository class
     * ------------------------------------------------------------------------
     * Class having methods to handle requests,
     * details. This class consists of
     * methods to show request form, show request details. 
     *
     *
     * @package  App\Http\Controllers
     * @version  7.0
     * @author   BRAD Contech Solutions Pvt Ltd.
     */

class RoleRepository{

	/**
     *  To store the role  
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

	public function storeRole($data)
	{
		try
		{
			Log::info('Role Repo:storeRole::Start');
			$roleID = '';
			DB::beginTransaction();
			 
          	$roleData = array('name' => $data['name'],
                        'description' => $data['description'],
                        'account_id' => $data['account'],
                        'created_by' =>  '1',
                        'is_deleted' => 'N'
                        );
          	$role = Role::Create($roleData); 
            if(!empty($data['Permissions'])){
              	foreach ($data['Permissions'] as $value) 
              	{
                	$Permissions = Permission::where('name',$value)->first();
                 	$role->givePermissionTo($Permissions);
              	}
            }
             
			DB::commit();
			Log::info('Role Repo:storeRole::End');
			return $role;
		}
		catch(QueryException $e)
		{
			DB::rollBack();
			Log::error('Role Repo:storeRole::Error'.$e->getMessage());
			throw $e;
		}
	}
	//------------------------------------------------------------------------------


	/**
     *  To update role  
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function updateRole($data)
    {
        try{
			Log::info('Role Repo: Update role::Start');
			$roleID = '';
			DB::beginTransaction();
	    	
         	$id = $data['id']; 
         	  
            $role['name'] = $data['name'];
            $role['description'] = $data['description'];
            $role['account_id'] = $data['account_id'];
            $is_saved = Role::where('id',$data['id'])->update($role);
            $stored_permissions = DB::table('role_has_permissions')->where('role_id',$data['id'])->pluck('permission_id');
            $role_ids = Role::where('id',$id)->first();
            $permission = isset($data['Permission']) ? $data['Permission'] : [];
            
            if(!empty($stored_permissions)){
                foreach ($stored_permissions as $key => $value) {
                    if(!in_array($value, $permission)){
                       $revoke =  DB::table('role_has_permissions')->where(['permission_id' => $value, 'role_id' => $data['id'] ])->first();  
                       $role_ids->revokePermissionTo($revoke);
                    }
                }
            }
            if(!empty($permission)){
                foreach ($permission as $key => $value) {
                  
                    if(!in_array($value, (array)$stored_permissions)){
                        $role_ids->givePermissionTo($value);
                    }
                }
            }

            $array1 = json_decode(json_encode($stored_permissions), true);
            $array2 = json_decode(json_encode($permission), true);
            $result1=array_diff($array1,$array2);
            $result2=array_diff($array2,$array1);
            
            
            DB::commit();
            Log::info('Role Repo: Update Role::End');

         	return $is_saved;
		}
		catch(QueryException $e){
			DB::rollBack();
			Log::error('Update Repo: Update Role::Error'.$e->getMessage());
			throw $e;
		}
	}
	//------------------------------------------------------------------------------

	/**
     *  To view the role list  
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

	public function getRoleList()
    {   
    	try{
    		Log::info('Role Repo: Get role list::Start');
    		$data=Role::get();
    		Log::info('Role Repo: Get role list::End');
			return $data;
		}
		catch(QueryException $e){
			Log::error('Role Repo: Get role list::Error'.$e->getMessage());
			throw $e;
		}
    }
    //------------------------------------------------------------------------------

    /**
     *  To view the User list  
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function roleDataTable()
    {   
        try{
            Log::info('Role Repo: Get user list::Start');
              
                $role_name=Auth::user()->roles->first()->name;
                $currentUserAccountId = Helpers::getCurrentUserAccountId();
                if ($role_name=='SuperAdmin') {
                    $data=Role::get();
                }
                else{
                    $data=Role::where('name','!=','SuperAdmin')->where('account_id', $currentUserAccountId)->get();
                }
              $list = DataTables::of($data)->addColumn('action', function($data){
              	$button ='';
                if(auth()->user()->hasPermissionTo('Edit Roles'))      
                {
              	$button = "<a href='".url("role/edit/".$data->id)."' class='text-primary mr-2'><i class='nav-icon i-Pen-2 font-weight-bold' data-toggle='tooltip' title='Edit Role' id='.$data->id.'></i></a>";
                }
                if(auth()->user()->hasPermissionTo('View Roles'))      
                {
              	$button .= "<a href='".url("role/view/".$data->id)."' class='text-primary mr-2'><i class='fa fa-eye'  id='".$data->id."' data-toggle='tooltip' title='View Role'></i></a>";
              
                }
              	return $button;
              })
              ->rawColumns(['action'])
              ->make(true);
            Log::info('Role Repo: Get user list::End');
            return $list;
        }
        catch(QueryException $e){
            Log::error('Role Repo: Get user list::Error'.$e->getMessage());
            throw $e;
        }
    }
    //------------------------------------------------------------------------------

    /**
     *  To edit the role list  
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

	public function editRole($id)
    {   
    	try{
    		Log::info('Role Repo: Get role list::Start');
    		DB::beginTransaction();
    		$data['role'] = Role::where('id',$id)->first();

	        $data['Permissions'] = DB::table('permissions')->get();

	        $stored_permissions = DB::table('role_has_permissions')->where('role_id',$id)->get();
	        $stored_permission=[];
	        foreach($stored_permissions as $p)
	        {
	          $stored_permission[]=$p->permission_id;
	        }
	        $data['stored_permission'] = $stored_permission;

    		DB::commit();
			Log::info('Role Repo: Get role list::End');
			return $data;
		}
		catch(QueryException $e){
			DB::rollBack();
			Log::error('Role Repo: Get role list::Error'.$e->getMessage());
			throw $e;
		}
    }
    //------------------------------------------------------------------------------


    /**
     *  To view the role   
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function viewRole($id)
    {   
        try{
            Log::info('Role Repo: View role::Start');

            $data['role'] = Role::leftJoin('accounts', 'roles.account_id', '=', 'accounts.id')->where('roles.id',$id)->select('roles.*','roles.name as role_name','accounts.account_name')->first();
            
            $data['Permissions'] = DB::table('permissions')->get();

            $stored_permissions = DB::table('role_has_permissions')->where('role_id',$id)->get();
            $stored_permission=[];
            foreach($stored_permissions as $p)
            {
              $stored_permission[]=$p->permission_id;
            }
            $data['stored_permission'] = $stored_permission;

            Log::info('Role Repo: View role::End');
            return $data;
        }
        catch(QueryException $e){
            Log::error('Role Repo: View role::Error'.$e->getMessage());
            throw $e;
        }
    }
//-----------------------------------------------------------------------------


    /**
     *  To view the role list  
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

	public function createRoleForm()
    {   
    	try
    	{
    		Log::info('Role Repo: Get role list::Start');
    		$Permissions=Permission::get();
    		Log::info('Role Repo: Get role list::End');
			return $Permissions;
		}
		catch(QueryException $e)
		{
			Log::error('Role Repo: Get role list::Error'.$e->getMessage());
			throw $e;
		}
    }
    //------------------------------------------------------------------------------

    /**
     *  To return all permissions list based on screen
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getPermissions()
    {   
        try
        {
            $permissions=array();
            Log::info('Role Repo: getPermissions::Start');
              $permissions['dashboard'] = Permission::where('screen','dashboard')->get();
              $permissions['userRole'] = Permission::where('screen','user and role')->get();
              $permissions['accountPermissions'] = Permission::where('screen','accounts management')->get();
              $permissions['corpusPermissions'] = Permission::where('screen','corpus fund')->get();
              $permissions['FCPermissions'] = Permission::where('screen','FC Setting')->get();
              $permissions['memberPermissions'] = Permission::where('screen','member management')->get();
              $permissions['PaymentsPermissions'] = Permission::where('screen','payments management')->get();
              $permissions['ReportsPermissions'] = Permission::where('screen','reports')->get();
              $permissions['memberDashboard'] = Permission::where('screen','member dashboard')->get();
            Log::info('Role Repo: getPermissions::End');
            return $permissions;
        }
        catch(QueryException $e)
        {
            Log::error('Role Repo: getPermissions::Error'.$e->getMessage());
            throw $e;
        }
    }
    //------------------------------------------------------------------------------


    /**
     *  To view the role list  
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

	public function deleteRole($id)
    {   
    	try{
    		Log::info('Role Repo: delete role::Start');
    		DB::beginTransaction();

            $role = Role::find($id);
            $role->is_deleted = 'Y';
            $role->updated_by = 'admin';
            $role->save();

            $role_id = $role->id;

            $user = User::where('role',$role_id)->get();
            foreach ($user as $value)
            {
                $value->status ='InActive';
                $value->updated_by = 'admin';
                $value->save();
            }
            DB::commit();
            Log::info('Role Repo: delete role::End');
            return true;
		}
		catch(QueryException $e){
			Log::error('Role Repo: delete role::Error'.$e->getMessage());
			throw $e;
		}
    }
    //------------------------------------------------------------------------------
    
    
    /**
     *  To get permssions list
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */
    
    public function getAccountPermssion($id)
    {
        try{
            Log::info('Role Repo: getAccountPermssion::Start');
            $data=array();
            DB::beginTransaction();            
            
            $data = DB::table('permissions')->where('account_id',$id)->where('is_deleted', 'N')->get();
            
            /* $stored_permissions = DB::table('role_has_permissions')->where('role_id',$id)->get();
            $stored_permission=[];
            foreach($stored_permissions as $p)
            {
                $stored_permission[]=$p->permission_id;
            }
            $data['stored_permission'] = $stored_permission; */
            
            DB::commit();
            Log::info('Role Repo: getAccountPermssion::End');
            return $data;
        }
        catch(QueryException $e){
            DB::rollBack();
            Log::error('Role Repo: getAccountPermssion::Error'.$e->getMessage());
            throw $e;
        }
    }
    //------------------------------------------------------------------------------
    
    
    /**
     *  To get permssions list of Edit role
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */
    
    public function getEdiRolePermssion($id)
    {
        try{
            Log::info('Role Repo: getAccountPermssion::Start');
            $data=array();
            DB::beginTransaction();
            
             $stored_permissions = DB::table('role_has_permissions')->where('role_id',$id)->get();
             $stored_permission=[];
             foreach($stored_permissions as $p)
             {
             $stored_permission[]=$p->permission_id;
             }
             $data = $stored_permission; 
            
            DB::commit();
            Log::info('Role Repo: getAccountPermssion::End');
            return $data;
        }
        catch(QueryException $e){
            DB::rollBack();
            Log::error('Role Repo: getAccountPermssion::Error'.$e->getMessage());
            throw $e;
        }
    }
    //------------------------------------------------------------------------------
}