<?php
namespace App\Repository;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\QueryException;
use Auth;
use DataTables;
use Session;
use Carbon\Carbon;
use App\Models\MembershipDetails;
use App\User;
class ReportsRepository{

    /**
     *  Return Total FC's released data
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getTotalFcReleasedData($params)
    {             
        try{
            Log::info('ReportsRepository: getTotalFcReleasedData::Start');

                $search = !empty($params['search']['value'])?$params['search']['value']:'';  
                $pageSize = !empty($params['length']) ? $params['length'] : 10;
                $start = isset($params['start']) ? $params['start'] : 0;
                $count_filter = 0;
                $data  = array();
                $account_id=Session::get('account_id');
                $role_name=Auth::user()->roles->first()->name;
                $query =MembershipDetails::leftjoin('users','users.id','=','membership_details.user_id')
                  ->leftjoin('fc_payments','fc_payments.payment_against_member','=','membership_details.user_id')
                  ->leftjoin('user_accounts','user_accounts.user_id','=','membership_details.user_id')
                  ->leftjoin('user_profiles','user_profiles.user_id','=','membership_details.user_id')
                  ->select('users.*','user_profiles.member_id','membership_details.*',
                    DB::raw('(SELECT COUNT(*) FROM fc_payments WHERE fc_payments.payment_against_member = users.id AND fc_payments.payment_type ="fc_amount") AS total_members'),
                    DB::raw('(SELECT COUNT(*) FROM fc_payments WHERE fc_payments.payment_against_member = users.id AND fc_payments.payment_type ="fc_amount" AND fc_payments.has_paid="N") AS pending_payments'),
                    DB::raw('DATE_FORMAT(membership_details.updated_at, "%d-%m-%Y") as updated_time'),
                    DB::raw('DATE_FORMAT(membership_details.confirm_date, "%d-%m-%Y") as confirm_date'),
                  DB::raw('(SELECT COUNT(*) FROM fc_payments WHERE fc_payments.payment_against_member = users.id AND fc_payments.payment_type ="fc_amount" AND fc_payments.has_paid="Y") AS received_payments'))
                ->where('status','Expired')
                ->distinct('users.id');
                 
                if (!empty($account_id) && $role_name!='SuperAdmin') {
                    $query->where('user_accounts.account_id',$account_id);
                }
                if(!empty($params['from']) && !empty($params['to']))  {
                    $from = trim($params['from']);
                    $to = trim($params['to']);
                    $from = date('Y-m-d', strtotime($from));
                    $to = date('Y-m-d', strtotime($to));
                
                    $query->where(function ($q) use ($from,$to) {
                        $q->where('membership_details.confirm_date', '>=', $from.' 00:00:00');
                        $q->where('membership_details.confirm_date', '<=', $to.' 23:59:59');
                    });
                }
                
                if(!empty($params['register_id'])) {
                    $query->where('user_profiles.member_id', 'LIKE', '%'.$params['register_id'].'%');
                }
                if(!empty($params['expired_id'])) {
                    $query->where('membership_details.user_id',$params['expired_id']);
                }
                
                if(!empty($params['member_phone'])) {
                    $query->where('users.mobile_number','LIKE', '%'.$params['member_phone'].'%');
                }
                if(!empty($params['member_email'])) {
                    $query->where('users.email','LIKE', '%'.$params['member_email'].'%');
                }

                $count_total = $query->count();
                if($search != ''){
                    $query->Where(function ($query) use ($search) {
                        $query->where('users.name','LIKE' , '%'.$search.'%')
                        ->orWhere('users.email','LIKE' , '%'.$search.'%')
                        ->orWhere('users.mobile_number','LIKE' , '%'.$search.'%')
                        ->orWhere('user_profiles.member_id','LIKE' , '%'.$search.'%')
                        ->orWhere('membership_details.fc_amount','LIKE' , '%'.$search.'%')
                        ->orWhere('fc_payments.payment_type','LIKE' , '%'.$search.'%');
                    });
                    $count_filter = $query->count();
                }
                $query->orderBy('membership_details.updated_at','DESC')->skip($start)->take($pageSize);
                if($count_filter == 0 && $search == ''){
                    $count_filter = $count_total;
                }
                $qData=$query->get();
                
                $data = DataTables::of($qData)->addColumn('action', function($data){
                    $button ='';
                    if(auth()->user()->hasPermissionTo('View Payment'))      
                    {
                        if ($data->total_fc>0 ) {
                            $button .="<a class='btn mr-2 mb-1 btn-rounded btn-primary viewPayment' data-value='' href='".url('/')."/payments/pending/view/".$data->user_id."'>View</a>";
                        }
                    }
                    return $button;
                })->with([
                    "recordsTotal" => $count_total,
                    "recordsFiltered" => $count_filter,
                ])
                ->skipPaging()
                ->rawColumns(['action'])
                ->make(true);  
                Log::info('ReportsRepository: getTotalFcReleasedData::End');
                return $data;
            }
            catch(QueryException $e){
                Log::error('ReportsRepository: getTotalFcReleasedData::Error'.$e->getMessage());
                throw $e;
            }
    }
    //-------------------------------------------------------------------------


    /**
     *  Return Total Advance payments data
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getTotalAdvancePaymentsData($params)
    {             
        try{
            Log::info('ReportsRepository: getTotalAdvancePaymentsData::Start');

                $search = !empty($params['search']['value'])?$params['search']['value']:'';  
                $pageSize = !empty($params['length']) ? $params['length'] : 10;
                $start = isset($params['start']) ? $params['start'] : 0;
                $count_filter = 0;
                $data  = array();
                $account_id=Session::get('account_id'); 
                $role_name=Auth::user()->roles->first()->name;
                $query =  User::leftJoin('payments', 'users.id', '=', 'payments.member_id')
                ->leftJoin('payment_details','payments.id','=','payment_details.payment_id')
                ->leftJoin('user_profiles','users.id','=','user_profiles.user_id')
                ->leftJoin('membership_details', 'users.id', '=', 'membership_details.user_id')
                ->leftJoin('roles', 'users.role', '=', 'roles.id')
                ->leftJoin('user_accounts', 'users.id', '=', 'user_accounts.user_id')
                ->select('users.*','user_profiles.member_id','membership_details.status','membership_details.join_date','payments.amount','payments.payment_date',
                    DB::raw('DATE_FORMAT(payment_date, "%d-%m-%Y") as payment_date'),
                    DB::raw('DATE_FORMAT(join_date, "%d-%m-%Y") as join_date'),
                    DB::raw('DATE_FORMAT(users.created_at,"%d-%m-%Y") as created_date'))
                ->selectRaw("(TIMESTAMPDIFF(YEAR, DATE(users.dob), CURRENT_DATE)) AS age")
                ->where('roles.name','Member')
                ->where('payment_details.payment_type','advance');
                
                if(!empty($params['from']) && !empty($params['to']))  {
                    $from = trim($params['from']);
                    $to = trim($params['to']);
                    $from = date('Y-m-d', strtotime($from));
                    $to = date('Y-m-d', strtotime($to));
                
                    $query->where(function ($q) use ($from,$to) {
                        $q->where('users.created_at', '>=', $from.' 00:00:00');
                        $q->where('users.created_at', '<=', $to.' 23:59:59');
                    });
                }
                if(!empty($params['age_from']) && !empty($params['age_to']))  {
                    $from = trim($params['age_from']);
                    $to = trim($params['age_to']);
                
                    $query->where(function ($q) use ($from,$to) {
                        $q->where('membership_details.age', '>=', $from);
                        $q->where('membership_details.age', '<=', $to);
                    });
                }
                if(!empty($params['register_id'])) {
                    $query->where('user_profiles.member_id', 'LIKE', '%'.$params['register_id'].'%');
                }
                if(!empty($params['member_phone'])) {
                    $query->where('users.mobile_number',$params['member_phone']);
                }

                if(!empty($params['member_email'])) {
                    $query->where('users.email',$params['member_email']);
                }

                if(!empty($params['member_name'])) {
                    $query->where('users.name','LIKE', '%'.$params['member_name'].'%');
                }
                if (!empty($account_id) && $role_name!='SuperAdmin') {
                    $query->where('user_accounts.account_id',$account_id);
                }
                $count_total = $query->count();
                if($search != ''){
                    $query->Where(function ($query) use ($search) {
                        $query->orWhere('users.name','LIKE' ,'%'.$search.'%')
                        ->orWhere('users.email','LIKE' , '%'.$search.'%')
                        ->orWhere('user_profiles.member_id','LIKE' , '%'.$search.'%')
                        ->orWhere('payments.amount','LIKE' , '%'.$search.'%')
                        ->orWhere('membership_details.age','LIKE' , '%'.$search.'%')
                        ->orWhere('users.mobile_number', 'LIKE' , '%'.$search.'%');
                    });
                    $count_filter = $query->count();
                }
                $query->skip($start)->take($pageSize);
                if($count_filter == 0 && $search == ''){
                    $count_filter = $count_total;
                }
                $qData=$query->orderBy('users.created_at','desc')->get();
                
                $data = DataTables::of($qData)
                ->with([
                    "recordsTotal" => $count_total,
                    "recordsFiltered" => $count_filter,
                ])
                ->skipPaging()
                ->make(true);  
                
                Log::info('ReportsRepository: getTotalAdvancePaymentsData::End');
                return $data;
            }
            catch(QueryException $e){
                Log::error('ReportsRepository: getTotalAdvancePaymentsData::Error'.$e->getMessage());
                throw $e;
            }
    }
    //-------------------------------------------------------------------------


    /**
     *  Return Total Corpus Funds Data
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getTotalCorpusFundsData($params)
    {             
        try{
            Log::info('ReportsRepository: getTotalCorpusFundsData::Start');

                $search = !empty($params['search']['value'])?$params['search']['value']:'';  
                $pageSize = !empty($params['length']) ? $params['length'] : 10;
                $start = isset($params['start']) ? $params['start'] : 0;
                $count_filter = 0;
                $data  = array();
                $account_id=Session::get('account_id'); 
                $role_name=Auth::user()->roles->first()->name;
                $query =  User::leftJoin('payments', 'users.id', '=', 'payments.member_id')
                ->leftJoin('payment_details','payments.id','=','payment_details.payment_id')
                ->leftJoin('user_profiles','users.id','=','user_profiles.user_id')
                ->leftJoin('membership_details', 'users.id', '=', 'membership_details.user_id')
                ->leftJoin('roles', 'users.role', '=', 'roles.id')
                ->leftJoin('user_accounts', 'users.id', '=', 'user_accounts.user_id')
                ->select('users.*','user_profiles.member_id','membership_details.status','membership_details.join_date','membership_details.age','payments.amount','payments.payment_date',
                    DB::raw('DATE_FORMAT(payment_date, "%d-%m-%Y") as payment_date'),
                    DB::raw('DATE_FORMAT(join_date, "%d-%m-%Y") as join_date'),
                    DB::raw('DATE_FORMAT(users.created_at,"%d-%m-%Y") as created_date'))
                ->where('roles.name','Member')
                ->where('payment_details.payment_type','corpus_amount');
                
                if(!empty($params['from']) && !empty($params['to']))  {
                    $from = trim($params['from']);
                    $to = trim($params['to']);
                    $from = date('Y-m-d', strtotime($from));
                    $to = date('Y-m-d', strtotime($to));
                
                    $query->where(function ($q) use ($from,$to) {
                        $q->where('users.created_at', '>=', $from.' 00:00:00');
                        $q->where('users.created_at', '<=', $to.' 23:59:59');
                    });
                }
                if(!empty($params['age_from']) && !empty($params['age_to']))  {
                    $from = trim($params['age_from']);
                    $to = trim($params['age_to']);
                
                    $query->where(function ($q) use ($from,$to) {
                        $q->where('membership_details.age', '>=', $from);
                        $q->where('membership_details.age', '<=', $to);
                    });
                }
                if(!empty($params['register_id'])) {
                    $query->where('user_profiles.member_id', 'LIKE', '%'.$params['register_id'].'%');
                }
                if(!empty($params['member_phone'])) {
                    $query->where('users.mobile_number',$params['member_phone']);
                }

                if(!empty($params['member_email'])) {
                    $query->where('users.email',$params['member_email']);
                }

                if(!empty($params['member_name'])) {
                    $query->where('users.name','LIKE', '%'.$params['member_name'].'%');
                }
                if (!empty($account_id) && $role_name!='SuperAdmin') {
                    $query->where('user_accounts.account_id',$account_id);
                }
                $count_total = $query->count();
                if($search != ''){
                    $query->Where(function ($query) use ($search) {
                        $query->orWhere('users.name','LIKE' ,'%'.$search.'%')
                        ->orWhere('users.email','LIKE' , '%'.$search.'%')
                        ->orWhere('user_profiles.member_id','LIKE' , '%'.$search.'%')
                        ->orWhere('payments.amount','LIKE' , '%'.$search.'%')
                        ->orWhere('payments.payment_date','LIKE' , '%'.$search.'%')
                        ->orWhere('membership_details.join_date','LIKE' , '%'.$search.'%')
                        ->orWhere('membership_details.age','LIKE' , '%'.$search.'%')
                        ->orWhere('users.mobile_number', 'LIKE' , '%'.$search.'%');
                    });
                    $count_filter = $query->count();
                }
                $query->skip($start)->take($pageSize);
                if($count_filter == 0 && $search == ''){
                    $count_filter = $count_total;
                }
                $qData=$query->orderBy('users.created_at','desc')->get();
                
                $data = DataTables::of($qData)->with([
                    "recordsTotal" => $count_total,
                    "recordsFiltered" => $count_filter,
                ])
                ->skipPaging()
                ->make(true);  
                
                Log::info('ReportsRepository: getTotalCorpusFundsData::End');
                return $data;
            }
            catch(QueryException $e){
                Log::error('ReportsRepository: getTotalCorpusFundsData::Error'.$e->getMessage());
                throw $e;
            }
    }
    //-------------------------------------------------------------------------
}