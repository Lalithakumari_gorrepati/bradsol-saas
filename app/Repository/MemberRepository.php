<?php
namespace App\Repository;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\QueryException;
use App\User;
use App\Models\UserAccount;
use App\Models\Account;
use App\Models\UserProfile;
use App\Models\Nominee;
use App\Models\Payment;
use App\Models\FCPayments;
use App\Models\PaymentDetails;
use App\Models\MembershipDetails;
use App\Models\AccidentalInsurance;
use Auth;
use DataTables;
use App\Http\Helper\Utilities;
use Spatie\Permission\Models\Role;
use App\Models\Countries;
use Carbon\Carbon;
use App\Http\Helper\StringConstants;
use Session;
use App\Repository\PaymentsRepository;
use App\Http\Helper\EmailUtilities;
use App\Http\Helper\SmsUtilities;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;

    /**
     * ------------------------------------------------------------------------
     *   MemberRepository class
     * ------------------------------------------------------------------------
     * Class having methods to handle requests,
     * details. This class consists of
     * methods to show request form, show request details. 
     *
     *
     * @package  App\Http\Controllers
     * @version  7.0
     * @author   BRAD Contech Solutions Pvt Ltd.
     */

class MemberRepository{
    private $paymentRepo;
    public function __construct(PaymentsRepository $paymentRepo){
        $this->paymentRepo = $paymentRepo;
    }


    /**
     *  Return all members data
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getMembersData($params)
    {             
        try{
            Log::info('Member Repo: return members data::Start');
                $search = !empty($params['search']['value'])?$params['search']['value']:'';  
                $pageSize = !empty($params['length']) ? $params['length'] : 10;
                $start = isset($params['start']) ? $params['start'] : 0;
                $count_filter = 0;
                $data  = array();
                $account_id=Session::get('account_id');
                $role_name=Auth::user()->roles->first()->name;
                $query =  User::leftJoin('payments', 'users.id', '=', 'payments.member_id')
                ->leftJoin('payment_details','payments.id','=','payment_details.payment_id')
                ->leftJoin('user_profiles','users.id','=','user_profiles.user_id')
                ->leftJoin('membership_details', 'users.id', '=', 'membership_details.user_id')
                ->leftJoin('roles', 'users.role', '=', 'roles.id')
                ->leftJoin('user_accounts', 'users.id', '=', 'user_accounts.user_id')
                ->select('users.*','user_profiles.member_id','membership_details.status','user_accounts.account_id','membership_details.join_date','membership_details.status as memeber_status','payments.amount','payments.payment_date',
                    DB::raw('DATE_FORMAT(payment_date, "%d-%m-%Y") as payment_date'),
                    DB::raw('DATE_FORMAT(users.created_at, "%d-%m-%Y") as created_date'),
                    DB::raw('DATE_FORMAT(join_date, "%d-%m-%Y") as join_date'))
                ->selectRaw("(TIMESTAMPDIFF(YEAR, DATE(users.dob), CURRENT_DATE)) AS age")
                ->where('roles.name','Member')
                ->where('payment_details.payment_type','corpus_amount');

                if(!empty($params['from']) && !empty($params['to']))  {
                    $from = trim($params['from']);
                    $to = trim($params['to']);
                    $from = date('Y-m-d', strtotime($from));
                    $to = date('Y-m-d', strtotime($to));
                
                    $query->where(function ($q) use ($from,$to) {
                        $q->where('users.created_at', '>=', $from.' 00:00:00');
                        $q->where('users.created_at', '<=', $to.' 23:59:59');
                    });
                }
                if (!empty($account_id) && $role_name!='SuperAdmin') {
                    $query->where('user_accounts.account_id',$account_id);
                }
                if(!empty($params['age_from']) && !empty($params['age_to']))  {
                    $from = trim($params['age_from']);
                    $to = trim($params['age_to']);
                
                    $query->where(function ($q) use ($from,$to) {
                        $q->where('membership_details.age', '>=', $from);
                        $q->where('membership_details.age', '<=', $to);
                    });
                }
                if(!empty($params['register_id'])) {
                    $query->where('user_profiles.member_id', 'LIKE', '%'.$params['register_id'].'%');
                }
                if(!empty($params['member_phone'])) {
                    $query->where('users.mobile_number',$params['member_phone']);
                }
                if(!empty($params['member_status'])) {
                    $query->where('membership_details.status',$params['member_status']);
                }
                if(!empty($params['member_email'])) {
                    $query->where('users.email',$params['member_email']);
                }
                if(!empty($params['member_name'])) {
                    $query->where('users.name','LIKE', '%'.$params['member_name'].'%');
                }

                $count_total = $query->count();
                if($search != ''){
                    $query->Where(function ($query) use ($search) {
                        $query->orWhere('users.name','LIKE' ,'%'.$search.'%')
                        ->orWhere('users.email','LIKE' , '%'.$search.'%')
                        ->orWhere('user_profiles.member_id','LIKE' , '%'.$search.'%')
                        ->orWhere('membership_details.status','LIKE' , '%'.$search.'%')
                        ->orWhere('payments.amount','LIKE' , '%'.$search.'%')
                        ->orWhere('payments.payment_date','LIKE' , '%'.$search.'%')
                        ->orWhere('membership_details.join_date','LIKE' , '%'.$search.'%')
                        ->orWhere('users.mobile_number', 'LIKE' , '%'.$search.'%');
                    });
                    $count_filter = $query->count();
                }
                $query->orderBy('created_at','desc')->skip($start)->take($pageSize);
                if($count_filter == 0 && $search == ''){
                    $count_filter = $count_total;
                }
                $qData=$query->get();
                $uniqueData=[];
                $uniqueIds=[];
                foreach ($qData as $key => $value) {
                    if(!in_array($value->member_id,$uniqueIds)){
                        array_push($uniqueData, $value);
                        array_push($uniqueIds, $value->member_id);
                    }
                }
                
                $data = DataTables::of($uniqueData)->addColumn('action', function($data){
                    $button ='';
                    if(auth()->user()->hasPermissionTo('Edit Member'))      
                    { 
                        $button = '<a href="'.url('/')."/member/edit/".$data->id.'" class="text-primary mr-2"><i class="nav-icon i-Pen-2 font-weight-bold" data-toggle="tooltip" title="Edit Member" id="'.$data->id.'"></i></a>';
                    }
                    if(auth()->user()->hasPermissionTo('View Member'))      
                    {
                        $button .= '<a href="'.url('/')."/member/view/".$data->id.'"class="text-primary"><i class="fa fa-eye"  data-toggle="tooltip" title="View Member" id="'.$data->id.'"></i></a>';
                    }
                    if(auth()->user()->hasPermissionTo('Change Member Status'))
                    {
                        if ($data->status !='Expired') {
                            
                        $button .= '<button class="btn mr-2 btn-primary btn-sm change_status_new" data-id="'.$data->id.'" data-status="'.$data->status.'" data-joining_date="'.$data->join_date.'">Status</button>';
                        
                        }
                    }
                    return $button;
                })
                ->with([
                    "recordsTotal" => $count_total,
                    "recordsFiltered" => $count_filter,
                ])
                ->skipPaging()
                ->rawColumns(['action'])
                ->make(true);  
                Log::info('Member Repo: return members data::End');
                return $data;
                
                
            }
            catch(QueryException $e){
                Log::error('Member Repo: return members data::Error'.$e->getMessage());
                throw $e;
            }
    }
    //-------------------------------------------------------------------------

    /**
     *  To store the member details  
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function storeMember($request){
        $res=false;
        try{
            Log::info('Member Repo: Create Member::Start');
            $data=$request->all();
            $user_id=Auth::id();
            $account_id = Session::get('account_id');
            
            DB::beginTransaction();
            $role=Role::where('name','Member')->first();
            
            if (!empty($data)&& !empty($role)) {
                $member_id=strtoupper($data['member_id']);

                Log::info('Member Repo: To store member details::Member data preparing started - '.$member_id);
                if (!empty($data['dob'])) {
                    $dob=date("Y-m-d", strtotime($data['dob']));
                } 

                $user_data =array( 
                'name'           => !empty($data['name'])?$data['name']:'',
                'email'          => !empty($data['email'])?$data['email']:'',
                'role'           => $role->id,
                'mobile_number'  => !empty($data['mobile_number'])?$data['mobile_number']:'',
                'gender'         => !empty($data['gender'])?$data['gender']:'',
                'dob'            => !empty($dob)?$dob:'',
                'password'       => \Hash::make($data['password']),
                'is_active'      => 'Y',
                'additional_info'=> !empty($data['additional_info'])?$data['additional_info']:'',
                'created_at'     => date('Y-m-d h:i:s'),
                'created_by'     => Auth::user()->email,
            );
            Log::info('Member Repo: To store member details::Member data insering started - '.$member_id);
            $newuser=User::create($user_data);  

            $AssignRole=$newuser->assignRole($role);
            
            Log::info('Member Repo: To store member details::Member data insering end - '.$member_id);
            if (!empty($newuser)) {
                 $res=$newuser->id;
                Log::info('Member Repo: To store member details::Member account insering started - '.$member_id);
                    $account_data=[];
                 
                    $account_data=array( 
                        'user_id'     => $newuser->id,
                        'account_id'  => $account_id,
                        'created_at'  => date('Y-m-d h:i:s'),
                        'created_by'  => Auth::user()->email,
                    );
                    
                    $account=UserAccount::insert($account_data);
                Log::info('Member Repo: To store member details::Member account insering end - '.$member_id);    
                
                Log::info('Member Repo: To store member details::Member profile details insering started - '.$member_id);
                    Log::info('Member Repo: To store member details::member id proofs upload start');
                    $customer_id_type = array();

                    if (!empty($request->file('customer_id_front')) || !empty($request->file('customer_id_back'))) {
                    
                        $customer_id_type['customer_id_type'] = !empty($data['customer_id_type'])?$data['customer_id_type']:'';
                        $front_image = $backside_image = '';
                        if(!empty($request->file('customer_id_front'))){
                            $file_front = $request->file('customer_id_front');
                            
                            $front_image = time() . '_' .$file_front->getClientOriginalName();

                            $file_front->move(public_path('uploads'),$front_image);
                        }
                        
                        if(!empty($request->file('customer_id_back'))){
                            $file_back = $request->file('customer_id_back');
                            $backside_image = time() . '_' .$file_back->getClientOriginalName();
                            $file_back->move(public_path('uploads'), $backside_image);
                        }
                        $customer_id_type['customer_id_front'] = !empty($front_image)?$front_image:'';
                        $customer_id_type['customer_id_back'] = !empty($backside_image)?$backside_image:'';
                    }

                    if($request->hasFile('profile_photo')){
                        $photo_file = $request->file('profile_photo');
                        
                        $photo_name = time().'_'.$photo_file->getClientOriginalName();
                        
                        $photo_file->move(public_path('uploads'), $photo_name);
                        $data['profile_photo'] = $photo_name;
                    }
                    else{
                        $data['profile_photo']='';
                    }

                    if($request->hasFile('age_proof')){
                        $age_proof_file = $request->file('age_proof');
                        
                        $file_name = time().'_'.$age_proof_file->getClientOriginalName();
                        
                        $age_proof_file->move(public_path('uploads'), $file_name);
                        $data['age_proof'] = $file_name;
                    }
                    else{
                        $data['age_proof']='';
                    }

                    //Member Medical reports
                    $filenames=[];
                    
                    if(!empty($data['reports']))
                    {    
                        foreach ($data['reports'] as $key => $value) {
                            if (!empty($value)) {
                                $name = time() .'_'. $value->getClientOriginalName();
                                $value->move(public_path('uploads'),$name);
                                $filenames[]=$name;
                            }
                        }
                        $data['reports']=json_encode($filenames);
                    }else{
                        $data['reports']='';
                    }

                    //Member Scanned application form
                    $application=[];
                    
                    if(!empty($data['application_form']))
                    {    
                        foreach ($data['application_form'] as $key => $value) {
                            if (!empty($value)) {
                                $name = time() .'_'. $value->getClientOriginalName();
                                $value->move(public_path('uploads'),$name);
                                $application[]=$name;
                            }
                        }
                        $data['application_form']=json_encode($application);
                    }else{
                        $data['application_form']='';
                    }
                    
                Log::info('Member Repo: To store member details::member id proofs upload end');
                    if ($data['health_declaration']=='on') {
                        $data['health_declaration']='Y';
                    }
                    else{
                         $data['health_declaration']='N';
                    }
                    $profile_data=[];
                    $profile_data=array( 
                        'user_id'     => $newuser->id,
                        'member_id'     => $member_id,
                        'alternative_email'  =>!empty( $data['alternative_email'])?$data['alternative_email']:'',
                        'alternative_phone'  =>!empty( $data['alternative_number'])?$data['alternative_number']:'',
                        'martial_status'  =>!empty( $data['marital_status'])?$data['marital_status']:'',
                        'country'  =>!empty( $data['country'])?$data['country']:'',
                        'state'  =>!empty( $data['state'])?$data['state']:'',
                        'city'  => !empty($data['city'])?$data['city']:'',
                        'id_proof'  => !empty($customer_id_type)?json_encode($customer_id_type):null,
                        'age_proof'  => !empty($data['age_proof'])?$data['age_proof']:'',
                        'physical_disability'  => !empty($data['physical_disability'])?$data['physical_disability']:'',
                        'physical_disability_type'  => !empty($data['physical_disability_type'])?$data['physical_disability_type']:'',
                        'previous_illness'  => $data['previous_illness'],
                        'illness_type'  => !empty($data['illness_type'])?$data['illness_type']:'',
                        'health_declaration'  => !empty($data['health_declaration'])?$data['health_declaration']:'',
                        'reports'  => !empty($data['reports'])?($data['reports']):null,
                        'scanned_application'=> !empty($data['application_form'])?($data['application_form']):null,
                        'address'  => !empty($data['member_address'])?$data['member_address']:'',
                        'interested_in_other_scheme'  => !empty($data['interested'])?$data['interested']:'',
                        'image'  => $data['profile_photo'],
                        'created_at'  => date('Y-m-d h:i:s'),
                        'created_by'  =>Auth::user()->email,
                    );
                
                    $profile=UserProfile::insert($profile_data);
                Log::info('Member Repo: To store member details::Member profile details insering end - '.$member_id);

                Log::info('Member Repo: To store member details::Member nominee details insering started - '.$member_id);

                    $nominee_data=[];
                    foreach ($data['NomineeDetails'] as $key => $value) {
                        $nominee_details=array();
                        if (!empty($value)) {
                            
                           Log::info('Member Repo:storeMember::Member nominee details insering started');
                                if (!empty($value['nominee_dob'])) {
                                    $nominee_dob=date("Y-m-d", strtotime($value['nominee_dob']));
                                }
                                
                                if(!empty($value['nominee_photo'])){
                                    $name = time() .'_'. $value['nominee_photo']->getClientOriginalName();
                                    
                                    $value['nominee_photo']->move(public_path('uploads'), $name);
                                    $nominee_photo = $name;
                                }
                                else{
                                    $nominee_photo='';
                                }
                                $nominee_details=array( 
                                    'member_id' => $newuser->id,
                                    'name'      => !empty($value['nominee_name'])?$value['nominee_name']:'',
                                    'fc_percentage'=> !empty($value['nominee_fcp'])?$value['nominee_fcp']:null,
                                    'email'     => !empty($value['nominee_email'])?$value['nominee_email']:'',
                                    'phone'     => !empty($value['nominee_phone'])?$value['nominee_phone']:'',
                                    'image'     => !empty($nominee_photo)?$nominee_photo:'',
                                    'address'   => !empty($value['nominee_address'])?$value['nominee_address']:'',
                                    'dob'       => !empty($nominee_dob)?$nominee_dob:'',
                                    'nominee_relationship'=> !empty($value['nominee_relation'])?$value['nominee_relation']:'',
                                    'created_at'  => date('Y-m-d h:i:s'),
                                    'created_by'  => Auth::user()->email,
                                );

                                if (!empty($nominee_details)) {
                                    array_push($nominee_data, $nominee_details);
                                }
                            Log::info('Member Repo:storeMember::Member nominee details insering ended');
                        }
                    }
                    
                    if (!empty($nominee_data)) {
                        $insert=Nominee::insert($nominee_data);
                        Log::info('Member Repo:storeMember::Nominee Data stored');
                        unset($nominee_data);
                        unset($nominee_details);
                    }
                Log::info('Member Repo: To store member details::Member nominee details insering end - '.$member_id);

                Log::info('Member Repo: To store member details::corpus payment details insering started - '.$member_id);
                    if (!empty($data['payment_date'])) {
                        $payment_date=date("Y-m-d", strtotime($data['payment_date']));
                    }
                    $payment_data=[];
                    $payment_data=array( 
                        'member_id'     => $newuser->id,
                        'account_id'    => $account_id,
                        'amount'        => $data['corpus_amount'],
                        'remarks'       => !empty($data['note'])?$data['note']:'',
                        'mode_of_payment'=> $data['payment_mode'],
                        'payment_date'  => !empty($payment_date)?$payment_date:null,
                        'created_at'  => date('Y-m-d h:i:s'),
                        'created_by'  => Auth::user()->email,
                    );
                
                    $payment_id=Payment::insertGetId($payment_data);
                    if($payment_id) {
                        $payment_details=[];
                       $payment_details=array( 
                        'payment_id'    => $payment_id,
                        'payment_against_member'=> null,
                        'payment_type'  =>'corpus_amount',
                        'created_at'  => date('Y-m-d h:i:s'),
                        'created_by'  => Auth::user()->email,
                        ); 
                        if (!empty($payment_details)) {
                            $insert=PaymentDetails::insert($payment_details);  
                        }                       
                    }

                Log::info('Member Repo: To store member details::corpus payment details insering end - '.$member_id);

                Log::info('Member Repo: To store member details::advance payment details insering started - '.$member_id);
                    if (!empty($data['advance_payment_date'])) {
                        $advance_payment_date=date("Y-m-d", strtotime($data['advance_payment_date']));
                    }
                    $advance_payment_data=[];
                    $advance_payment_data=array( 
                        'member_id'     => $newuser->id,
                        'account_id'    => $account_id,
                        'amount'        => $data['advance_amount'],
                        'remarks'       => '',
                        'mode_of_payment'=> $data['advance_payment_mode'],
                        'payment_date'  => !empty($advance_payment_date)?$advance_payment_date:null,
                        'created_at'  => date('Y-m-d h:i:s'),
                        'created_by'  => Auth::user()->email,
                    );
                
                    $advance_payment_id=Payment::insertGetId($advance_payment_data);
                    if($advance_payment_id) {
                        $advance_payment_details=[];
                       $advance_payment_details=array( 
                        'payment_id'    => $advance_payment_id,
                        'payment_against_member'=> null,
                        'payment_type'  =>'advance',
                        'created_at'  => date('Y-m-d h:i:s'),
                        'created_by'  => Auth::user()->email,
                        ); 
                        if (!empty($advance_payment_details)) {
                            $insert=PaymentDetails::insert($advance_payment_details);
                        }                       
                    }

                Log::info('Member Repo: To store member details::advance payment details insering end - '.$member_id);

                Log::info('Member Repo: To store member details::Membership details insering started - '.$member_id);
                    if (!empty($data['membership_date'])) {
                        $join_date=date("Y-m-d", strtotime($data['membership_date']));
                    }
                    $membership_details=[];
                    $membership_details=array( 
                        'user_id'       => $newuser->id,
                        'corpus_amount' => $data['corpus_amount'],
                        'join_date'     => !empty($join_date)?$join_date:null,
                        'age'           => $data['member_age'],
                        'status'        => $data['member_status'],
                        'created_at'    => date('Y-m-d h:i:s'),
                        'created_by'    => Auth::user()->email,
                    );
                    $membership=MembershipDetails::insert($membership_details);
                Log::info('Member Repo: To store member details::Membership details insering end - '.$member_id);

                $current_year=date('Y');
                $membership_year=date('Y',strtotime($data['membership_date']));

                if ($membership_year<$current_year) {
                    $age=Carbon::parse($dob)->diff($join_date)->format('%y');
                    $member_age=$age;
                }
                else{
                    $member_age=$data['member_age'];
                }

                if ($member_age<=30) {
                    $diff=30-$member_age;
                   
                    $insurance_details=[];
                    $insurance_data=[];
                    if ($diff==0) {
                        Log::info('Member Repo: To store member details::Insurance details inserting started - '.$member_id);
                            $insurance_details=array( 
                                'member_id'     => $newuser->id,
                                'year'          => $membership_year,
                                'status'        => 'N',
                                'payment_date'  => null,
                                'created_at'    => date('Y-m-d h:i:s'),
                                'created_by'    => Auth::user()->email,
                            );
                            $membership=AccidentalInsurance::insert($insurance_details);
                        Log::info('Member Repo: To store member details::Insurance details inserting end - '.$member_id);
                    }elseif($diff>0){

                        for ($i=0; $i <=$diff; $i++) {
                            $insurance_details=array( 
                                'member_id'     => $newuser->id,
                                'year'          => $membership_year + $i,
                                'status'        => 'N',
                                'payment_date'  => null,
                                'created_at'    => date('Y-m-d h:i:s'),
                                'created_by'    => Auth::user()->email,
                            );

                            if (!empty($insurance_details)) {
                                array_push($insurance_data, $insurance_details);
                            }
                        }
                        $membership=AccidentalInsurance::insert($insurance_data);
                        Log::info('Member Repo: To store member details::Insurance details inserting end - '.$member_id);
                    }
                }
            }
            
            DB::commit(); 
            Log::info('Member Repo: storeMember::Send Welcome Email -Start');
                $url=Config::get('app.url')."/login"; 
                $emailContent=[];
                $name=$data['name'];
                $sendTo=!empty($data['email'])?$data['email']:'';
                $emailContent['sendTo']=$sendTo;
                $emailContent['subject']='Welcome Email';
                $emailContent['body']='Dear '.$name.',<br>
                                      Jai Mahesh!!<br><br>
                                      We are glad to have you onboard with one of the flagship projects of Mahesh Foundation – Family Security Scheme (FSS). We also appreciate your foresightedness for picking up this scheme which has found to be very beneficial for family members in case of any untoward incident like death of the insured member.<br><br>
                                      We would like to highlight some of the key things that you as a FSS member should be aware of:<br>
                                      <ul><li>For any death during a financial year the premium amount is calculated based on the simple math of dividing the insured amount amongst the active members and raising the invoice accordingly.</li>
                                        <li>The invoice is raised twice in a year – one in the month of April and the other in the month of October.</li>
                                        <li>We expect the member to pay the premium amount via cheque only within the specified duration mentioned in the invoice.</li>
                                        <li>The insured amount is disbursed to the nominee within a week – 10 days post all legal formalities are cleared.<br><br></li></ul>
                                        For any concerns or clarifications or to know more about the scheme, you can always reach out to either the Secretary of Mahesh Foundation or the Mahesh Foundation Office during working hours, 9 AM to 5:30 PM from Monday - Friday. We have our office at Raghav Ratna Towers on the 7th floor.<br><br>
                                       You may also visit the FSS portal to access scheme updates and your transactions per below:<br><br>
                                      URL:&nbsp;'.$url.'  
                                      Username:&nbsp; '.$sendTo.' <br>
                                      Password:&nbsp; '.$data['password'].' <br><br>
                                      We would like to thank you once again and congratulate you on joining this very unique program and in a way also helping fellow community members in their difficult times. We also request you to recommend and refer any family members and friends to this program and help us help our community members avail of these benefits.<br><br>
                                      Thank You once again,<br>
                                      Team Mahesh Foundation';
                    
                if (!empty($emailContent) && !empty($sendTo)) {
                    $emailId=EmailUtilities::sendEmailNotification($emailContent,$sendTo);
                }
            Log::info('Member Repo: storeMember::Send Welcome Email -End');

            Log::info('Member Repo: storeMember::Send Welcome SMS -Start');

                $smsContent=[];
                $name=$data['name'];
                $sendTo=!empty($data['phone'])?$data['phone']:'';
                $smsContent['sendTo']=$sendTo;
                $smsContent['body']='Dear '.$name.' ji
                                      Jai Mahesh!!
                                      Mahesh Foundation welcomes you onboard the FSS scheme. Please do review your email and also the members portal for further details.';
                
                if (!empty($smsContent) && !empty($sendTo)) {
                    $smsId=SmsUtilities::sendSmsNotification($smsContent,$sendTo);
                }
             Log::info('Member Repo: storeMember::Send Welcome SMS -End');   
            $res=true;
            }
            Log::info('Member Repo: create Member::End');
        }catch(QueryException $e){
            DB::rollBack();
            Log::error('Member Repo: create Member::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }
    //-------------------------------------------------------------------------

    /**
     *  To return member data based on id 
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getMemberDataById($id)
    {   
        try{
            $data=array();
            Log::info('Member Repo:return member data based on id::Start');
            if (!empty($id)) {
                $data =  User::leftJoin('payments', 'users.id', '=', 'payments.member_id')
                    ->leftJoin('user_accounts', 'users.id', '=', 'user_accounts.user_id')
                    ->leftJoin('nominees', 'users.id', '=', 'nominees.member_id')
                    ->leftJoin('user_profiles', 'users.id', '=', 'user_profiles.user_id')
                    ->leftJoin('membership_details', 'users.id', '=', 'membership_details.user_id')
                    ->select('users.*','user_profiles.*','user_profiles.member_id as user_member_id',
                        'membership_details.status as member_status',
                        'membership_details.corpus_amount as corpus_amount','payments.*','payments.id as payment_id','user_accounts.*','nominees.id as nominee_id','nominees.name as nominee_name','nominees.email as nominee_email','nominees.phone as nominee_phone','nominees.fc_percentage',
                        'nominees.dob as nominee_dob','nominees.gender as nominee_gender','nominees.image as nominee_image','nominees.address as nominee_address','nominees.nominee_relationship',
                        DB::raw('DATE_FORMAT(payment_date, "%d-%m-%Y") as payment_date'),
                        DB::raw('DATE_FORMAT(users.dob, "%d-%m-%Y") as dob'),
                        DB::raw('DATE_FORMAT(nominees.dob, "%d-%m-%Y") as nominee_dob'),
                        DB::raw('DATE_FORMAT(membership_details.join_date, "%d-%m-%Y") as join_date')
                    )
                    ->selectRaw("(TIMESTAMPDIFF(YEAR, DATE(users.dob), CURRENT_DATE)) AS member_age")
                    ->selectRaw("(CASE is_active WHEN 'N' THEN 'InActive' WHEN 'Y' THEN 'Active' END) AS is_active")
                    ->selectRaw("(CASE interested_in_other_scheme WHEN 'N' THEN 'No' WHEN 'Y' THEN 'Yes' END) AS interested_in")
                     ->selectRaw("(CASE physical_disability WHEN 'N' THEN 'No' WHEN 'Y' THEN 'Yes' END) AS physical_disability")
                     ->selectRaw("(CASE previous_illness WHEN 'N' THEN 'No' WHEN 'Y' THEN 'Yes' END) AS previous_illness")
                     ->selectRaw("(CASE health_declaration WHEN 'N' THEN 'Not Accepted' WHEN 'Y' THEN 'Accepted' END) AS declaration")
                    ->where('users.id',$id)->first();
                   
                    $nominee_details=Self::getMemberNomineeDetails($id);  
                    if (!empty($data)) {
                        $data['id_proofs']=json_decode($data->id_proof, true);
                        $data['reports']=json_decode($data->reports);
                        $data['application']=json_decode($data->scanned_application);
                        $data['dropdowns']=Utilities::getDropdownValues();
                        $data['nominees']=Self::getMemberNomineeDetails($id);
                        $data['advance_payment']=Self::getAdvancePaymentDetails($id);
                    }

            }
            Log::info('Member Repo:return member data based on id::End');
            return $data;
        }catch(QueryException $e){
            Log::error('Member Repo:return member data based on id::Error'.$e->getMessage());
            throw $e;
        }
    }
    //-------------------------------------------------------------------------

    /**
     *  To return member total nominee data based on id 
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getMemberNomineeDetails($id)
    {   
        try{
            $data=array();
            Log::info('Member Repo:getMemberNomineeDetails::Start');
            if (!empty($id)) {
                $data =  Nominee::select('nominees.id as nominee_id','nominees.name as nominee_name','nominees.email as nominee_email','nominees.phone as nominee_phone',
                    'nominees.dob as nominee_dob','nominees.gender as nominee_gender','nominees.image as nominee_image','nominees.fc_percentage','nominees.address as nominee_address','nominees.nominee_relationship',
                    DB::raw('DATE_FORMAT(dob, "%d-%m-%Y") as nominee_dob'))
                ->where('member_id',$id)->get();
            }

            Log::info('Member Repo:getMemberNomineeDetails::End');
            return $data;
        }catch(QueryException $e){
            Log::error('Member Repo:getMemberNomineeDetails::Error'.$e->getMessage());
            throw $e;
        }
    }
    //-------------------------------------------------------------------------

    /**
     *  To return member advance payment details based on id 
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getAdvancePaymentDetails($id)
    {   
        try{
            $data=array();
            Log::info('Member Repo:getAdvancePaymentDetails::Start');
            if (!empty($id)) {
                $data =  Payment::leftJoin('payment_details', 'payments.id', '=', 'payment_details.payment_id')->select('payments.id as advance_payment_id','amount as advance_amount','mode_of_payment as advance_mode_of_payment',
                    DB::raw('DATE_FORMAT(payments.payment_date, "%d-%m-%Y") as advance_payment_date'))
                ->where('payments.member_id',$id)
                ->where('payment_details.payment_type','advance')->first();
            }

            Log::info('Member Repo:getAdvancePaymentDetails::End');
            return $data;
        }catch(QueryException $e){
            Log::error('Member Repo:getAdvancePaymentDetails::Error'.$e->getMessage());
            throw $e;
        }
    }
    //-------------------------------------------------------------------------


    /**
     *  Update the member personal details function
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function updatePerosnalDetails($request){
        $res=false;
        try{
            Log::info('Member Repo: Update member personal details::Start');
            $data=$request->all();
            $member_id=$data['member_id'];
           
            Log::info('Member Repo: To update member personal details::Member data preparing started - '.$member_id);
            DB::beginTransaction();
                $user_data=[];
                if (!empty($data['dob'])) {
                    $dob=date("Y-m-d", strtotime($data['dob']));
                }
                $user_data =array( 
                    'name'           => !empty($data['name'])?$data['name']:'',
                    'email'          => !empty($data['email'])?$data['email']:'',
                    'mobile_number'  => !empty($data['mobile_number'])?$data['mobile_number']:'',
                    'gender'         => !empty($data['gender'])?$data['gender']:'',
                    'dob'            => !empty($dob)?$dob:'',
                    'updated_at'     => date('Y-m-d h:i:s'),
                    'updated_by'     => Auth::user()->email,
                );
                
                $user_profile=[];
                     
                $member =  UserProfile::select('id_proof','age_proof')->where('user_id',$data['id'])->first();

                $customer_id_type= array();
                
                $customer_id_type=json_decode($member['id_proof'],true);
                $data['age_proof_name']=$member['age_proof'];
                $front_image = $backside_image = '';
                if(!empty($data['customer_id_front_changed']) && ($data['customer_id_front_changed'] == 'Y' || $data['customer_id_front_changed'] == 'D') && !empty($data['customer_front_old'])){
                    
                    $customer_id_type['customer_id_front'] ='';
                }
                
                if(!empty($data['customer_id_front_changed']) && $data['customer_id_front_changed'] == 'Y' && !empty($request->file('customer_id_front'))){
                    $file_front = $request->file('customer_id_front');

                    $front_image = time() . '_' .$file_front->getClientOriginalName();
                    $file_front->move(public_path('uploads'), $front_image);
                    $customer_id_type['customer_id_front'] = !empty($front_image)?$front_image:'';
                }
                
                if(!empty($data['customer_id_back_changed']) && ($data['customer_id_back_changed'] == 'Y' ||$data['customer_id_back_changed'] == 'D') && !empty($data['customer_back_old'])){
                    
                    $customer_id_type['customer_id_back'] ='';
                }
                if(!empty($data['customer_id_back_changed'])  && $data['customer_id_back_changed'] == 'Y' && !empty($request->file('customer_id_back'))){
                    $file_back = $request->file('customer_id_back');
                    
                    $backside_image = time() . '_' .$file_back->getClientOriginalName();
                    $file_back->move(public_path('uploads'), $backside_image);
                    $customer_id_type['customer_id_back'] = !empty($backside_image)?$backside_image:'';
                }
                
                if(!empty($customer_id_type)){
                    $customer_id_type['customer_id_type'] = !empty($data['customer_id_type'])?$data['customer_id_type']:'';
                }
                if (empty($customer_id_type['customer_id_front']) && empty($customer_id_type['customer_id_back'])) {
                    $customer_id_type['customer_id_type']='';
                }

                if(!empty($data['age_proof_changed']) && ($data['age_proof_changed'] == 'D'||$data['age_proof_changed'] == 'Y') && !empty($data['age_proof_old'])){
                    
                    $data['age_proof_name'] ='';
                }
                if(!empty($data['age_proof_changed'])  && $data['age_proof_changed'] == 'Y' && !empty($request->file('age_proof'))){
                    $file = $request->file('age_proof');
                    
                    $age_proof_image = time() . '_' .$file->getClientOriginalName();
                    $file->move(public_path('uploads'), $age_proof_image);
                    $data['age_proof_name'] = !empty($age_proof_image)?$age_proof_image:'';
                }
                
                $filenames=[];
                
                if(!empty($data['application_form']))
                {    
                    foreach ($data['application_form'] as $key => $value) {
                        if (!empty($value)) {
                            $name = time() .'_'.$value->getClientOriginalName();
                            $value->move(public_path('uploads'),$name);
                            $filenames[]=$name;
                        }
                    }
                    $data['scanned_application']=json_encode($filenames);

                    if(!empty($data['application1'])){
                        $updated_files=array_merge($filenames,$data['application1']);
                        $data['scanned_application']=json_encode($updated_files);
                    }
                }
                elseif(!empty($data['application1'])){
                    $data['scanned_application']=json_encode($data['application1']);
                }

                $user_profile=array( 
                    'alternative_email'  => !empty($data['alternative_email'])?$data['alternative_email']:'',
                    'alternative_phone'  => !empty($data['alternative_number'])?$data['alternative_number']:'',
                    'martial_status'  => !empty($data['marital_status'])?$data['marital_status']:'',
                    'country'  => !empty($data['country'])?$data['country']:'',
                    'state'  => !empty($data['state'])?$data['state']:'',
                    'city'  => !empty($data['city'])?$data['city']:'',
                    'id_proof'  => !empty($customer_id_type)?json_encode($customer_id_type):null,
                    'age_proof'  => !empty($data['age_proof_name'])?$data['age_proof_name']:null,
                    'address'  => !empty($data['member_address'])?$data['member_address']:'',
                    'scanned_application'=> !empty($data['scanned_application'])?($data['scanned_application']):null,
                    'updated_at'  => date('Y-m-d h:i:s'),
                    'updated_by'  =>Auth::user()->email,
                );
                
                Log::info('Member Repo: To update member personal details::Member data preparing end- '.$member_id);   
                $update_user=User::where('id',$data['id'])->update($user_data); 
                $update_profile=UserProfile::where('user_id',$data['id'])->update($user_profile);
                DB::commit(); 
                $res=true;
            Log::info('Member Repo: update member personal details:End');
        }catch(QueryException $e){
            DB::rollBack();
            Log::error('Member Repo: update member personal Details::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }
    //-------------------------------------------------------------------------


    /**
     *  Update the member nominee details function
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function updateNomineeDetails($request){
        $res=false;
        try{
            Log::info('Member Repo: Update member nominee details::Start');
            $data=$request->all();
            $member_id=$data['members_id'];
           
            Log::info('Member Repo: To update member nominee details::Member data preparing started - '.$member_id);
            DB::beginTransaction();
                $nominee_details=[];
                $nominee_data=[];
                foreach ($data['NomineeDetails'] as $key => $value) {
                    if (!empty($value['nominee_id'])) {
                       $old_image =  Nominee::where('id',$value['nominee_id'])->value('image');
                        $value['image_name']=$old_image;
                    }

                    if(!empty($value['nominee_image_changed']) && ($value['nominee_image_changed'] == 'D'||$value['nominee_image_changed'] == 'Y')){

                            if (!empty($value['nominee_image'])) {
                               $nominee_image = time() .'_'. $value['nominee_image']->getClientOriginalName();
                                $value['nominee_image']->move(public_path('uploads'), $nominee_image);
                                $value['image_name'] = !empty($nominee_image)?$nominee_image:'';
                            } 
                            else{
                                $value['image_name'] ='';
                            }   
                    }
                    if(!empty($value['nominee_image_changed'])  && $value['nominee_image_changed'] == 'Y' && !empty($value['nominee_image'])){
                        
                        $nominee_image = time() .'_'. $value['nominee_image']->getClientOriginalName();
                        $value['nominee_image']->move(public_path('uploads'), $nominee_image);

                        $value['image_name'] = !empty($nominee_image)?$nominee_image:'';
                    }

                    if(empty($value['nominee_image_changed']) && !empty($value['nominee_image'])){
                        
                        $nominee_image = time() .'_'. $value['nominee_image']->getClientOriginalName();
                        $value['nominee_image']->move(public_path('uploads'), $nominee_image);

                        $value['image_name'] = !empty($nominee_image)?$nominee_image:'';
                    }
                    
                    if (!empty($value['nominee_dob'])) {
                        $dob=date("Y-m-d", strtotime($value['nominee_dob']));
                    }
                    $nominee_details =array( 
                        'name'    => !empty($value['nominee_name'])?$value['nominee_name']:'',
                        'fc_percentage'=> !empty($value['nominee_fcp'])?$value['nominee_fcp']:null,
                        'phone'   => !empty($value['nominee_phone'])?$value['nominee_phone']:'',
                        'email'   => !empty($value['nominee_email'])?$value['nominee_email']:'',
                        'gender'  => !empty($value['nominee_gender'])?$value['nominee_gender']:'',
                        'dob'     => !empty($dob)?$dob:'',
                        'nominee_relationship' => !empty($value['nominee_relation'])?$value['nominee_relation']:'',
                        'address'  => !empty($value['nominee_address'])?$value['nominee_address']:'',
                        'image'  => !empty($value['image_name'])?$value['image_name']:'',
                        'updated_at'     => date('Y-m-d h:i:s'),
                        'updated_by'     => Auth::user()->email,
                    );
                    Log::info('Member Repo: To update member nominee details::Member data preparing end- '.$member_id);   
                    if (!empty($value['nominee_id'])) {
                        $update_profile=Nominee::where('id',$value['nominee_id'])->update($nominee_details); 
                        unset($value);   
                    }
                    else{
                        Log::info('Member Repo: updateNomineeDetails::New nominee data preparing start- '.$member_id);

                        if(!empty($value['nominee_photo'])){
                            $name = time() .'_'. $value['nominee_photo']->getClientOriginalName();
                            $value['nominee_photo']->move(public_path('uploads'), $name);
                            $nominee_photo = $name;
                        }
                        else{
                            $nominee_photo='';
                        }

                        $nominee_details =array(
                            'member_id' => $data['id'], 
                            'name'  => !empty($value['nominee_name'])?$value['nominee_name']:'',
                            'fc_percentage'=> !empty($value['nominee_fcp'])?$value['nominee_fcp']:null,
                            'phone'   => !empty($value['nominee_phone'])?$value['nominee_phone']:'',
                            'email'   => !empty($value['nominee_email'])?$value['nominee_email']:'',
                            'gender'  => !empty($value['nominee_gender'])?$value['nominee_gender']:'',
                            'dob'     => !empty($dob)?$dob:'',
                            'nominee_relationship' => !empty($value['nominee_relation'])?$value['nominee_relation']:'',
                            'address'  => !empty($value['nominee_address'])?$value['nominee_address']:'',
                            'image'  => !empty($nominee_photo)?$nominee_photo:'',
                            'created_at'  => date('Y-m-d h:i:s'),
                            'created_by'  => Auth::user()->email,
                        );

                        Log::info('Member Repo: updateNomineeDetails::New nominee data preparing start- '.$member_id);
                        if (!empty($nominee_details)) {
                            array_push($nominee_data, $nominee_details);
                        }
                    }
                }
                if (!empty($nominee_data)) {
                    $insert=Nominee::insert($nominee_data);
                    Log::info('Member Repo:storeMember::New Nominee Data stored');
                    unset($nominee_data);
                    unset($nominee_details);
                }
                DB::commit(); 
                $res=true;
            Log::info('Member Repo: update member nominee details:End');
        }catch(QueryException $e){
            DB::rollBack();
            Log::error('Member Repo: update member nominee Details::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }
    //-------------------------------------------------------------------------

     /**
     *  Delete Particular nominee details of member based on id
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function deleteNomineeDetails($id)
    {   
        try{
            $res=false;
            Log::info('Member Repo:deleteNomineeDetails::Start');
            if (!empty($id)) {
                DB::beginTransaction();
                $res=Nominee::where('id',$id)->delete();
                $res=true;
                DB::commit(); 
            }
            Log::info('Member Repo:deleteNomineeDetails::End');
        }catch(QueryException $e){
            DB::rollBack();
            Log::error('Member Repo:deleteNomineeDetails::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }
    //-------------------------------------------------------------------------

    /**
     *  Update the member payment details function
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function updatePaymentDetails($request){
        $res=false;
        try{
            Log::info('Member Repo: Update member payment details::Start');
            $data=$request->all();
           
            $member_id=$data['members_id'];
           
            Log::info('Member Repo: To update member payment details::Member data preparing started - '.$member_id);
            DB::beginTransaction();
                $payment_details=[];

                if (!empty($data['payment_date'])) {
                    $payment_date=date("Y-m-d", strtotime($data['payment_date']));
                }

                $payment_details =array( 
                    'amount'   => !empty($data['corpus_amount'])?$data['corpus_amount']:'',
                    'mode_of_payment'   => !empty($data['payment_mode'])?$data['payment_mode']:'',
                    'payment_date'     => !empty($payment_date)?$payment_date:'',
                    'remarks'  => !empty($data['note'])?$data['note']:'',
                    'updated_at'     => date('Y-m-d h:i:s'),
                    'updated_by'     => Auth::user()->email,
                );
                
                $membership =array( 
                    'corpus_amount'   => !empty($data['corpus_amount'])?$data['corpus_amount']:'',
                    'age'             => !empty($data['member_age'])?$data['member_age']:'',
                    'updated_at'  => date('Y-m-d h:i:s'),
                    'updated_by'  => Auth::user()->email,
                );

                $user_profile=array( 
                    'interested_in_other_scheme'=> !empty($data['interested'])?$data['interested']:'',
                    'updated_at'  => date('Y-m-d h:i:s'),
                    'updated_by'  =>Auth::user()->email,
                );
                Log::info('Member Repo: To update member payment details::Member data preparing end- '.$member_id);   
                $update_profile=Payment::where('id',$data['payment_id'])->update($payment_details);
                
                $update=MembershipDetails::where('user_id',$data['id'])->update($membership);
                $update_profile=UserProfile::where('user_id',$data['id'])->update($user_profile);
                Log::info('Member Repo: To update member payment details::Advance Payment prepare start- '.$member_id);

                    if (!empty($data['advance_payment_date'])) {
                        $advance_payment_date=date("Y-m-d", strtotime($data['advance_payment_date']));
                    }

                    $advance_payment_details =array( 
                    'amount'   => !empty($data['advance_amount'])?$data['advance_amount']:'',
                    'mode_of_payment'   => !empty($data['advance_payment_mode'])?$data['advance_payment_mode']:'',
                    'payment_date'     => !empty($advance_payment_date)?$advance_payment_date:'',
                    'remarks'  => !empty($data['note'])?$data['note']:'',
                    'updated_at'     => date('Y-m-d h:i:s'),
                    'updated_by'     => Auth::user()->email,
                );
                Log::info('Member Repo: To update member payment details::Advance Payment prepare end- '.$member_id);
                $update_payment=Payment::where('id',$data['advance_pay_id'])->update($advance_payment_details);

                DB::commit(); 
                $res=true;
            Log::info('Member Repo: update member payment details:End');
        }catch(QueryException $e){
            DB::rollBack();
            Log::error('Member Repo: update member payment Details::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }
    //-------------------------------------------------------------------------

    /**
     *  Update the member medical details function
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function updateMedicalDetails($request){
        $res=false;
        try{
            Log::info('Member Repo: Update Member medical details::Start');
            $data=$request->all();
            
            $member_id=$data['members_id'];
           
            Log::info('Member Repo: To update member medical details::Member data preparing started - '.$member_id);
                DB::beginTransaction();
                $medical_details=[];
                if ($data['health_declaration']=='on') {
                    $data['health_declaration']='Y';
                }
                else{
                    $data['health_declaration']='N';
                }

                $filenames=[];
                $data['Reports']=null;
                if(!empty($data['reports']))
                {    
                    foreach ($data['reports'] as $key => $value) {
                        if (!empty($value)) {
                            $name = time() .'_'.$value->getClientOriginalName();
                            $value->move(public_path('uploads'),$name);
                            $filenames[]=$name;
                        }
                    }
                    $data['Reports']=json_encode($filenames);

                    if(!empty($data['reports1'])){
                        $updated_files=array_merge($filenames,$data['reports1']);
                        $data['Reports']=json_encode($updated_files);
                    }
                }
                elseif(!empty($data['reports1'])){
                    $data['Reports']=json_encode($data['reports1']);
                }
                
                $medical_details =array( 
                    'physical_disability'  => !empty($data['physical_disability'])?$data['physical_disability']:'',
                    'physical_disability_type'   => !empty($data['physical_disability_type'])?$data['physical_disability_type']:'',
                    'previous_illness'   => !empty($data['previous_illness'])?$data['previous_illness']:'',
                    'illness_type'     => !empty($data['illness_type'])?$data['illness_type']:'',
                    'health_declaration'  => !empty($data['health_declaration'])?$data['health_declaration']:'',
                    'reports'  => $data['Reports'],
                    'updated_at'     => date('Y-m-d h:i:s'),
                    'updated_by'     => Auth::user()->email,
                );
                
                Log::info('Member Repo: To update member payment details::Member data preparing end- '.$member_id);   
                $update_profile=UserProfile::where('user_id',$data['id'])->update($medical_details);
                DB::commit(); 
                $res=true;
            Log::info('Member Repo: update member medical details:End');
        }catch(QueryException $e){
            DB::rollBack();
            Log::error('Member Repo: update member medical Details::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }
    //-------------------------------------------------------------------------

    /**
     *  Update the member access details function
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function updateMemberAccessDetails($request){
        $res=false;
        try{
            Log::info('Member Repo: Update member access details::Start');
            $data=$request->all();
           
            $member_id=$data['members_id'];
           
            Log::info('Member Repo: To update member payment details::Member data preparing started - '.$member_id);
                DB::beginTransaction();
                $member_details=[];

                $member_details =array( 
                    'additional_info'  => !empty($data['additional_info'])?$data['additional_info']:'',
                    'updated_at'     => date('Y-m-d h:i:s'),
                    'updated_by'     => Auth::user()->email,
                );
                
                $old_image =  UserProfile::where('user_id',$data['id'])->value('image');

                $data['image_name']=$old_image;

                if(!empty($data['profile_photo_changed']) && ($data['profile_photo_changed'] == 'D'||$data['profile_photo_changed'] == 'Y') && !empty($data['profile_photo_old'])){
                    
                    $data['image_name'] ='';
                }
                if(!empty($data['profile_photo_changed'])  && $data['profile_photo_changed'] == 'Y' && !empty($request->file('profile_photo'))){
                    $file = $request->file('profile_photo');
                    
                    $profile_photo = time() . '_' .$file->getClientOriginalName();
                    $file->move(public_path('uploads'), $profile_photo);
                    $data['image_name'] = !empty($profile_photo)?$profile_photo:'';
                }

                $profile =array( 
                    'image' => !empty($data['image_name'])?$data['image_name']:'',
                    'updated_at'     => date('Y-m-d h:i:s'),
                    'updated_by'     => Auth::user()->email,
                );

                if (!empty($data['membership_date'])) {
                    $join_date=date("Y-m-d", strtotime($data['membership_date']));
                }

                $membership =array( 
                    'status'      => $data['member_status'],
                    'join_date'   => $join_date,
                    'updated_at'  => date('Y-m-d h:i:s'),
                    'updated_by'  => Auth::user()->email,
                );

                Log::info('Member Repo: To update member access details::Member data preparing end- '.$member_id);   
                $update_profile=User::where('id',$data['id'])->update($member_details);
                $update=UserProfile::where('user_id',$data['id'])->update($profile);
                $update=MembershipDetails::where('user_id',$data['id'])->update($membership);
                DB::commit(); 
                $res=true;
            Log::info('Member Repo: update member access details:End');
        }catch(QueryException $e){
            DB::rollBack();
            Log::error('Member Repo: update member access Details::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }
    //-------------------------------------------------------------------------

    /**
     *  Update the member status function
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function updateMemberStatus($data){
        $res=false;
        try{
            Log::info('Member Repo: Update member status::Start');
            DB::beginTransaction();

            $member_details=[];
            $login_status =[];
            if ($data['member_status']=='Active' || $data['member_status']=='Inactive'|| $data['member_status']=='Probationary') {
                $member_details =array( 
                    'status'     => $data['member_status'],
                    'updated_at' => date('Y-m-d h:i:s'),
                    'updated_by' => Auth::user()->email,
                );
                if ($data['member_status']=='Active'||$data['member_status']=='Probationary'){
                    $is_active='Y';
                }
                elseif ($data['member_status']=='Inactive') {
                    $is_active='N';
                }
            }
            else if($data['member_status']=='Expired'){
                $confirm_date=date("Y-m-d", strtotime($data['confirm_date']));
                $member_details =array( 
                    'status'      => $data['member_status'],
                    'confirm_date'=> $confirm_date,
                    'fc_amount'   => $data['fc_amount'],
                    'released_fc_amount'=> $data['final_fc_amount'],
                    'note'        => $data['fc_note'],
                    'updated_at'  => date('Y-m-d h:i:s'),
                    'updated_by'  => Auth::user()->email,
                );
                $is_active='N';
            }
            if (!empty($member_details)) {
                $update_profile=MembershipDetails::where('user_id',$data['id'])->update($member_details);

                $login_status =array( 
                    'is_active'  =>$is_active,
                    'updated_at' => date('Y-m-d h:i:s'),
                    'updated_by' => Auth::user()->email,
                );
                $update_status=User::where('id',$data['id'])->update($login_status);
                // if ($update_profile && $data['member_status']=='Expired' && is_numeric($data['fc_amount'])) {
                //     $store=$this->paymentRepo->storeFcBill($data,'single_expired_member');
                // }
            }
            
            DB::commit(); 
            $res=true;
            Log::info('Member Repo: update member status:End');
        }catch(QueryException $e){
            DB::rollBack();
            Log::error('Member Repo: update member status::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }
    //-------------------------------------------------------------------------

    /**
     *  Return expired members data
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getExpiredMembersData($params)
    {             
        try{
            Log::info('Member Repo: return expired members data::Start');

                $search = !empty($params['search']['value'])?$params['search']['value']:'';  
                $pageSize = !empty($params['length']) ? $params['length'] : 10;
                $start = isset($params['start']) ? $params['start'] : 0;
                $count_filter = 0;
                $data  = array();
                $account_id=Session::get('account_id');
                $role_name=Auth::user()->roles->first()->name;
                $query =  User::leftJoin('user_profiles','users.id','=','user_profiles.user_id')
                ->leftJoin('membership_details', 'users.id', '=', 'membership_details.user_id')
                ->leftJoin('roles', 'users.role', '=', 'roles.id')
                ->leftJoin('user_accounts', 'users.id', '=', 'user_accounts.user_id')
                ->select('users.*','user_profiles.member_id','membership_details.status','membership_details.join_date','membership_details.confirm_date',
                    'membership_details.fc_amount',
                    DB::raw('DATE_FORMAT(confirm_date, "%d-%m-%Y") as confirm_date'),
                    DB::raw('DATE_FORMAT(join_date, "%d-%m-%Y") as join_date'))
                ->selectRaw("(TIMESTAMPDIFF(YEAR, DATE(users.dob), DATE(membership_details.confirm_date))) AS age")
                ->where('roles.name','Member')
                ->where('membership_details.status','Expired');

                if(!empty($params['from']) && !empty($params['to']))  {
                    $from = trim($params['from']);
                    $to = trim($params['to']);
                    $from = date('Y-m-d', strtotime($from));
                    $to = date('Y-m-d', strtotime($to));
                
                    $query->where(function ($q) use ($from,$to) {
                        $q->where('membership_details.confirm_date', '>=', $from.' 00:00:00');
                        $q->where('membership_details.confirm_date', '<=', $to.' 23:59:59');
                    });
                }
                if(!empty($params['age_from']) && !empty($params['age_to']))  {
                    $from = trim($params['age_from']);
                    $to = trim($params['age_to']);
                
                    $query->where(function ($q) use ($from,$to) {
                        $q->where('membership_details.age', '>=', $from);
                        $q->where('membership_details.age', '<=', $to);
                    });
                }
                if(!empty($params['register_id'])) {
                    $query->where('user_profiles.member_id', 'LIKE', '%'.$params['register_id'].'%');
                }
                if(!empty($params['member_phone'])) {
                    $query->where('users.mobile_number',$params['member_phone']);
                }
                if(!empty($params['member_email'])) {
                    $query->where('users.email',$params['member_email']);
                }
                if(!empty($params['member_name'])) {
                    $query->where('users.name','LIKE', '%'.$params['member_name'].'%');
                }
                if (!empty($account_id) && $role_name!='SuperAdmin') {
                    $query->where('user_accounts.account_id',$account_id);
                }

                $count_total = $query->count();
                if($search != ''){
                    $query->Where(function ($query) use ($search) {
                        $query->orWhere('users.name','LIKE' ,'%'.$search.'%')
                        ->orWhere('users.email','LIKE' , '%'.$search.'%')
                        ->orWhere('user_profiles.member_id','LIKE' , '%'.$search.'%')
                        ->orWhere('membership_details.fc_amount','LIKE' , '%'.$search.'%')
                        ->orWhere('membership_details.confirm_date','LIKE' , '%'.$search.'%')
                        ->orWhere('membership_details.join_date','LIKE' , '%'.$search.'%')
                        ->orWhere('users.mobile_number', 'LIKE' , '%'.$search.'%');
                    });
                    $count_filter = $query->count();
                }
                $query->orderBy('membership_details.updated_at','DESC')->skip($start)->take($pageSize);
                if($count_filter == 0 && $search == ''){
                    $count_filter = $count_total;
                }
                $qData=$query->get();
                $uniqueData=[];
                $uniqueIds=[];
                foreach ($qData as $key => $value) {
                    $value->created_date=Utilities::formatDate($value->updated_at);
                    if(!in_array($value->member_id,$uniqueIds)){
                        array_push($uniqueData, $value);
                        array_push($uniqueIds, $value->member_id);
                    }
                }
                
                $data = DataTables::of($uniqueData)->addColumn('action', function($data){
                    $button ='';
                    if(auth()->user()->hasPermissionTo('Edit Member'))      
                    { 
                        $button = '<a href="'.url('/')."/member/edit/".$data->id.'" class="text-primary mr-2"><i class="nav-icon i-Pen-2 font-weight-bold" data-toggle="tooltip" title="Edit Member" id="'.$data->id.'"></i></a>';
                    }
                    if(auth()->user()->hasPermissionTo('View Member'))      
                    {
                        $button .= '<a href="'.url('/')."/member/view/".$data->id.'"class="text-primary"><i class="fa fa-eye"  data-toggle="tooltip" title="View Member" id="'.$data->id.'"></i></a>';
                    }
                    return $button;
                })->with([
                    "recordsTotal" => $count_total,
                    "recordsFiltered" => $count_filter,
                ])
                ->skipPaging()
                ->rawColumns(['action'])
                    ->make(true);  
                Log::info('Member Repo: return expired members data::End');
                return $data;
            }
            catch(QueryException $e){
                Log::error('Member Repo: return expired members data::Error'.$e->getMessage());
                throw $e;
            }
    }
    //-------------------------------------------------------------------------

    /**
     *  Return Inactive members data
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getInactiveMembersData($params)
    {             
        try{
            Log::info('Member Repo: return Inactive members data::Start');
                $search = !empty($params['search']['value'])?$params['search']['value']:'';  
                $pageSize = !empty($params['length']) ? $params['length'] : 10;
                $start = isset($params['start']) ? $params['start'] : 0;
                $count_filter = 0;
                $data  = array();
                $account_id=Session::get('account_id'); 
                $role_name=Auth::user()->roles->first()->name;
                $query =  User::leftJoin('payments', 'users.id', '=', 'payments.member_id')
                ->leftJoin('payment_details','payments.id','=','payment_details.payment_id')
                ->leftJoin('user_profiles','users.id','=','user_profiles.user_id')
                ->leftJoin('membership_details', 'users.id', '=', 'membership_details.user_id')
                ->leftJoin('roles', 'users.role', '=', 'roles.id')
                ->leftJoin('user_accounts', 'users.id', '=', 'user_accounts.user_id')
                ->select('users.*','user_profiles.member_id','membership_details.status','membership_details.join_date','membership_details.age','payments.amount','payments.payment_date',
                    DB::raw('DATE_FORMAT(payment_date, "%d-%m-%Y") as payment_date'),
                    DB::raw('DATE_FORMAT(users.updated_at, "%d-%m-%Y") as created_date'),
                    DB::raw('DATE_FORMAT(join_date, "%d-%m-%Y") as join_date'))
                ->where('roles.name','Member')
                ->where('membership_details.status','Inactive')
                ->where('payment_details.payment_type','corpus_amount');
                
                if(!empty($params['from']) && !empty($params['to']))  {
                    $from = trim($params['from']);
                    $to = trim($params['to']);
                    $from = date('Y-m-d', strtotime($from));
                    $to = date('Y-m-d', strtotime($to));
                
                    $query->where(function ($q) use ($from,$to) {
                        $q->where('users.updated_at', '>=', $from.' 00:00:00');
                        $q->where('users.updated_at', '<=', $to.' 23:59:59');
                    });
                }
                if(!empty($params['age_from']) && !empty($params['age_to']))  {
                    $from = trim($params['age_from']);
                    $to = trim($params['age_to']);
                
                    $query->where(function ($q) use ($from,$to) {
                        $q->where('membership_details.age', '>=', $from);
                        $q->where('membership_details.age', '<=', $to);
                    });
                }
                if(!empty($params['register_id'])) {
                    $query->where('user_profiles.member_id', 'LIKE', '%'.$params['register_id'].'%');
                }
                if(!empty($params['member_phone'])) {
                    $query->where('users.mobile_number',$params['member_phone']);
                }

                if(!empty($params['member_email'])) {
                    $query->where('users.email',$params['member_email']);
                }

                if(!empty($params['member_name'])) {
                    $query->where('users.name','LIKE', '%'.$params['member_name'].'%');
                }
                if (!empty($account_id) && $role_name!='SuperAdmin') {
                    $query->where('user_accounts.account_id',$account_id);
                }
                $count_total = $query->count();
                if($search != ''){
                    $query->Where(function ($query) use ($search) {
                        $query->orWhere('users.name','LIKE' ,'%'.$search.'%')
                        ->orWhere('users.email','LIKE' , '%'.$search.'%')
                        ->orWhere('user_profiles.member_id','LIKE' , '%'.$search.'%')
                        ->orWhere('payments.amount','LIKE' , '%'.$search.'%')
                        ->orWhere('payments.payment_date','LIKE' , '%'.$search.'%')
                        ->orWhere('membership_details.join_date','LIKE' , '%'.$search.'%')
                        ->orWhere('membership_details.updated_at','LIKE' , '%'.$search.'%')
                        ->orWhere('users.mobile_number', 'LIKE' , '%'.$search.'%');
                    });
                    $count_filter = $query->count();
                }
                $query->skip($start)->take($pageSize);
                if($count_filter == 0 && $search == ''){
                    $count_filter = $count_total;
                }
                $qData=$query->orderBy('membership_details.updated_at','desc')->get();
                
                $data = DataTables::of($qData)->addColumn('action', function($data){
                    $button ='';
                    if(auth()->user()->hasPermissionTo('Edit Member'))      
                    { 
                        $button = '<a href="'.url('/')."/member/edit/".$data->id.'" class="text-primary mr-2"><i class="nav-icon i-Pen-2 font-weight-bold" data-toggle="tooltip" title="Edit Member" id="'.$data->id.'"></i></a>';
                    }
                    if(auth()->user()->hasPermissionTo('View Member'))      
                    {
                        $button .= '<a href="'.url('/')."/member/view/".$data->id.'"class="text-primary"><i class="fa fa-eye"  data-toggle="tooltip" title="View Member" id="'.$data->id.'"></i></a>';
                    }
                    if(auth()->user()->hasPermissionTo('Change Member Status'))
                    {
                        $button .= '<button class="btn mr-2 btn-primary btn-sm change_status_new" data-id="'.$data->id.'" data-status="Active">Active</button>';
                    }
                    return $button;
                })->with([
                    "recordsTotal" => $count_total,
                    "recordsFiltered" => $count_filter,
                ])
                ->skipPaging()
                ->rawColumns(['action'])
                    ->make(true);  
                Log::info('Member Repo: return Inactive members data::End');
                return $data;
            }
            catch(QueryException $e){
                Log::error('Member Repo: return Inactive members data::Error'.$e->getMessage());
                throw $e;
            }
    }
    //-------------------------------------------------------------------------


    /**
     *  Return Active members data
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getActiveMembersData($params)
    {             
        try{
            Log::info('Member Repo: return Active members data::Start');
                $search = !empty($params['search']['value'])?$params['search']['value']:'';  
                $pageSize = !empty($params['length']) ? $params['length'] : 10;
                $start = isset($params['start']) ? $params['start'] : 0;
                $count_filter = 0;
                $data  = array();
                $account_id=Session::get('account_id'); 
                $role_name=Auth::user()->roles->first()->name;
                $query =  User::leftJoin('payments', 'users.id', '=', 'payments.member_id')
                ->leftJoin('payment_details','payments.id','=','payment_details.payment_id')
                ->leftJoin('user_profiles','users.id','=','user_profiles.user_id')
                ->leftJoin('membership_details', 'users.id', '=', 'membership_details.user_id')
                ->leftJoin('roles', 'users.role', '=', 'roles.id')
                ->leftJoin('user_accounts', 'users.id', '=', 'user_accounts.user_id')
                ->select('users.*','user_profiles.member_id','membership_details.status','membership_details.join_date','payments.amount','payments.payment_date',
                    DB::raw('DATE_FORMAT(payment_date, "%d-%m-%Y") as payment_date'),
                    DB::raw('DATE_FORMAT(join_date, "%d-%m-%Y") as join_date'),
                    DB::raw('DATE_FORMAT(users.created_at,"%d-%m-%Y") as created_date'))
                ->selectRaw("(TIMESTAMPDIFF(YEAR, DATE(users.dob), CURRENT_DATE)) AS age")
                ->where('roles.name','Member')
                ->where('membership_details.status','Active')
                ->where('payment_details.payment_type','corpus_amount');
                
                if(!empty($params['from']) && !empty($params['to']))  {
                    $from = trim($params['from']);
                    $to = trim($params['to']);
                    $from = date('Y-m-d', strtotime($from));
                    $to = date('Y-m-d', strtotime($to));
                
                    $query->where(function ($q) use ($from,$to) {
                        $q->where('users.created_at', '>=', $from.' 00:00:00');
                        $q->where('users.created_at', '<=', $to.' 23:59:59');
                    });
                }
                if(!empty($params['age_from']) && !empty($params['age_to']))  {
                    $from = trim($params['age_from']);
                    $to = trim($params['age_to']);
                
                    $query->where(function ($q) use ($from,$to) {
                        $q->where('membership_details.age', '>=', $from);
                        $q->where('membership_details.age', '<=', $to);
                    });
                }
                if(!empty($params['register_id'])) {
                    $query->where('user_profiles.member_id', 'LIKE', '%'.$params['register_id'].'%');
                }
                if(!empty($params['member_phone'])) {
                    $query->where('users.mobile_number',$params['member_phone']);
                }

                if(!empty($params['member_email'])) {
                    $query->where('users.email',$params['member_email']);
                }

                if(!empty($params['member_name'])) {
                    $query->where('users.name','LIKE', '%'.$params['member_name'].'%');
                }
                if (!empty($account_id) && $role_name!='SuperAdmin') {
                    $query->where('user_accounts.account_id',$account_id);
                }
                $count_total = $query->count();
                if($search != ''){
                    $query->Where(function ($query) use ($search) {
                        $query->orWhere('users.name','LIKE' ,'%'.$search.'%')
                        ->orWhere('users.email','LIKE' , '%'.$search.'%')
                        ->orWhere('user_profiles.member_id','LIKE' , '%'.$search.'%')
                        ->orWhere('payments.amount','LIKE' , '%'.$search.'%')
                        ->orWhere('payments.payment_date','LIKE' , '%'.$search.'%')
                        ->orWhere('membership_details.join_date','LIKE' , '%'.$search.'%')
                        ->orWhere('users.mobile_number', 'LIKE' , '%'.$search.'%');
                    });
                    $count_filter = $query->count();
                }
                $query->skip($start)->take($pageSize);
                if($count_filter == 0 && $search == ''){
                    $count_filter = $count_total;
                }
                $qData=$query->orderBy('membership_details.updated_at','desc')->get();
                
                $data = DataTables::of($qData)->addColumn('action', function($data){
                    $button ='';
                    if(auth()->user()->hasPermissionTo('Edit Member'))      
                    { 
                        $button = '<a href="'.url('/')."/member/edit/".$data->id.'" class="text-primary mr-2"><i class="nav-icon i-Pen-2 font-weight-bold" data-toggle="tooltip" title="Edit Member" id="'.$data->id.'"></i></a>';
                    }
                    if(auth()->user()->hasPermissionTo('View Member'))      
                    {
                        $button .= '<a href="'.url('/')."/member/view/".$data->id.'"class="text-primary"><i class="fa fa-eye"  data-toggle="tooltip" title="View Member" id="'.$data->id.'"></i></a>';
                    }

                    if(auth()->user()->hasPermissionTo('Change Member Status'))
                    {
                        $button .= '<button class="btn mr-2 btn-primary btn-sm change_status_new" data-id="'.$data->id.'" data-status="'.$data->status.'" data-joining_date="'.$data->join_date.'">Status</button>';
                    }
                    return $button;
                })->with([
                    "recordsTotal" => $count_total,
                    "recordsFiltered" => $count_filter,
                ])
                ->skipPaging()
                ->rawColumns(['action'])
                    ->make(true);  
                Log::info('Member Repo: return Active members data::End');
                return $data;
            }
            catch(QueryException $e){
                Log::error('Member Repo: return Active members data::Error'.$e->getMessage());
                throw $e;
            }
    }
    //-------------------------------------------------------------------------
    

    /**
     *  Return all pending advance members data
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getPendingAdvanceMembersData()
    {             
        try{
            Log::info('Member Repo: getPendingAdvanceMembersData::Start');
                
                $account_id=Session::get('account_id');
                $role_name=Auth::user()->roles->first()->name;
                $query =  FCPayments::leftJoin('users', 'fc_payments.member_id', '=', 'users.id')
                    ->leftJoin('user_profiles','fc_payments.member_id','=','user_profiles.user_id')
                    ->leftJoin('membership_details', 'fc_payments.member_id', '=', 'membership_details.user_id')
                    ->leftJoin('user_accounts', 'fc_payments.member_id', '=', 'user_accounts.user_id')
                    ->select('users.name','users.id As user_id','fc_payments.*','fc_payments.member_id','user_profiles.member_id','membership_details.status','membership_details.age','membership_details.fc_amount','user_accounts.account_id')
                    ->where('fc_payments.payment_type','advance')
                    ->where('membership_details.status','Active')
                    ->where('fc_payments.has_paid','N');

                    if (!empty($account_id) && $role_name!='SuperAdmin') {
                        $query->where('user_accounts.account_id',$account_id);
                    }
                    $qData=$query->get();
                    
                    $uniqueData=[];
                    $uniqueIds=[];
                    foreach ($qData as $key => $value) {
                        if(!in_array($value->member_id,$uniqueIds)){
                            array_push($uniqueData, $value);
                            array_push($uniqueIds, $value->member_id);
                        }
                    }
                  
                Log::info('Member Repo: getPendingAdvanceMembersData::End');
                return $uniqueData;
            }
            catch(QueryException $e){
                Log::error('Member Repo: getPendingAdvanceMembersData::Error'.$e->getMessage());
                throw $e;
            }
    }
    //-------------------------------------------------------------------------

    /**
     *  Return Probationary members datatable
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getProbationaryMembersData($params)
    {             
        try{
            Log::info('Member Repo: return probationary members data::Start');

                $search = !empty($params['search']['value'])?$params['search']['value']:'';  
                $pageSize = !empty($params['length']) ? $params['length'] : 10;
                $start = isset($params['start']) ? $params['start'] : 0;
                $count_filter = 0;
                $data  = array();
                $account_id=Session::get('account_id');
                $role_name=Auth::user()->roles->first()->name;
                $current_date = date("Y-m-d");
                $prev_date = Carbon::today()->subYear();
                $last_year_date=date("Y-m-d", strtotime($prev_date));

                $query =  User::leftJoin('payments', 'users.id', '=', 'payments.member_id')
                ->leftJoin('payment_details','payments.id','=','payment_details.payment_id')
                ->leftJoin('user_profiles','users.id','=','user_profiles.user_id')
                ->leftJoin('membership_details', 'users.id', '=', 'membership_details.user_id')
                ->leftJoin('roles', 'users.role', '=', 'roles.id')
                ->leftJoin('user_accounts', 'users.id', '=', 'user_accounts.user_id')
                ->select('users.*','user_profiles.member_id','membership_details.status','membership_details.join_date','payments.amount','payments.payment_date',
                    DB::raw('DATE_FORMAT(payment_date, "%d-%m-%Y") as payment_date'),
                    DB::raw('DATE_FORMAT(users.updated_at, "%d-%m-%Y") as created_date'),
                    DB::raw('DATE_FORMAT(join_date, "%d-%m-%Y") as join_date'))
                ->selectRaw("(TIMESTAMPDIFF(YEAR, DATE(users.dob), CURRENT_DATE)) AS age")
                ->where('roles.name','Member')
                ->where('payment_details.payment_type','corpus_amount');
                
                 $query->where(function ($q) use($last_year_date,$current_date) {
                    $q->whereBetween('membership_details.join_date', [$last_year_date,$current_date])
                    ->orWhere('membership_details.status', 'Probationary');
                });    

                if(!empty($params['from']) && !empty($params['to']))  {
                    $from = trim($params['from']);
                    $to = trim($params['to']);
                    $from = date('Y-m-d', strtotime($from));
                    $to = date('Y-m-d', strtotime($to));
                
                    $query->where(function ($q) use ($from,$to) {
                        $q->where('users.updated_at', '>=', $from.' 00:00:00');
                        $q->where('users.updated_at', '<=', $to.' 23:59:59');
                    });
                }
                if(!empty($params['age_from']) && !empty($params['age_to']))  {
                    $from = trim($params['age_from']);
                    $to = trim($params['age_to']);
                
                    $query->where(function ($q) use ($from,$to) {
                        $q->where('membership_details.age', '>=', $from);
                        $q->where('membership_details.age', '<=', $to);
                    });
                }
                if(!empty($params['register_id'])) {
                    $query->where('user_profiles.member_id', 'LIKE', '%'.$params['register_id'].'%');
                }
                if(!empty($params['member_phone'])) {
                    $query->where('users.mobile_number',$params['member_phone']);
                }
                if(!empty($params['member_email'])) {
                    $query->where('users.email',$params['member_email']);
                }
                if(!empty($params['member_name'])) {
                    $query->where('users.name','LIKE', '%'.$params['member_name'].'%');
                }
                if (!empty($account_id) && $role_name!='SuperAdmin') {
                    $query->where('user_accounts.account_id',$account_id);
                }

                $count_total = $query->count();
                if($search != ''){
                    $query->Where(function ($query) use ($search) {
                        $query->orWhere('users.name','LIKE' ,'%'.$search.'%')
                        ->orWhere('users.email','LIKE' , '%'.$search.'%')
                        ->orWhere('user_profiles.member_id','LIKE' , '%'.$search.'%')
                        ->orWhere('payments.amount','LIKE' , '%'.$search.'%')
                        ->orWhere('payments.payment_date','LIKE' , '%'.$search.'%')
                        ->orWhere('membership_details.join_date','LIKE' , '%'.$search.'%')
                        ->orWhere('users.mobile_number', 'LIKE' , '%'.$search.'%');
                    });
                    $count_filter = $query->count();
                }
                $query->orderBy('membership_details.created_at','DESC')->skip($start)->take($pageSize);
                if($count_filter == 0 && $search == ''){
                    $count_filter = $count_total;
                }
                $qData=$query->get();
                
                $data = DataTables::of($qData)->addColumn('action', function($data){
                    $button ='';
                    if(auth()->user()->hasPermissionTo('Edit Member'))      
                    { 
                        $button = '<a href="'.url('/')."/member/edit/".$data->id.'" class="text-primary mr-2"><i class="nav-icon i-Pen-2 font-weight-bold" data-toggle="tooltip" title="Edit Member" id="'.$data->id.'"></i></a>';
                    }
                    if(auth()->user()->hasPermissionTo('View Member'))      
                    {
                        $button .= '<a href="'.url('/')."/member/view/".$data->id.'"class="text-primary"><i class="fa fa-eye"  data-toggle="tooltip" title="View Member" id="'.$data->id.'"></i></a>';
                    }
                    if(auth()->user()->hasPermissionTo('Change Member Status'))
                    {
                        $button .= '<button class="btn mr-2 btn-primary btn-sm change_status_new" data-id="'.$data->id.'" data-status="'.$data->status.'" data-joining_date="'.$data->join_date.'">Status</button>';
                    }
                    return $button;
                })->with([
                    "recordsTotal" => $count_total,
                    "recordsFiltered" => $count_filter,
                ])
                ->skipPaging()
                ->rawColumns(['action'])
                    ->make(true);  
                Log::info('Member Repo: return probationary members data::End');
                return $data;
            }
            catch(QueryException $e){
                Log::error('Member Repo: return probationary members data::Error'.$e->getMessage());
                throw $e;
            }
    }
    //-------------------------------------------------------------------------

    /**
     *  Function to change status from Probationary to Active if membership completed one 
     *  year 
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function updateProbationaryProfiles(){
        $res=false;
        try{
            Log::info('Member Repo: updateProbationaryProfiles::Start');
            DB::beginTransaction();
            $last_year_date=date("Y-m-d", strtotime(Carbon::today()->subDays(1)->subYear()));
            
            Log::info('Member Repo: updateProbationaryProfiles::get members:Start'); 
                $probationary_members=MembershipDetails::where('status','Probationary')
                ->where('join_date',"=", $last_year_date)->get();
            Log::info('Member Repo: updateProbationaryProfiles::get members:End');
                
            $probationary_count=count($probationary_members);
            Log::info('Member Repo: updateProbationaryProfiles::total members-'.$probationary_count);  

                $status=array(
                    'status'=>'Active'
                );

                if ($probationary_count>0) {
                    foreach ($probationary_members as $key => $value) {
                        if (!empty($value)) {
                            Log::info('Member Repo:updateProbationaryProfiles:: update status of user-'.$value->user_id);
                            $update=MembershipDetails::where('user_id', $value->user_id)->update($status);
                            Log::info('Member Repo: updateProbationaryProfiles::Status Changed-'.$value->user_id);
                        }
                    }
                }
            DB::commit(); 
            $res=true;                       
            Log::info('Member Repo: updateProbationaryProfiles:End');
        }catch(QueryException $e){
            DB::rollBack();
            Log::error('Member Repo: updateProbationaryProfiles::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }
    //-------------------------------------------------------------------------


    /**
     *  Function to import bulk member data from excel sheet and store to db tables 
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function bulkDataImport(){
        $res=false;
        try{
            Log::info('Member Repo: bulkDataImport::Start');
            $path="storage/MF_FSS_Sheet.xlsx";
            $store =Excel::import(new UsersImport, $path);
                if ($store) {
                    $res=true  ;                     
                }                       
            Log::info('Member Repo: bulkDataImport:End');
        }catch(QueryException $e){
            DB::rollBack();
            Log::error('Member Repo: bulkDataImport::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }
    //-------------------------------------------------------------------------

}