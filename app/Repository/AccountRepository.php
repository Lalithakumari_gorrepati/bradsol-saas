<?php
namespace App\Repository;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\QueryException;
use Spatie\Permission\Models\Role;
use Maklad\Permission\Traits\HasRoles;
use App\User;
use App\Models\UserAccount;
use App\Models\Account;
use Auth;
use DataTables;
use App\Http\Helper\Utilities;
use Carbon\Carbon;
    /**
     * ------------------------------------------------------------------------
     *   AccountRepository class
     * ------------------------------------------------------------------------
     * Class having methods to handle requests,
     * details. This class consists of
     * methods to show request form, show request details. 
     *
     *
     * @package  App\Http\Controllers
     * @version  7.0
     * @author   BRAD Contech Solutions Pvt Ltd.
     */

class AccountRepository{

    /**
     *  To store the Account details  
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function storeAccount($data){
        $res=false;
        try{
            Log::info('Account Repo: Create Account::Start');
            
            if (!empty($data)) {
               DB::beginTransaction();
                $store = [];
                Log::info('Account Repo: To store account details::Account data preparing started');
                $store =array( 
                    'account_name'       => $data['account_name'],
                    'account_domain'     => $data['account_domain'],
                    'contact_name'       => !empty($data['contact_name'])?$data['contact_name']:'',
                    'contact_email'      => !empty($data['contact_email'])?$data['contact_email']:'',
                    'contact_phone'      => !empty($data['contact_phone'])?$data['contact_phone']:'',
                    'logo'               => !empty($data['logo'])?$data['logo']:'',
                    'additional_info' => !empty($data['additional_info'])?$data['additional_info']:'',
                    'created_at'  => date('Y-m-d h:i:s'),
                    'created_by'  => Auth::user()->email,
                );
                Log::info('Account Repo: To store account details::Account data preparing end');

                if (!empty($store)) {
                    Log::info('Account Repo: To store account details::Account data insering started');
                    
                    $account=Account::create($store); 
                    $role_id=Role::where('name','Admin')->value('id');

                    if (!empty($account)) {
                        Log::info('Account Repo:storeAccount::Account user data inserting start');
                        $user_id = User::insertGetId([
                            'name' =>$data['user_name'],
                            'email' =>$data['user_email'],
                            'mobile_number'=>$data['user_phone'],
                            'role'=>$role_id,
                            'gender'=>null,
                            'dob'=>null,
                            'password' => \Hash::make($data['password']),
                            'is_deleted'=>'N',
                            'is_active'=>'Y',
                            'created_by' => 'superadmin@gmail.com',
                            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                        ]);
                        Log::info('Account Repo:storeAccount::Account user data inserting end');

                        if (!empty($user_id)) {
                            Log::info('Account Repo: storeAccount::User account inserting start');

                            $user=User::where('id',$user_id)->first();
                            $user->assignRole('Admin');

                            $account_data=[];
                            $account_data=array( 
                                'user_id'     => $user_id,
                                'account_id'  => $account->id,
                                'created_at'  => date('Y-m-d h:i:s'),
                                'created_by'  => 'superadmin@gmail.com',
                            );
                            if (!empty($account_data)) {
                                $user=UserAccount::create($account_data); 
                                Log::info('Account Repo: storeAccount::User account inserting end');
                            }
                        }
                        Log::info('Account Repo: To store account details::Account data inserting end');   
                    }
                    DB::commit();
                    $res=true;
                }
                Log::info('Account Repo: create Account::End');  
            }
            
        }catch(QueryException $e){
            DB::rollBack();
            Log::error('Account Repo: create Account::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }
    //-------------------------------------------------------------------------

    /**
     *  To return the accounts list data
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getList($params)
    {             
        try{
            Log::info('Account Repo: return accounts list::Start');
                $search = $params->input('search.value');

                $pageSize = !empty($params['length']) ? $params['length'] : 10;
                $start = isset($params['start']) ? $params['start'] : 0;
                $count_filter = 0;
                $data  = array();
                $query = Account::selectRaw("(CASE is_active WHEN 'N' THEN 'InActive' WHEN 'Y' THEN 'Active' END) AS status,id, account_name,contact_name,contact_phone,contact_email,additional_info,created_at");
                
                $count_total = $query->count();
                if($search != ''){
                    $query->Where(function ($query) use ($search) {
                        $query->orWhere('account_name','NOT LIKE' ,'%'.$search.'%')
                        ->orWhere('contact_name','NOT LIKE' , '%'.$search.'%')
                        ->orWhere('contact_phone', 'NOT LIKE' , '%'.$search.'%')
                        ->orWhere('contact_email', 'NOT LIKE' , '%'.$search.'%')
                        ->orWhere('additional_info' , 'NOT LIKE', '%'.$search.'%');
                    });
                    $count_filter = $query->count();
                }
                $query->orderBy('created_at','desc')->skip($start)->take($pageSize);
                if($count_filter == 0 && $search == ''){
                    $count_filter = $count_total;
                }
                $qData=$query->get();
                foreach ($qData as $key => $value) {
                    $value->created_date=Utilities::formatDate($value->created_at);
                }
                $data = DataTables::of($qData)->addColumn('action', function($data){
                    $button ='';
                    if(auth()->user()->hasPermissionTo('Edit Account'))      
                    { 
                        $button = '<a href="'.url('/')."/account/edit/".$data->id.'" class="text-primary mr-2"><i class="nav-icon i-Pen-2 font-weight-bold" data-toggle="tooltip" title="Edit Account" id="'.$data->id.'"></i></a>';
                    }
                    if(auth()->user()->hasPermissionTo('View Account'))      
                    {
                        $button .= '<a href="'.url('/')."/account/view/".$data->id.'"class="text-primary"><i class="fa fa-eye"  data-toggle="tooltip" title="View Account" id="'.$data->id.'"></i></a>';
                    }
                    return $button;
                })->with([
                    "recordsTotal" => $count_total,
                    "recordsFiltered" => $count_filter,
                ])
                ->skipPaging()
                ->rawColumns(['action'])
                ->make(true);  
                Log::info('Account Repo: return accounts list::End');
                return $data;
            }
            catch(QueryException $e){
                Log::error('Account Repo: return accounts list::Error'.$e->getMessage());
                throw $e;
            }
    }
    //-------------------------------------------------------------------------


    /**
     *  Return Account Data based on id
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getAccountData($id)
    {   
        $data=[];
        try{
            Log::info('Account Repo: Get Account Data::Start');
            if (!empty($id)) {
                $data=Account::select('*')->selectRaw("(CASE is_active WHEN 'N' THEN 'InActive' WHEN 'Y' THEN 'Active' END) AS status")->
                where('id',$id)->first();
            }
  
            Log::info('Account Repo: Get Account Data::End');
            return $data;
        }
        catch(QueryException $e){
            Log::error('Account Repo: Get Account Data::Error'.$e->getMessage());
            throw $e;
        }
    }
    //-------------------------------------------------------------------------

     /**
     *  Function to update account details 
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function updateAccount($data)
    {   
        $res=false;
        try{
            Log::info('Account Repo: Update Account Data::Start');
            if (!empty($data)) {
                DB::beginTransaction();
                
                $params =array( 
                    'account_name'       => $data['account_name'],
                    'account_domain'     => $data['account_domain'],
                    'contact_name'       => $data['contact_name'],
                    'contact_email'      => $data['contact_email'],
                    'contact_phone'      => $data['contact_phone'],
                    'logo'               => $data['logo'],
                    'is_active'          => $data['status'],
                    'additional_info' => !empty($data['additional_info'])?$data['additional_info']:'',
                    'updated_at'  => date('Y-m-d h:i:s'),
                    'updated_by'  => Auth::user()->email,
                );

                $res=Account::where('id',$data['id'])->update($params);
             
                DB::commit(); 
            }
  
            Log::info('Account Repo: Update Account Data::End');
            return $res;
        }
        catch(QueryException $e){
            DB::rollBack();
            Log::error('Account Repo: Update Account Data::Error'.$e->getMessage());
            throw $e;
        }
    }
    //-------------------------------------------------------------------------

}