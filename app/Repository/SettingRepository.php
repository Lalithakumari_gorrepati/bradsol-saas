<?php
namespace App\Repository;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\QueryException;
use Auth;
use DataTables;
use App\Models\CorpusFundSetting;
use App\Models\Setting;

    /**
     * ------------------------------------------------------------------------
     *   SettingRepository class
     * ------------------------------------------------------------------------
     * Class having methods to handle requests,
     * details. This class consists of
     * methods to show request form, show request details. 
     *
     *
     * @package  App\Http\Controllers
     * @version  6.2
     * @author   BRAD Contech Solutions Pvt Ltd.
     */

class SettingRepository{

    /**
     *  To return the Corpus fund list data
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getFundsList()
    {   
        try{
            Log::info('SettingRepo: return corpus fund list data::Start');
            $qData=CorpusFundSetting::where(['is_deleted'=>'N'])->get();

            $data = DataTables::of($qData)->addColumn('action', function($data){
                    $button ='';
                     if(auth()->user()->hasPermissionTo('Edit Corpus Fund'))      
                    { 
                        $button = '<a href="'.url('/')."/corpusFund/edit/".$data->id.'" class="text-primary mr-2"><i class="nav-icon i-Pen-2 font-weight-bold" data-toggle="tooltip" title="Edit Fund" id="'.$data->id.'"></i></a>';
                    }
                    return $button;
                })->rawColumns(['action'])
                    ->make(true); 
            Log::info('SettingRepo: return corpus fund list data::End');
            return $data;
        }catch(QueryException $e){
            Log::error('SettingRepo: return corpus fund list data::Error'.$e->getMessage());
            throw $e;
        }
    }  
//-----------------------------------------------------------------------------


    /**
     *  To return the Corpus fund data based on id
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getCorpusFundsData($id)
    {   
        try{
            Log::info('SettingRepo: return specific corpus fund data::Start');
            $data=[];
            if (!empty($id)) {
                $data=CorpusFundSetting::where('id',$id)->first();
            }
             
            Log::info('SettingRepo: return specific corpus fund data::End');
            return $data;
        }catch(QueryException $e){
            Log::error('SettingRepo: return specific corpus fund data::Error'.$e->getMessage());
            throw $e;
        }
    }  
//-----------------------------------------------------------------------------

    /**
     *  To store Corpus fund data
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function storeCorpusFund($parms)
    {   
        $res=false;
        try{
            Log::info('SettingRepo: update corpus fund data::Start');
            if (!empty($parms)) {
             DB::beginTransaction();
             $data =array( 
                    'age_from'       => $parms['age_from'],
                    'age_to'       => $parms['age_to'],
                    'corpus_fund'       => $parms['corpus_fund'],
                    'created_at'  => date('Y-m-d h:i:s'),
                    'created_by'  => Auth::id(),
                );

                $res=CorpusFundSetting::insert($data);
                DB::commit(); 
            }
             
            Log::info('SettingRepo: update corpus fund data::End');
            return $res;
        }catch(QueryException $e){
            Log::error('SettingRepo: update corpus fund data::Error'.$e->getMessage());
            throw $e;
        }
    }  
//-----------------------------------------------------------------------------

    /**
     *  To update Corpus fund data
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function updateCorpusFund($parms)
    {   
        $res=false;
        try{
            Log::info('SettingRepo: update corpus fund data::Start');
            if (!empty($parms)) {
             DB::beginTransaction();
             $data =array( 
                    'age_from'       => $parms['age_from'],
                    'age_to'       => $parms['age_to'],
                    'corpus_fund'       => $parms['corpus_fund'],
                    'updated_at'  => date('Y-m-d h:i:s'),
                    'updated_by'  => Auth::id(),
                );

                $res=CorpusFundSetting::where('id',$parms['id'])->update($data);
                DB::commit(); 
            }
             
            Log::info('SettingRepo: update corpus fund data::End');
            return $res;
        }catch(QueryException $e){
            Log::error('SettingRepo: update corpus fund data::Error'.$e->getMessage());
            throw $e;
        }
    }  
//-----------------------------------------------------------------------------

    /**
     *  To return the Fraternity Contribution Settings data
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getFCData()
    {   
        try{
            Log::info('SettingRepo: return FC Settings Data::Start');
            $data=[];
            
            $scheme_name=Setting::where('setting_type','FC_Setting')->where('name','Scheme Name')->pluck('value')->first();
            $data['scheme_name']=!empty($scheme_name)?$scheme_name:''; 

            $fc_amount=Setting::where('setting_type','FC_Setting')->where('name','Fraternity Amount')->pluck('value')->first();
            $data['fc_amount']=!empty($fc_amount)?$fc_amount:'';

            $age_from=Setting::where('setting_type','FC_Setting')->where('name','Eligibility Age From')->pluck('value')->first();
            $data['age_from']=!empty($age_from)?$age_from:''; 

            $age_to=Setting::where('setting_type','FC_Setting')->where('name','Eligibility Age To')->pluck('value')->first();
            $data['age_to']=!empty($age_to)?$age_to:''; 

            $fc_first_year=Setting::where('setting_type','FC_Setting')->where('name','FC First Year')->pluck('value')->first();
            $data['fc_first_year']=!empty($fc_first_year)?$fc_first_year:'';

            $fc_second_year=Setting::where('setting_type','FC_Setting')->where('name','FC Second Year')->pluck('value')->first();
            $data['fc_second_year']=!empty($fc_second_year)?$fc_second_year:'';

            $advance_amount=Setting::where('setting_type','FC_Setting')->where('name','Advance Amount Half Yearly')->pluck('value')->first();
            $data['advance_amount']=!empty($advance_amount)?$advance_amount:'';

            $terms_conditions=Setting::where('setting_type','FC_Setting')->where('name','Terms and Conditions')->pluck('value')->first();
            $data['terms_conditions']=!empty($terms_conditions)?json_decode($terms_conditions,true):'';

            $penalty=Setting::where('setting_type','FC_Setting')->where('name','Due Penalty Per Month')->pluck('value')->first();

            $data['penalty']=!empty($penalty)?$penalty:''; 

            $laws=Setting::where('setting_type','FC_Setting')->where('name','All By Laws')->pluck('value')->first();

            $data['laws']=!empty($laws)?$laws:''; 
            
            Log::info('SettingRepo: return FC Settings Data::End');
            return $data;
        }catch(QueryException $e){
            Log::error('SettingRepo: return FC Settings Data::Error'.$e->getMessage());
            throw $e;
        }
    }  
//-----------------------------------------------------------------------------

    /**
     *  To update FC Setting data
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function updateFCSetting($params)
    {   
        $res=false;
        try{
            Log::info('SettingRepo: update FC Setting data::Start');
            if (!empty($params)) {
                    
                DB::beginTransaction();
                $filenames=[];
                $params['Terms_and_Conditions']='null';
                if(!empty($params['terms_conditions']))
                {    
                    foreach ($params['terms_conditions'] as $key => $value) {
                        if (!empty($value)) {
                            $name = time() . $value->getClientOriginalName();
                            $value->move(public_path('uploads'),$name);
                            $filenames[]=$name;
                        }
                    }
                    $params['Terms_and_Conditions']=json_encode($filenames);
                    if(!empty($params['terms_conditions1'])){
                        $updated_files=array_merge($filenames,$params['terms_conditions1']);
                        $params['Terms_and_Conditions']=json_encode($updated_files);
                    }
                }
                elseif(!empty($params['terms_conditions1'])){
                    $params['Terms_and_Conditions']=json_encode($params['terms_conditions1']);
                }
               
                foreach ($params as $key => $value) {
                    $key1=str_replace('_', ' ', $key);
                    $update_data=array('value'=>$value,'updated_by'=>Auth::user()->email);  
                    $update=Setting::where('name',$key1)->update($update_data);
                } 
                DB::commit(); 
                $res=true;
            }
            Log::info('SettingRepo: update FC Setting data::End');
        }catch(QueryException $e){
            Log::error('SettingRepo: update FC Setting data::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }  
//-----------------------------------------------------------------------------


}