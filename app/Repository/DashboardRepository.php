<?php
namespace App\Repository;

use Illuminate\Support\Facades\Log;
use Illuminate\Database\QueryException;
use App\Models\Account;
use App\Models\FCPayments;
use App\Models\Invoice;
use App\User;
use Session;
use Auth;
use Carbon\Carbon;
use DB;
    /**
     * ------------------------------------------------------------------------
     *   DashboardRepository class
     * ------------------------------------------------------------------------
     * Class having methods to handle requests,
     * details. This class consists of
     * methods to show request form, show request details. 
     *
     *
     * @package  App\Http\Controllers
     * @version  6.2
     * @author   BRAD Contech Solutions Pvt Ltd.
     */

class DashboardRepository{

    /**
     *  To return the all Admin Dashboard uses counts  
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */
   public function getCounts($params=null)
    {     
        try{
            Log::info('Dashboard Repo: get count::Start');
            $account_id=Session::get('account_id');
            $role_name=Auth::user()->roles->first()->name;

            //Accounts related count for superadmin login
            $totalAccounts = Account::where('is_deleted','N');
            $activeAccounts = Account::where('is_deleted','N')->where('is_active','Y');
            $inActiveAccounts = Account::where('is_deleted','N')->where('is_active','N');

            $data['totalAccounts'] = $totalAccounts->count();
            $data['activeAccounts'] = $activeAccounts->count();
            $data['inActiveAccounts'] = $inActiveAccounts->count();

            //Admin dashboard users count
            $query=User::leftJoin('user_accounts', 'users.id', '=','user_accounts.user_id')
               ->leftJoin('roles', 'users.role', '=', 'roles.id')
               ->where('roles.name','!=','Member');

            if (!empty($account_id) && $role_name!='SuperAdmin') {
                $query->where('user_accounts.account_id',$account_id);
            }
            $data['totalUsers']=$query->distinct('users.id')->count();
            $data['activeUsers']=$query->distinct('users.id')->where('users.is_active','Y')->count();

            $inActiveUsers=User::leftJoin('user_accounts', 'users.id', '=','user_accounts.user_id')
               ->leftJoin('roles', 'users.role', '=', 'roles.id')
               ->where('roles.name','!=','Member')
               ->distinct('users.id')
               ->where('users.is_active','N');
            $data['inActiveUsers']=$inActiveUsers->count();

            //Memeber registrations count
            $memeber=User::leftJoin('user_accounts', 'users.id', '=','user_accounts.user_id')
               ->leftJoin('roles', 'users.role', '=', 'roles.id')
               ->leftJoin('membership_details', 'users.id', '=', 'membership_details.user_id')
               ->where('roles.name','=','Member');
            
            if (!empty($account_id) && $role_name!='SuperAdmin') {
                $memeber->where('user_accounts.account_id',$account_id);
            }
            $data['totalMembers']=$memeber->distinct('users.id')->count();
            $data['activeMembers']=$memeber->where('membership_details.status','Active')->distinct('users.id')->count();

            $InactiveMemeber=User::leftJoin('user_accounts', 'users.id', '=','user_accounts.user_id')
               ->leftJoin('roles', 'users.role', '=', 'roles.id')
               ->leftJoin('membership_details', 'users.id', '=', 'membership_details.user_id')
               ->where('roles.name','=','Member')
               ->where('membership_details.status','Inactive');
            if (!empty($account_id) && $role_name!='SuperAdmin') {
                $InactiveMemeber->where('user_accounts.account_id',$account_id);
            }

            $data['inactiveMembers']=$InactiveMemeber->where('membership_details.status','Inactive')->distinct('users.id')->count();

            $expiredMembers=User::leftJoin('user_accounts', 'users.id', '=','user_accounts.user_id')
               ->leftJoin('roles', 'users.role', '=', 'roles.id')
               ->leftJoin('membership_details', 'users.id', '=', 'membership_details.user_id')
               ->where('roles.name','=','Member')
               ->where('membership_details.status','Expired');
            if (!empty($account_id) && $role_name!='SuperAdmin') {
                $expiredMembers->where('user_accounts.account_id',$account_id);
            }   
            
            $data['expiredMembers']=$expiredMembers->where('membership_details.status','Expired')->distinct('users.id')->count();

            $current_date = date("Y-m-d");
            $last_year_date=date("Y-m-d", strtotime(Carbon::today()->subYear()));
            $probationaryMembers=User::leftJoin('user_accounts', 'users.id', '=','user_accounts.user_id')
               ->leftJoin('roles', 'users.role', '=', 'roles.id')
               ->leftJoin('membership_details', 'users.id', '=', 'membership_details.user_id')
               ->where('roles.name','=','Member');
            if (!empty($account_id) && $role_name!='SuperAdmin') {
                $expiredMembers->where('user_accounts.account_id',$account_id);
            }   
            $probationaryMembers->where(function ($q) use($last_year_date,$current_date) {
                    $q->whereBetween('membership_details.join_date', [$last_year_date,$current_date])
                    ->orWhere('membership_details.status', 'Probationary');
                }); 
            $data['probationaryMembers']=$probationaryMembers->distinct('users.id')->count();

            $pending_bill = Invoice::leftJoin('users', 'invoices.member_id', '=', 'users.id')
                    ->leftJoin('user_accounts', 'invoices.member_id', '=', 'user_accounts.user_id')
                    ->leftJoin('invoice_has_fcs', 'invoices.id', '=','invoice_has_fcs.invoice_id')
                    ->leftJoin('fc_payments', function($join)
                         {
                            $join->on('invoice_has_fcs.fc_payment_id', '=', 'fc_payments.id')
                             ->where('fc_payments.payment_type','=',"fc_amount")
                             ->where('fc_payments.has_paid','=',"N");
                             $join->on('fc_payments.member_id','=','invoices.member_id');
                         })
                    ->where(DB::raw("(SELECT round(SUM(total_payment)) FROM fc_payments WHERE fc_payments.member_id = invoices.member_id AND fc_payments.invoice_id = invoices.id AND fc_payments.has_paid='N') "),'>','0')
                    ->groupBy('invoice_has_fcs.invoice_id');
            if (!empty($account_id) && $role_name!='SuperAdmin') {
                $pending_bill->where('user_accounts.account_id',$account_id);
            } 
            $pendingBills=$pending_bill->get();        
            $data['pendingBills']=count($pendingBills);        
            $pending =FCPayments::leftJoin('users', 'fc_payments.member_id', '=', 'users.id')
                    ->leftJoin('user_accounts', 'fc_payments.member_id', '=', 'user_accounts.user_id')
                    ->where('fc_payments.has_paid','N');
            if (!empty($account_id) && $role_name!='SuperAdmin') {
                $pending->where('user_accounts.account_id',$account_id);
            }
            $data['pendingPayments']=$pending->count();

            $received=FCPayments::leftJoin('users','fc_payments.member_id','=','users.id')
                    ->leftJoin('user_accounts', 'fc_payments.member_id', '=', 'user_accounts.user_id')
                    ->where('fc_payments.has_paid','Y');
            if (!empty($account_id) && $role_name!='SuperAdmin') {
                $received->where('user_accounts.account_id',$account_id);
            }
            $data['receivedPayments']=$received->count();


            $insurance=User::leftJoin('membership_details', 'users.id', '=', 'membership_details.user_id')
                ->leftJoin('roles', 'users.role', '=', 'roles.id')
                ->leftJoin('user_accounts', 'users.id', '=', 'user_accounts.user_id')
                ->leftJoin('accidental_insurances', 'users.id', '=', 'accidental_insurances.member_id')
                ->where(DB::raw("(TIMESTAMPDIFF(YEAR, DATE(users.dob), CURRENT_DATE))"),'<=','30')
                ->where('accidental_insurances.year',date('Y'))
                ->where('roles.name','Member')
                ->where('membership_details.status','!=','Expired');
            if (!empty($account_id) && $role_name!='SuperAdmin') {
                $insurance->where('user_accounts.account_id',$account_id);
            }
            $data['insurance']=$insurance->count();


            $critical_defaulter=User::leftJoin('user_profiles','users.id','=','user_profiles.user_id')
                    ->leftJoin('membership_details', 'users.id', '=', 'membership_details.user_id')
                    ->leftJoin('user_accounts', 'users.id', '=', 'user_accounts.user_id')
                    ->where(DB::raw("(SELECT round(SUM(total_payment)) FROM fc_payments WHERE fc_payments.member_id = users.id AND fc_payments.has_paid='N') "),'>','0');;

                $critical_defaulter->where(function ($q) {
                    $q->where('membership_details.status', 'Active')
                    ->orWhere('membership_details.status', 'Probationary');
                });
            if (!empty($account_id) && $role_name!='SuperAdmin') {
                $critical_defaulter->where('user_accounts.account_id',$account_id);
            }
            $data['criticalDefaulter']=$critical_defaulter->count();     

            Log::info('Dashboard Repo: get count::End');
            return $data;
        }catch(QueryException $e){
            Log::error('Dashboard Repo: get count::Error'.$e->getMessage());
            throw $e;
        }
    }  

//-----------------------------------------------------------------------------

    /**
     *  To return the all member dashboard counts  
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */
   public function getMemberCounts($params=null)
    {     
        try{
            Log::info('Dashboard Repo: getMemberCounts::Start');
            $account_id=Session::get('account_id');
            $role_name=Auth::user()->roles->first()->name;
            $id=Auth::id();

            $pendingPayments=FCPayments::where('fc_payments.member_id',$id)
                    ->where('fc_payments.payment_type','fc_amount')
                    ->where('fc_payments.has_paid','N');
            
            $clearedPayments= FCPayments::where('fc_payments.member_id',$id)
                    ->where('fc_payments.payment_type','fc_amount')
                    ->where('fc_payments.has_paid','Y');
            
            $data['pendingPayments'] = $pendingPayments->count();
            $data['clearedPayments'] = $clearedPayments->count();
            
           Log::info('Dashboard Repo: getMemberCounts::End');
            return $data;
        }catch(QueryException $e){
            Log::error('Dashboard Repo: getMemberCounts::Error'.$e->getMessage());
            throw $e;
        }
    }
//----------------------------------------------------------------------------    


}
