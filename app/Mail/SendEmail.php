<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
class SendEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $email_content;
    public $file;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email_content,$file)
    {
        $this->email_content = $email_content;
        $this->file = $file;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        try{
        Log::info('SendEmail: send email::Start');
        $send_email = false;
        if (empty($this->file)) {
            
            $send_email= $this->markdown('email.emailContent')->subject($this->email_content['subject'])
            ->with(['data'=>$this->email_content['body']]);
        }
        else{

            $file_name=!empty($this->file['file_name'])?$this->file['file_name']:'';
            $mime_type=!empty($this->file['file_mime_type'])?$this->file['file_mime_type']:'';
            $file=!empty($this->file['file'])?$this->file['file']:'';

            $subject =!empty($this->email_content['subject'])?$this->email_content['subject']:'';
            $content =!empty($this->email_content['body'])?$this->email_content['body']:'';

            if(!empty($subject) && !empty($content) && !empty($file) && !empty($file_name)  && !empty($mime_type)){

                $send_email= $this->markdown('email.emailContent')->subject($subject)->with(['data'=>$content])->attachData($file, $file_name, ['mime'=>$mime_type]);
            }
        }    
        Log::info('SendEmail: send email::End');
        return $send_email;   
        
        }catch(Exception $e) {
            Log::error('SendEmail:: Error -'.$e->getMessage());
        }
    }
}
