<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Exception;
use App\Repository\MemberRepository;
class MakeProfilesActive extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'MakeProfilesActive';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to make probationary profiles active if membership becomes one year';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(MemberRepository $memberRepo)
    {
        parent::__construct();

        $this->memberRepo = $memberRepo;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()    
    {
        $res=false;
        try {
            Log::info('Change Probationary status to Active::Start');
                $res=$this->memberRepo->updateProbationaryProfiles();
            Log::info('Change Probationary status to Active::End');
        } catch (Exception $e) {
            Log::error('Change Probationary status to Active::Error' . $e->getMessage());
        }
        return $res;
    }
}
