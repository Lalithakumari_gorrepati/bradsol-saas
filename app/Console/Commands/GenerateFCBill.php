<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Exception;
use App\Repository\PaymentsRepository;
class GenerateFCBill extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'GenerateFCBill';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Fc amount bill for all active members';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(PaymentsRepository $paymentRepo)
    {
        parent::__construct();
        $this->paymentRepo = $paymentRepo;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $res=false;
        try {
            Log::info('generate FC Bill::Start');
                $res=$this->paymentRepo->generateFcBill();
            Log::info('generate FC Bill::End');
        } catch (Exception $e) {
            Log::error('generate FC Bill::Error' . $e->getMessage());
        }
        return $res;
    }
}
