<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Exception;
use App\Repository\MemberRepository;
class BulkDataImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'BulkDataImport';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import excel sheet data into database tables';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(MemberRepository $memberRepo)
    {
        parent::__construct();
        $this->memberRepo = $memberRepo;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $res=false;
        try {
            Log::info('Import excel sheet data into DB:Start');
                $res=$this->memberRepo->bulkDataImport();
            Log::info('Import excel sheet data into DB::End');
        } catch (Exception $e) {
            Log::error('Import excel sheet data into DB:Error' . $e->getMessage());
        }
        return $res;
    }
}
