<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Exception;
use App\Repository\PaymentsRepository;

class ReminderEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ReminderEmail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To send bill payment reminder email to member after every 15 days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(PaymentsRepository $paymentRepo)
    {
        parent::__construct();
        $this->paymentRepo = $paymentRepo;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $res=false;
        try {
            Log::info('send reminder email job::Start');
                $res=$this->paymentRepo->sendReminderEmail();
            Log::info('send reminder email job::End');
        } catch (Exception $e) {
            Log::error('send reminder email::Error' . $e->getMessage());
        }
        return $res;
    }
}
