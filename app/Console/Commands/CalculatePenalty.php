<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Exception;
use App\Repository\PaymentsRepository;
class CalculatePenalty extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CalculatePenalty';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(PaymentsRepository $paymentRepo)
    {
        parent::__construct();
        $this->paymentRepo = $paymentRepo;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $res=false;
        try {
            Log::info('Calculate and Update Penalty::Start');
                $res=$this->paymentRepo->calculateUpdatePenalty();
            Log::info('Calculat and Update Penalty::End');
        } catch (Exception $e) {
            Log::error('Calculate and Update Penalty::Error' . $e->getMessage());
        }
        return $res;
    }
}
