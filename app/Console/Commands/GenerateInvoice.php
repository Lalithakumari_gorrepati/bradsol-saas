<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Exception;
use App\Repository\PaymentsRepository;
class GenerateInvoice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'GenerateInvoice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(PaymentsRepository $paymentRepo)
    {
        parent::__construct();
        $this->paymentRepo = $paymentRepo;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $res=false;
        try {
            Log::info('generate Invoice Pdf::Start');
                $res=$this->paymentRepo->generateInvoicePdf();
            Log::info('generate Invoice Pdf::End');
        } catch (Exception $e) {
            Log::error('generate Invoice Pdf::Error' . $e->getMessage());
        }
        return $res;
    }
}
