<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Exception;
use App\Repository\PaymentsRepository;
class GenerateAdvanceAmountBill extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'GenerateAdvanceAmountBill';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate advance amount bill on billing date for members in first year';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(PaymentsRepository $paymentRepo)
    {
        parent::__construct();
        $this->paymentRepo = $paymentRepo;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $res=false;
        try {
            Log::info('Generate Advance Amount Bill::Start');
                $res=$this->paymentRepo->generateAdvanceAmountBill();
            Log::info('Generate Advance Amount Bill::End');
        } catch (Exception $e) {
            Log::error('Generate Advance Amount Bill::Error' . $e->getMessage());
        }
        return $res;
    }
}
