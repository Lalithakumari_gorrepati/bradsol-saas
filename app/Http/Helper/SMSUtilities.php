<?php

namespace App\Http\Helper;
use Exception;
use Illuminate\Support\Facades\Log;

class SMSUtilities {

	public static function sendSmsNotification($data,$to){
		$res=false;

		try{
			Log::info('SMSUtilities:sendSMSNotification:: start');
			$link=SmsConstants::$LINK;
			$from=SmsConstants::$SMS_FROM;
			$user=SmsConstants::$SMS_USERNAME;
			$password=SmsConstants::$SMS_PASSWORD;
			$to = trim(str_replace('+', '', $to));

			$url=$link."username=".$user."&password=".$password."&destination=".$to."&source=".$from."&message=".urlencode($data['body'])."&type=0";
			
			$result= self::callApi($url);

			if(!empty($result)&&$result !='Invalid Parameters'){
				$res=true;
			}else{
				
				$res=false;
			}
		} catch(Exception $e) {
			Log::error('SMSUtilities:sendSMSNotification::Error'.$e->getMessage());
			$res=  false;
		}
		Log::info('SMSUtilities:sendSMSNotification:: end');
		return $res;
	}

	private static function callApi($url){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPGET, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        Log::info('curl after initialization');
        $result= curl_exec($ch);
        Log::info($result);
        Log::info('curl executed');
        Log::info('curl after initialization'.curl_error($ch));
        curl_close($ch);
        return $result;
    }

}