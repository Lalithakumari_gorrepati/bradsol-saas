<?php 
namespace App\Http\Helper;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Aws\AwsClient;
use DB;
use Carbon\Carbon;
use DateTime;
use App\User;
use App\Models\CorpusFundSetting;
use App\Models\DropdownType;
use App\Models\Dropdown;
use App\Models\Countries;
use App\Models\MembershipDetails;
use App\Models\Setting;
use App\Http\Helper\StringConstants;
use App\Models\FCPayments;
class Utilities {
		
	static function getFileStringFromFile($file){
		$data = (object) array();
		$data->body = file_get_contents($file);
		$data->extension = $file->getClientOriginalExtension();
		return $data;
	}
	
	static function getFileStringFromBase64($base64){
		$list = explode(';base64,', $base64);
		$data = (object) array();
		$data->body = base64_decode($list[1]);
		$list = explode('image/', $list[0]);
		$data->extension = $list[1];
		return $data;
	}
	
	static function GetRandomGenKey(){
		return rand(1000,15000).rand(1000,150000).time();
	}
	static function PutObjectinS3($key,$data){
		$bucket = 'jm-web-content';
		$S3Client = Utilities::getS3Client();
		$status = $S3Client->putObject(
				array(
						'Bucket' => $bucket,
						'Key' => $key,
						'Body' => $data->body,
						'ACL' => 'public-read',
						'ContentEncoding' => 'base64',
						'ContentType' => 'image/'+$data->extension,
						'Metadata' => array(
								'ContentEncoding' => 'base64',
								'ContentType' => 'image/'+$data->extension,
						)
				)
				);
		return $status;
	}
	static function DeleteObjectFromS3($key){
		$bucket = 'jm-web-content';
		if(trim($key) == '') return false;
		$S3Client = Utilities::getS3Client();
		$status = $S3Client->deleteObject(
				array(
						'Bucket' => $bucket,
						'Key' => $key
				)
				);
		return $status;
	}
	
	static function getS3Client(){
		$client = new AwsClient([
				'version' => 'latest',
				'region'  => 'ap-southeast-2',
				'defaultBucket' => 'vacancy.care',
				'Bucket' => 'vacancy.care',
				// 				'defaultAcl' => 'public-read',
				'credentials' => [
						'key'    => env('AWS_ACCESS_KEY_ID'),
						'secret' => env('AWS_SECRET_ACCESS_KEY'),
				],
		]);
		return $client;
	}
	
	static function validationFailedData($data){
		$data = array(
				'status' => 'validation_error',
				'data' => $data,
				'message' => 'validation_error',
		);
		return $data;
	}
	
	static function successData($data,$message) {
		$data = array(
				'status' => 'success',
				'data' => $data,
				'message' => $message
		);
		return $data;
	}
	
	static function errorData($data,$message=null) {
		$data = array(
				'status' => 'error',
				'data' => $data,
				'message' => $message
		);
		return $data;
	}

	static function formatDate($datetime){
		
		$date = Carbon::parse($datetime);

		$new_date=date('dS F Y', strtotime($date));
	
		return $new_date;
	}

	static function getCorpusAmount($age){

		$data=[];
		$setting=Utilities::getFCSetting();
		$eligiblity_age_from=$setting['age_from'];
		$eligiblity_age_to=$setting['age_to'];

		if (!empty($age)) {
			if ($age>=$eligiblity_age_from && $age<=$eligiblity_age_to) {
					$amount=CorpusFundSetting::where(function($query) use($age){
	                $query->where('age_from','<=', $age)
	                      ->where('age_to','>=', $age);
	            	})->value('corpus_fund');
	            	$data['status']='success';
					$data['amount']=$amount;
			}else{
				$data['status']='fail';
				$data['amount']='age must be between '.$eligiblity_age_from .' to '.$eligiblity_age_to .' years';
			}
		}
		return $data;
	}

	static function getFCAmount($id,$date){
		$fc_amount=0;
		if (!empty($id) && !empty($date)) {
			$confirm_date=date("Y-m-d", strtotime($date));
			$membership_date=MembershipDetails::where('user_id', $id)->value('join_date');
			$diff = abs(strtotime($confirm_date) - strtotime($membership_date));
			$years = floor($diff / (365*60*60*24));
			
			if ($years<1) {
				$fc_amount=Setting::where('setting_type','FC_Setting')
				           ->where('name','FC First Year')
				           ->pluck('value')->first();
   			}
			else if($years>=1 && $years <2){
				$fc_amount=Setting::where('setting_type','FC_Setting')
							->where('name','FC Second Year')
							->pluck('value')->first();
			}
			else if($years>=2){
				$fc_amount=Setting::where('setting_type','FC_Setting')
							->where('name','Fraternity Amount')
							->pluck('value')->first();
			}
		}
		return $fc_amount;
	}


	static function getPendingDue($params){
		$res=array();
		try{
			Log::info('Utilities:getPendingDue::Start');
			if (!empty($params)) {

				$id=$params['id'];
				$eligible_fc=$params['eligible_fc'];
				$advance=$params['advance'];
				$pending_due=FCPayments::where('member_id',$id)
	                     ->where('payment_type','fc_amount')
	                     ->where('has_paid','N')
	                     ->selectRaw("sum(round(total_payment)) AS amount_to_pay")
	                     ->first();
	            if (!empty($pending_due->amount_to_pay)) {
	            	$res['due']=$pending_due->amount_to_pay;   
	            }else{
	            	$res['due']='0';
	            }

	            if ($eligible_fc!='Not Eligible') {
	            	$res['released_fc']=$eligible_fc-$res['due']+$advance; 
            	}else{
            		$res['released_fc']=$advance;
            	}
			}	
    		Log::info('Utilities:getPendingDue::End');         
		}
		catch(Exception $e) {
            Log::error('Utilities:getPendingDue::Error'.$e->getMessage());
            throw $e;
        }
		return $res;
	}

	static function getDropdownValues(){

		$data['country_data'] = Countries::where('status','Active')->get(); 
                
        $Marital_id = DropdownType::where(['dropdown_name'=>'Marital Status'])->value('id');
        $data['Marital_data'] = Dropdown::where(['dropdown_id'=>$Marital_id])->where('status','Active')->get();

        $drop_id = DropdownType::where(['dropdown_name'=>'Customer Id Type'])->value('id');
        $data['customer_id_proofs'] = Dropdown::where(['dropdown_id'=>$drop_id])->where('status','Active')->get();

        $illness_id = DropdownType::where(['dropdown_name'=>'Illness Type'])->value('id');
        $data['Illness_types'] = Dropdown::where(['dropdown_id'=>$illness_id])->where('status','Active')->get(); 

        $paytype_id = DropdownType::where(['dropdown_name'=>'Payment Mode'])->value('id');
        $data['payment_modes'] = Dropdown::where(['dropdown_id'=>$paytype_id])->where('status','Active')->get();
        
        return $data;
	}

	static function getCorpusFundValues(){

		$data = CorpusFundSetting::orderBy('age_from','ASC')->get(); 
        return $data;
	}

	static function getActiveMembersData(){

		$data =User::leftJoin('membership_details', 'users.id', '=', 'membership_details.user_id')
		   ->leftJoin('user_accounts','users.id','=','user_accounts.user_id')
		   ->select('users.name','users.id','user_accounts.account_id')
		   ->where('membership_details.status','Active')->get(); 
               
        return $data;
	}

	static function getExpiredMembersData(){

		$data =User::leftJoin('membership_details', 'users.id', '=', 'membership_details.user_id')
		   ->leftJoin('user_accounts','users.id','=','user_accounts.user_id')
		   ->select('users.name','users.id','user_accounts.account_id',
		   	DB::raw('(SELECT COUNT(*) FROM fc_payments WHERE fc_payments.payment_against_member = users.id AND fc_payments.payment_type ="fc_amount" AND fc_payments.has_paid ="N") AS total_members'))
		   ->where('membership_details.status','Expired')->get(); 
               
        return $data;
	}

	static function getFCSetting(){

		$age_from=Setting::where('setting_type','FC_Setting')->where('name','Eligibility Age From')->pluck('value')->first();
        $data['age_from']=!empty($age_from)?$age_from:''; 

        $age_to=Setting::where('setting_type','FC_Setting')->where('name','Eligibility Age To')->pluck('value')->first();
        $data['age_to']=!empty($age_to)?$age_to:''; 
        
        $advance_amount=Setting::where('setting_type','FC_Setting')->where('name','Advance Amount Half Yearly')->pluck('value')->first();
        $data['advance_amount']=!empty($advance_amount)?$advance_amount:'';  

        $penalty=Setting::where('setting_type','FC_Setting')->where('name','Due Penalty Per Month')->pluck('value')->first();
        $data['penalty']=!empty($penalty)?$penalty:'';	  
        return $data;
	}

	static function billDate($date,$type){
		$data=[];
		
		if (!empty($date) && !empty($type)) {
            if ($type=='current_date') {
            	$data['bill_date']=date("Y-m-d");
            	$data['final_due_date']=date("Y-m-d", strtotime("+30 days"));
            }
			// elseif ($type=='expired_date') {
            	// $month = date('F', strtotime($date));
            	// $current_year = date('Y', strtotime($date));
            	// $last_year=$current_year-1;
				// 	$year = date('Y', strtotime($date));

	  //           $april_bill_months=StringConstants::$APRIL_BILL_MONTHS;
	  //           $october_bill_months=StringConstants::$OCTOBER_BILL_MONTHS;
	  //           if (in_array($month, $april_bill_months)) {
	  //               $bill_date=StringConstants::$FIRST_BILL_DATE;
	  //           }
	  //           else if(in_array($month,$october_bill_months)){
	  //               $bill_date=StringConstants::$SECOND_BILL_DATE;
	  //           }
	  //           if ($month=='October' ||$month=='November'||$month=='December') {
	  //               $year=$year+1;
	  //           }
	  //           $data['bill_date']=$year.'-'.$bill_date;
	  //           $billDate = Carbon::parse($data['bill_date']);
	  //           $due_date=$billDate->addDays(30);
	  //           $data['final_due_date']=date("Y-m-d", strtotime($due_date));
			// }
		}
		return $data;	
	}

	static function amountInWords($number){
	  	$no = floor($number);
	   	$point = round($number - $no, 2) * 100;
	    $hundred = null;
	    $digits_1 = strlen($no);
	    $i = 0;
	    $str = array();
	    $words = array('0' => '', '1' => 'One', '2' => 'Two',
	    '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
	    '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
	    '10' => 'Ten', '11' => 'Eleven', '12' => 'Twelve',
	    '13' => 'Thirteen', '14' => 'Fourteen',
	    '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
	    '18' => 'Eighteen', '19' =>'Nineteen', '20' => 'Twenty',
	    '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
	    '60' => 'Sixty', '70' => 'Seventy',
	    '80' => 'Eighty', '90' => 'Ninety');
	    $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');

 		
	    while ($i < $digits_1) {
	     	$divider = ($i == 2) ? 10 : 100;
	     	$number = floor($no % $divider);
	     	
	     	$no = floor($no / $divider);
	     	$i += ($divider == 10) ? 1 : 2;
	     	if ($number) {
		        $plural = (($counter = count($str)) && $number > 9) ? '' : null;
		        $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
	        	$str [] = ($number < 21) ? $words[$number] .
	            " " . $digits[$counter] . $plural . " " . $hundred
	            :
	            $words[floor($number / 10) * 10]. " " . $words[$number % 10] . " "
	            . $digits[$counter] . $plural . " " . $hundred;
	     	} else $str[] = null;
	  	}
		  $str = array_reverse($str);

		  $result = implode('', $str);
		  
	  	$amount_in_word= $result . "Rupees Only";
 		return $amount_in_word;		
}	

static function getAccountIds(){
    
    return "Hii";
}
}