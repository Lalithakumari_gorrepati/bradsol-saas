<?php

namespace App\Http\Helper;	
use App\Http\Helper\EnvConfig;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendEmail;
use App\Mail\sendEmailWithAttchment;
use Exception;

class EmailUtilities {

	public static function sendEmailNotification($data, $to_email,$file=null){
		
		try{
		Log::info('Email Utilities:sendEmailNotification:: start');
			Mail::to($to_email)->send(new SendEmail($data,$file));
	       	if (count(Mail::failures()) > 0) {
	    	   		$res=false;
	    	} else {   	
	    		$res=true;
	    	}
		} catch(Exception $e) {
			if (count(Mail::failures()) > 0) {
				$res=false;
			} else {
				$res=true;
			}
		}
		Log::info('Email Utilities:sendEmailNotification:: end');
		return $res;
    }
   	
}