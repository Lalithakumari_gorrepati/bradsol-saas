<?php
namespace App\Http\Helper;

class StringConstants {
    public static $ADMINISTRATIVE_EXPENSE='100';
    public static $CRITICAL_DEFAULTER='1500';
    public static $LATE_INTEREST='15%';
    public static $FIRST_BILL_DATE='04-01';
    public static $APRIL_BILL_MONTHS=["October","November","December","January","February","March"];
    public static $SECOND_BILL_DATE='10-01';
    public static $OCTOBER_BILL_MONTHS=["April","May","June","July","August","September"];
	
}