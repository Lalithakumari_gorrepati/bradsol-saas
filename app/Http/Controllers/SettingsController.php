<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Exception;
use App\Repository\SettingRepository;
use Validator;
use App\Http\Helper\Utilities;
use Session;
use App\Models\Account;
use Auth;
	/**
     * ------------------------------------------------------------------------
     *   SettingsController class
     * ------------------------------------------------------------------------
     * Controller having methods to handle requests,
     * details. This class consists of
     * methods to show request form, show request details. 
     *
     *
     * @package  App\Http\Controllers
     * @version  7.0
     * @author   BRAD Contech Solutions Pvt Ltd.
     */

class SettingsController extends Controller
{
   	private $settingRepo;
    
    public function __construct(SettingRepository $settingRepo){
     /**
     * Set the database connection.
     */   
        $this->settingRepo = $settingRepo;
    }

    /**
     *  return Corpus fund List View  
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function viewCorpusFundList()
    {  
        try
        {
            $account_id = Session::get('account_id');
            $account_name=Account::where('id',$account_id)
                          ->value('account_name');
            if(auth()->user()->hasPermissionTo('Corpus Fund Setting') && $account_name=='MF-FSS')      
            { 
                Log::info('Settings controller:Corpus fund List View::Entered');
                return view('corpusFund.list');  
            }  
            else{
                Log::info('Settings Controller:viewCorpusFundList::No access');
                return view('errors.no_access');
            }
        }
    
        catch (Exception $e) 
        {
            Log::error('Settings controller:Corpus fund List View::Error'.$e->getMessage());
            throw $e;
        }
    }   
    //-------------------------------------------------------------------------

    /**
     *  return Corpus fund data
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getCorpusFundData()
    {  
                         
        try
        {
            Log::info('Settings controller: To return Corpus fund Data::Start');
            	$data = $this->settingRepo->getFundsList(); 
             
            Log::info('Settings controller: To return Corpus fund Data::End');   
            return $data;
        }
    
        catch (Exception $e) 
        {
            Log::error('Settings controller: To return Corpus fund Data::Error'.$e->getMessage());
            throw $e;
        }
    }   
    //-------------------------------------------------------------------------


    /**
     *  return Corpus fund create form view  
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function createCorpusFund()
    {  
                         
        try
        {
            $account_id = Session::get('account_id');
            $account_name=Account::where('id',$account_id)
                          ->value('account_name');
            if(auth()->user()->hasPermissionTo('Add Corpus Fund') && $account_name=='MF-FSS')      
            { 
                Log::info('Settings controller:Corpus fund Create View::Entered');
            	return view('corpusFund.create'); 
            }
            else{
                Log::info('Settings Controller:createCorpusFund::No access');
                return view('errors.no_access');
            }
        }
    
        catch (Exception $e) 
        {
            Log::error('Settings controller:Corpus fund Create View::Error'.$e->getMessage());
            throw $e;
        }
    }   
    //-------------------------------------------------------------------------

    /**
     *  update Corpus fund data
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function storeCorpusFund(Request $request)
    {  
        $response='fail';                
        try
        {
            Log::info('Settings controller:Update Corpus fund::Start');
            $data=$request->all();
            $setting=Utilities::getFCSetting();
            $eligiblity_age_from=$setting['age_from'];
            $eligiblity_age_to=$setting['age_to'];

            $validator =    Validator::make($data,[
            'age_from'      => 'required|lte:age_to|unique:corpus_fund_settings,age_from|numeric|min:'.$eligiblity_age_from.'|max:'.$eligiblity_age_to,
            'age_to'        => 'required|unique:corpus_fund_settings,age_to|numeric|min:'.$eligiblity_age_from.'|max:'.$eligiblity_age_to,
            'corpus_fund'   => 'required|unique:corpus_fund_settings,corpus_fund|numeric|max:9999999',
           ],
       		[
       		'age_from.lte'  =>'The age from must be less than or equal to age to',
            'age_from.min'  =>'The age from must be between '.$eligiblity_age_from.' to '.$eligiblity_age_to,
            'age_from.max'  =>'The age from must be between '.$eligiblity_age_from.' to '.$eligiblity_age_to,
            'age_to.min'      =>' The age from must be between '.$eligiblity_age_from.' to '.$eligiblity_age_to,
            'age_to.max'      =>'The age from must be between '.$eligiblity_age_from.' to '.$eligiblity_age_to,
            'corpus_fund.max'=>'Corpus fund should not exceed maxlenth of 7 digits',
             
            ]);
            if(!$validator->fails())
            { 
            	$res = $this->settingRepo->storeCorpusFund($data);
            	if (!empty($res)) {
            	 	$response='success';
            	}
            }
         	else
            {
               return response()->json(['errors'=>$validator->errors()]);
            }
            Log::info('Settings controller:Update Corpus fund::End');   
            return $response;
        }
    
        catch (Exception $e) 
        {
            Log::error('Settings controller:Update Corpus fund::Error'.$e->getMessage());
            throw $e;
        }
    }   
    //-------------------------------------------------------------------------

    /**
     *  return Corpus fund edit view
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function editCorpusFund($id)
    {  
                         
        try
        {   
            $account_id = Session::get('account_id');
            $account_name=Account::where('id',$account_id)
                          ->value('account_name');
            if(auth()->user()->hasPermissionTo('Edit Corpus Fund') && $account_name=='MF-FSS')      
            { 
                Log::info('Settings controller:Corpus fund edit view::Start');
                $data=array();
                if (!empty($id)) {
                	$data = $this->settingRepo->getCorpusFundsData($id); 
                }
                Log::info('Settings controller:Corpus fund edit view::End');   
                return view('corpusFund.edit')->with('data',$data);
            }
            else{
                Log::info('Settings Controller:editCorpusFund::No access');
                return view('errors.no_access');
            }
        }
    
        catch (Exception $e) 
        {
            Log::error('Settings controller:Corpus fund edit view::Error'.$e->getMessage());
            throw $e;
        }
    }   
    //-------------------------------------------------------------------------

    /**
     *  update Corpus fund data
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function updateCorpusFund(Request $request)
    {  
        $response='fail';                
        try
        {
            Log::info('Settings controller:Update Corpus fund::Start');
            $data=$request->all();
            $setting=Utilities::getFCSetting();
            $eligiblity_age_from=$setting['age_from'];
            $eligiblity_age_to=$setting['age_to'];
            
            $validator =    Validator::make($data,[
            'age_from'      => 'required|lte:age_to|unique:corpus_fund_settings,age_from,'.$data['id'].'|numeric|min:'.$eligiblity_age_from.'|max:'.$eligiblity_age_to,
            'age_to'        => 'required|unique:corpus_fund_settings,age_to,'.$data['id'].'|numeric|min:'.$eligiblity_age_from.'|max:'.$eligiblity_age_to,
            'corpus_fund'   => 'required|unique:corpus_fund_settings,corpus_fund,'.$data['id'].'|numeric|max:9999999',
            
           ],
       		[
            'age_from.lte'  =>'The age from must be less than or equal to age to',
       		'age_from.min'  =>'The age from must be between '.$eligiblity_age_from.' to '.$eligiblity_age_to,
            'age_from.max'  =>'The age from must be between '.$eligiblity_age_from.' to '.$eligiblity_age_to,
            'age_to.min'      =>' The age from must be between '.$eligiblity_age_from.' to '.$eligiblity_age_to,
            'age_to.max'      =>'The age from must be between '.$eligiblity_age_from.' to '.$eligiblity_age_to,
            'corpus_fund.max'=>'Corpus fund should not exceed maxlenth of 7 digits',
            ]);
            if(!$validator->fails())
            { 
            	$res = $this->settingRepo->updateCorpusFund($data);
            	if (!empty($res)) {
            	 	$response='success';
            	}
            }
         	else
            {
               return response()->json(['errors'=>$validator->errors()]);
            }
            Log::info('Settings controller:Update Corpus fund::End');   
            return $response;
        }
    
        catch (Exception $e) 
        {
            Log::error('Settings controller:Update Corpus fund::Error'.$e->getMessage());
            throw $e;
        }
    }   
    //-------------------------------------------------------------------------

    /**
     * return Fraternity Contribution Setting view
     *
     * @access  public
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function viewFCSetting()
    {  
        try
        {
            $account_id = Session::get('account_id');
            $account_name=Account::where('id',$account_id)
                          ->value('account_name');
            if(auth()->user()->hasAnyPermission(['View FC Setting','About Scheme']) && $account_name=='MF-FSS')      
            { 
                Log::info('Settings controller:FC Setting view::Start');
                $data=array();
                
                $data = $this->settingRepo->getFCData(); 
                
                Log::info('Settings controller:FC Setting view::End');   
                return view('fcSetting.view')->with('data',$data);
            }
            else{
                Log::info('Settings Controller:viewFCSetting::No access');
                return view('errors.no_access');
            }
        }
        catch (Exception $e) 
        {
            Log::error('Settings controller:FC Setting view::Error'.$e->getMessage());
            throw $e;
        }
    }   
    //-------------------------------------------------------------------------

    /**
     * return Fraternity Contribution Setting edit form view
     *
     * @access  public
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function editFCSetting()
    {  
        try
        {
            $account_id = Session::get('account_id');
            $account_name=Account::where('id',$account_id)
                          ->value('account_name');
            if(auth()->user()->hasPermissionTo('Edit FC Setting') && $account_name=='MF-FSS')      
            { 
                Log::info('Settings controller:FC Setting edit::Start');
                $data=array();
                
                $data = $this->settingRepo->getFCData(); 
                
                Log::info('Settings controller:FC Setting edit::End');   
                return view('fcSetting.edit')->with('data',$data);
            }
            else{
                Log::info('Settings Controller:editFCSetting::No access');
                return view('errors.no_access');
            }
        }
        catch (Exception $e) 
        {
            Log::error('Settings controller:FC Setting edit::Error'.$e->getMessage());
            throw $e;
        }
    }   
    //-------------------------------------------------------------------------

}
