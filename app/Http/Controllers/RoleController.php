<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Repository\RoleRepository;
use App\Repository\RegisterRepository;
use Illuminate\Support\Facades\Log;
use Validator;
use App\User;
use DB;
use Exception;
use DataTables;
use Carbon\Carbon;
use Auth;
use App\Http\Helper\Utilities;



    /**
     * ------------------------------------------------------------------------
     *   RoleController class
     * ------------------------------------------------------------------------
     * Controller having methods to handle requests,
     * details. This class consists of
     * methods to show request form, show request details. 
     *
     *
     * @package  App\Http\Controllers
     * @version  7.0
     * @author   BRAD Contech Solutions Pvt Ltd.
     */

class RoleController extends Controller
{
    private $roleRepo;
    private $registerRepo;
    
    public function __construct(RoleRepository $roleRepo,RegisterRepository $registerRepo){
    
        $this->roleRepo = $roleRepo;
        $this->registerRepo = $registerRepo;
    }

    /**
     *  To View All The Role for List 
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

   public function getRoleList()
    {  
        try
        {
          if(auth()->user()->hasPermissionTo('Role Management'))      
          { 
            Log::info('Role Controller: View role list::Start');
              $data = $this->roleRepo->getRoleList(); 
            Log::info('Role Controller: View role list::End'); 
            return view('role.list')->with('data',$data);  
          }
          else{
            Log::info('Role Controller:getRoleList::No access');
            return view('errors.no_access');
          } 
        }
        catch (Exception $e) 
        {
            Log::error('Role controller: To View The User the List::Error'.$e->getMessage());
            throw $e;
        }
    }  
    //-------------------------------------------------------------------------

     /**
     *  To Add new Role The Form  
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */
     public function roleDataTable(Request $request)
    {  
        try
        {   
            Log::info('Role controller: To View The User the List::Start');

             $data = $this->roleRepo->roleDataTable();  
             return $data;
             Log::info('Role controller: To View The User the List::End');
        }
        catch (Exception $e) 
        {
        	Log::error('Role controller: To View The User the List::Error'.$e->getMessage());
            return response()->json(Utilities::errorData(null, "Something went wrong"));
        }
    }

    //-------------------------------------------------------------------------

     /**
     *  To Add new Role The Form  
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function createRole()
    {
      try
        {
          if(auth()->user()->hasPermissionTo('Add Roles'))      
          { 
            Log::info('Role Controller: createRole::Start');
              $data = $this->roleRepo->getPermissions();
              
              $accounts=$this->registerRepo->getAccounts();
            Log::info('Role Controller: createRole::End'); 
            	return view('role.create',compact('data','accounts')); 
          }
          else{
            Log::info('Role Controller:createRole::No access');
            return view('errors.no_access');
          } 
        }
        catch (Exception $e) 
        {
            Log::error('Role Controller: View permission list::Error'.$e->getMessage());
            throw $e;
        }
    }
    //-------------------------------------------------------------------------


     /**
     *  To Create the New Role data  
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */
    
    public function storeRole(Request $request)
    {
       try 
       {
          Log::info('Role Controller:storeRole::Start');
          $data = Validator::make($request->all(), [
          'name'     => 'required|regex:/^[a-zA-Z]{1}/|unique:roles',
          'description'    => 'required|max:100',
          'account'    => 'required',
          ], [
          'name.required'    =>'The role name field is required',
          'name.unique'    =>'The role name already exists',
          'name.regex'    =>'The role name is Invalid',
          'description.required'=>'The description field is required ',
          ])->validate();

          
          $id = $this->roleRepo->storeRole($request->all()); 

          if($id)
          {
          	session()->flash('success', 'Role has been created successfully');
          }else
	      {
	          session()->flash('fail', 'something went wrong');
	      }
          
          return redirect('role/list');
          Log::info('Role Controller:storeRole::End');
       } 
       catch (Exception $e) {
          Log::error('Role Controller:storeRole::Error'.$e->getMessage());
          throw $e;
       }
       
     }
   
     //-------------------------------------------------------------------------
 
    /**
     *  To View the Role
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function viewRole($id)
    {
      try
      {
         if(auth()->user()->hasPermissionTo('View Roles'))      
         {   
          Log::info('Role Controller: viewRole::Start');
            $data = $this->roleRepo->viewRole($id); 
            $role = $data['role'];
            $permissions= $this->roleRepo->getPermissions();
            $stored_permissions = $data['stored_permission'];
          Log::info('Role Controller: viewRole::End');

        	return view('role.view',compact('permissions','role','stored_permissions'));
       	 }
         else{
            Log::info('Role Controller:viewRole::No access');
            return view('errors.no_access');
          } 
      }
      catch (Exception $e) 
      {
          Log::error('Role Controller: Delete list::Error'.$e->getMessage());
          throw $e;
      }
    }
    //------------------------------------------------------------------------------


    /**
     *  To Edit the Roles  
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

   public function editRole($id)
    {
      try
      {
        if(auth()->user()->hasPermissionTo('Edit Roles'))      
        { 
          Log::info('Role Controller: edit role::Start');
            $data = $this->roleRepo->editRole($id); 
            
            $role = $data['role'];
            $permissions= $this->roleRepo->getPermissions();
            $stored_permissions = $data['stored_permission'];
            $accounts=$this->registerRepo->getAccounts();
          Log::info('Role Controller: edit role::End');

       		return view('role.edit',compact('permissions','role','stored_permissions','accounts'));
     	  }
        else{
            Log::info('Role Controller:editRole::No access');
            return view('errors.no_access');
        }
      }
      catch (Exception $e) 
      {
          Log::error('Role Controller: edit role::Error'.$e->getMessage());
          throw $e;
      }
    }

     //-------------------------------------------------------------------------

    /**
     *  To Update the role  
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

  public function updateRole(Request $request)
  {
    try
    {
      Log::info('Role Controller: Update role::Start');
        $data = Validator::make($request->all(), [
	          'name'           => 'required',
	          'description'    => 'required|max:100',
          	], 
         	[
	          'name.required'    =>'The role name field is required',
	          'description.regex'=>'The description field is required ',
	        ])->validate();
          
          	$is_saved = $this->roleRepo->updateRole($request->all()); 

	        if($is_saved)
	        {
	          session()->flash('success', 'Role has been updated');
	        }
	        else
	        {
	          session()->flash('fail', 'something went wrong');
	        }
        Log::info('Role Controller: Update role::End');
       	return redirect('role/list');
    }
    catch (Exception $e) 
    {
        Log::error('Register Repo: Register Userlist::Error'.$e->getMessage());
        throw $e;
    }
  }
  
  //-------------------------------------------------------------------------
  
  /**
   *  To get role permssions based on Account
   *
   * @access  public
   * @param   Illuminate\Http\Request $request
   * @return  Response
   * @author  BRAD Contech Solutions Pvt Ltd.
   */
  
  public function getAccountPermissions(Request $request)
  {
      
      try
      {
          Log::info('Role Controller:getAccountPermissions::Start');
          $data = $request->id;
          $edit_permssions=array();
           $account_permissions =array();   
          $all_permissions = $this->roleRepo->getAccountPermssion($data);
          if (count($all_permissions)>0) {
              foreach($all_permissions as $single_permissions){
                  
                  if(!empty($account_permissions[$single_permissions -> screen])){
                      array_push($account_permissions[$single_permissions -> screen], $single_permissions->name);
                  }
                  else
                      $account_permissions[$single_permissions -> screen][$single_permissions->id] = $single_permissions->name;
                      
              }
              
          }
          if(!empty($request->roleId)){
              $stored_permissions = $this->roleRepo->getEdiRolePermssion($request->roleId);
          }
              
              Log::info('Role Controller: getAccountPermissions::End');
              if(!empty($request->roleId)){
                  $edit_permssions['all_permissions']=$account_permissions;
                  $edit_permssions['stored_permissions']=$stored_permissions;
                  return $edit_permssions;
              }else
                  return $account_permissions;
      }
      catch (Exception $e)
      {
          Log::error('Register Repo: Register Userlist::Error'.$e->getMessage());
          throw $e;
      }
  }
  
  //-------------------------------------------------------------------------
  
  /**
   *  To get role permssions based on Account
   *
   * @access  public
   * @param   Illuminate\Http\Request $request
   * @return  Response
   * @author  BRAD Contech Solutions Pvt Ltd.
   */
  
  public function getEditAccountPermissions(Request $request)
  {
      
      try
      {
          Log::info('Role Controller:getAccountPermissions::Start');
          $data = $request->id;
          $account_permissions =array();
          $all_permissions = $this->roleRepo->getAccountPermssion($data);
          if (count($all_permissions)>0) {
              foreach($all_permissions as $single_permissions){
                  
                  if(!empty($account_permissions[$single_permissions -> screen])){
                      array_push($account_permissions[$single_permissions -> screen], $single_permissions->name);
                  }
                  else
                      $account_permissions[$single_permissions -> screen][$single_permissions->id] = $single_permissions->name;
                      
              }
              
          }else{
              
          }
          
          Log::info('Role Controller: getAccountPermissions::End');
          return $account_permissions;
      }
      catch (Exception $e)
      {
          Log::error('Register Repo: Register Userlist::Error'.$e->getMessage());
          throw $e;
      }
  }
  
}
//------------------------------------------------------------------------