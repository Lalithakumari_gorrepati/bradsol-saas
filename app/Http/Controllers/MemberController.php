<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Validator;
use App\User;
use Exception;
use Auth;
use App\Repository\MemberRepository;
use App\Models\Countries;
use App\Models\UserProfile;
use App\Models\States;
use App\Models\Cities;
use App\Http\Helper\Utilities;
	
	/**
     * ------------------------------------------------------------------------
     *   MemberController class
     * ------------------------------------------------------------------------
     * Controller having methods to handle requests,
     * details. This class consists of
     * methods to show request form, show request details. 
     *
     *
     * @package  App\Http\Controllers
     * @version  7.0
     * @author   BRAD Contech Solutions Pvt Ltd.
     */
class MemberController extends Controller
{
    private $memberRepo;
    
    public function __construct(MemberRepository $memberRepo){
    
        $this->memberRepo = $memberRepo;
    }

    /**
     *  return members list view
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

   public function viewMemberList()
    {  
        try
        {   
            if(auth()->user()->hasPermissionTo('Total Registrations'))      
            { 
                Log::info('MemberController: return members list view::Start');
                $data=Utilities::getCorpusFundValues();
                $advance=Utilities::getFCSetting();
                Log::info('MemberController: return members list view::End'); 
                return view('members.list',compact('data','advance'));
            }
            else{
                Log::info('MemberController:viewMemberList::No access');
                return view('errors.no_access');
            }
        }
        catch (Exception $e) 
        {
            Log::error('MemberController: return members list view::Error'.$e->getMessage());
            throw $e;
        }
    }  
    //-------------------------------------------------------------------------

   	/**
     *  Return all members data
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */
     public function getMembersData(Request $request)
    {  
        try
        {   
            Log::info('MemberController: return all members data::Start');
             	$data = $this->memberRepo->getMembersData($request);  
         	Log::info('MemberController: return all members data::End');
             return $data;
        }
        catch (Exception $e) 
        {
        	Log::error('MemberController: return all members data::Error'.$e->getMessage());
            throw $e;
        }
    }
  	//------------------------------------------------------------------------- 

  	/**
     *  return create new member form 
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function createMember()
    {
      try
        {
        	
             
            if(auth()->user()->hasPermissionTo('Add Member'))      
            { 
                Log::info('MemberController: return create member view::Start');
                $data['dropdowns']=Utilities::getDropdownValues(); 
                $setting=Utilities::getFCSetting();
                
                Log::info('MemberController: return create member view::End');
            	return view('members.create')->with('data',$data)->with('setting',$setting); 
            } 
            else{
                Log::info('MemberController:viewMemberList::No access');
                return view('errors.no_access');
            }
        }
        catch (Exception $e) 
        {
            Log::error('MemberController:return create member view::Error'.$e->getMessage());
            throw $e;
        }
    }
    //-------------------------------------------------------------------------

    /**
     *  return member view form 
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function viewMember($id)
    {
      try
        {
            Log::info('MemberController: return member view form::Start');   
            if(auth()->user()->hasPermissionTo('View Member'))      
            {   
                $data=$this->memberRepo->getMemberDataById($id);
                Log::info('MemberController: return member view form::End');
                if (!empty($data)) {
                    return view('members.view')->with('data',$data); 
                }
                else{
                    Log::info('MemberController:viewMember::Data not found');
                    abort(404);
                }
            } 
            else{
                Log::info('MemberController:viewMember::No access');
                return view('errors.no_access');
            }

        }
        catch (Exception $e) 
        {
            Log::error('MemberController:return member view form::Error'.$e->getMessage());
            throw $e;
        }
    }
    //-------------------------------------------------------------------------

    /**
     *  return member edit form
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function editMember($id)
    {
      try
        {
            Log::info('MemberController: return member edit form::Start');
             
            if(auth()->user()->hasPermissionTo('Edit Member'))      
            {   
                $data=$this->memberRepo->getMemberDataById($id);

                $setting=Utilities::getFCSetting(); 
                $advance=Utilities::getFCSetting();
                Log::info('MemberController: return member edit form::End');

                if (!empty($data)) {
                    return view('members.edit')->with('data',$data)->with('setting',$setting)->with('advance',$advance); 
                }else{
                    abort(404);
                }
            } 
            else{
                Log::info('MemberController:editMember::No access');
                return view('errors.no_access');
            }
        }
        catch (Exception $e) 
        {
            Log::error('MemberController:return member edit form::Error'.$e->getMessage());
            throw $e;
        }
    }
    //-------------------------------------------------------------------------

    /**
     * Get States of Country
     */
    public function getStatesofCountry($id){
        $response = Utilities::errorData(null, "something went wrong!");
        try{
            Log::info('MemberController:Get States of Country ::Start');
            if(!empty($id)){
                $states = States::where(['country_id'=>$id,'status'=>'Active'])->get();
                $response = Utilities::successData($states, "success");
            }
            Log::info('MemberController:Get States of Country ::End');
        }catch(Exception $e) {
            Log::error('MemberController:Get States of Country::Error'.$e->getMessage());
        }
        return response()->json($response);
    }
    
    /**
     * Get Cities of State
     */
    public function getCitiesofState($id){
        $response = Utilities::errorData(null, "something went wrong!");
        try{
            Log::info('MemberController:Get Cities of State::Start');
            if(!empty($id)){
                $cities = Cities::where(['state_id'=>$id,'status'=>'Active'])->get();
                $response = Utilities::successData($cities, "success");
            }
            Log::info('MemberController:Get Cities of State::End');
        }catch(Exception $e) {
            Log::error('MemberController:Get Cities of State::Error'.$e->getMessage());
        }
        return response()->json($response);
    }
    //-------------------------------------------------------------------------

    /**
     * Return Corpus Amount
     */
    public function getCorpusAmount(Request $request){

        $response = Utilities::errorData(null, "something went wrong!");
        try{
            $data=$request->all();
            Log::info('MemberController:Get Corpus Amount::Start');
            if(!empty($data['age'])){
                $response = Utilities::getCorpusAmount($data['age']);
            }
            Log::info('MemberController:Get Corpus Amount::End');
        }catch(Exception $e) {
            Log::error('MemberController:Get Corpus Amount::Error'.$e->getMessage());
            throw $e;
        }
        return $response;
    }
    //-------------------------------------------------------------------------

    /**
     * Function to return FC Amount
    */
    public function getFCAmount(Request $request){

        $response = Utilities::errorData(null, "something went wrong!");
        try{
            $data=$request->all();
            
            Log::info('MemberController:return FC Amount::Start');
            if(!empty($data['id']) && !empty($data['date'])){
                $response = Utilities::getFCAmount($data['id'],$data['date']);
            }
            Log::info('MemberController:return FC Amount::End');
        }catch(Exception $e) {
            Log::error('MemberController:return FC Amount::Error'.$e->getMessage());
            throw $e;
        }
        return $response;
    }
    //-------------------------------------------------------------------------

    /**
     * Function to return total pending due of member
    */
    public function getPendingDue(Request $request){

        $response = Utilities::errorData(null, "something went wrong!");
        try{
            $data=$request->all();
            
            Log::info('MemberController:return Pending Due::Start');
            if(!empty($data['id'])){
                $response = Utilities::getPendingDue($data);
            }
            Log::info('MemberController:return Pending Due::End');
        }catch(Exception $e) {
            Log::error('MemberController:return Pending Due::Error'.$e->getMessage());
            throw $e;
        }
        return $response;
    }
    //-------------------------------------------------------------------------

    /**
     *  return expired members list view
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function viewExpiredMemberList()
    {  
        try
        {
            Log::info('MemberController: return expired members list view::Start');
                                
            if (auth()->user()->hasPermissionTo('Expired members')) {

                $data=Utilities::getCorpusFundValues();
                Log::info('MemberController: return expired members list view::End');
                return view('members.expired')->with('data',$data); 
            }
            else{
                Log::info('MemberController:viewExpiredMemberList::No access');
                return view('errors.no_access');
            }
                
        }
        catch (Exception $e) 
        {
            Log::error('MemberController: return expired members list view::Error'.$e->getMessage());
            throw $e;
        }
    }  
    //-------------------------------------------------------------------------

    /**
     *  Return expired members data
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */
     public function getExpiredMembersData(Request $request)
    {  
        try
        {   
            Log::info('MemberController: return expired members data::Start');
                $data = $this->memberRepo->getExpiredMembersData($request);  
            Log::info('MemberController: return expired members data::End');
            return $data;
        }
        catch (Exception $e) 
        {
            Log::error('MemberController: return expired members data::Error'.$e->getMessage());
            throw $e;
        }
    }
    //-------------------------------------------------------------------------

    /**
     *  return Inactive members list view
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function viewInactiveMemberList()
    {  
        try
        {
            Log::info('MemberController: return Inactive members list view::Start');
            if (auth()->user()->hasPermissionTo('Inactive members')) {
                $data=Utilities::getCorpusFundValues();
                Log::info('MemberController: return Inactive members list view::End');
                return view('members.inactive')->with('data',$data); 
            }
            else{
                Log::info('MemberController:viewInactiveMemberList::No access');
                return view('errors.no_access');
            }
        }
        catch (Exception $e) 
        {
            Log::error('MemberController: return Inactive members list view::Error'.$e->getMessage());
            throw $e;
        }
    }  
    //-------------------------------------------------------------------------


    /**
     *  return active members list view
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function viewActiveMemberList()
    {  
        try
        {
            Log::info('MemberController: return Active members list view::Start');
            if (auth()->user()->hasPermissionTo('Active members')) {
                 $data=Utilities::getCorpusFundValues();
                $advance=Utilities::getFCSetting();
                Log::info('MemberController: return Active members list view::End');
                return view('members.active',compact('data','advance')); 
            }
            else{
                Log::info('MemberController:viewActiveMemberList::No access');
                return view('errors.no_access');
            }
        }
        catch (Exception $e) 
        {
            Log::error('MemberController: return Active members list view::Error'.$e->getMessage());
            throw $e;
        }
    }  
    //-------------------------------------------------------------------------

    /**
     *  Return Active members data
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */
     public function getActiveMembersData(Request $request)
    {  
        try
        {   
            Log::info('MemberController: return Active members data::Start');
                $data = $this->memberRepo->getActiveMembersData($request);  
            Log::info('MemberController: return Active members data::End');
            return $data;
        }
        catch (Exception $e) 
        {
            Log::error('MemberController: return Active members data::Error'.$e->getMessage());
            throw $e;
        }
    }
    //-------------------------------------------------------------------------

    /**
     *  Return Inactive members data
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */
     public function getInactiveMembersData(Request $request)
    {  
        try
        {   
            Log::info('MemberController: return Inactive members data::Start');
                $data = $this->memberRepo->getInactiveMembersData($request);  
            Log::info('MemberController: return Inactive members data::End');
            return $data;
        }
        catch (Exception $e) 
        {
            Log::error('MemberController: return Inactive members data::Error'.$e->getMessage());
            throw $e;
        }
    }
    //-------------------------------------------------------------------------

    /**
     *  return member profile details 
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getMemberProfile()
    {
      try
        {
            if(auth()->user()->hasPermissionTo('My Profile'))      
            {   
                Log::info('MemberController: return member edit form::Start');
                $id=Auth::user()->id;
                
                $data=$this->memberRepo->getMemberDataById($id);
                
                Log::info('MemberController: return member edit form::End');

                if (!empty($data)) {
                    return view('members.myProfile')->with('data',$data); 
                }else{
                    Log::info('MemberController:getMemberProfile::Not found');
                    abort(404);
                }
            } 
            else{
                Log::info('MemberController:getMemberProfile::No access');
                return view('errors.no_access');
            }
        }
        catch (Exception $e) 
        {
            Log::error('MemberController:return member edit form::Error'.$e->getMessage());
            throw $e;
        }
    }
    //-------------------------------------------------------------------------


    /**
     *  return Probationary members list view
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function viewProbationaryMemberList()
    {  
        try
        {
            Log::info('MemberController: return probationary members list view::Start');
                                
            if (auth()->user()->hasPermissionTo('Probationary members')) {

                $data=Utilities::getCorpusFundValues();
                $advance=Utilities::getFCSetting();
                Log::info('MemberController: return probationary members list view::End');
                return view('members.probationary')->with('data',$data)
                ->with('advance',$advance); 
            }
            else{
                Log::info('MemberController:viewProbationaryMemberList::No access');
                return view('errors.no_access');
            }
                
        }
        catch (Exception $e) 
        {
            Log::error('MemberController: return expired members list view::Error'.$e->getMessage());
            throw $e;
        }
    }  
    //-------------------------------------------------------------------------

    /**
     *  Return Probationary members data
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */
     public function getProbationaryMembersData(Request $request)
    {  
        try
        {   
            Log::info('MemberController: return Probationary members data::Start');
                $data = $this->memberRepo->getProbationaryMembersData($request);  
            Log::info('MemberController: return Probationary members data::End');
            return $data;
        }
        catch (Exception $e) 
        {
            Log::error('MemberController: return Probationary members data::Error'.$e->getMessage());
            throw $e;
        }
    }
    //-------------------------------------------------------------------------

     /*  To check duplicate values for member_id, email and phone
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function checkDuplicateValues(Request $request){
        $response = Utilities::errorData(null, "something went wrong!");
        $message = '';
        try
        {   
            
            Log::info('MemberController:To check unique validations ::Start');
                
                if (!empty($request->id)) {
                    $validator    =Validator::make($request->all(),[
                        'email'         => 'unique:users,email,'.$request->id,
                        'mobile_number' => 'unique:users,mobile_number,'.$request->id,
                    ]); 
                }else{
                     $validator    =Validator::make($request->all(),[
                        'member_id'     => 'unique:user_profiles,member_id',
                        'email'         => 'unique:users|email',
                        'mobile_number' => 'unique:users',
                    ]);   
                }
                
            
            $response = Utilities::successData(null, $validator->errors());
            Log::info('MemberController:To check unique validations ::End');
            }
         catch(QueryException $e){
            Log::error('MemberController:To check unique validations ::Error'.$e->getMessage());
         }
         return response()->json($response);
        
    }
    //-----------------------------------------------------------------------------------

}	
