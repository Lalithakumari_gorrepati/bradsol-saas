<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use App\Repository\WebNotificationRepository;
use DataTables;
use App\Models\WebNotifications;

class WebNotificationController extends Controller
{


    private $webRepo;
    
    public function __construct(WebNotificationRepository $webRepo){
     /**
     * Set the database connection. reference app\helper.php
     */   
        //selectDatabase();
        $this->webRepo = $webRepo;
    }

  /**
     *  To View All The web noti for List 
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

  

   public function webNotificationsList()
    {  
        try
        {
           Log::info('Web notification Controller: View webNotificationsList::Start');
           
           $this->webRepo->updateViewStatus();
           
           Log::info('Web notification Controller: View webNotificationsList::End'); 
            return view('web_notifications');   
        }
        catch (QueryException $e) 
        {
            Log::error('Web notification Controller: To View webNotificationsList::Error'.$e->getMessage());
            return view('errors.custom');
        }
    }  
    //-------------------------------------------------------------------------

    
    /**
     *  To get data for web noti list
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */
    
    
    
    public function webNotificationsListData()
    {
    	try
    	{
    		Log::info('Web notification Controller: View webNotificationsList::Start');
    		 
    		$params = $data = array();
    		$params = $_REQUEST;
    		$data = $this->webRepo->webNotificationsListData($params);
    		return DataTables::of($data)->addColumn('actions', function($data){
    			$actions ='';
    			if($data->read_status == 'Unread'){
                    $actions= '<input type="checkbox" class="checkbox m-auto" id="'.$data->id.'" value="'.$data->id.'" name="mark_read">';
    			}
    			return $actions;
    		})
    		->setRowClass(function ($data) {
    			return $data->read_status == 'Unread' ? 'font-weight-bold' : '';
    		})
    		->rawColumns(['actions'])
    		->make(true);
    		Log::info('Web notification Controller: View webNotificationsList::End');
    		echo $data;
    		
    	}
    	catch (QueryException $e)
    	{
    		Log::error('Web notification Controller: To View webNotificationsList::Error'.$e->getMessage());
    		return view('errors.custom');
    	}
    }
    //-------------------------------------------------------------------------
    
	public function updatewebNotifications()
    {
    	$data = array();
    	try
    	{
    		Log::info('Web notification Controller: update notification::Start');
    		$params = array();
    		$params = $_REQUEST;
    		if(!empty($params['notificationIds'])){
	    		$ids = explode(',', $params['notificationIds']);
	    		$ids = array_filter($ids);   
	    		WebNotifications::whereIn('id',$ids)->update(['read_status' => 'read']);
	          	$data['status']="success";
	          	$data['message']="Messages have been marked as Read!";
    		}
          	
          	Log::info('Web notification Controller: update notification::End');
    		
    	}
    	catch (QueryException $e)
    	{
    		Log::error('Web notification Controller: update notification::Error'.$e->getMessage());
    	}
    	return $data;
    }
    
    public function getTopFivewebNotifications()
    {
    	try
    	{
    		Log::info('Web notification Controller: getTopFivewebNotifications::Start');
    		
    		$data = $this->webRepo->getTopFivewebNotifications();
			$message = '';
    		foreach ($data as $d){
    			$message .= '<div class="text-center p-2 font-weight-bold"> <span>'.$d->message.'</span> </div> <hr style="margin: unset;">';
    		}
    		
    		
    		$data['status']="success";
    		$data['messages']=$message;
    		
    		Log::info('Web notification Controller: getTopFivewebNotifications::End');
    		return $data;
    	}
    	catch (QueryException $e)
    	{
    		Log::error('Web notification Controller: getTopFivewebNotifications::Error'.$e->getMessage());
    		return view('errors.custom');
    	}
    }
    
}
