<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Exception;
use App\Repository\AccountRepository;

	/**
     * ------------------------------------------------------------------------
     *   AccountController class
     * ------------------------------------------------------------------------
     * Controller having methods to handle requests,
     * details. This class consists of
     * methods to show request form, show request details. 
     *
     *
     * @package  App\Http\Controllers
     * @version  7.0
     * @author   BRAD Contech Solutions Pvt Ltd.
     */
class AccountController extends Controller
{
 	private $accountRepo;
    
    public function __construct(AccountRepository $accountRepo){
       
        $this->accountRepo = $accountRepo;
    }

    /**
     *  To return the create accounts form page 
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */
    public function createAccount()
    {
        if(auth()->user()->hasPermissionTo('Add Account'))      
        {
            Log::info('AccountController:viewAccountList::Entered');
            return view('accounts.create'); 
        } 
        else{
            Log::info('AccountController:viewAccountList::No access');
            return view('errors.no_access');
        }
    }
   //-------------------------------------------------------------------------


    /**
     *  To return accounts list View
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function viewAccountList()
    {                       
        try
        {   
            if(auth()->user()->hasPermissionTo('View Accounts List'))      
            {
                Log::info('AccountController:viewAccountList::Entered');
                return view('accounts.list'); 
            } 
            else{
                Log::info('AccountController:viewAccountList::No access');
                return view('errors.no_access');
            } 
        }
        catch (Exception $e) 
        {
            Log::error('Account controller: To View The User the List::Error'.$e->getMessage());
            throw $e;
        }
    }   
    //-------------------------------------------------------------------------


    /**
     *  To return accounts view details page
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function viewAccount($id)
    {  
        try
        {
            Log::info('Account Controller :Return Account View::Start');
            if(auth()->user()->hasPermissionTo('View Account'))      
            {
                $data=$this->accountRepo->getAccountData($id); 
                Log::info('Account Controller:Return Account View::End');
                if (!empty($data)) {
                   return view('accounts.view')->with('data',$data);
                }
                else{
                    Log::info('Account Controller:viewAccount::Data not found');
                    abort(404);
                } 
            }
            else{
                Log::info('Account Controller:viewAccount::No access');
                return view('errors.no_access');
            }
        }
        catch (Exception $e) 
        {
            Log::error('Account Controller: Return Account View::Error'.$e->getMessage());
            throw $e;
        }
    }   
    //-------------------------------------------------------------------------

    /**
     *  To return accounts edit details page
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function editAccount($id)
    {                   
        try
        {
            Log::info('Account Controller :Return Edit Account View::Start');
            if(auth()->user()->hasPermissionTo('Edit Account'))      
            {
                $data=$this->accountRepo->getAccountData($id); 
                Log::info('Account Controller :Return Edit Account View::End');
                if (!empty($data)) {
                   return view('accounts.edit')->with('data',$data);  
                }
                else{
                    Log::info('Account Controller:editAccount::Data not found');
                    abort(404);
                }
            }
            else{
                Log::info('Account Controller:editAccount::No access');
                return view('errors.no_access');
            }
        }
        catch (Exception $e) 
        {
            Log::error('Account Controller: Return Edit Account View::Error'.$e->getMessage());
            throw $e;
        }
    }   
    //-------------------------------------------------------------------------
}
