<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Exception;
use App\Http\Helper\Utilities;
use App\Repository\ReportsRepository;
use Auth;
class ReportsController extends Controller
{
    private $reportRepo;
    public function __construct(ReportsRepository $reportRepo){
       
        $this->reportRepo = $reportRepo;
    }

    /**
     *  return total fc's released list view
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

   public function viewTotalFCsReleased()
    {  
        try
        {
            Log::info('ReportsController:viewTotalFCs::Start');
            if (auth()->user()->hasPermissionTo('FC Released Reports')) {
                $expiredMembers=Utilities::getExpiredMembersData();
        		Log::info('ReportsController: viewTotalFCs::End'); 
  	            return view('reports.totalFCs',compact('expiredMembers')); 
            }
            else{
                Log::info('ReportsController:viewTotalFCs::No access');
                return view('errors.no_access');
            }
        }
        catch (Exception $e) 
        {
            Log::error('ReportsController:viewTotalFCs::Error'.$e->getMessage());
            throw $e;
        }
    }  
    //-------------------------------------------------------------------------

    /**
     *  Return Total FC's released data
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

   public function getFcReleasedData(Request $request)
    {  
        try
        {
            Log::info('ReportsController: getFcReleasedData::Start');
            	$data=$this->reportRepo->getTotalFcReleasedData($request->all()); 
    		Log::info('ReportsController: getFcReleasedData::End'); 
            return $data;
        }
        catch (Exception $e) 
        {
            Log::error('ReportsController: getFcReleasedData::Error'.$e->getMessage());
            throw $e;
        }
    }  
    //------------------------------------------------------------------------- 


    /**
     *  return total advance payments list view
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

   public function viewTotalAdvancePayments()
    {  
        try
        {
            Log::info('ReportsController:viewTotalAdvancePayments::Start');
            if (auth()->user()->hasPermissionTo('Total Advance Payments Reports')) {
                $data=Utilities::getCorpusFundValues();
                $advance=Utilities::getFCSetting();
                Log::info('ReportsController: viewTotalAdvancePayments::End'); 
                return view('reports.advancePayments',compact('data','advance')); 
            }
            else{
                Log::info('ReportsController:viewTotalAdvancePayments::No access');
                return view('errors.no_access');
            }
        }
        catch (Exception $e) 
        {
            Log::error('ReportsController:viewTotalAdvancePayments::Error'.$e->getMessage());
            throw $e;
        }
    }  
    //-------------------------------------------------------------------------

    /**
     *  Return Total Advance payments data 
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

   public function getTotalAdvancePaymentsData(Request $request)
    {  
        try
        {
            Log::info('ReportsController: getTotalAdvancePaymentsData::Start');
                $data=$this->reportRepo->getTotalAdvancePaymentsData($request->all()); 
            Log::info('ReportsController: getTotalAdvancePaymentsData::End'); 
            return $data;
        }
        catch (Exception $e) 
        {
            Log::error('ReportsController: getTotalAdvancePaymentsData::Error'.$e->getMessage());
            throw $e;
        }
    }  
    //-------------------------------------------------------------------------


    /**
     *  return total corpus fund list view
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

   public function viewTotalCorpusFunds()
    {  
        try
        {
            Log::info('ReportsController:viewTotalCorpusFunds::Start');
            if (auth()->user()->hasPermissionTo('Corpus Fund Reports')) {
                $data=Utilities::getCorpusFundValues();
                $advance=Utilities::getFCSetting();
                Log::info('ReportsController: viewTotalCorpusFunds::End'); 
                return view('reports.corpusFund',compact('data','advance')); 
            }
            else{
                Log::info('ReportsController:viewTotalCorpusFunds::No access');
                return view('errors.no_access');
            }
        }
        catch (Exception $e) 
        {
            Log::error('ReportsController:viewTotalCorpusFunds::Error'.$e->getMessage());
            throw $e;
        }
    }  
    //-------------------------------------------------------------------------

    /**
     *  Return Total Corpus Funds Data
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

   public function getTotalCorpusFundsData(Request $request)
    {  
        try
        {
            Log::info('ReportsController: getTotalCorpusFundsData::Start');
                $data=$this->reportRepo->getTotalCorpusFundsData($request->all()); 
            Log::info('ReportsController: getTotalCorpusFundsData::End'); 
            return $data;
        }
        catch (Exception $e) 
        {
            Log::error('ReportsController: getTotalCorpusFundsData::Error'.$e->getMessage());
            throw $e;
        }
    }  
    //-------------------------------------------------------------------------
}
