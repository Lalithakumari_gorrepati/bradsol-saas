<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use App\Http\Helper\EmailUtilities;
use App\Http\Helper\EmailConstants;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use URL;
class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;
    
    
   
    
    public function sendResetLinkEmail(Request $request)
    {
    	$this->validateEmail($request);
    	
    	$user = User::where ('email', $request->email)->first();
    	if (!$user) {
    		return redirect()->back()->withErrors(['email' => trans('The entered email is invalid')]);
    	}
    	
    	//create a new token to be sent to the user.
    	DB::table('password_resets')->insert([
    			'email' => $request->email,
    			'token' => Str::random(60), //change 60 to any length you want
    			'created_at' => Carbon::now()
    	]);
    	
    	$tokenData = DB::table('password_resets')
    	->where('email', $request->email)->first();
    	
    	$token = $tokenData->token;
    	$emailElements = array (
    			"{Name}" => $user->name,

                "{link}" => '<a href="'.URL::to('/'). '/password/reset/' . $token . '?email=' . urlencode($request->email).'">click here</a>'
    	);	
     
    	$allParams = EmailUtilities::EmailElements ( $emailElements );	
    	$templateName = EmailConstants::$Forgot_Password;

    	$emailTemplate = EmailUtilities::prepareEmail ( $templateName, $allParams );
    	$emailId=EmailUtilities::sendEmailNotification($emailTemplate,$request->email);
    	if ($emailId) {
    		EmailUtilities::saveEmailLog ($emailId,$user->id,$user->id);
    		$response='passwords.sent';
    	} 
    	return $response == Password::RESET_LINK_SENT
    	? $this->sendResetLinkResponse($request, $response)
    	: $this->sendResetLinkFailedResponse($request, $response);
    }
}
