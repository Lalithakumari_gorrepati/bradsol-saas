<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\User;
use App\Models\Account;
use Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    
    
  /**
     *  To view the content
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  redirect dashboard page
     * @author  BRAD Contech Solutions Pvt Ltd.
     */
    public function login(Request $request)
    {
    	try{
    		Log::info('Display Dashboard page after login successfull::Start');
    		$messages = [
    				'email'   => 'Invalid Username or Password.',
    		];
    		 
    		$validator = Validator::make($request->all(), User::$login_validation_rule);
    		if (!$validator->fails()) {
    			$data = $request->only('email', 'password');
    			if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'is_active' => 'Y'])) {

                    $account_id=User::leftjoin('user_accounts','users.id','user_accounts.user_id')->where('users.email',$request->email)->pluck('account_id')->first();
                    $logo=Account::where('id',$account_id)->value('logo');
                    
                    $request->session()->put('account_id', $account_id);
                    $request->session()->put('logo', $logo);
    				return redirect('/dashboard');
    			}else{
    				Log::info('Login User::Invalid Username or Password.');
    				return back()->withInput()->withErrors($messages);
    			}
    		}else{
    			Log::info('Login User::Validation Error');
    			return back()->withInput()->withErrors($validator->errors());
    		} 
    	} catch(Exception $e) {
    		Log::error('Display Dashboard page after login successfull::Error'.$e->getMessage());
    		return route('login');
    	}
    }
   
    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
    	return 'email';
    }
    
    /**
     *  To view the content
     *
     * @access  public
     * @param
     * @return  redirect dashboard page if logged out
     * @author  BRAD Contech Solutions Pvt Ltd.
     */
    public function logout()
    {
    	Auth::logout();
    	\Session::flush();
    	return redirect('/');
    }
    
    
}
