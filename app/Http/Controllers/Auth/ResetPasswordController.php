<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Helper\EmailUtilities;
use App\Http\Helper\EmailConstants;
class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
    
    public function showResetForm(Request $request, $token = null)
    {
    	return view('auth.passwords.reset')->with(
    			['token' => $token, 'email' => $request->email]
    			);
    }
    public function reset(Request $request)
    {
    	
    $request->validate($this->rules(), $this->validationErrorMessages());
    
    $password = $request->password;
    // Validate the token
    $tokenData = DB::table('password_resets')
    ->where('token', $request->token)->first();
    // Redirect the user back to the password reset request form if the token is invalid
    if (!$tokenData) return view('auth.passwords.email');
    
    $user = User::where('email', $tokenData->email)->first();
    // Redirect the user back if the email is invalid
    if (!$user) return redirect()->back()->withErrors(['email' => 'Email not found']);
    //Hash and update the new password
    $user->password = \Hash::make($password);
    $user->update(); //or $user->save();
    
    //login the user immediately they change password successfully
    return redirect('/login');
    
    //Delete the token
    DB::table('password_resets')->where('email', $user->email)
    ->delete();
    
    $emailElements = array (
    		"{Name}" => $user->name,
    );
    $allParams = EmailUtilities::EmailElements ( $emailElements );
    $templateName = EmailConstants::$Reset_Password;
    $emailTemplate = EmailUtilities::prepareEmail ( $templateName, $allParams );
    $emailId=EmailUtilities::sendEmailNotification($emailTemplate,$request->email); 
    if ($emailId) {
    	EmailUtilities::saveEmailLog ($emailId,$user->id,$user->id);
    	$response='passwords.reset';
    } 
    return $response == Password::PASSWORD_RESET
    ? $this->sendResetResponse($request, $response)
    : $this->sendResetFailedResponse($request, $response);
    
    
    }
}
