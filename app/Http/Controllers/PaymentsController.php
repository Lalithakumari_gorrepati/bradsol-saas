<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Exception;
use App\Repository\PaymentsRepository;
use App\Http\Helper\Utilities;
use App\Repository\MemberRepository;
use Auth;
use PDF;
class PaymentsController extends Controller
{
    private $paymentRepo;
    private $memberRepo;
    public function __construct(PaymentsRepository $paymentRepo,MemberRepository $memberRepo){
       
        $this->paymentRepo = $paymentRepo;
        $this->memberRepo = $memberRepo;
    }

    /**
     *  return pending payments list view
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

   public function viewPendingPayments()
    {  
        try
        {
            Log::info('PaymentsController: return pending payments list view::Start');
            if (auth()->user()->hasPermissionTo('Pending Payments')) {
                $data=Utilities::getCorpusFundValues();
                $expiredMembers=Utilities::getExpiredMembersData();
                
                $activeMembers=Utilities::getActiveMembersData();
        		Log::info('PaymentsController: viewPendingPayments::End'); 
  	            return view('payments.pending',compact('data','expiredMembers','activeMembers')); 
            }
            else{
                Log::info('PaymentsController:viewPendingPayments::No access');
                return view('errors.no_access');
            }
        }
        catch (Exception $e) 
        {
            Log::error('PaymentsController: return pending payments list view::Error'.$e->getMessage());
            throw $e;
        }
    }  
    //-------------------------------------------------------------------------

    /**
     *  Return pending payments data
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

   public function getPendingPaymentsData(Request $request)
    {  
        try
        {
            Log::info('PaymentsController: return pending payments data view::Start');
            $data=$this->paymentRepo->getPendingPaymentsData($request->all()); 
    		Log::info('PaymentsController: return pending payments data view::End'); 
            return $data;
        }
        catch (Exception $e) 
        {
            Log::error('PaymentsController: return pending payments data view::Error'.$e->getMessage());
            throw $e;
        }
    }  
    //-------------------------------------------------------------------------

    /**
     *  return pending payments list view for particular customer
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

   public function viewPaymentById($id)
    {  
        try
        {   
            if (auth()->user()->hasPermissionTo('View Payment')) {
                Log::info('PaymentsController: return payments view::Entered');
                return view('payments.view')->with('member_id',$id); 
            }
            else{
                Log::info('PaymentsController:viewPayment::No access');
                return view('errors.no_access');
            }
        }
        catch (Exception $e) 
        {
            Log::error('PaymentsController: return pending payments list view::Error'.$e->getMessage());
            throw $e;
        }
    }  
    //-------------------------------------------------------------------------

    /**
     *  return payments data based on id 
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

   public function getPaymentsData(Request $request)
    {  
        try
        {
            Log::info('PaymentsController: return payments data::Start');
                $data=$this->paymentRepo->getPaymentsData($request->all());
            Log::info('PaymentsController: getPaymentsData::End'); 
            return $data; 
        }
        catch (Exception $e) 
        {
            Log::error('PaymentsController: return pending payments list view::Error'.$e->getMessage());
            throw $e;
        }
    }  
    //-------------------------------------------------------------------------


     /**
     *  return received payments list view
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

   public function viewReceivedPayments()
    {  
        try
        {
            if (auth()->user()->hasPermissionTo('Received Payments')) {
                Log::info('PaymentsController: return received payments list view::Start');
                    $dropdowns=Utilities::getDropdownValues(); 
                    $data=Utilities::getCorpusFundValues();
                    $expiredMembers=Utilities::getExpiredMembersData();
                    
                    $activeMembers=Utilities::getActiveMembersData();
                    $pendingMembers=$this->memberRepo->getPendingAdvanceMembersData();
                    
                Log::info('PaymentsController: viewReceivedPayments::End'); 
                return view('payments.received',compact('data','expiredMembers','activeMembers','pendingMembers','dropdowns')); 
            }
            else{
                Log::info('PaymentsController:viewReceivedPayments::No access');
                return view('errors.no_access');
            }
        }
        catch (Exception $e) 
        {
            Log::error('PaymentsController: return received payments list view::Error'.$e->getMessage());
            throw $e;
        }
    }  
    //-------------------------------------------------------------------------

    /**
     *  Return received payments data
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

   public function getReceivedPaymentsData(Request $request)
    {  
        try
        {
            Log::info('PaymentsController: return received payments data::Start');
                $data=$this->paymentRepo->getReceivedPaymentsData($request->all()); 
            Log::info('PaymentsController: return received payments data::End'); 
            return $data;
        }
        catch (Exception $e) 
        {
            Log::error('PaymentsController: return received payments data::Error'.$e->getMessage());
            throw $e;
        }
    }  
    //-------------------------------------------------------------------------

    /**
     * Return pending advance amount or pending fc amount of particular member based 
     * on payment type  
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

   public function getDueAmount(Request $request)
    {  
        try
        {
            Log::info('PaymentsController: return DueAmount::Start');                
                $data=$this->paymentRepo->getDueAmount($request->all()); 
            Log::info('PaymentsController: return DueAmount::End'); 
            return $data;
        }
        catch (Exception $e) 
        {
            Log::error('PaymentsController: return DueAmount::Error'.$e->getMessage());
            throw $e;
        }
    }  
    //-------------------------------------------------------------------------

    /**
     * Return pending fc due members list against expired member  
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

   public function getMembers(Request $request)
    {  
        try
        {
            Log::info('PaymentsController: return pending fc due Member::Start');                
                $data=$this->paymentRepo->getMembers($request->all()); 
            Log::info('PaymentsController: return pending fc due Member::End'); 
            return $data;
        }
        catch (Exception $e) 
        {
            Log::error('PaymentsController: return pending fc due Member::Error'.$e->getMessage());
            throw $e;
        }
    }  
    //-------------------------------------------------------------------------

    /**
     * Return pending payments view page of particular member 
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

   public function getMemberPendingPayment()
    {  
        try
        {
            if (auth()->user()->hasPermissionTo('Member Pending Payments')) {
                Log::info('PaymentsController:getMemberPendingPayment::Start');
                $id=Auth::user()->id;
                $expiredMembers=Utilities::getExpiredMembersData();
                Log::info('PaymentsController:getMemberPendingPayment::Start');
                return view('members.pendingPayments')->with('member_id',$id)->with('expiredMembers',$expiredMembers);    
            }
            else{
                Log::info('PaymentsController:getMemberPendingPayment::No access');
                return view('errors.no_access');
            }
        }
        catch (Exception $e) 
        {
            Log::error('PaymentsController:getMemberPendingPayment::Error'.$e->getMessage());
            throw $e;
        }
    }  
    //-------------------------------------------------------------------------

    /**
     * Return cleared payments view page of particular member
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

   public function getMemberClearedPayment(Request $request)
    {  
        try
        {
            if (auth()->user()->hasPermissionTo('Member Cleared Payments')) {
                Log::info('PaymentsController:getMemberClearedPayment::start');
                $id=Auth::user()->id;
                $expiredMembers=Utilities::getExpiredMembersData();                 
                Log::info('PaymentsController:getMemberClearedPayment::end');
                return view('members.clearedPayments')->with('member_id',$id)->with('expiredMembers',$expiredMembers);    
            }
            else{
                Log::info('PaymentsController:getMemberClearedPayment::No access');
                return view('errors.no_access');
            }
        }
        catch (Exception $e) 
        {
            Log::error('PaymentsController: getMemberClearedPayment::Error'.$e->getMessage());
            throw $e;
        }
    }  
    //-------------------------------------------------------------------------

     /**
     * Return member cleared payments data 
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

   public function getMemberClearedPaymentsData(Request $request)
    {  
        try
        {
            Log::info('PaymentsController:getMemberClearedPaymentsData::Start'); 
            $params=$request->all();
            
            $data=$this->paymentRepo->getClearedPaymentsData($request->all());                  
            Log::info('PaymentsController:getMemberClearedPaymentsData::End');
            return $data; 
        }
        catch (Exception $e) 
        {
            Log::error('PaymentsController: getMemberClearedPaymentsData::Error'.$e->getMessage());
            throw $e;
        }
    }  
    //-------------------------------------------------------------------------

    /**
     * Return due penalties view page
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

   public function viewPenalties(Request $request)
    {  
        try
        {
            if (auth()->user()->hasPermissionTo('View Penalties')) {
                Log::info('PaymentsController:viewPenalties::entered');
                $dropdowns=Utilities::getDropdownValues();                      
                return view('payments.penalties')->with('dropdowns',$dropdowns);    
            }
            else{
                Log::info('PaymentsController:viewPenalties::No access');
                return view('errors.no_access');
            }
        }
        catch (Exception $e) 
        {
            Log::error('PaymentsController: viewPenalties::Error'.$e->getMessage());
            throw $e;
        }
    }  
    //-------------------------------------------------------------------------

    /**
     * Return penalties data 
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

   public function getPenaltiesData(Request $request)
    {  
        try
        {
            Log::info('PaymentsController:getPenaltiesData::Start'); 
                $data=$this->paymentRepo->getPenaltiesData($request->all());
            Log::info('PaymentsController:getPenaltiesData::End');
            return $data; 
        }
        catch (Exception $e) 
        {
            Log::error('PaymentsController: getPenaltiesData::Error'.$e->getMessage());
            throw $e;
        }
    }  
    //-------------------------------------------------------------------------


    /**
     * Return accidental insurance list view
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

   public function viewInsuranceList(Request $request)
    {  
        try
        {
            if (auth()->user()->hasPermissionTo('Accidental Insurance List')) {
                Log::info('PaymentsController:viewInsuranceList::entered');
                                     
                return view('payments.insurance_list');    
            }
            else{
                Log::info('PaymentsController:viewInsuranceList::No access');
                return view('errors.no_access');
            }
        }
        catch (Exception $e) 
        {
            Log::error('PaymentsController: viewInsuranceList::Error'.$e->getMessage());
            throw $e;
        }
    }  
    //-------------------------------------------------------------------------

    /**
     * Return accidental insurance data (list of all members whose AGE is 30 or below 30) 
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

   public function getInsuranceListData(Request $request)
    {  
        try
        {
            Log::info('PaymentsController:getInsuranceListData::Start'); 
                $data=$this->paymentRepo->getInsuranceListData($request->all());
            Log::info('PaymentsController:getInsuranceListData::End');
            return $data; 
        }
        catch (Exception $e) 
        {
            Log::error('PaymentsController: getInsuranceListData::Error'.$e->getMessage());
            throw $e;
        }
    }   
    //-------------------------------------------------------------------------

    /**
     * Return member's yearly accidental insurance data 
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

   public function getMemberInsuranceData(Request $request)
    {  
        try
        {
            Log::info('PaymentsController:getMemberInsuranceData::Start'); 
                $data=$this->paymentRepo->getMemberInsuranceData($request->all());
            Log::info('PaymentsController:getMemberInsuranceData::End');
            return $data; 
        }
        catch (Exception $e) 
        {
            Log::error('PaymentsController: getMemberInsuranceData::Error'.$e->getMessage());
            throw $e;
        }
    }   
    //-------------------------------------------------------------------------


    /**
     * Return critical defaulters list view
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

   public function viewCriticalDefaultersList(Request $request)
    {  
        try
        {
            if (auth()->user()->hasPermissionTo('Critical Defaulters')) {
                Log::info('PaymentsController:viewCriticalDefaultersList::entered');
                                     
                return view('payments.critical_defaulters');    
            }
            else{
                Log::info('PaymentsController:viewCriticalDefaultersList::No access');
                return view('errors.no_access');
            }
        }
        catch (Exception $e) 
        {
            Log::error('PaymentsController: viewCriticalDefaultersList::Error'.$e->getMessage());
            throw $e;
        }
    }  
    //-------------------------------------------------------------------------

     /**
     * Return critical defaulters list(active and probationary memebers data)
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

   public function getCriticalDefaultersData(Request $request)
    {  
        try
        {
            Log::info('PaymentsController:getCriticalDefaultersData::Start'); 
                $data=$this->paymentRepo->getCriticalDefaultersData($request->all());
            Log::info('PaymentsController:getCriticalDefaultersData::End');
            return $data; 
        }
        catch (Exception $e) 
        {
            Log::error('PaymentsController: getCriticalDefaultersData::Error'.$e->getMessage());
            throw $e;
        }
    }   
    //------------------------------------------------------------------------------


    /**
     *  Function To retuen total bills generated for member
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */
    public function getMemberInvoiceList(Request $request)
    {
        
        try{
            Log::info('PaymentsController:getMemberInvoiceList::Start');
                $data=$this->paymentRepo->getMemberInvoiceList($request->all());
            Log::info('PaymentsController:getMemberInvoiceList::End');
        } catch(QueryException $e) {
            Log::error('PaymentsController:getMemberInvoiceList::Error'.$e->getMessage());
            throw $e;
        }
        return $data;
    }
    //---------------------------------------------------------------------------------------
}
