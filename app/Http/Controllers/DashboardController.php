<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\QueryException;
use App\Repository\DashboardRepository;
use App\Repository\MemberRepository;
use App\Repository\SettingRepository;
use Auth;
class DashboardController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    private $dashboardRepo;
    private $memberRepo;
    private $settingRepo;
    public function __construct(DashboardRepository $dashboardRepo,MemberRepository $memberRepo,SettingRepository $settingRepo)
    {
        $this->dashboardRepo = $dashboardRepo;
        $this->memberRepo = $memberRepo;
        $this->settingRepo = $settingRepo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        try
        {    

            Log::info('DashboardController:Dashboard view::Start');
            $id=Auth::user()->id;
            $role_name=Auth::user()->roles->first()->name;
            if (!empty($role_name) && $role_name=='Member') {
                if (!empty($id)) {
                    $member_data=$this->memberRepo->getMemberDataById($id);
                    $counts = $this->dashboardRepo->getMemberCounts();
                    $data =$this->settingRepo->getFCData();;
                    $laws=$data['laws'];
                    if (!empty($member_data)) {
                        return view('layouts.dashboard.dashboardv2')->with('data',$member_data)->with('counts',$counts)->with('laws',$laws);   
                    }else{
                        Log::info('DashboardController:Member Dashboard view::Data not found');
                        abort(404);
                    }
                }
            }
            else{
                $data = $this->dashboardRepo->getCounts();
                Log::info('DashboardController:Dashboard view::End');
                return view('layouts.dashboard.dashboardv2')->with('data',$data);
            }
        }
        catch (QueryException $e) 
        {
            Log::error('DashboardController:Dashboard view::Error'.$e->getMessage());
            throw $e;
       }
    }
}
