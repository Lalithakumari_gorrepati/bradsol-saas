<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Maklad\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\Log;
use Validator;
use App\User;
use App\Models\Account;
use App\Repository\RegisterRepository;
use Auth;
use Exception;
use DataTables;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use App\Http\Helper\Utilities;
use Carbon\Carbon;
use Session;
    /**
     * ------------------------------------------------------------------------
     *   RegisterController class
     * ------------------------------------------------------------------------
     * Controller having methods to handle requests,
     * details. This class consists of
     * methods to show request form, show request details. 
     *
     *
     * @package  App\Http\Controllers
     * @version  6.2
     * @author   BRAD Contech Solutions Pvt Ltd.
     */

class RegisterController extends Controller
{


    private $registerRepo;
    
    
    public function __construct(RegisterRepository $registerRepo){
     /**
     * Set the database connection.
     */   
        $this->registerRepo = $registerRepo;
    }
   
     /**
     *  To Register the User Form  
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */
    public function createUser()
    {
        try
        {   
            if(auth()->user()->hasPermissionTo('Add Users'))      
            { 
                Log::info('Register controller:To Register the User Form ::Start');
                $account_id=Session::get('account_id');
                $data['roles'] = $this->registerRepo->getRole();
                $data['accounts'] = $this->registerRepo->getAccounts();
                $data['account_id'] = Session::get('account_id');

                Log::info('Register controller:To Register the User Form ::End');
                return view('user.create')->with('data',$data);
            }
            else{
                Log::info('Register Controller:createUser::No access');
                return view('errors.no_access');
            }
        }
        catch (QueryException $e) 
        {
            Log::error('Register controller:To Register the User Form ::Error'.$e->getMessage());
            throw $e;
       }
        
    }
   //-------------------------------------------------------------------------
   
   /**
     *  To Register User detail for the function 
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function storeUser(Request $request)
    {
    	$res = Utilities::errorData(null, 'Something went Wrong!');
        try
        {
            Log::info('Register controller: To Register User detail::Start');
            $data = $request->all();
            
           $validator=Validator::make($data,[
            'name'             =>   'required|regex:/^[a-zA-Z]{1}/|max:100',
            'email'            =>   'required|unique:users|email',
            'role'             =>   'required',
            'mobile_number'    =>   'required|numeric|unique:users',
            'password'         =>   'required|min:8|max:30',
            'confirmpassword'  =>   'required|same:password',
            'additional_info'  =>   'max:1000',
           ],
           ['name.required'      =>' Name is Required',
           'name.regex'      =>' The name is Invalid',
             'email.required'    =>'Email is Required',
             'name.max'          =>'Name should be less than 100 chararcter',
             'email.unique'      =>'Email Already exists',
             'mobile_number.required'    =>'Mobile Number is Required',
             'mobile_number.unique'      =>'Mobile Number Already exists ',
             'mobile_number.integer'     =>'Mobile Number should be Number',
             'role.required'     =>'Role is Required',
             'password.required' =>'Password is Required',
             'confirmpassword.required' =>'Confirm The Password',
            ]);
            if(!$validator->fails())
            {      
                $id = $this->registerRepo->storeUser($data); 
                if($id)
                {  
                   $res= response()->json(Utilities::successData(null,'success'));
                  Log::info('Register controller: To Register User detail::End');
                }
            }
            else
            {
                return response()->json(Utilities::validationFailedData($validator->errors()));
                Log::info('Register Controller: storeUser::ValidationError');
            }
            
        }
        catch (Exception $e) 
        {
            Log::error('Register controller: To Register User detail::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }

//-------------------------------------------------------------------------
   
    /**
     *  To View The User the List  
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function viewUserList()
    {                           
        try
        {
            if(auth()->user()->hasPermissionTo('User Management'))      
           {
                Log::info('Register controller: To View The User the List::Start');
                $var = $this->registerRepo->getUserList(); 
                $data = $var['data'];
                $role = $var['role']; 
                $accounts = $this->registerRepo->getAccounts(); 
                Log::info('Register controller: To View The User the List::End');   
                return view('user.list',compact('data','role','accounts')); 
            }  
            else{
                Log::info('Register Controller:viewUserList::No access');
                return view('errors.no_access');
            }
        }
    
        catch (Exception $e) 
        {
            Log::error('Register controller: To View The User the List::Error'.$e->getMessage());
            throw $e;
        }
    }   
    //-------------------------------------------------------------------------

    /**
     *  To View The User the List  
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getList(Request $request)
    {  
    try
        {  
            Log::info('Register controller: To View The User the List::Start');

            $params=$request->all();

            $data = $this->registerRepo->getList($params); 
          		
            Log::info('Register controller: To View The User the List::End');   
           
             return DataTables::of($data)->addColumn('action', function($data){
            	
            	$button ='';
                  if(auth()->user()->hasPermissionTo('Edit Users'))      
                 {  
                    if ($data->role_name=='Member') {
                     $button = '<a href="'.url('/')."/member/edit/".$data->id.'" class="text-primary mr-2"><i class="nav-icon i-Pen-2 font-weight-bold" data-toggle="tooltip" title="Edit User" id="'.$data->id.'"></i></a>';
                     }else{
                        $button = '<a href="'.url('/')."/user/edit/".$data->id.'" class="text-primary mr-2"><i class="nav-icon i-Pen-2 font-weight-bold" data-toggle="tooltip" title="Edit User" id="'.$data->id.'"></i></a>';
                     }
                    } 
                  
                    if(auth()->user()->hasPermissionTo('View Users'))      
                    { 
                    $button .= '<a href="'.url('/')."/user/view/".$data->id.'"class="text-primary"><i class="fa fa-eye"  data-toggle="tooltip" title="View User" id="'.$data->id.'"></i></a>';
                    } 
             return $button;
                    })
                    ->rawColumns(['action'])
                    ->make(true);   
        }
        catch (Exception $e) 
        {
            Log::error('Register controller: To View The User the List::Error'.$e->getMessage());
            throw $e;
        }
    }
    
//-------------------------------------------------------------------------
   /**
     *  To View User Details The Form  
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */


    public function viewUser($id)
    {
        try
        {
            if(auth()->user()->hasPermissionTo('View Users'))      
           {
                Log::info('Register controller: To View User Details ::Start');
                $var= $this->registerRepo->viewUser($id); 
                $data = $var['data'];
                
                $role = $var['role']; 
                $accounts=$this->registerRepo->getAccounts();
                $user_accounts = $var['user_accounts'];
                $oldAccounts=json_decode($user_accounts);

                Log::info('Register controller: To View User Details::End');

                return view('user.view',compact('accounts','data','role','oldAccounts'));
            }
            else{
                Log::info('Register Controller:viewUser::No access');
                return view('errors.no_access');
            }
        }
        catch (Exception $e) 
        {
            Log::error('Register controller: To View User Details::Error'.$e->getMessage());
            throw $e;
        }
       
    } 
  //-------------------------------------------------------------------------
   /**
     *  To Edit User Details   
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function editUser($id)
    {
        try
        {
            if(auth()->user()->hasPermissionTo('Edit Users'))      
           {
                Log::info('Register controller: Edit User::Start');
                $account_id=Session::get('account_id');
                $var= $this->registerRepo->viewUser($id); 
                $data = $var['data'];
                
                $role = $var['role'];
                $user_accounts = $var['user_accounts'];
                $accounts=$this->registerRepo->getAccounts();
                
                $oldAccounts=json_decode($user_accounts);

                Log::info('Register controller: Edit User::End');   

                return view('user.edit',compact('data','role','accounts','user_accounts','oldAccounts','account_id'));
            }
            else{
                Log::info('Register Controller:editUser::No access');
                return view('errors.no_access');
            }
            
        }
        catch (Exception $e) 
        {
            Log::error('Register controller: Edit User::Error'.$e->getMessage());
            throw $e;
        }
    }

    //-------------------------------------------------------------------------
   /**
     *  To updateUser the Form  
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function updateUser(Request $request)
    {
        $res = Utilities::errorData(null, 'Something went Wrong!');
        try
        {
          $data = $request->all(); 
          
         Log::info('Register controller: Update User::Start');
            $validator =Validator::make($data,[
            'name'          => 'required|regex:/^[a-zA-Z]{1}/|max:100',
            'role'          => 'required',
            'email'          =>'required|unique:users,email,'.$request['id'],
            'mobile_number' =>'required|numeric|unique:users,mobile_number,'.$request['id'],
            'additional_info'=>'max:1000'
           ],
           ['name.required'   =>' Name is Required',
            'name.regex'      =>' The name is Invalid',
            'role'            =>'Role is required',
            'mobile_number.required'  => 'Mobile Number is required',
            'mobile_number.unique'    =>'Mobile Number Already exists ',
             'mobile_number.numeric'  =>'Mobile Number should be Number',
             'email.unique'      =>'Email Already exists',
           ]);

            if(!$validator->fails())
            {      
                if(empty($data['user_status']) && !isset($data['user_status']))
                {
                    $data['is_active'] = 'N';
                }
                else{
                     $data['is_active'] = 'Y';
                }
               
                $id = $this->registerRepo->updateUser($data);
                if($id)
                {  
                   $res= response()->json(Utilities::successData(null,'success'));
                   Log::info('Register controller: To Update User detail::End');
                }
            }
            else
            {
                return response()->json(Utilities::validationFailedData($validator->errors()));
                Log::info('Register Controller: updateUser::ValidationError');
            }
        }
        catch (Exception $e) 
        {
            Log::error('Register controller: Update User::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }
    //-------------------------------------------------------------------------
    
    /**
     *  To Delete  the user   
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function deleteUser($id)
    {
        try
        {
            if(request()->ajax())
            {
                Log::info('Register Controller: Delete User::Start');
                $data = $this->registerRepo->deleteUser($id); 
                
                //web notification
                $message = 'User Status has been Changed';
                
                $user=Auth::user();
                $user_email=$user['email'];
                
                $AdminRole =Role::where('name','Admin')->select('id')->first();
                 
                Log::info('Register Controller: Delete User::End');   
                echo "Change Status Successfully"; 
            }
        }
        catch (Exception $e) 
        {
            Log::error('Register Controller: Delete User::Error'.$e->getMessage());
            throw $e;
        }
    }
    //-------------------------------------------------------------------------   

    /**
     *  To update account id in session   
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function updateSession(Request $request)
    {
        $res='fail'; 
        try
        {
            if(request()->ajax())
            {
                Log::info('Register Controller:updateSession::Start');
                if (!empty($request->id)) {

                    $logo=Account::where('id',$request->id)->value('logo');
                    
                    $request->session()->pull('account_id', 'default');
                    $request->session()->pull('logo', 'default');
                    session(['account_id' =>$request->id,'logo' =>$logo]);
                    $res='success';
                }
                                    
                Log::info('Register Controller:updateSession::End');   
            }
        }
        catch (Exception $e) 
        {
            Log::error('Register Controller:updateSession::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }
    //-------------------------------------------------------------------------   

    /**
     * Function to return user profile edit form   
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function editUserProfile()
    {
        try
        {
            if(auth()->user()->hasPermissionTo('Update Profile & Password'))      
           {
                Log::info('Register controller: Edit User::Start');
                    $id=Auth::id();
                    $account_id=Session::get('account_id');
                    $var= $this->registerRepo->viewUser($id); 
                    $data = $var['data'];
                    $user_accounts = $var['user_accounts'];
                    $accounts=$this->registerRepo->getAccounts();
                    $oldAccounts=json_decode($user_accounts);
                Log::info('Register controller: Edit User::End');   
                return view('user.edit_profile',compact('data','account_id','accounts','oldAccounts','user_accounts'));
            }
            else{
                Log::info('Register Controller:editUser::No access');
                return view('errors.no_access');
            }
        }
        catch (Exception $e) 
        {
            Log::error('Register controller: Edit User::Error'.$e->getMessage());
            throw $e;
        }
    }

      //-------------------------------------------------------------------------


    /**
     *  Function to update the user password
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */
    
    public function updateUserPassword(Request $request)
    {
        try
        {
           
            Log::info('Register controller: updateUserPassword::Start');
            $validator=Validator::make($request->all(),[
                'oldPassword'     =>'required',
                'newPassword'     =>'required|min:6|max:30',
                'confirmPassword' =>'required|min:6|max:30|required_with:newPassword|same:newPassword'
            ]);

            if(!$validator->fails())
            {
                $oldPassword = User::where('id', '=', $request['id'])->first();
                $data['password'] = \Hash::make($request['newPassword']);
                $data['updated_at'] = date('Y-m-d H:i:s');
                
                if (\Hash::check($request['oldPassword'], $oldPassword->password)) {
                    User::where('id', $request['id'])->update($data);
                    Log::info('Register controller: updateUserPassword::end');
                    return response()->json(Utilities::successData(null,'success'));
                }
                elseif($oldPassword->password==$request['oldPassword']){
                    User::where('id', $request['id'])->update($data);
                    Log::info('Register controller: updateUserPassword::end');
                    $password=$data['password'];
                    return response()->json(Utilities::successData($password,'success'));
                }else{
                    return response()->json(Utilities::validationFailedData(['oldPassword'=>['Old password does not match!']]));
                }
                
            }else{
                Log::info('Register controller: updateUserPassword::ValidationError');
                return response()->json(Utilities::validationFailedData($validator->errors()));
            }
        }
        catch (QueryException $e)
        {
            Log::error('Register controller: updateUserPassword::Error'.$e->getMessage());
            return response()->json(Utilities::errorData(null,'error'));
        }
    }
    
    //-------------------------------------------------------------------------
    
}
