<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Validator;
use App\User;
use Illuminate\Database\QueryException;
use Auth;
use App\Repository\MemberRepository;
use App\Http\Helper\Utilities;
use App\Models\Setting;
use Illuminate\Validation\Rule;
class MemberController extends Controller
{
	private $memberRepo;
    
    public function __construct(MemberRepository $memberRepo){
       
        $this->memberRepo = $memberRepo;
    }

    /**
     *  Create New Member function 
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function storeMember(Request $request)
    {
        $res = Utilities::errorData(null, 'Something went Wrong!');
        try
        {
        Log::info('Member controller:Store Member details::Start');
            $data=$request->all();
            
            $setting=Utilities::getFCSetting();
            $eligiblity_age_from=$setting['age_from'];
            $eligiblity_age_to=$setting['age_to'];
            $validator    =    Validator::make($data,[
                'name'              => 'required|regex:/^[a-zA-Z]{3}/|max:100',
                'member_id'         => 'required|unique:user_profiles,member_id',
                'marital_status'    => 'required',
                'email'             => 'required|unique:users|email',
                'mobile_number'     => 'required|numeric|unique:users',
                'country'           => 'required',
                'state'             => 'required',
                'city'              => 'required',
                'dob'               => 'required',
                'gender'            => 'required',
                'customer_id_type'  => 'required',
                'application_form'  => 'required',
                'physical_disability'=> 'required',
                'previous_illness'  => 'required',
                'health_declaration'=> 'required',
                'reports'           => Rule::requiredIf(function () use ($request) {
                    return (!empty($request->member_age) && $request->member_age >= 50)?true:false;
                    }),
                'NomineeDetails.*.nominee_name'  => 'required|regex:/^[a-zA-Z]{3}/|max:100',
                'NomineeDetails.*.nominee_phone' => 'required|numeric',
                'NomineeDetails.*.nominee_email' => 'required|max:100',
                'NomineeDetails.*.nominee_gender'=> 'required',
                'NomineeDetails.*.nominee_dob'   => 'required',
                'NomineeDetails.*.nominee_fcp'   => 'required|numeric|max:100',
                'NomineeDetails.*.nominee_relation'  => 'required|regex:/^[a-zA-Z]{3}/|max:100',
                'NomineeDetails.*.nominee_address'   => 'required|max:1000',
                'member_age'      => 'required|numeric|min:'.$eligiblity_age_from.'|max:'.$eligiblity_age_to,
                'corpus_amount'     => 'required',
                'payment_mode'      => 'required',
                'payment_date'      => 'required',
                'advance_payment_mode'=> 'required',
                'advance_payment_date'=> 'required',
                'note'              => 'max:1000',
                'interested'        => 'required',
                'additional_info'   => 'max:500',
                'member_status'     => 'required',
                'member_address'    => 'max:1000',
                'password'          => 'required|min:6|max:30',
                'confirmpassword'   => 'required|min:6|max:30',
                'membership_date'   => 'required',
           ],[
                'member_age.min'  =>'The age from must be between '.$eligiblity_age_from.' to '.$eligiblity_age_to,
                'member_age.max'  =>'The age from must be between '.$eligiblity_age_from.' to '.$eligiblity_age_to,
                'NomineeDetails.*.nominee_name.required'=>'Nominee name field is required',
                'NomineeDetails.*.nominee_phone.required'=>'Nominee phone field is required',
                'NomineeDetails.*.nominee_email.required'=>'Nominee email field is required',
                'NomineeDetails.*.nominee_gender.required'=>'Nominee gender field is required',
                'NomineeDetails.*.nominee_dob.required'      =>'Nominee dob field is required',
                'NomineeDetails.*.nominee_relation.required' =>'Nominee relation field is required',
                'NomineeDetails.*.nominee_address.required'  =>'Nominee address field is required',
                'NomineeDetails.*.nominee_name.regex'=>'Enter valid name',
                'NomineeDetails.*.nominee_relation.regex' =>'Enter valid relation',
                'NomineeDetails.*.nominee_name.regex'=>'Enter valid name',
                'NomineeDetails.*.nominee_relation.regex' =>'Enter valid relation',
                'NomineeDetails.*.nominee_phone.numeric' =>'Enter valid phone number',
                'NomineeDetails.*.nominee_name.max'=>'Nominee name can be max 100 characters',
                'NomineeDetails.*.nominee_email.max'=>'Nominee email can be max 100 characters',
                'NomineeDetails.*.nominee_relation.max'=>'Nominee relation can be max 100 characters',
                'NomineeDetails.*.nominee_address.max'=>'Nominee address can be max 1000 characters',
            
            ]);
           
            if(!$validator->fails())
            {   
                $id = $this->memberRepo->storeMember($request); 
                if ($id) {
                   $res= response()->json(Utilities::successData(null,'success'));
                   Log::info('Member Controller:Store Member details::End');
                }
            }
            else
            {
                Log::info('Member Controller: Store Member details::ValidationError');
                return response()->json(Utilities::validationFailedData($validator->errors()));
            }          
        }
        catch (QueryException $e) 
        {
            Log::error('Member controller:Store Member details::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }
    //-------------------------------------------------------------------------

    /**
     *  Update Member Personal details function
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function updatePerosnalDetails(Request $request)
    {
        $res = Utilities::errorData(null, 'Something went Wrong!');
        try
        {
        Log::info('Member controller:Update Member Personal details::Start');
            $data=$request->all();
            
            $validator=Validator::make($data,[
                    'name'              =>'required|regex:/^[ a-zA-Z][ a-zA-Z0-9_@.!#&+-]*$/|min:3|max:100',
                    'email'             =>'required|unique:users,email,'.$data['id'],

                    'alternative_email' =>'nullable',
                    'mobile_number'     =>'required|numeric|unique:users,mobile_number,'.$data['id'],
                    'alternative_number'=>['nullable','numeric'],
                    'country'           =>'required',
                    'state'             =>'required',
                    'city'              =>'required',
                    'marital_status'    =>'required',
                    'member_address'    =>'max:1000',
                    'application_form'=>Rule::requiredIf(function () use ($request) {
                    return (empty($request->application1))?true:false;
                    }),
            ],[
                'mobile_number.unique'  =>'Member mobile number already exists',
                'application_form.required'  =>'This field is required',
            ]);
            if(!$validator->fails())
            {
                $update=$this->memberRepo->updatePerosnalDetails($request);
                if ($update) {
                   $res= response()->json(Utilities::successData(null,'success'));
                   Log::info('Member Controller: To update Member Personal details::End');
                }
            }
            else{
                Log::info('Member Controller: To update Member Personal details::ValidationError');
                return response()->json(Utilities::validationFailedData($validator->errors()));
            }
        }
        catch (QueryException $e) 
        {
            Log::error('Member controller:Update Member Personal details::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }
    //-------------------------------------------------------------------------


    /**
     *  Update Member Medical details function
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function updateMedicalDetails(Request $request)
    {
        $res = Utilities::errorData(null, 'Something went Wrong!');
        try
        {
            Log::info('Member controller:Update Member medical details::Start');
                $data=$request->all();
                $validator=Validator::make($data,[
                        'physical_disability'=> 'required',
                        'previous_illness'   => 'required',
                        'health_declaration' => 'required',
                        'reports' =>Rule::requiredIf(function () use ($request) {
                                return (!empty($request->member_age) && $request->member_age >= 50&& empty($request->reports1))?true:false;}),
                    ]);
                if(!$validator->fails())
                {
                    $update=$this->memberRepo->updateMedicalDetails($request);
                    if ($update) {
                     $res= response()->json(Utilities::successData(null,'success'));
                     Log::info('Member Controller: To update Member medical details::End');
                    }
                }
                else{
                    Log::info('Member Controller: To update Member medical details::ValidationError');
                    return response()->json(Utilities::validationFailedData($validator->errors()));
                }
            }        
        
        catch (QueryException $e) 
        {
            Log::error('Member controller:Update Member medical details::Error'.$e->getMessage());
            throw $e;
        }
        return $res;    
    }
    //-------------------------------------------------------------------------

    /**
     *  Update Member Nominee details function
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function updateNomineeDetails(Request $request)
    {
        $res = Utilities::errorData(null, 'Something went Wrong!');
        try
        {
            Log::info('Member controller:Update Member nominee details::Start');
            $data=$request->all();
            $validator=Validator::make($data,[
                'NomineeDetails.*.nominee_name'  => 'required|regex:/^[a-zA-Z]{3}/|max:100',
                'NomineeDetails.*.nominee_phone' => 'required|numeric',
                'NomineeDetails.*.nominee_email' => 'required|max:100',
                'NomineeDetails.*.nominee_gender'=> 'required',
                'NomineeDetails.*.nominee_dob'   => 'required',
                'NomineeDetails.*.nominee_relation'  => 'required|regex:/^[a-zA-Z]{3}/|max:100',
                'NomineeDetails.*.nominee_address'   => 'required|max:1000',
                'NomineeDetails.*.nominee_fcp'   => 'required|numeric|max:100',
            ],[
                'NomineeDetails.*.nominee_name.required'=>'Nominee name field is required',
                'NomineeDetails.*.nominee_phone.required'=>'Nominee phone field is required',
                'NomineeDetails.*.nominee_email.required'=>'Nominee email field is required',
                'NomineeDetails.*.nominee_gender.required'=>'Nominee gender field is required',
                'NomineeDetails.*.nominee_dob.required'      =>'Nominee dob field is required',
                'NomineeDetails.*.nominee_relation.required' =>'Nominee relation field is required',
                'NomineeDetails.*.nominee_address.required'  =>'Nominee address field is required',
                'NomineeDetails.*.nominee_name.regex'=>'Enter valid name',
                'NomineeDetails.*.nominee_relation.regex' =>'Enter valid relation',
                'NomineeDetails.*.nominee_phone.numeric' =>'Enter valid phone number',
                'NomineeDetails.*.nominee_name.max'=>'Nominee name can be max 100 characters',
                'NomineeDetails.*.nominee_email.max'=>'Nominee email can be max 100 characters',
                'NomineeDetails.*.nominee_relation.max'=>'Nominee relation can be max 100 characters',
                'NomineeDetails.*.nominee_address.max'=>'Nominee address can be max 1000 characters',
            
            ]);

            if(!$validator->fails())
            {
                $update=$this->memberRepo->updateNomineeDetails($request);
                if ($update) {
                   $res= response()->json(Utilities::successData(null,'success'));
                   Log::info('Member Controller: To update Member nominee details::End');
                }
            }
            else{
                Log::info('Member Controller: To update Member nominee details::ValidationError');
                return response()->json(Utilities::validationFailedData($validator->errors()));
            }
        }
        catch (QueryException $e) 
        {
            Log::error('Member controller:Update Member details::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }
    //-------------------------------------------------------------------------

     /**
     *  Delete Particular nominee details of member based on id
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function deleteNomineeDetails($id)
    {
        $res = Utilities::errorData(null, 'Something went Wrong!');
        try
        {
            Log::info('Member controller:deleteNomineeDetails::Start');

            if(!empty($id))
            {
                $update=$this->memberRepo->deleteNomineeDetails($id);
                if ($update) {
                   $res= response()->json(Utilities::successData(null,'success'));
                   Log::info('Member Controller: To deleteNomineeDetails::End');
                }
            }
        }
        catch (QueryException $e) 
        {
            Log::error('Member controller:deleteNomineeDetails::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }
    //-------------------------------------------------------------------------

    /**
     *  Update Member Payment details function
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function updatePaymentDetails(Request $request)
    {
        $res = Utilities::errorData(null, 'Something went Wrong!');
        try
        {
            Log::info('Member Controller: To update Member payment details::Start');
                $data=$request->all();
                $setting=Utilities::getFCSetting();
                $eligiblity_age_from=$setting['age_from'];
                $eligiblity_age_to=$setting['age_to'];
                $validator    =    Validator::make($data,[
                    'member_age'      => 'required|numeric|min:'.$eligiblity_age_from.'|max:'.$eligiblity_age_to,
                    'corpus_amount'   =>'required',
                    'payment_mode'    => 'required',
                    'payment_date'    => 'required',
                    'advance_amount'  => 'required',
                    'advance_payment_mode'=> 'required',
                    'advance_payment_date'=> 'required',
                    'note'            => 'max:1000',
                ],[
                'member_age.min'  =>'The age from must be between '.$eligiblity_age_from.' to '.$eligiblity_age_to,
                'member_age.max'  =>'The age from must be between '.$eligiblity_age_from.' to '.$eligiblity_age_to,
            
            ]);
           
            if(!$validator->fails())
            {   
                $update=$this->memberRepo->updatePaymentDetails($request);
                if ($update) {
                   $res= response()->json(Utilities::successData(null,'success'));
                   Log::info('Member Controller: To update Member payment details::End');
                } 
            }
            else
            {
                Log::info('Member Controller: To update Member payment details::ValidationError');
               return response()->json(Utilities::validationFailedData($validator->errors()));
            }          
        }
        catch (QueryException $e) 
        {
            Log::error('Member controller:Update payment tab details::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }
    //-------------------------------------------------------------------------

    /**
     *  Update Member access details function
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function updateMemberDetails(Request $request)
    {
        $res = Utilities::errorData(null, 'Something went Wrong!');
        try
        {
            Log::info('Member Controller: To update Member access details::Start');
                $data=$request->all();

                $validator    =    Validator::make($data,[
                    'member_status'    =>'required',
                    'additional_info' => 'max:500',
                ]);
           
            if(!$validator->fails())
            {   
                $update=$this->memberRepo->updateMemberAccessDetails($request);
                if ($update) {
                   $res= response()->json(Utilities::successData(null,'success'));
                   Log::info('Member Controller: To update Member access details::End');
                } 
            }
            else
            {
                Log::info('Member Controller: To update Member access details::ValidationError');
               return response()->json(Utilities::validationFailedData($validator->errors()));
            }          
        }
        catch (QueryException $e) 
        {
            Log::error('Member controller:Update Member access details::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }
    //-------------------------------------------------------------------------

    /**
     *  Update Member Status function
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function updateMemberStatus(Request $request)
    {
        $res = Utilities::errorData(null, 'Something went Wrong!');
        try
        {
            Log::info('Member Controller: To update Member status::Start');
                $data=$request->all();
                
                $validator    =    Validator::make($data,[
                    'member_status'    =>'required',
                    'fc_note'    =>'max:1000',
                ]);
           
            if(!$validator->fails())
            {   
                $update=$this->memberRepo->updateMemberStatus($data);
                if ($update) {
                   $res= response()->json(Utilities::successData(null,'success'));
                   Log::info('Member Controller: To update Member status::End');
                } 
            }
            else
            {
                Log::info('Member Controller: To update Member status::ValidationError');
               return response()->json(Utilities::validationFailedData($validator->errors()));
            }          
        }
        catch (QueryException $e) 
        {
            Log::error('Member controller:Update Member status::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }
    //-------------------------------------------------------------------------


}
