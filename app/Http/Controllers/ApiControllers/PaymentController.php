<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\QueryException;
use App\Repository\PaymentsRepository;
use Validator;
use App\Http\Helper\Utilities;
use Illuminate\Validation\Rule;
class PaymentController extends Controller
{
    private $paymentRepo;
    
    public function __construct(PaymentsRepository $paymentRepo){
        
        $this->paymentRepo = $paymentRepo;
    }

    /**
     *  Store pending advance payment or fc payment of member 
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function StorePayment(Request $request)
    {
        $res = Utilities::errorData(null, 'Something went Wrong!');
        try
        {
        Log::info('PaymentController:StorePayment::Start');
            $data=$request->all();
            
            $validator    =    Validator::make($data,[
                'type_of_payment'=>'required',
                'expired_member'=>Rule::requiredIf(function () use ($request) {
                    return (!empty($request->type_of_payment) && $request->type_of_payment == 'fc_amount')?true:false;
                    }),
                'member_name'=>Rule::requiredIf(function () use ($request) {
                    return (!empty($request->type_of_payment) && $request->type_of_payment == 'fc_amount')?true:false;}),
                'advance_member_name'=>Rule::requiredIf(function () use ($request) {
                    return (!empty($request->type_of_payment) && $request->type_of_payment == 'advance')?true:false;}),
                'payment_mode'=>'required',
                'payment_date'=>'required',
                'amount'=>'required',
                'payment_note'=>'max:500',
            ]);
           
            if(!$validator->fails())
            {   
                $id = $this->paymentRepo->storePayment($request->all()); 
                if ($id) {
                   $res= response()->json(Utilities::successData(null,'success'));
                   Log::info('PaymentController:StorePayment::End');
                }
            }
            else
            {
                Log::info('PaymentController: StorePayment::ValidationError');
                return response()->json(Utilities::validationFailedData($validator->errors()));
            }          
        }
        catch (QueryException $e) 
        {
            Log::error('PaymentController:StorePayment::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }
    //-------------------------------------------------------------------------

    /**
     * Update member accidental insurance status with payment date
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

   public function updateInsurance(Request $request)
    {  
        $res = Utilities::errorData(null, 'Something went Wrong!');
        try
        {
            Log::info('PaymentsController:updateInsurance::Start'); 
            $data=$request->all();
            $validator    =    Validator::make($data,[
                'insurance_year'=>'required',
                'status'=>'required',
                'payment_date'=>'required',
            ]);

            if(!$validator->fails())
            {   
                $id = $this->paymentRepo->updateInsurance($request->all()); 
                if ($id) {
                   $res= response()->json(Utilities::successData(null,'success'));
                   Log::info('PaymentController:updateInsurance::End');
                }
            }
            else
            {
                Log::info('PaymentController: StorePayment::ValidationError');
                return response()->json(Utilities::validationFailedData($validator->errors()));
            } 
            return $res; 
        }
        catch (Exception $e) 
        {
            Log::error('PaymentsController: updateInsurance::Error'.$e->getMessage());
            throw $e;
        }
    }   
    //-------------------------------------------------------------------------


    /**
     *  Store penalty payment and update penalty status
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function storePenalty(Request $request)
    {
        $res = Utilities::errorData(null, 'Something went Wrong!');
        try
        {
        Log::info('PaymentController:StorePenalty::Start');
            $data=$request->all();
            
            $validator    =    Validator::make($data,[
                'payment_mode'=>'required',
                'payment_date'=>'required',
                'amount'=>'required',
            ]);
           
            if(!$validator->fails())
            {   
                $id = $this->paymentRepo->storePenalty($request->all()); 
                if ($id) {
                   $res= response()->json(Utilities::successData(null,'success'));
                   Log::info('PaymentController:StorePenalty::End');
                }
            }
            else
            {
                Log::info('PaymentController: StorePenalty::ValidationError');
                return response()->json(Utilities::validationFailedData($validator->errors()));
            }          
        }
        catch (QueryException $e) 
        {
            Log::error('PaymentController:StorePenalty::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }
    //-------------------------------------------------------------------------
}
