<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\QueryException;
use Auth;
use Validator;
use App\Repository\SettingRepository;
use App\Http\Helper\Utilities;
	/**
     * ------------------------------------------------------------------------
     *   SettingController class
     * ------------------------------------------------------------------------
     * Controller having methods to handle requests,
     * details. This class consists of
     * methods to store, update request details. 
     *
     *
     * @package  App\Http\Controllers
     * @version  7.0
     * @author   BRAD Contech Solutions Pvt Ltd.
     */
class SettingController extends Controller
{
    private $settingRepo;
    
    public function __construct(SettingRepository $settingRepo){
        
        $this->settingRepo = $settingRepo;
    }

    /**
     *  update Fraternity Settings data
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function updateFCSetting(Request $request)
    {  
        $res = Utilities::errorData(null, 'Something went Wrong!');              
        try
        {
            Log::info('Setting controller:Update FC Setting::Start');
            $data=$request->all();
           	
            $validator = Validator::make($data,[
	            'Scheme_Name' 	 		=> 'required|regex:/^[a-zA-Z]{3}/|max:100',
	            'Eligibility_Age_From'  => 'required|lte:Eligibility_Age_To|numeric',
	            'Eligibility_Age_To'    => 'required|numeric',
	            'Fraternity_Amount'   	=> 'required|numeric|max:99999999',
	            'FC_First_Year'  		=> 'required',
	            'FC_Second_Year' 		=> 'required|numeric|max:99999999',
	            'Advance_Amount_Half_Yearly' => 'required|numeric|max:99999999',
                'Due_Penalty_Per_Month' => 'required|numeric|max:999999',
                'All_By_Laws'           => 'required|max:3000',
            ],
       		[
	            'Eligibility_Age_From.lte'=>'The eligibility age from must be less than or equal to age to',
                'Fraternity_Amount.max'=>'Fraternity Amount should not exceed maxlenth of 8 digits',
	            'FC_Second_Year.max'=>'FC Amount should not exceed maxlenth of 8 digits',
	            'Advance_Amount_Half_Yearly.max'=>'Advance Amount should not exceed maxlenth of 8 digits',
                'Due_Penalty_Per_Month.max'=>'Due Penalty Per Month should not exceed maxlenth of 6 digits',
            ]);
            if(!$validator->fails())
            { 
            	$update = $this->settingRepo->updateFCSetting($data);
                if ($update) {
                   $res= response()->json(Utilities::successData(null,'success'));
                   Log::info('Setting Controller:Update FC Setting::End');
                }
            }
         	else
            {
        	   Log::info('Setting Controller:Update FC Setting::ValidationError');
               return response()->json(Utilities::validationFailedData($validator->errors()));
            }
        }
    
        catch (QueryException $e) 
        {
            Log::error('Settings controller:Update Corpus fund::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }   
    //-------------------------------------------------------------------------
}
