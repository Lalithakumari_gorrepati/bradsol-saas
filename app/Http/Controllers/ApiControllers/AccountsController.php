<?php

namespace App\Http\Controllers\ApiControllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\QueryException;
use App\Repository\AccountRepository;
use Validator;
use App\Http\Helper\Utilities;
    /**
     * ------------------------------------------------------------------------
     *   AccountsController class
     * ------------------------------------------------------------------------
     * Controller having methods to handle requests,
     * details. This class consists of
     * methods to show request form, show request details. 
     *
     *
     * @package  App\Http\Controllers
     * @version  6.2
     * @author   BRAD Contech Solutions Pvt Ltd.
     */
class AccountsController extends Controller
{
 	private $accountRepo;
    
    public function __construct(AccountRepository $accountRepo){
       
        $this->accountRepo = $accountRepo;
    }


    /**
     *  Return accounts List  
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function getAccountsList(Request $request)
    {  
        try
        {  
            Log::info('AccountController: To return Accounts List::Start');

            $params=$request->all();
           
            $data = $this->accountRepo->getList($request); 
                
            Log::info('AccountController: To View Accounts List::End');   
           
            return $data;
        }
        catch (QueryException $e) 
        {
            Log::error('AccountController: To View Accounts List::Error'.$e->getMessage());
            throw $e;
        }
    }
    
    //----------------------------------------------------------------------

    /**
     *  Create Account function 
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function storeAccount(Request $request)
    {
        $res = Utilities::errorData(null, 'Something went Wrong!');
        try
        {
        Log::info('Accounts controller:Store Account details::Start');
            $data=$request->all();

            $validator = Validator::make($data,[
                'account_name'   =>'required|unique:accounts|
                                    regex:/^[A-Za-z_~\-!@#\$%\^&\*\(\)]{3}/|max:100',
                'account_domain' =>'required|unique:accounts|max:100',
                'contact_name'   =>'required|regex:/^[A-Za-z_~\-!@#\$%\^&\*\(\)]{3}/|max:100',
                'contact_email'  =>'required|unique:accounts|email',
                'contact_phone'  =>'required|numeric|unique:accounts',
                'additional_info'=>'max:1000',
                'user_name'      =>'required|regex:/^[a-zA-Z]{3}/|max:100',
                'user_email'     =>'required|unique:users,email|email',
                'user_phone'     =>'required|numeric|unique:users,mobile_number',
                'password'       => 'required|min:8|max:30',
                'confirmpassword'=> 'required|same:password',
            ],
             ['confirmpassword.required' =>'Confirm password field is required.',
             'confirmpassword.same' =>'Confirm password and password must match.'
             ]
        );
            if(!$validator->fails())
            {   
                if($request->hasFile('image_path')){
                    $file = $request->file('image_path');
                    $name = time() . $file->getClientOriginalName();
                    $file->move(public_path('uploads'), $name);
                    $data['logo'] = $name;
                }
                else{
                    $data['logo']='';
                }
                
                $id = $this->accountRepo->storeAccount($data); 
                if (!empty($id)) {
                    $res= response()->json(Utilities::successData(null,'success'));
                    Log::info('Accounts controller:Store Account details::End');
                }
            }
            else
            {   
                return response()->json(Utilities::validationFailedData($validator->errors()));
                Log::info('Accounts Controller: Store Account details::ValidationError');
            }
        }
        catch (QueryException $e) 
        {
            Log::error('Accounts controller:Store Account details::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }
    //-------------------------------------------------------------------------

    /**
     *  Update Account detail function 
     *
     * @access  public
     * @param   Illuminate\Http\Request $request
     * @return  Response
     * @author  BRAD Contech Solutions Pvt Ltd.
     */

    public function updteAccount(Request $request)
    {
        $data=$request->all();
        
        try
        {
        Log::info('Accounts controller:Update Account details::Start');
            $validator        =Validator::make($data,[
                'account_name'   =>'required|regex:/^[A-Za-z_~\-!@#\$%\^&\*\(\)]{3}/|max:100',
                'account_domain' =>'required|max:100|unique:accounts,account_domain,'.$data['id'],
                'contact_name'   =>'required|regex:/^[A-Za-z_~\-!@#\$%\^&\*\(\)]{3}/|max:100',
                'contact_email'  =>'required|unique:accounts,contact_email,'.$data['id'],
                'contact_phone'  =>'required|numeric|unique:accounts,contact_phone,'.$data['id'],
                'additional_info' => 'max:1000',
            ]);
            if(!$validator->fails())
            {   
                unset($data['csrf-token']);
                if($request->hasFile('image_path')){
                    $file = $request->file('image_path');
                    
                    $name = time() . $file->getClientOriginalName();
                    
                    $file->move(public_path('uploads'), $name);
                    $data['logo'] = $name;
                }
                else{
                    if (!empty($data['image_path'])) {
                       $data['logo']=$data['image_path'];
                    }
                    else{
                        $data['logo']='';
                    }
                }

                if(empty($data['status']) && !isset($data['status']))
                {
                    $data['status'] = 'N';
                }
                else{
                    $data['status'] = 'Y';
                }
                
                $update = $this->accountRepo->updateAccount($data); 
                if ($update) {
                     return "success";
                }
                else{
                    return "fail";
                } 
            }
            else
            {
                return response()->json(['errors'=>$validator->errors()]);
            }
            
            Log::info('Accounts controller:Update Account details::End');
        }
        catch (QueryException $e) 
        {
            Log::error('Accounts controller:Update Account details::Error'.$e->getMessage());
            throw $e;
        }
    }
    //-------------------------------------------------------------------------
}
