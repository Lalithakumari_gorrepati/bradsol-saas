<?php 
namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Helpers
{
    public static function getCurrentUserAccountId()
    {
        
        $id = Auth::id(); 
        $account_id=DB::table('user_accounts')->where('user_id',$id)->value('account_id');
        
        return $account_id;
    }
}

