<?php

namespace App\Imports;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\QueryException;
use App\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Models\UserAccount;
use App\Models\Account;
use App\Models\UserProfile;
use App\Models\Nominee;
use App\Models\Payment;
use App\Models\FCPayments;
use App\Models\PaymentDetails;
use App\Models\MembershipDetails;
use App\Models\AccidentalInsurance;
use Carbon\Carbon;
use App\Http\Helper\Utilities;
use DB;
class UsersImport implements ToCollection,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    public function collection(Collection $rows)
    {
        $res=true;
        try{
            Log::info('UsersImport: BulkMembersImport::Start');
                $role=Role::where('name','Member')->first();
                $account_id=Account::where('account_name','MF-FSS')->value('id');
                $count=count($rows); 
            Log::info('UsersImport: BulkMembersImport::Total Members Count -'.$count);
            DB::beginTransaction();
            foreach ($rows as $row) 
            {   

                if(!empty($row) && !empty($row['member_id'])){
                if ($row['member_status']=='Active' ||$row['member_status']=='Probationary') {
                    $is_active='Y';
                }
                elseif ($row['member_status']=='Inactive' ||$row['member_status']=='Expired') {
                        $is_active='N';
                }
            
                $member_id=strtoupper($row['member_id']);
                Log::info('UsersImport: BulkMembersImport::User inserting started -'.$member_id);
                if (!empty($row['dob'])) {
                    $date = intval($row['dob']);
                    $dob=\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($date)->format('Y-m-d');
                }

                $user=array(
                    'name'           => !empty($row['name'])?$row['name']:'',
                    'email'          => !empty($row['email'])?$row['email']:'',
                    'role'           => $role->id,
                    'mobile_number'  => !empty($row['mobile_number'])?$row['mobile_number']:'',
                    'gender'         => !empty($row['gender'])?$row['gender']:'',
                    'dob'            => !empty($dob)?$dob:'',
                    'password'       => \Hash::make('password'),
                    'is_active'      => $is_active,
                    'additional_info'=> !empty($row['additional_info'])?$row['additional_info']:'',
                    'created_at'     => date('Y-m-d h:i:s'),
                    'created_by'     => 'Job',
                );
                
                $newuser=User::create($user);  
                Log::info('UsersImport: BulkMembersImport::User inserting ended- '.$member_id);
                
                $AssignRole=$newuser->assignRole($role);
                Log::info('UsersImport: BulkMembersImport::Role Assigned to - '.$member_id);
                
                if (!empty($newuser)) {
                    $user_id=$newuser->id;
                    Log::info('UsersImport: BulkMembersImport::Member account creating started - '.$member_id);
                        $account_data=[];
                     
                        $account_data=array( 
                            'user_id'     => $user_id,
                            'account_id'  => $account_id,
                            'created_at'  => date('Y-m-d h:i:s'),
                            'created_by'  => 'job',
                        );
                        
                        $account=UserAccount::insert($account_data);
                    Log::info('UsersImport: BulkMembersImport::Member account creating end - '.$member_id); 
                } 

                Log::info('UsersImport: BulkMembersImport::Member profile details inserting started - '.$member_id);
                
                $customer_id_type = array();
                $customer_id_type['customer_id_type'] = !empty($row['id_proof_type'])?$row['id_proof_type']:'';

                $profile_data=[];
                $profile_data=array( 
                    'user_id'     => $user_id,
                    'member_id'     => $member_id,
                    'alternative_email'  =>!empty( $row['alternative_email'])?$row['alternative_email']:'',
                    'alternative_phone'  =>!empty( $row['alternative_number'])?$row['alternative_number']:'',
                    'martial_status'  =>!empty( $row['marital_status'])?$row['marital_status']:'',
                    'country'  =>!empty( $row['country'])?$row['country']:'',
                    'state'  =>!empty( $row['state'])?$row['state']:'',
                    'city'  => !empty($row['city'])?$row['city']:'',
                    'id_proof'  => !empty($customer_id_type)?json_encode($customer_id_type):null,
                    'age_proof'  => !empty($row['age_proof'])?$row['age_proof']:'',
                    'physical_disability'  => !empty($row['physical_disability'])?$row['physical_disability']:'',
                    'physical_disability_type'  => !empty($row['physical_disability_type'])?$row['physical_disability_type']:'',
                    'previous_illness'  => !empty($row['previous_illness'])?$row['previous_illness']:'',
                    'illness_type'  => !empty($row['illness_type'])?$row['illness_type']:'',
                    'health_declaration'  => 'Y',
                    'reports'  => null,
                    'scanned_application'=> null,
                    'address'  => !empty($row['address'])?$row['address']:'',
                    'image'  => '',
                    'created_at'  => date('Y-m-d h:i:s'),
                    'created_by'  =>'Job',
                );
                
                $profile=UserProfile::insert($profile_data);
                Log::info('UsersImport: BulkMembersImport::Member profile details inserting end - '.$member_id);

                Log::info('UsersImport: BulkMembersImport::Member nominee details inserting started - '.$member_id);
                    if (!empty($row['nominee_dob'])) {
                        $date = intval($row['nominee_dob']);
                        $nominee_dob=\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($date)->format('Y-m-d');
                    }
                    
                    $nominee_details=array( 
                        'member_id' => $user_id,
                        'name'      => !empty($row['nominee_name'])?$row['nominee_name']:'',
                        'email'     => !empty($row['nominee_email'])?$row['nominee_email']:'',
                        'phone'     => !empty($row['nominee_phone'])?$row['nominee_phone']:'',
                        'image'     => '',
                        'address'   => !empty($row['nominee_address'])?$row['nominee_address']:'',
                        'dob'       => !empty($nominee_dob)?$nominee_dob:'',
                        'nominee_relationship'=> !empty($row['nominee_relation'])?$row['nominee_relation']:'',
                        'created_at'  => date('Y-m-d h:i:s'),
                        'created_by'  => 'Job',
                    );
                    
                    if (!empty($nominee_details)) {
                        $insert=Nominee::insert($nominee_details);
                    }
                Log::info('UsersImport: BulkMembersImport::Member nominee details inserting end - '.$member_id);

                Log::info('UsersImport: BulkMembersImport::corpus payment details inserting started - '.$member_id);
                    if (!empty($row['corpus_payment_date'])) {
                        $date = intval($row['corpus_payment_date']);
                        $payment_date=\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($date)->format('Y-m-d');
                    }

                    if (!empty($row['joining_date'])) {
                        $date = intval($row['joining_date']);
                        $join_date=\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($date)->format('Y-m-d');
                    }
                    $age=Carbon::parse($dob)->diff($join_date)->format('%y');   
                    $data=Utilities::getCorpusAmount($age); 
                    $corpus_amount=$data['amount'];

                    $payment_data=[];
                    $payment_data=array( 
                        'member_id'     => $user_id,
                        'account_id'    => $account_id,
                        'amount'        => !empty($corpus_amount)?$corpus_amount:'',
                        'remarks'       => !empty($row['note'])?$row['note']:'',
                        'mode_of_payment'=> !empty($row['corpus_payment_mode'])?$row['corpus_payment_mode']:'',
                        'payment_date'  => !empty($payment_date)?$payment_date:null,
                        'created_at'  => date('Y-m-d h:i:s'),
                        'created_by'  => 'Job',
                    );
                        
                    $payment_id=Payment::insertGetId($payment_data);
                    if($payment_id) {
                        $payment_details=[];
                        $payment_details=array( 
                            'payment_id'    => $payment_id,
                            'payment_against_member'=> null,
                            'payment_type'  =>'corpus_amount',
                            'created_at'  => date('Y-m-d h:i:s'),
                            'created_by'  => 'Job',
                        ); 
                        if (!empty($payment_details)) {
                            $insert=PaymentDetails::insert($payment_details);  
                        }                       
                    }
                Log::info('UsersImport: BulkMembersImport::corpus payment details inserting end - '.$member_id);

                Log::info('UsersImport: BulkMembersImport::advance payment details inserting started - '.$member_id);
                    if (!empty($row['advance_payment_date'])) {
                        $date = intval($row['advance_payment_date']);
                        $advance_payment_date=\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($date)->format('Y-m-d');
                    }
                    $advance_payment_data=[];
                    $advance_payment_data=array( 
                        'member_id'     => $user_id,
                        'account_id'    => $account_id,
                        'amount'        => '4000',
                        'remarks'       => '',
                        'mode_of_payment'=> !empty($row['advance_payment_mode'])?$row['advance_payment_mode']:'',
                        'payment_date'  => !empty($advance_payment_date)?$advance_payment_date:null,
                        'created_at'  => date('Y-m-d h:i:s'),
                        'created_by'  => 'Job',
                    );
                
                    $advance_payment_id=Payment::insertGetId($advance_payment_data);
                    if($advance_payment_id) {
                        $advance_payment_details=[];
                       $advance_payment_details=array( 
                        'payment_id'    => $advance_payment_id,
                        'payment_against_member'=> null,
                        'payment_type'  =>'advance',
                        'created_at'  => date('Y-m-d h:i:s'),
                        'created_by'  => 'Job',
                        ); 
                        if (!empty($advance_payment_details)) {
                            $insert=PaymentDetails::insert($advance_payment_details);
                        }                       
                    }

                Log::info('UsersImport: BulkMembersImport::advance payment details inserting end - '.$member_id);

                Log::info('UsersImport: BulkMembersImport::Membership details inserting started - '.$member_id);
                    if (!empty($row['expired_date'])) {
                        $date = intval($row['expired_date']);
                        $expired_date=\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($date)->format('Y-m-d');
                    }
                    $membership_details=[];
                    $membership_details=array( 
                        'user_id'       => $user_id,
                        'corpus_amount' => $corpus_amount,
                        'join_date'     => !empty($join_date)?$join_date:null,
                        'age'           => $age,
                        'status'        => $row['member_status'],
                        'fc_amount'     => !empty($row['fc_amount'])?$row['fc_amount']:null,
                        'confirm_date'=> !empty($expired_date)?$expired_date:null,
                        'created_at'    => date('Y-m-d h:i:s'),
                        'created_by'    => 'Job',
                    );
                    $membership=MembershipDetails::insert($membership_details);
                Log::info('UsersImport: BulkMembersImport::Membership details inserting end - '.$member_id);

                $membership_year=date('Y',strtotime($join_date));

                if ($age<=30) {
                Log::info('UsersImport: BulkMembersImport::Insurance details inserting start - '.$member_id);    
                    $diff=30-$age;
                    $insurance_details=[];
                    $insurance_data=[];
                    if ($diff==0) {
                        $insurance_details=array( 
                            'member_id'     => $user_id,
                            'year'          => $membership_year,
                            'status'        => 'N',
                            'payment_date'  => null,
                            'created_at'    => date('Y-m-d h:i:s'),
                            'created_by'    => 'Job',
                        );
                        $membership=AccidentalInsurance::insert($insurance_details);
                    Log::info('UsersImport: BulkMembersImport::Insurance details inserting end - '.$member_id);
                    }elseif($diff>0){

                        for ($i=0; $i <=$diff; $i++) {
                            $insurance_details=array( 
                                'member_id'     => $user_id,
                                'year'          => $membership_year + $i,
                                'status'        => 'N',
                                'payment_date'  => null,
                                'created_at'    => date('Y-m-d h:i:s'),
                                'created_by'    => 'Job',
                            );
                            if (!empty($insurance_details)) {
                                array_push($insurance_data, $insurance_details);
                            }
                        }
                        $membership=AccidentalInsurance::insert($insurance_data);
                        Log::info('UsersImport: BulkMembersImport::Insurance details inserting end - '.$member_id);
                    }
                }
            }
            }

            DB::commit(); 
            $res=true;
            Log::info('UsersImport: BulkMembersImport::End');
        }catch(QueryException $e){
            DB::rollBack();
            Log::error('UsersImport:BulkMembersImport::Error'.$e->getMessage());
            throw $e;
        }
        return $res;
    }
}
