<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FCPayments extends Model
{
    protected $table = 'fc_payments';
}
