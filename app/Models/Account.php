<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $table = 'accounts';

    protected $fillable = [
        'account_name','account_domain','logo' ,'contact_name', 'contact_email','contact_phone','additional_info','is_deleted','status',
    ];
}
