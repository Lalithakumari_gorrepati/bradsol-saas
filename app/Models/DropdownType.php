<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DropdownType extends Model
{
    public $timestamps = false;
}
