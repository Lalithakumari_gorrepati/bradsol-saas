<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WebNotifications extends Model
{
    protected $table = 'web_notifications';
}
