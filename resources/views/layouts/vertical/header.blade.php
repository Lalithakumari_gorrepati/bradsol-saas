<!-- header start -->
<style>
.dropbtn {
  background-color: #fff;
  color: #639;
  padding: 5px;
  padding-top: 12px !important;
  font-size: 14px;
  border: none;
}

.dropbtn1 {
  background-color: #fff;
  color: #639;
  padding: 1px;
  padding-top: 10px !important;
  font-size: 12px;
  border: none;
}

.dropdown, .dropdownmess {
  position: relative;
  display: inline-block;
}

.dropdownmess {
	padding-right: 82px;
}

.bg-color:hover {
	color:#fff;
    background-color: #639;
}

a#profile:hover {
    color: white;
}



.dropdown-content {
  display: none;
  position: absolute;
  background-color: #fff;
  min-width: 130px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

.dropdown-contentmess{
	display: none;
  position: absolute;
  background-color: #fff;
  min-width:375px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.badge{
	position: absolute;
	top: -10px;
	color: black;
}

#notification{
font-size: 18px;
}
.cstm-user-profile-name{
    min-width: 133px;
}

.dropdown:hover .dropdown-content {display: block;}
@media screen and (max-width: 550px) {
  .dropdownmess {
    padding-right: 0px;
  }
}
@media screen and (max-width: 377px) {
  .cstm-user-profile-name{
        min-width: 100px;
        max-width: 100px;
    }
}
</style>
<header class=" main-header bg-white d-flex justify-content-between p-2">
    <div class="header-toggle">
        <div class="menu-toggle mobile-menu-icon">
            <div></div>
            <div></div>
            <div></div>
        </div>
        
    </div>
    <input type="hidden" value="{{csrf_token()}}" name="_token" id="token">
    <div class="header-part-right">
      <span>
      @hasanyrole('Admin')
        @php 
          $account_id = Session::get('account_id');
          $id=Auth::id();
          $accounts=DB::table('user_accounts')->leftJoin('accounts', 'user_accounts.account_id', '=', 'accounts.id')->where('user_accounts.user_id',$id)->get();
        @endphp 
          
          @if (!empty($accounts))
            <select class="form-control" name="accounts" id="accounts" style="width:110px;
            margin-left: -50px; margin-right: 30px">
              @foreach ($accounts as $account)
                <option value="{{$account->id}}" <?= ($account_id == $account->id) ? 'selected' : ''?>>{{$account->account_name}}</option>
              @endforeach   
            </select> 
          @endif        
      @endhasanyrole
      </span>
      <div class="dropdown cstm-user-profile-name">
        @php $role_name=Auth::user()->roles->first()->name; @endphp
        @if($role_name=='Member')
            <button class="dropbtn1" style="width: 130px;margin-left: -30px;">{{Auth::user()->name}}</button>
        @else
            <button class="dropbtn" style="width: 130px;margin-left: -30px;">{{Auth::user()->name}}</button>
        @endif
        <div class="dropdown-content">   
          @if($role_name!='Member')
            <div class="bg-color">
              <a href="{{ url('user/edit-profile') }}" class="btn btn-default btn-flat btn-border-orange" id="profile">Profile</a>
            </div>
            @endif
          
          <div class="bg-color">
            <a href="{{ route('logout') }}" class="btn btn-default btn-flat btn-border-orange" id="signOut">Sign Out</a>
          </div>
        </div>
      </div>
        <!-- Full screen toggle -->
        <span class="mr-1">  
            @php
              $logo = Session::get('logo');
            @endphp  
            @if (!empty($logo))
              <img alt="image" width="80" height="50"  src="{{asset('uploads')}}/{{$logo}}">
            @else
              <img alt="image" width="80" height="50"  src="{{asset('images/logo.png')}}">
             @endif   
        </span>
      <!-- Grid menu Dropdown -->
</div>       
</header>
<!-- header close -->

<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<script>


$(document).mouseup(function(e)
		{
		    var container = $("#mesage_content");
		    // if the target of the click isn't the container nor a descendant of the container
		    if (!container.is(e.target) && container.has(e.target).length === 0)
		    {
		        container.hide();
		    }
		});

$(function() {
    $("#accounts").change(function() {
       var id= $('option:selected', this).val();
        $.ajax({
          method:'POST',
          headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          url:"{{ url('user/update/session')}}",
          data:{'id':id},
        }).done(function( data ) {
          if(data=='success'){
              window.location.href = '{{ url('/')}}';
          }
      });
    });
});

</script>