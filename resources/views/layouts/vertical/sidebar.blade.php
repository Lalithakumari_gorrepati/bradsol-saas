
@php 
    $logo = Session::get('logo');
    $account_id = Session::get('account_id');
    $account_name=DB::table('accounts')->where('id',$account_id)->value('account_name');
@endphp
<!-- start sidebar -->
<div class="sidebar-panel">
    <div class="gull-brand pr-3 text-center m-4 mb-2 d-flex justify-content-center align-items-center">
        @if (!empty($logo))
            <img alt="image" width="100" height="50"  src="{{asset('uploads')}}/{{$logo}}"> 
        @else
        <img class="pl-3" alt="image" width="70" height="50"  src="{{asset('images/logo.png')}}">
        @endif
        <div class="sidebar-compact-switch ml-auto"><span></span></div>
    </div>
    
    <!-- user -->
    <div class="scroll-nav" data-perfect-scrollbar data-suppress-scroll-x="true">

        <!-- user close -->
        <!-- side-nav start -->
        <div class="side-nav">

            <div class="main-menu">

                <ul class="metismenu" id="menu">
                    @can('Admin Dashboard')
                    <li class="Ul_li--hover {{ (request()->is('dashboard')) ? 'mm-active' : '' }}">
                        <a class="" href="{{url('/dashboard')}}">
                            <i class="i-Bar-Chart text-20 mr-2 text-muted"></i>
                            <span class="item-name  text-muted">My Dashboard</span>
                        </a>
                    </li>
                    @endcan
                    @can('Accounts Management Main Menu')
                    <li class="Ul_li--hover">
                        <a class="has-arrow" href="#">
                            <i class="fa fa-user-circle text-20 mr-2 text-muted"></i>
                            <span class="item-name  text-muted">Accounts Management</span>
                        </a>
                        <ul class="mm-collapse">
                            @can('Accounts Management')
                            <li class="Ul_li--hover {{ (request()->is('account/list')) ? 'mm-active' : '' }}">
                                <a href="{{url('account/list')}}">
                                   
                                    <i class="fa fa-user-circle text-20 mr-2 text-muted" aria-hidden="true"></i>

                                    <span class="item-name">Account Management</span>
                                </a>
                            </li>
                            @endcan  
                            @can('Role Management')
                                <li class="Ul_li--hover {{ (request()->is('role/list')) ? 'mm-active' : '' }}">
                                    <a href="{{url('role/list')}}">
                                        <i class="nav-icon i-Bell1 text-20 mr-2 text-muted"></i>
                                        <span class="item-name">Role Management</span>
                                    </a>
                                </li>
                            @endcan
                            @can('User Management')
                                <li class="Ul_li--hover {{ (request()->is('user/list')) ? 'mm-active' : '' }}">
                                    <a href="{{url('user/list')}}">
                                        <i class="i-Administrator text-20 mr-2 text-muted"></i>
                                        <span class="item-name">User Management </span>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                    @endcan
                    
                    @if( Helpers::getCurrentUserAccountId()  == 2 )
                    @can('Members Management')
                    <li class="Ul_li--hover">
                        <a class="has-arrow" href="#">
                            <i class="fas fa-user-cog text-20 mr-2 text-muted"></i>
                            <span class="item-name  text-muted">Members Management</span>
                        </a>
                        <ul class="mm-collapse">

                            @can('Total Registrations')
                            <li class="Ul_li--hover {{ (request()->is('member/list')) ? 'mm-active' : '' }}">
                                <a href="{{ url('member/list') }}">
                                    <i class="fas fa-users text-20 mr-2 text-muted"></i>
                                    <span class="item-name">Member Registrations</span>
                                </a>
                            </li>
                            @endcan

                            @can('Active members')
                            <li class="Ul_li--hover {{ (request()->is('member/active/list')) ? 'mm-active' : '' }}">
                                <a href="{{ url('member/active/list') }}">
                                    <i class="fas fa-user-plus text-20 mr-2 text-muted"></i>
                                    <span class="item-name">Active Memebers</span>
                                </a>
                            </li>
                            @endcan
                            @can('Inactive members')
                            <li class="Ul_li--hover {{ (request()->is('member/inactive/list')) ? 'mm-active' : '' }}">
                                <a href="{{ url('member/inactive/list') }}">
                                    <i class="fas fa-user-times text-20 mr-2 text-muted"></i>
                                    <span class="item-name">InActive Members</span>
                                </a>
                            </li>
                            @endcan
                            @can('Expired members')
                            <li class="Ul_li--hover {{ (request()->is('member/expired/list')) ? 'mm-active' : '' }}">
                                <a href="{{ url('member/expired/list') }}">
                                    <i class="fas fa-user-slash text-20 mr-2 text-muted"></i>
                                    <span class="item-name">Expired Members</span>
                                </a>
                            </li>
                            @endcan

                            @can('Probationary members')
                            <li class="Ul_li--hover {{ (request()->is('member/probationary/list')) ? 'mm-active' : '' }}">
                                <a href="{{ url('member/probationary/list') }}">
                                    <i class="fas fa-user-check text-20 mr-2 text-muted"></i>
                                    <span class="item-name">Probationary Members</span>
                                </a>
                            </li>
                            @endcan
                        </ul>
                    </li>
                    @endcan
                    @can('Payments Management')
                    <li class="Ul_li--hover">
                        <a class="has-arrow" href="#">
                            <i class="fas fa-money-check-alt text-20 mr-2 text-muted"></i>
                            <span class="item-name  text-muted">Payments Management</span>
                        </a>
                        <ul class="mm-collapse">
                        @can('Received Payments')    
                            <li class="Ul_li--hover {{ (request()->is('payments/received/list')) ? 'mm-active' : '' }}">
                                <a href="{{ url('payments/received/list') }}">
                                    <i class="fas fa-file-invoice text-20 mr-2 text-muted"></i>
                                    <span class="item-name">Received Payments</span>
                                </a>
                            </li>
                        @endcan 
                           
                        @can('Pending Payments')    
                            <li class="Ul_li--hover {{ (request()->is('payments/pending/list')) ? 'mm-active' : '' }}">
                                <a href="{{ url('payments/pending/list') }}">

                                    <i class="fas fa-money-bill text-20 mr-2 text-muted"></i>
                                    <span class="item-name">Pending Bills</span>
                                </a>
                            </li>
                        @endcan   
                        @can('View Penalties')    
                            <li class="Ul_li--hover {{ (request()->is('payments/penalties/list')) ? 'mm-active' : '' }}">
                                <a href="{{ url('payments/penalties/list') }}">
                                    <i class="fas fa-rupee-sign text-20 mr-2 text-muted"></i>
                                    <span class="item-name">Penalties</span>
                                </a>
                            </li>
                        @endcan 

                        @can('Accidental Insurance List')    
                            <li class="Ul_li--hover {{ (request()->is('insurance/list')) ? 'mm-active' : '' }}">
                                <a href="{{ url('insurance/list') }}">

                                    <i class="fas fa-money-bill text-20 mr-2 text-muted"></i>
                                    <span class="item-name">Accidental Insurance List</span>
                                </a>
                            </li>
                        @endcan   
                        @can('Critical Defaulters')    
                            <li class="Ul_li--hover {{ (request()->is('critical_defaulters/list')) ? 'mm-active' : '' }}">
                                <a href="{{ url('critical_defaulters/list') }}">
                                    <i class="fas fa-rupee-sign text-20 mr-2 text-muted"></i>
                                    <span class="item-name">Critical Defaulters</span>
                                </a>
                            </li>
                        @endcan    
                        </ul>
                    </li>
                    @endcan
                    @can('Configurations Setting')
                    <li class="Ul_li--hover">
                        <a class="has-arrow" href="#">
                            <i class="i-Library text-20 mr-2 text-muted"></i>

                            <span class="item-name  text-muted">Configurations</span>
                        </a>
                        <ul class="mm-collapse">
                            
                            {{-- <li class="Ul_li--hover {{ (request()->is('#')) ? 'mm-active' : '' }}">
                                <a href="#">
                                    <i class="nav-icon i-Bell1"></i>
                                    <span class="item-name">General Setting</span>
                                </a>
                            </li>

                            <li class="Ul_li--hover {{ (request()->is('#')) ? 'mm-active' : '' }}">
                                <a href="#">
                                    <i class="nav-icon i-Bell1"></i>
                                    <span class="item-name">Email/SMS Setting</span>
                                </a>
                            </li> --}}
                            @if($account_name=='MF-FSS')
                                @can('Corpus Fund Setting')
                                    <li class="Ul_li--hover {{ (request()->is('corpusFund/list')) ? 'mm-active' : '' }}">
                                        <a href="{{url('corpusFund/list')}}">
                                            <i class="fas fa-hand-holding-usd text-20 mr-2 text-muted"></i>
                                            <span class="item-name">Corpus Fund Table Setting</span>
                                        </a>
                                    </li>
                                @endcan
                                
                                @can('Fraternity Contribution Setting')
                                    <li class="Ul_li--hover {{ (request()->is('fcSetting/view')) ? 'mm-active' : '' }}">
                                        <a href="{{ url('fcSetting/view') }}">
                                            <i class="i-Administrator text-20 mr-2 text-muted"></i>
                                            <span class="item-name">Fraternity Contribution Setting</span>
                                        </a>
                                    </li>
                                @endcan
                            @endif    
                        </ul>
                    </li>
                    @endcan

                    @can('Reports Management')
                    <li class="Ul_li--hover">
                        <a class="has-arrow" href="#">
                            <i class="fas fa-clipboard-list text-20 mr-2 text-muted"></i>
                            <span class="item-name  text-muted">Reports</span>
                        </a>
                        @if($account_name=='MF-FSS')
                        <ul class="mm-collapse">
                            @can('FC Released Reports')
                            <li class="Ul_li--hover {{ (request()->is('reports/fc/list')) ? 'mm-active' : '' }}">
                                <a href="{{ url('reports/fc/list') }}">
                                    <i class="fas fa-server text-20 mr-2 text-muted"></i>
                                    <span class="item-name">FC's Released</span>
                                </a>
                            </li>
                            @endcan
                            @can('Total Advance Payments Reports')
                            <li class="Ul_li--hover {{ (request()->is('reports/advance/list')) ? 'mm-active' : '' }}">
                                <a href="{{ url('reports/advance/list') }}">
                                    <i class="fas fa-rupee-sign text-20 mr-2 text-muted"></i>
                                    <span class="item-name">Total Advance Payments</span>
                                </a>
                            </li>
                           @endcan
                            @can('Corpus Fund Reports')
                            <li class="Ul_li--hover {{ (request()->is('reports/corpusFund/list')) ? 'mm-active' : '' }}">
                                <a href="{{ url('reports/corpusFund/list') }}">
                                    <i class="fas fa-rupee-sign text-20 mr-2 text-muted"></i>
                                    <span class="item-name">Corpus Fund Reports</span>
                                </a>
                            </li>
                            @endcan
                        </ul>
                        @endif 
                    </li>
                    @endcan
                   
                    @role('Member')
                    @can('Member Dashboard')
                    <li class="Ul_li--hover">
                    <ul class="mm-collapse">
                    <li class="Ul_li--hover">
                        @can('Member Dashboard')
                        <li class="Ul_li--hover {{ (request()->is('dashboard')) ? 'mm-active' : '' }}">
                            <a href="{{ url('dashboard') }}">
                                <i class="i-Bar-Chart text-20 mr-2 text-muted"></i>
                                <span class="item-name">My Dashboard</span>
                            </a>
                        </li>
                        @endcan
                        @can('My Profile')
                        <li class="Ul_li--hover {{ (request()->is('member/profile')) ? 'mm-active' : '' }}">
                            <a href="{{ url('member/profile') }}">
                                <i class="fa fa-user-circle text-20 mr-2 text-muted"></i>
                                <span class="item-name">My Profile</span>
                            </a>
                        </li>
                        @endcan
                        @can('Member Pending Payments')
                        <li class="Ul_li--hover {{ (request()->is('member/pendingPayment')) ? 'mm-active' : '' }}">
                            <a href="{{ url('member/pendingPayment') }}">
                                <i class="fas fa-money-bill text-20 mr-2 text-muted"></i>
                                <span class="item-name">Pending Payments</span>
                            </a>
                        </li>
                        @endcan
                        @can('Member Cleared Payments')
                        <li class="Ul_li--hover {{ (request()->is('member/clearedPayment')) ? 'mm-active' : '' }}">
                            <a href="{{ url('member/clearedPayment') }}">
                                <i class="fas fa-file-invoice text-20 mr-2 text-muted"></i>
                                <span class="item-name">Cleared Payments</span>
                            </a>
                        </li>
                        @endcan
                        @can('Member General Setting')
                        {{-- <li class="Ul_li--hover {{ (request()->is('#')) ? 'mm-active' : '' }}">
                            <a href="#">
                                <i class="i-Administrator text-20 mr-2 text-muted"></i>
                                <span class="item-name">General Setting</span>
                            </a>
                        </li> --}}
                        @endcan

                        @can('About Scheme')
                        <li class="Ul_li--hover {{ (request()->is('fcSetting/view')) ? 'mm-active' : '' }}">
                            <a href="{{ url('fcSetting/view') }}">
                                <i class="fas fa-info-circle text-20 mr-2 text-muted"></i>
                                <span class="item-name">About Scheme</span>
                            </a>
                        </li>
                        @endcan
                    </li>
                </ul>
            </li>
                    @endcan
                    @endrole  
                    @endif   
                    <li class="Ul_li--hover {{ (request()->is('logout')) ? 'mm-active' : '' }}">
                        <a class="" href="{{url('/logout')}}">
                            <i class="i-Administrator text-20 mr-2 text-muted"></i>
                            <span class="item-name">Logout</span>
                        </a>
                    </li>
                </ul>

            </div>
        </div>
    </div>

    <!-- side-nav-close -->
</div>
<!-- end sidebar -->
<div class="switch-overlay"></div>