@extends('layouts.master')

@section('page-css')
 <link rel="stylesheet" href="{{asset('theme/styles/vendor/datatables.min.css')}}">
 <link rel="stylesheet" href="{{asset('theme/styles/vendor/metisMenu.min.css')}}">
 <link rel="stylesheet" href="{{asset('theme/fonts/fontawesome-free-5.10.1-web/css/all.css')}}">

@endsection
@section('main-content')
      <div class="separator-breadcrumb border-top"></div>
            @can('Admin Dashboard')
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <!-- CARD ICON -->
                    <div class="row">
                        @role('Total Accounts')
                        @can('Total Male Profiles')
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="card card-icon mb-4">
                                <div class="card-body text-center">
                                    <i class="fas fa-user-alt" style="font-size:48px;color:#639"></i>
                                    <p class="text-muted mt-2 mb-2">Total Accounts</p>
                                    <p class="text-primary text-24 line-height-1 m-0">{{$data['totalAccounts']}}</p>
                                </div>
                            </div>
                        </div>
                        @endcan
                        @can('Total Active Accounts')     
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="card card-icon mb-4">
                                <div class="card-body text-center">
                                    <i class="fas fa-user-alt" style="font-size:48px;color:#639"></i>
                                    <p class="text-muted mt-2 mb-2">Active Accounts</p>
                                    <p class="text-primary text-24 line-height-1 m-0">{{$data['activeAccounts']}}</p>
                                </div>
                            </div>
                        </div>
                        @endcan

                        @can('Total InActive Accounts')
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="card card-icon mb-4">
                                <div class="card-body text-center">
                                    <i class="fas fa-user-alt-slash" style="font-size:48px;color:#639"></i>
                                    <p class="text-muted mt-2 mb-2">InActive Accounts</p>
                                    <p class="text-primary text-24 line-height-1 m-0">{{$data['inActiveAccounts']}}</p>
                                </div>
                            </div>
                        </div>
                        @endcan
                        @endrole

                        @role('SuperAdmin|Admin')
                        @can('Total Users')
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="card card-icon mb-4">
                                <div class="card-body text-center">
                                    <i class="fas fa-user" style="font-size:48px;color:#639"></i>
                                    <p class="text-muted mt-2 mb-2">Total Users</p>
                                    <p class="text-primary text-24 line-height-1 m-0">{{$data['totalUsers']}}</p>
                                </div>
                            </div>
                        </div>
                        @endcan
                        @can('Total Active Users')
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="card card-icon mb-4">
                                <div class="card-body text-center">
                                    <i class="fas fa-user" style="font-size:48px;color:#639"></i>
                                    <p class="text-muted mt-2 mb-2">Active Users</p>
                                    <p class="text-primary text-24 line-height-1 m-0">{{$data['activeUsers']}}</p>
                                </div>
                            </div>
                        </div>
                        @endcan
                        @can('Total InActive Users')
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="card card-icon mb-4">
                                <div class="card-body text-center">
                                    <i class="fas fa-user-times" style="font-size:48px;color:#639"></i>
                                    <p class="text-muted mt-2 mb-2">InActive Users</p>
                                    <p class="text-primary text-24 line-height-1 m-0">{{$data['inActiveUsers']}}</p>
                                </div>
                            </div>
                        </div>
                        @endcan
                        @endrole
                        @can('Total Registration')
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="card card-icon mb-4">
                                <div class="card-body text-center">
                                    <i class="fas fa-user" style="font-size:48px;color:#639"></i>
                                    <p class="text-muted mt-2 mb-2">Member Registrations</p>
                                    <p class="text-primary text-24 line-height-1 m-0">{{$data['totalMembers']}}</p>
                                </div>
                            </div>
                        </div>
                        @endcan

                        @can('Total Active Members')
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="card card-icon mb-4">
                                <div class="card-body text-center">
                                    <i class="fas fa-user" style="font-size:48px;color:#639"></i>
                                    <p class="text-muted mt-2 mb-2">Active Members</p>
                                    <p class="text-primary text-24 line-height-1 m-0">{{$data['activeMembers']}}</p>
                                </div>
                            </div>
                        </div>
                        @endcan

                        @can('Total Inactive Members')
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="card card-icon mb-4">
                                <div class="card-body text-center">
                                    <i class="fas fa-user-times" style="font-size:48px;color:#639"></i>
                                    <p class="text-muted mt-2 mb-2">Inactive Members</p>
                                    <p class="text-primary text-24 line-height-1 m-0">{{$data['inactiveMembers']}}</p>
                                </div>
                            </div>
                        </div>
                        @endcan

                        @can('Total Expired Members')
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="card card-icon mb-4">
                                <div class="card-body text-center">
                                    <i class="fas fa-user-slash" style="font-size:48px;color:#639"></i>
                                    <p class="text-muted mt-2 mb-2">Expired Members</p>
                                    <p class="text-primary text-24 line-height-1 m-0">{{$data['expiredMembers']}}</p>
                                </div>
                            </div>
                        </div>
                        @endcan

                        @can('Total Probationary Members')
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="card card-icon mb-4">
                                <div class="card-body text-center">
                                    <i class="fas fa-user" style="font-size:48px;color:#639"></i>
                                    <p class="text-muted mt-2 mb-2">Probationary Members</p>
                                    <p class="text-primary text-24 line-height-1 m-0">{{$data['probationaryMembers']}}</p>
                                </div>
                            </div>
                        </div>
                        @endcan

                        @can('Total Pending Payments')
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="card card-icon mb-4">
                                <div class="card-body text-center">
                                    <i class="fa fa-money-bill" style="font-size:48px;color:#639"></i>
                                    <p class="text-muted mt-2 mb-2">Pending Bills</p>
                                    <p class="text-primary text-24 line-height-1 m-0">{{$data['pendingBills']}}</p>
                                </div>
                            </div>
                        </div>
                        @endcan

                        @can('Total Pending Payments')
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="card card-icon mb-4">
                                <div class="card-body text-center">
                                    <i class="fa fa-money-bill" style="font-size:48px;color:#639"></i>
                                    <p class="text-muted mt-2 mb-2">Pending FC's</p>
                                    <p class="text-primary text-24 line-height-1 m-0">{{$data['pendingPayments']}}</p>
                                </div>
                            </div>
                        </div>
                        @endcan

                        @can('Total Received Payments')
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="card card-icon mb-4">
                                <div class="card-body text-center">
                                    <i class="fa fa-file-invoice" style="font-size:48px;color:#639"></i>
                                    <p class="text-muted mt-2 mb-2">Received Payments</p>
                                    <p class="text-primary text-24 line-height-1 m-0">{{$data['receivedPayments']}}</p>
                                </div>
                            </div>
                        </div>
                        @endcan

                        @can('Total Accidental Insurance')
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="card card-icon mb-4">
                                <div class="card-body text-center">
                                    <i class="fa fa-file-invoice" style="font-size:48px;color:#639"></i>
                                    <p class="text-muted mt-2 mb-2">Accidental Insurance</p>
                                    <p class="text-primary text-24 line-height-1 m-0">{{$data['insurance']}}</p>
                                </div>
                            </div>
                        </div>
                        @endcan

                        @can('Total Critical Defaulters')
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="card card-icon mb-4">
                                <div class="card-body text-center">
                                    <i class="fa fa-file-invoice" style="font-size:48px;color:#639"></i>
                                    <p class="text-muted mt-2 mb-2">Critical Defaulters</p>
                                    <p class="text-primary text-24 line-height-1 m-0">{{$data['criticalDefaulter']}}</p>
                                </div>
                            </div>
                        </div>
                        @endcan
                    </div>
                </div>
            </div>
            @endcan
            <!-- end of row-->

            @can('Member Dashboard')
            @role('Member')
            <div class="row pb-3">
                <div class="col-lg-12 col-md-12">
                    <div class="col-lg-10 col-md-10 col-sm-12">
                        <div class="card card-icon p-4">
                            <h4 class="text-danger">Hi {{$data['name']}}! Welcome to Mahesh Foundation-Family Security Scheme</h4>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 pb-3" style="text-align: center;">
                                       @if(!empty($data->image))
                                       <img src="{{asset('uploads')}}/{{$data->image}}" style="height:150px; width:150px;border-radius: 50%">
                                        @else 
                                        <img class="pl-3" alt="image" width="100" height="100"  src="{{asset('images/no_img_available.jpg')}}">
                                        @endif 
                                        <br><br>  
                                        <a href="{{ url('member/profile') }}" class=" pb-3"><b>View Profile</b></a>
                                    </div><br>
                                    <div class="col-md-4 mt-2">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="">
                                                    <h6 class="labelname">Name</h6>
                                                </label>
                                            </div>
                                            <div class="col-1">:</div>
                                            <div class="col-6" >
                                                <label for="">
                                                    <h6 class="value">{{$data['name']}}</h6>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-4">
                                                <label for="">
                                                    <h6 class="labelname">DOB</h6>
                                                </label>
                                            </div>
                                            <div class="col-1">:</div>
                                            <div class="col-6" >
                                                <label for="">
                                                    <h6 class="value">{{$data['dob']}}</h6>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-4">
                                                <label for="">
                                                    <h6 class="labelname">Age</h6>
                                                </label>
                                            </div>
                                            <div class="col-1">:</div>
                                            <div class="col-6" >
                                                <label for="">
                                                    <h6 class="value">{{$data['member_age']}}</h6>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-4">
                                                <label for="">
                                                    <h6 class="labelname">Phone</h6>
                                                </label>
                                            </div>
                                            <div class="col-1">:</div>
                                            <div class="col-6" >
                                                <label for="">
                                                    <h6 class="value">{{$data['mobile_number']}}</h6>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-4">
                                                <label for="">
                                                    <h6 class="labelname">Email</h6>
                                                </label>
                                            </div>
                                            <div class="col-1">:</div>
                                            <div class="col-6" >
                                                <label for="">
                                                    <h6 class="value" style="word-break: break-all;">{{$data['email']}}</h6>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-4">
                                                <label for="">
                                                    <h6 class="labelname">Address</h6>
                                                </label>
                                            </div>
                                            <div class="col-1">:</div>
                                            <div class="col-6" >
                                                <label for="">
                                                    <h6 class="value">{{$data['address']}}</h6>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="">
                                                    <h6 class="labelname">Membership Date</h6>
                                                </label>
                                            </div>
                                            <div class="col-1">:</div>
                                            <div class="col-6" >
                                                <label for="">
                                                    <h6 class="value">{{$data['join_date']}}</h6>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-4">
                                                <label for="">
                                                    <h6 class="labelname">Nominee name</h6>
                                                </label>
                                            </div>
                                            <div class="col-1">:</div>
                                            <div class="col-6" >
                                                <label for="">
                                                    <h6 class="value">{{$data['nominee_name']}}</h6>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-4">
                                                <label for="">
                                                    <h6 class="labelname">Relation</h6>
                                                </label>
                                            </div>
                                            <div class="col-1">:</div>
                                            <div class="col-6" >
                                                <label for="">
                                                    <h6 class="value">{{$data['nominee_relationship']}}</h6>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-4">
                                                <label for="">
                                                    <h6 class="labelname">Nominee Phone</h6>
                                                </label>
                                            </div>
                                            <div class="col-1">:</div>
                                            <div class="col-6" >
                                                <label for="">
                                                    <h6 class="value">{{$data['nominee_phone']}}</h6>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-4">
                                                <label for="">
                                                    <h6 class="labelname">Nominee Email</h6>
                                                </label>
                                            </div>
                                            <div class="col-1">:</div>
                                            <div class="col-6" >
                                                <label for="">
                                                    <h6 class="value" style="word-break: break-all;">{{$data['nominee_email']}}</h6>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <!-- CARD ICON -->
                    <div class="row pl-3">
                        <div class="col-lg-5 col-md-6 col-sm-6" style="margin-left: 1px">
                            <div class="card card-icon mb-4">
                                <div class="card-body text-center">
                                    <i class="fas fa-money-bill" style="font-size:48px;color:#639"></i>
                                    <p class="text-muted mt-2 mb-2">
                                        <a href="{{ url('member/pendingPayment') }}" class="ml-3 p-3">Pending Payments</a>
                                    </p>
                                    <p class="text-primary text-24 line-height-1 m-0"><a href="{{ url('member/pendingPayment') }}" class="ml-3 p-3">{{$counts['pendingPayments']}}</a></p>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-5 col-md-6 col-sm-6" style="margin-left: -10px">
                            <div class="card card-icon mb-4">
                                <div class="card-body text-center">
                                    <i class="fas fa-file-invoice" style="font-size:48px;color:#639"></i>
                                    <p class="text-muted mt-2 mb-2">
                                        <a href="{{ url('member/clearedPayment') }}" class="ml-2 p-3">Cleared Payments</a>
                                    </p>
                                    <p class="text-primary text-24 line-height-1 m-0"><a href="{{ url('member/clearedPayment') }}" class="ml-3 p-3">{{$counts['clearedPayments']}}</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row pb-3">
                <div class="col-lg-12 col-md-12 p-1">
                    <div class="col-lg-10 col-md-10 col-sm-12">
                        <div class="card card-icon p-4" style="margin-left: 7px">
                            <h5 class="text-danger">Scheme By Laws</h5>
                            <div class="card-body">
                                <div class="row">
                                        <label for="">
                                            <h6 class="value">{!! $laws !!}</h6>
                                        </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endrole
            @endcan
@endsection
