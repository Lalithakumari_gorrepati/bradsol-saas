
@extends('layouts.master')
@section('before-css')

<link rel="stylesheet" href="{{asset('assets/styles/vendor/metisMenu.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/fonts/fontawesome-free-5.10.1-web/css/all.css')}}">
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">


<style>
   .pip {
  display: inline-block;
  margin: 10px 10px 0 0;
}
.remove {
  display: block;
 color: black;
  text-align: right;
  cursor: pointer;
}
.remove:hover {
  background: white;
  color: black;
}
.fileUpload {
  position: relative;
  overflow: hidden;
 
}
.fileUpload input.upload {
  position: absolute;
  top: 0;
  right: 0;
  margin: 0;
  padding: 0;
  font-size: 20px;
  cursor: pointer;
  opacity: 0;
  background-color:#fff;
  filter: alpha(opacity=0);
}

.filenameupload {
    width: 70%;
    padding: 3px;
    border-radius: 3px;
    margin-left: 14px;
}

#upload_prev {
  border:thin solid #000;
  width: 65%;
  padding:0.5em 1em 1.5em 1em;
  margin-left: 17px;
    border-radius: 5px;
}

#upload_prev span {
    display: flex;
  padding: 0 5px;
  font-size:12px;
}

p.close {
    margin: 0 5px 0 5px;
    cursor: pointer;
  padding-bottom:0;
}

 </style>


@endsection

@section('main-content')

<div class="col-md-12 alert_notice" style="display:none">
                        <div class="alert alert-primary" role="alert">
                            <strong id="emailDeleted"></strong>
                            <button class="close float-right" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                    </div>	

                <div class="col-md-12">
                      <h4>Edit User</h4>
                
               <div class="card mb-5">
                    
                    <div class="card-body">
                    
                    	<nav>
			                <div class="nav nav-tabs" id="nav-tab" role="tablist">
			                  	<a class="nav-item nav-link active" id="home-basic-tab" data-toggle="tab" href="#homeBasic" role="tab" aria-controls="homeBasic" aria-selected="true">User Details</a>
			                    <a class="nav-item nav-link" id="profile-basic-tab" data-toggle="tab" href="#profileBasic" role="tab" aria-controls="profileBasic" aria-selected="false">Password</a>
			                </div>                          
			            </nav>
                                    
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="homeBasic" role="tabpanel" aria-labelledby="home-basic-tab">
                                   <form action="#" id="editform" method="post" enctype="multipart/form-data">
                               @csrf
                                <input type="hidden" name="id" value="{{$data->id}}">

                                <div class="row">
                                    <div class="col-md-6 form-group mb-3">
                                    <label for="name">Full Name<span style="color: red">*</span></label>
                                        <input type="name" name="name" class="form-control" id="name" value="{{$data->name}}" >
                                        <span class="text-danger err-template">
                                        @if($errors->has('name'))
                                            {{ $errors->first('name') }}
                                        @endif
                                        </span>
                                    </div>
                                    <div class="col-md-6 form-group mb-3">
                                     <label for="email">Email<span style="color: red">*</span></label>
                                        <input type="email" name="email" class="form-control" id="email" value="{{$data->email}}" disabled>
                                    </div>
                                </div>
                               
                                <div class="row">
                                    <div class="col-md-6 form-group mb-3">
                                    <label for="phone"> Phone Number<span style="color: red">*</span></label>
                                        <input type="text" name="phone" class="form-control" id="phone" value="{{$data->phone}}" >
                                        <label class="error" id="mobileError" style="display: none;">
                                        
                                        </label>
                                    </div>
                                    <div class="col-md-6 form-group mb-3">
                                    <label for="phone">Alternate Phone Number</label>
                                        <input type="text" name="alternet_phone" class="form-control" id="alternet_phone" placeholder="Enter Alternative Number" value="{{$data->alternet_phone}}">
                                      <label class="error" id="altermobileError" style="display: none;">
                                        @if($errors->has('alternet_phone'))
                                            {{ $errors->first('alternet_phone') }}
                                        @endif
                                        </label>
                                    </div>
                                </div>
                                 <div class="row">
                                     <div class="col-md-6 form-group mb-3">
                                     <label for="alternatephone">Relation</label>
                                        <input type="text"  name="relation" class="form-control" id="relation" placeholder="Enter Relation"  value="{{$data->relation}}">
                                    </div>
                                     <div class="col-md-6 form-group mb-3">
                                  <label for="alternatephone">Relative name</label>
                                        <input type="text"  name="relative_name" class="form-control" id="relative_name" placeholder="Enter Relative"  value="{{$data->relative_name}}">
                                    </div> 
                                   </div>
                                 
                                  <div class="row">
                                    
                                    <div class="col-md-6 form-group mb-3">
                                    <label for="phone">Date Of Joining</label>
                                    <input type="hidden" name = "date_of_joining" id="date_of_joining" value="{{$data->date_of_joining}}">
                                        <input type="text" name="date_of_joining" class="form-control" id="date_of_joining" placeholder="Enter Date Of Joining" value="{{$data->date_of_joining}}" disabled >
                                       
                                    </div>

                                     <div class="col-md-6 form-group mb-3">
                                    <label for="expiridate">Contract End Date</label>
                                    <input type="hidden" name = "contract_expiry_date" id="contract_expiry_date" value="{{$data->contract_expiry_date}}">
                                        <input type="text" name="contract_expiry_date" class="form-control" id="contract_expiry_date" placeholder="Enter Contract Expiring Date" value="{{$data->contract_expiry_date}}" disabled>
                                        <span hidden="" class="text-danger" id="joiningError">
                                            Contract end date should be greater than Joining date
                                       </span>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6 form-group mb-3">
                                    <label for="phone">Role<span style="color: red">*</span></label>
                                    <input type="hidden" name = "role" id="role" value="{{$data->role}}">
                                        <select class="form-control"  name="role" disabled>
                                             <option value="">Select Role</option>
                                            @foreach ($role as $roles)
                                              <option value="{{$roles->id}}"<?= ($data->role == $roles->id) ? 'selected' : ''?> >{{$roles->name}}</option>
                                              
                                            @endforeach
                                        </select>
                                        <span class="text-danger err-template">
                                        @if($errors->has('role'))
                                            {{ $errors->first('role') }}
                                        @endif
                                        </span>
                                    </div>
                                    <div class="col-md-6 form-group mb-3">
                                      <label for="salary">Salary Information</label>
                                      <input type="hidden" name = "salary_details" id="salary_details" value="{{$data->salary_details}}">
                                      <input type="text" name="salary_details" id="salary_details" class="form-control" placeholder="Enter Salary" value="{{$data->salary_details}}" disabled>
                                    </div>
                                </div>
                                
                               <div class="row">
                                    <div class="col-md-6 form-group mb-3">
                                    <label for="role">Change Profile Picture</label><br>
                                        @if(!empty($data->image_path))
                                    <span class="pip">  
                                    <span class="remove" >
                                        <i class='fa fa-times'></i></span> 
                                      <img src="https://jm-web-content.s3.ap-south-1.amazonaws.com/{{\Config::get('app.env')}}/users/{{$data->id}}/profile-image/{{$data->image_path}}" style="height:150px; width:150px;" id="OpenImgUpload"> 
                                  </span>
                                      <input type="file" name="image_path" id="image_path" style="display:none" accept="image/jpgc,image/png,image/jpeg,image/gif"/> 
                                       <input type="hidden" name="image_path" value="{{$data->image_path}}" id="uploadPic">
                  
                                      @else
                                         <span class="pip">  
                                    <span class="remove" >
                                        <i class='fa fa-times'></i></span> 
                                      <img  style="display:none;height:150px; width:150px;"id="OpenImgUpload">
                                  </span>
                                          <input type="file" name="image_path" id="image_path" accept="image/jpg,image/png,image/jpeg,image/gif"/> 

                                      @endif
                                    </div>
                                    
                                 <div class="col-md-6 form-group mb-3">

                                    <label for="multipal file">Attachments</label>
                                      
                                     @if(!empty($multiuplodepath))
                                      @foreach ($multiuplodepath as $key=>$images)
                                        
                                        @if($images !='')
                                        <div class="row">
                                         <div class="col-sm-10 " id="{{$key}}">
                                         <tr> <td> <a href="https://jm-web-content.s3.ap-south-1.amazonaws.com/{{\Config::get('app.env')}}/users/{{$data->id}}/documents/{{$images}}" target="_blank"> {{$images}}</a></td></tr> 
                                            <a style="color: white;"  onclick="removeimage('{{$key}}')"><span style="color: black;">&times;</span></a>
                                          <input type="hidden" class="{{$key}}" name="multi_filepath1[]" value="{{$images}}">
                                          </div>
                                          </div>
                                         @endif
                                         @endforeach
                                         @else
                                         <h4>No Attachments found...</h4>
                                         @endif
                                    </div>
                            </div>
                                <div class="row">
                                <div class="col-md-6 form-group mb-3">
                                    <label for="status">User Status</label>
                                   <label class="switch pr-5 switch-primary mr-3">
                                   
                                   <input type="hidden" name = "user_status" id="user_status" value="{{$data->status}}">
                                    <input type="checkbox" disabled name="user_status" <?=($data->status == 'Active')? "checked" : " " ?> ><span class="slider">
                                    </span>
                                </label>                  
                                </div>
                                </div>
                          
                                <div class="row">
                                 <div class="col-md-6 form-group mb-3">

                                   <label for="multipath">Attachment</label>
                                    <input id="uploadFile" class="form-control
                                    " placeholder="Add files from My Computer"/>
                                   <div class="fileUpload btn btn-primary btn-rounded m-1">
                                   <span >Browse</span>
                                    <input id="uploadBtn" type="file" class="upload" multiple="multiple" name="multi_filepath[]"/>
                                    <span class="text-danger err-template">
                                        @if($errors->has('multi_filepath'))
                                            {{ $errors->first('multi_filepath') }}
                                        @endif
                                        </span>
                                    
                                    </div>
                                  </div>
                                  <div class="col-md-6 form-group mb-3">

                                    <label for="">Uploded Files</label>
                                   <div id="upload_prev" class="col-sm-6"></div> 
                                    </div>
                                </div>
                                  <div class="row">
                                   <div class="col-md-12">

                                    <label for="description">Additional Information</label>
                                        <textarea id="additional_info" name="additional_info" rows="4" cols="100" class="form-control">{{$data->additional_info}}</textarea>
                                        <span class="text-danger err-password" id="password_error_msg" >
                                                  @if($errors->has('password'))
                                                    {{ $errors->first('password') }}
                                                  @endif
                                       </span>
                                 </div></div>
                               <div class="row col-md-12 btn-center">
                                     <button id="btnSubmit" class="btn btn-primary btn-rounded mt-3">Submit</button>
                               </div>
                                
                            </form>
                            </div>
                                
                                <div class="tab-pane fade" id="profileBasic" role="tabpanel" aria-labelledby="profile-basic-tab">
                                    
                                    <div class="card-body">
                                    <form action="#" id="editPasswordForm" method="post" enctype="multipart/form-data">
                                       	@csrf
                                		<input type="hidden" name="id" value="{{$data->id}}">
                                        <div class="form-group row">
                                            <label for="staticEmail6" class="ul-form__label ul-form--margin col-lg-3 col-form-label ">Enter Old Password:</label>
                                            <div class="col-lg-6">
                                                <input type="password" class="form-control" id="oldPassword" name="oldPassword" placeholder="Enter Old Password">
                                            	<span class="text-danger err-password" id="oldPwdError">
                                        		</span>
                                            </div>
                                    	</div>
                                    	<div class="form-group row">
                                            <label for="staticEmail6" class="ul-form__label ul-form--margin col-lg-3 col-form-label ">Enter New Password:</label>
                                            <div class="col-lg-6">
                                                <input type="password" class="form-control" id="newPassword" name="newPassword" placeholder="Enter New Password">
                                            </div>
                                   		 </div>
                                   		 <div class="form-group row">
                                            <label for="staticEmail6" class="ul-form__label ul-form--margin col-lg-3 col-form-label ">Confirm Password:</label>
                                            <div class="col-lg-6">
                                                <input type="password" class="form-control" id="confirmPassword" name="confirmPassword" placeholder="Confirm Password">
                                            	<span class="text-danger err-password pass_incorrect">
                                        		</span>
                                            </div>
                                   		 </div>
                                   		 <div class="row col-md-12 btn-center">
                                     		<button type="button" id="passwordSubmit" class="btn btn-primary btn-rounded mt-3">Submit</button>
                               			</div>
                                	</form>
                                </div>
                            </div>
                            </div>
                        </div>
                    
                        </div>
                    </div>
                </div>
  
@endsection

@section('page-js')

<script src="{{asset('assets/js/vendor/metisMenu.min.js')}}"></script>
<script src="{{asset('assets/js/es5/dashboard.v2.script.js')}}"></script>
<script src="{{asset('assets/js/layout-sidebar-vertical.js')}}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>


@endsection

@section('bottom-js')
<script>
  $( function() {
    $( "#contract_expiry_date" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd-mm-yy'
    });
  } );

  $( function() {
    $( "#date_of_joining" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd-mm-yy'
    });
  } );


$("#contract_expiry_date").change(function() {
 var Joining = $('#date_of_joining').val();
 var ending= $('#contract_expiry_date').val();
  if(ending==Joining || Joining>ending){
        $('#joiningError').removeAttr('hidden',true)
    }
     
     else{
        $('#joiningError').attr('hidden',true)
       }
 
});


  $(document).on('click','.close',function(){
  $(this).parents('span').remove();

})

document.getElementById('uploadBtn').onchange = uploadOnChange;
    
function uploadOnChange() {
    document.getElementById("uploadFile").value = this.value;
    var filename = this.value;
    var lastIndex = filename.lastIndexOf("\\");
    if (lastIndex >= 0) {
        filename = filename.substring(lastIndex + 1);
    }
    var files = $('#uploadBtn')[0].files;
    for (var i = 0; i < files.length; i++) {
     $("#upload_prev").append('<span>'+'<div class="filenameupload">'+files[i].name+'</div>'+'<p class="close">X</p></span>');
    }
    document.getElementById('filename').value = filename;
}


var img = $('#uploadPic').val();

if(img == null || img == '' || img == undefined){
	 $(".pip").hide();
}



//code for select image
$('#OpenImgUpload').click(function(){ 
   $('#image_path').trigger('click');
   });
 

  // code for preview image
      function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#OpenImgUpload').show();
        $('.pip').show();
        $('#OpenImgUpload').attr('src', e.target.result);


        $(".remove").click(function(){
            $('.pip').hide();
             $('#OpenImgUpload').hide();
             $('#image_path').val("");
          });
      
    }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

$("#image_path").change(function() {

  readURL(this);
});
$("#uploadPic").change(function() {
  readURL(this);
});

$(".remove").click(function(){
            $('.pip').hide();
             $('#OpenImgUpload').hide();
             $('#image_path').val("");
             $('#uploadPic').attr('type', 'file');
             $('#uploadPic').val("");
             
          });

function removeimage(id){

 
  $('#'+id).hide()
  $('.'+id).val(" ");
  $('.'+id).attr("disabled", "disabled"); 

}

$( "#oldPassword" ).keyup(function() {
	$("#oldPwdError").html('').hide();
});

 $("#passwordSubmit").on('click', function(){	
	var data=$('#editPasswordForm').serialize();
	if($('#editPasswordForm').valid()){
		$.ajax({
	        method: "POST",
	        url : "{{url('updateUserPassword')}}",
	        data     :data,
	      })
        .done(function( data ) {
	        if(data.status == 'success'){
	        	$("#oldPwdError").html('').show();
	        	$(".alert-primary").html("Password Updated Successfully!");
                $(".alert_notice").show();
                $('html, body').animate({
                    scrollTop: $('.main-header').offset().top
                }, 1000);
                setTimeout(function(){ $('.alert_notice').fadeOut() }, 5000);
                window.location.href = "{{url('/logout')}}";
	        }else if(data.status == 'validation_error'){
		        if(data.data){
		        	$("#oldPwdError").html(data.data).show();
			    }
	        }
	        else{
	        	$(".alert-primary").html("Something Went Wrong!");
                $(".alert_notice").show();
                $('html, body').animate({
                    scrollTop: $('.main-header').offset().top
                }, 1000);
                setTimeout(function(){ $('.alert_notice').fadeOut() }, 5000);
	        }
		});
	}
}); 



$.validator.addMethod("only_text", function(value, element) {
	return this.optional(element) || value == value.match(/^[ a-zA-Z][ a-zA-Z_/-]*$/);
	},"Enter valid name");

$.validator.addMethod("numeric", function (value, element, param) {
	 return this.optional(element) || /^[-]?[0-9]+$/.test(value);
	}, "Please enter only numbers.");

$.validator.addMethod("notEqualTo", function(value, element, param) {
	return this.optional(element) || value != $(param).val();
	}, "Old password and new Password should not be same!");

$('#editform').validate({
  rules: {
      name:{
    	  required: true,
          maxlength:100,
      },
     phone:{
    	 required: true,
    	 numeric : true,
    	 minlength:10,
         maxlength:10,
      }, 
      alternet_phone:{
    	  numeric : true,
    	  minlength:10,
          maxlength:10,
      },
      salary_details:{
    	  numeric : true,
      }, 
       role:{
          required: true,
      },  
      relation:{
      	only_text:true,
      },
      relative_name:{
      	only_text:true,
      },
      additional_info:{
    	  maxlength:1000,
      }
      
  },
  messages:{
	  additional_info:{
		  maxlength:"This can not exceed 1000 characters"
	    }
	  }
});


$('#editPasswordForm').validate({
  rules: {
	  confirmPassword:{
    	  required: true,
        minlength:6,
        maxlength:30,
    	  equalTo: "#newPassword"
      },
      oldPassword:{
    	 required: true,
       maxlength:30
      }, 
      newPassword:{
    	  required: true,
        minlength:6,
        maxlength:30,
    	  notEqualTo:"#oldPassword"
      },
  },
messages:{
    confirm_password:{
    	equalTo:"Confirm Password Should be same as Password"
    }
  }
});

$( "#phone" ).keyup(function() {
	$("#mobileError").html('').hide();
});

$( "#alternet_phone" ).keyup(function() {
	$("#altermobileError").html('').hide();
});



	$('#editform').submit(function(evt) {
	    evt.preventDefault();
	
	    var formData = new FormData(this);
	    if($('#editform').valid()){
	    $.ajax({
	    type: 'POST',
	    url: "{{url('updateUserProfile')}}",
	    data:formData,
	    cache:false,
	    contentType: false,
	    processData: false,
	    success: function(data) {
	    	if(data.status == 'success'){
	        	$(".alert-primary").html("User Details Updated Successfully!");
	            $(".alert_notice").show();
	            $('html, body').animate({
                    scrollTop: $('.main-header').offset().top
                }, 1000);
	            setTimeout(function(){ $('.alert_notice').fadeOut() }, 5000);
	            $('#nav-tab a[href="#profileBasic"]').tab('show');
	            $("#altermobileError").html('').hide();
	            $("#mobileError").html('').hide();
	        }else if(data.status == 'validation_error'){
		        //alert(data.data.phone);
		        if(data.data.phone){
			        $("#mobileError").html(data.data.phone).show();
			    }
		        if(data.data.alternet_phone){
		        		$("#altermobileError").html("The Alternate Phone has already been taken.").show();
			    }
	        }
	    },
	    error: function(data) {
	    	$(".alert-primary").html("Something Went Wrong!");
            $(".alert_notice").show();
            $('html, body').animate({
                scrollTop: $('.main-header').offset().top
            }, 1000);
            setTimeout(function(){ $('.alert_notice').fadeOut() }, 5000);
	    }
	    });
	    }
});



</script>


@endsection
