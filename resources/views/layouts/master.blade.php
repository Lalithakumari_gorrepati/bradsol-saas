<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">
		
		<title>Bradsol SAAS</title>
		<link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet"/>
		
		@yield('before-css') 
		{{-- theme css --}}
		<link id="gull-theme" rel="stylesheet" href="{{  asset('theme/styles/css/lite-purple.min.css')}}"/>
		<link rel="stylesheet" href="{{asset('theme/styles/vendor/perfect-scrollbar.css')}}"/>
		{{-- theme css --}}
		
		@yield('page-css')
		{{-- page specific css --}}
		<link rel="stylesheet" href="{{asset('theme/styles/vendor/datatables.min.css')}}"/>
		<link rel="stylesheet" href="{{asset('theme/styles/vendor/metisMenu.min.css')}}"/>
		<link rel="stylesheet" href="{{asset('theme/styles/style.css')}}"/>
		<link rel="stylesheet" href="{{asset('theme/fonts/fontawesome-free-5.10.1-web/css/all.css')}}"/>
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.2/select2.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css">
		{{-- page specific css --}}
		<link href="{{ asset('css/custom.css') }}" rel="stylesheet">
		<style>
		    li .mm-active{
		   		background-color: lightgrey !important;
		   	}
		   	.form-group label.error {
		    font-size: 12px;
		     color: red; 
		    margin-bottom: 4px;
			}
    	</style>
    
	</head>

	<body class="text-left">
		@php $layout = session('layout'); @endphp

		<!-- Pre Loader Strat  -->
		<div class='loadscreen' id="preloader">
			<div class="loader spinner-bubble spinner-bubble-primary"></div>
		</div>
		<!-- Pre Loader end  -->

		<!-- ============ Vetical SIdebar Layout start ============= -->
		<div class="app-admin-wrap layout-sidebar-vertical sidebar-full">
			@include('layouts.vertical.sidebar')
			<div class="main-content-wrap  mobile-menu-content bg-off-white m-0">
				@include('layouts.vertical.header')
				<div class="main-content pt-4">@yield('main-content')</div>
				@include('layouts.footer')
			</div>
			<div class="sidebar-overlay open"></div>
		</div>
		<!-- ============ Vetical SIdebar Layout End ============= -->

		{{-- common js --}}
		<script src="{{  asset('theme/js/common-bundle-script.js')}}"></script>
		
		{{-- theme javascript --}} 
		<script src="{{asset('theme/js/script.js')}}"></script>
		<script src="{{asset('theme/js/es5/script_2.js')}}"></script>
		<script src="{{asset('theme/js/vendor/metisMenu.min.js')}}"></script>
		<script src="{{asset('theme/js/vendor/datatables.min.js')}}"></script>
		<script src="{{asset('theme/js/es5/dashboard.v2.script.js')}}"></script>
		<script src="{{asset('theme/js/layout-sidebar-vertical.js')}}"></script>
		<script src="{{asset('theme/js/tooltip.script.js')}}"></script>
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		{{-- theme javascript --}} 
	
	    {{-- additional scripts --}}
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
		<script defer src="https://use.fontawesome.com/releases/v5.5.0/js/all.js"></script>
		<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
		<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
 		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/3.2/select2.min.js"></script>
		<script src="{{  asset('js/custom.js')}}"></script>

		@yield('bottom-js')

		@yield('page-js') 

	</body>
</html>