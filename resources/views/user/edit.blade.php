@extends('layouts.master')
@section('main-content')
      <div class="col-md-12" id="successmsg" style="display: none">
            <div class="alert alert-primary " id="alert-primary">
                
                <button class="close float-right" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"></span></button>
            </div>
      </div>
                
        <div class="col-md-12">
              <h4 class="pb-2">Edit Details<span class="float-right mr-2"><a class="btn btn-primary btn-rounded" href="{{url('user/list')}}" >Cancel</a></span></h4>
           
            <div class="card mb-5">
              <div class="card-body">
                <form id="editform">
                  @csrf
                        <input type="hidden" name="id" value="{{$data->user_id}}" id="customer_id">

                        <div class="row">
                            <div class="col-md-6 form-group mb-3">
                            <label for="name">Full Name<span style="color: red">*</span></label>
                                <input type="text" name="name" class="form-control" id="name" value="{{$data->name}}" placeholder="Enter name">
                                <span class="error text-danger" id="name_error"></span>
                            </div>
                            <div class="col-md-6 form-group mb-3">
                             <label for="email">Email<span style="color: red">*</span></label>
                                <input type="email" name="email" class="form-control" id="email" value="{{$data->email}}" placeholder="Enter email" readonly="">
                                <span class="error text-danger" id="email_error"></span>
                            </div>
                        </div>
                       
                        <div class="row">
                           <div class="col-md-6 form-group mb-3">
                            <label for="mobile_number">Mobile Number<span style="color: red">*</span></label>
                            
                                <input type="text" minlength="10" maxlength="10" size="10" name="mobile_number" class="form-control" id="mobile_number" placeholder="Enter Phone" value="{{$data->mobile_number}}">
                                <span class="error text-danger err-phone" id="mobile_number_error"></span>
                            </div>

                            <div class="col-md-6 form-group mb-3">
                            <label for="role">Roles<span style="color: red">*</span></label>
                                <select class="form-control"  name="role" >
                                   <option value="" hidden>Select Role</option>
                                    @foreach ($role as $roles)
                                      @if ($roles->name !='Member' && $roles->name !='SuperAdmin')
                                        <option value="{{$roles->id}}"<?= ($data->role == $roles->id) ? 'selected' : ''?> >{{$roles->name}}</option>
                                      @endif
                                    @endforeach
                                </select>
                                <span class="text-danger" id="role_error">
                                </span>
                            </div>

                        </div>
                        
                         <div class="row">
                            <div class="col-md-6 form-group mb-3">
                            <label for="user_accounts">Account</label>
                              @role('SuperAdmin')
                                <select class="selectbox form-control"  name="account_id[]" id="user_accounts" multiple>
                                  <option></option>
                                  <optgroup label="Account">
                                    @foreach ($accounts as $account)
                                      <option value="{{$account->id}}" {{in_array($account->id, $oldAccounts ?: []) ? "selected": ""}}>{{$account->account_name}}</option>
                                    @endforeach
                                    </optgroup>
                                </select>
                                @else

                                <select name="account_id[]" class="form-control" disabled="">
                                @foreach ($accounts as $account)
                                  <option value="{{$account->id}}" <?= ($account_id ==$account->id) ? 'selected' : ''?>>{{$account->account_name}}</option>
                                @endforeach
                              </select> 
                              <input type="hidden" name="account_id[]" value="{{$account_id}}">
                              @endrole
                                <span class="text-danger" id="account_id_error">
                                </span>
                            </div>

                            <div class="col-md-6 form-group mt-4">
                              <label for="status">User Status</label>
                              <label class="switch pr-5 switch-primary mr-3">
                                  <input type="checkbox" name="user_status" <?=($data->status == 'Active')? "checked" : " " ?> ><span class="slider">
                                  </span>
                              </label>                  
                            </div>
                        </div>

                        <div class="row">
                          <div class="col-md-6 form-group mb-3">
                            <label for="additional_info">Additional Information</label>
                              <textarea id="additional_info" name="additional_info" rows="4" cols="50" class="form-control" maxlength="1001">{{$data->additional_info}}</textarea>
                              <span class="error text-danger err-password" id="additional_info_error" >
                              </span>                 
                          </div>
                      </div>
                       
                     <div class="row col-md-12 btn-center">
                           <button type="submit" id="submitButton" class="btn btn-primary btn-rounded mt-1">Submit</button>
                     </div>
                  </div>
              </form>
          </div>
        </div>
  </div>
@endsection

@section('bottom-js')
<script>
  $(document).ready(function(){
  var select = $('.selectbox');
  var single_select = $('.selectbox2');
  function formatSelection(state) {
    return state.text;   
}

function formatResult(state) {
    if (!state.id) return state.text; 
    var id = 'state' + state.id.toLowerCase();
    var label = $('<label></label> ' , { for: id })
            .text(state.text);
    var checkbox = $('<input type="checkbox">' , { id: id });
    
    return checkbox.add(label);   
}

  select.select2({
    closeOnSelect: true,
    allowClear: true,
    formatResult: formatResult,
    formatSelection: formatSelection,
    theme: "bootstrap",
    escapeMarkup: function (m) {
        return m;
    },
    matcher: function(term, text) {

        if (term.length === 0) return true;
        texts = text.split(" ");

        allCombinations = permute(texts, [], []);

        for (i in allCombinations) {
          if (allCombinations[i].join(" ").toUpperCase().indexOf(term.toUpperCase()) === 0) {
            return true;
          }
        }
        return false;
      }

});

  $( function() {
    $( "#dob" ).datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: "-90:+0",
      dateFormat: 'dd-mm-yy',
      maxDate:0
    });
  } );

$('#editform').validate({
    ignore: "",
    ignore: [],
    rules: {
        name:{
          only_text: true,
          required: true,
          minlength:3,
          maxlength:100,
        },
        email:{
            required: true,
            custom_email: true,
            maxlength:100,
        },
        mobile_number:{
          required: true,
      	  numeric : true,
          maxlength:10,
        },
        'account_id[]':{
            // required: true,
        },
        role:{
            required: true,
        },
        additional_info:{
            maxlength: 1000,
        },
      },
      messages:{
            name:{
              required: "Name is Required"
            },
             email:{
              required: "Email is Required"
            },
             mobile_number:{
              required: "Mobile Number is Required"
            },
             role:{
              required: "Role is Required"
            },
            additional_info:{
              maxlength: "This field can not exceed 1000 characters"
            },
            
          },
    submitHandler:function(){
      $('#submitButton').attr('disabled',true);
      $('.loadscreen').show();
      $.ajax({
          method: "POST",
          headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }, 
          url: "{{url('user/update')}}",
          data:new FormData($("#editform")[0]),
          contentType: false,
          processData: false,
        })
      .done(function( data ) {
            $('.loadscreen').hide();  
            $('#submitButton').removeAttr('disabled',true);
          if(data.status== 'success'){
              $("#editform")[0].reset();
              $("#alert-primary").text('User has been Updated successfully!');
              $("#successmsg").show();
              $('html, body').animate({
                scrollTop: $('.main-header').offset().top
            }, 1000);
              setTimeout(function(){ $('#successmsg').fadeOut() }, 3000);
              window.location.href = "{{url('user/list')}}";
          }
          else if(data.status== 'validation_error'){
              $.each(data.data, function (key, val) {
                $("#"+key+"_error").text(val[0]);
                $("#"+key+"_error").show();
                $(document).on("keyup", "input[name='"+key+"']", function(e){
                      $("#"+key+"_error").hide();
                 });
                 $(document).on("change", "input[name='"+key+"']", function(e) {
                      $("#"+key+"_error").hide();
                 });
            });
          }
          else{
              $("#alert-primary").text('Something Went Wrong !');
              $("#successmsg").show();
              $('html, body').animate({
                scrollTop: $('.main-header').offset().top
              }, 1000);
              setTimeout(function(){ $('#successmsg').fadeOut() }, 3000);
          }
      });
    }
  });
  
  $('#user_accounts').on('change',function(){
      $('#account_id_error').hide();
  })

  $(function(){
      $('#mobile_number').keypress(function(e){
        if(e.which!=8 && e.which!=0 && (e.which<48 || e.which>57))
        {
          return false;
        }
      });
    });
});
</script>

@endsection
