@extends('layouts.master')
 
@section('main-content')
      <div class="col-md-12" id="successmsg" style="display: none">
        <div class="alert alert-primary " id="alert-primary">
            <button class="close float-right" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"></span></button>
        </div>
      </div>
 <div class="col-md-12">
        <h4 class="pb-2">Add New User<span class="float-right"><a class="btn btn-primary btn-rounded" href="{{url('user/list')}}">Cancel</a></span></h4>
    <div class="card mb-5">
      <div class="card-body">
          <form id="userform">
             <input type="hidden" value="{{csrf_token()}}" name="csrf-token" id="token">
                 <div class="row">
                      <div class="col-md-6 form-group mb-3">
                        <label for="name">Full Name<span style="color: red">*</span></label>
                        <input type="text" name="name" class="form-control" id="name" placeholder="Enter Full Name"  value="{{ old('name') }}">
                          <span class="error text-danger" id="name_error"></span>
                      </div> 
                    <div class="col-md-6 form-group mb-3">
                      <label for="email">Email<span style="color: red">*</span></label>
                        <input type="email" name="email" class="form-control" id="email" placeholder="Enter Email"  value="{{ old('email') }}" autocomplete="off">
                         <span class="error text-danger err-email" id="email_error"></span>
                    </div>
                </div>

                  <div class="row">
                      <div class="col-md-6 form-group mb-3">
                        <label for="mobile">Mobile Number<span style="color: red">*</span></label>
                        <input type="text" minlength="10" maxlength="10" size="10" name="mobile_number" class="form-control" id="mobile_number" placeholder="Enter mobile number" value="{{ old('mobile_number') }}" autocomplete="off">
                        <span class="error text-danger err-mobile_number" id="mobile_number_error"></span>
                      </div>

                      <div class="col-md-6 form-group mb-3">
                        <label for="role">Role<span style="color: red">*</span></label>
                          <select name="role" class=" form-control @error('role') is-invalid @enderror">
                            <option value="" hidden>Please Select</option>
                            @foreach ($data['roles'] as $roles)
                              @if ($roles->name !='Member' && $roles->name !='SuperAdmin')
                                <option value="{{$roles->id}}">{{$roles->name}}</option>
                              @endif
                            @endforeach
                          </select>
                          <span class="error text-danger err-role" id="role_error"></span>
                      </div>
                  </div>
                    <div class="row">
                      <div class="col-md-6 form-group mb-3">
                     <label for="password">Password<span style="color: red">*</span></label>
                        <input type="password" name="password" class="form-control" id="password" placeholder="Enter Password" value="{{ old('password') }}" autocomplete="new-password">
                        <span class="error text-danger err-password" id="password_error">
                       </span>
                   </div>
                   <div class="col-md-6 form-group mb-3">
                     <label for="confirm_password">Confirm Password<span style="color: red">*</span></label>
                    
                        <input type="password" name="confirmpassword" class="form-control" id="cppassword" placeholder="Enter Confirm Password">
                        <span class="text-danger err-confirmpassword" id="confirmpassword_error">
                        </span>
                    </div>
                </div>

                <div class="row">
                  <div class="col-md-6 form-group mb-3">
                     <label for="account">Account</label>
                     @role('SuperAdmin')
                      <select name="account[]" class="selectbox form-control @error('account') is-invalid @enderror" multiple>
                            <option></option>
                            @foreach ($data['accounts'] as $account)
                              <option value="{{$account->id}}">{{$account->account_name}}</option>
                            @endforeach
                          </select>
                      @else
                          <select name="account[]" class="form-control" disabled="">
                            @foreach ($data['accounts'] as $account)
                              <option value="{{$account->id}}" <?= ($data['account_id'] == $account->id) ? 'selected' : ''?>>{{$account->account_name}}</option>
                            @endforeach
                          </select> 
                          <input type="hidden" name="account[]" value="{{$data['account_id']}}">
                      @endrole    
                        <span class="text-danger" id="account_error">
                    </div>
                    <div class="col-md-6 form-group mb-3">
                      <label for="additional_info">Additional Information</label>
                          <textarea id="additional_info" name="additional_info" rows="4" cols="50" class="form-control" maxlength="1001"></textarea>
                          <span class="error text-danger err-password" id="additional_info_error" >
                         </span>
                    </div>
                </div>
                 
                <div class="row">
                    <div class="col-sm-12 btn-center">
                      <button type="submit" class="btn btn-primary btn-rounded m-1" id="submitButton">Submit</button> 
                    </div>
                </div>
          </form>
        </div>
    </div>
@endsection

@section('page-js')

<script>
  var select = $('.selectbox');
  var single_select = $('.selectbox2');
  function formatSelection(state) {
    return state.text;   
}

function formatResult(state) {
//     console.log(state)
    if (!state.id) return state.text; 
    var id = 'state' + state.id.toLowerCase();
    var label = $('<label></label> ' , { for: id })
            .text(state.text);
    var checkbox = $('<input type="checkbox">' , { id: id });
    
    return checkbox.add(label);   
}

  select.select2({
    closeOnSelect: true,
    allowClear: true,
    formatResult: formatResult,
    formatSelection: formatSelection,
    theme: "bootstrap",
    escapeMarkup: function (m) {
        return m;
    },
    matcher: function(term, text) {

        if (term.length === 0) return true;
        texts = text.split(" ");

        allCombinations = permute(texts, [], []);

        for (i in allCombinations) {
          if (allCombinations[i].join(" ").toUpperCase().indexOf(term.toUpperCase()) === 0) {
            return true;
          }
        }
        return false;
      }

});

  $( function() {
    $( "#dob" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd-mm-yy',
      yearRange: "-90:+0",
      maxDate: 0,
    });
  });


    $(function(){
      $('#mobile_number').keypress(function(e){
        if(e.which!=8 && e.which!=0 && (e.which<48 || e.which>57))
        {
          return false;
        }
      });
    });

    $('#userform').validate({

          ignore: "",
          ignore: [],
        rules: {
            name:{
               only_text: true,
               required: true,
                maxlength:100,
            },
            email:{
                required: true,
                custom_email: true,
                maxlength:100,
            },
            mobile_number:{
              required: true,
                number : true,
                maxlength:10,   
            }, 
            role:{
                required: true,
            },
            'account[]':{
                // required: true,
            },  
            password:{
                required: true,  
                minlength: 8,  
                maxlength: 30,  
            },
            confirmpassword:{
              required: true, 
              equalTo : "#password",
              minlength: 8,
              maxlength: 30, 
            },
            additional_info:{
              maxlength: 1000, 
            },            
          },
           messages:{
            name:{
              required: "Name is Required"
            },
             email:{
              required: "Email is Required"
            },
             mobile_number:{
              required: "Mobile Number is Required"
            },
             role:{
              required: "Role is Required"
            },
            additional_info:{
              maxlength: "This field can not exceed 1000 characters"
            },
             password:{
              required: "Password is Required"
            },
            confirmpassword:{
              equalTo: "Confirm Password should be same as Password"
            },
          },
          submitHandler:function(){
            $('#submitButton').attr('disabled',true);
            $('.loadscreen').show();
            $.ajax({
                method: "POST",
                headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },  
                url: "{{url('user/store')}}",
                data:new FormData($("#userform")[0]),
                contentType: false,
                processData: false,
              })
            .done(function( data ) {
                $('.loadscreen').hide();
                $('#submitButton').removeAttr('disabled',true);
                if(data.status== 'success'){
                    $("#userform")[0].reset();
                    $("#alert-primary").text('User has been Created successfully!');
                    $("#successmsg").show();
                    $('html, body').animate({
                      scrollTop: $('.main-header').offset().top
                  }, 1000);
                    setTimeout(function(){ $('#successmsg').fadeOut() }, 3000);
                    window.location.href = "{{url('user/list')}}";
                }
                 else if(data.status=='validation_error'){
                    $.each(data.data, function (key, val) {
                        $("#"+key+"_error").text(val[0]);
                        $("#"+key+"_error").show();
                        $(document).on("keyup", "input[name='"+key+"']", function(e){
                              $("#"+key+"_error").hide();
                         });
                         $(document).on("change", "input[name='"+key+"']", function(e) {
                              $("#"+key+"_error").hide();
                         });
                    });
                }
                else{
                    $("#alert-primary").text('Something Went Wrong !');
                    $("#successmsg").show();
                    $('html, body').animate({
                      scrollTop: $('.main-header').offset().top
                    }, 1000);
                    setTimeout(function(){ $('#successmsg').fadeOut() }, 3000);
                }
                
            });
            }
        });
</script>

@endsection

