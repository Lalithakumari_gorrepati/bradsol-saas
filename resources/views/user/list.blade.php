@extends('layouts.master')

@section('main-content')
<div class="row">
  <div class="col-md-12">
          @if(session()->has('success'))
          <div class="col-md-12 alert_notice">
              <div class="alert alert-primary " role="alert">
                  {{ session()->get('success') }}
                  <button class="close float-right" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              </div>
          </div>
          @endif
          <div class="card o-hidden mb-4">
              <div class="card-header">
                  <h3 class="w-50 float-left card-title m-0">List of Users</h3>
                  @can('Add Users')
                   <a href="{{url('user/create')}}"  class="btn btn-primary btn-rounded m-1" style="float:right">Add New User</a>
                   @endcan
              </div>
              <div class="card-body">
		         <div class="row">
		           <div class="col-md-12 mb-3 font-size"><b>Filters By:</b></div>
                <div style="padding-top: 6px; padding-left: 10px;">User Role</div>
                    <div class="col-md-3">
                        <div class="form-group">
                          <select class="form-control" name="role" id="filterbyRole"  >
                            <option value="" hidden>Please Select</option>
                            @foreach($role as $value)
                            @if($value->name!='SuperAdmin')
                              <option value="{{$value->id}}" >{{$value->name}}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                    </div>
                    @role('SuperAdmin')
                    <div style="padding-top: 6px; padding-left: 10px;">Accounts</div>
                        <div class="col-md-3">
                      <div class="form-group">
                        <select class="form-control" name="account" id="account"  >
                          <option value="" hidden>Please Select</option>
                          @foreach($accounts as $value)
                            <option value="{{$value->id}}" >{{$value->account_name}}</option>
                          @endforeach
                        </select>
                      </div>
                      </div>
                    @endrole
                
                      <!-- <div class="col-md-2"></div> -->
                  <div class="col-sm-1" style="padding-left: 30px;">
                      <button type="button" id="filter" name="filter" class="btn btn-primary btn-rounded">Filter</button></div>
                      <div class="col-sm-1" style="padding-left: 30px;">
                      <button type="button" id="reset" name="reset" class="btn btn-primary btn-rounded">Reset</button>
                  </div>
		         </div>
                     
             <br>
             <input type="hidden" value="{{csrf_token()}}" name="_token" id="token">
            <table id="userlist_table" class="table table-bordered  text-center">
                  <thead>
                      <tr>
                          <!-- <th scope="col">S.No</th> -->
                          <th scope="col">Name</th>
                          <th scope="col">Email</th>
                          <th scope="col">Phone</th>
                          <th scope="col">Status</th>
                          <th scope="col">User created Date</th>
                          <th scope="col">Role</th>
                          <th width="width: 50px;" scope="col">Action</th>
                      </tr>
                  </thead>
                  <tbody id="data"></tbody>
               </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('bottom-js')
<script>
  $(document).ready(function(){
      setTimeout(function(){
        $('.alert_notice').hide('slow');
      }, 3000);

        var userData = $('#userlist_table').DataTable({
          "Processing":true,
          "serverSide":true,
          
          "ajax":{
                url : "{{url('user/data-list')}}",
                method :"POST",
                headers: {
			          'X-CSRF-TOKEN': $('#token').val()
			      },
                data : function ( d ) {
                    return $.extend( {}, d, {
                         "role" : $('#filterbyRole').val(),
                         "account": $('#account').val(),
                      });
                  },
        },
        "scrollY"       : "500px",
        "scrollCollapse": true,
       
        "columns": [
         {"data":"name"},
        {"data":"email"},
        {"data":"mobile_number"},
        {"data":"status"},
        {"data":"created_date"},
        {"data":"role_name"},
        {"data":"action"},
          
        ],
        "aaSorting": [],
        "aLengthMenu": [[10,25, 50, 75,100, -1], 
                [10,25, 50, 75,100, "All"]],
        "columnDefs": [
        { "orderable": false, "targets": [6]}
        ],
        dom: 'lBfrtip',
        "buttons": [
               {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2,3,4,5]
                }
            },
        ]
        });

        $('#filter').click(function(){
            userData.draw();
        });

        $('#reset').click(function(){
            $("#filterbyRole").val('').trigger('change');
            $("#account").val('');
            userData.draw();
        });

});      
  
</script>

@endsection

