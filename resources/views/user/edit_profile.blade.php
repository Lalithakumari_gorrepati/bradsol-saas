@extends('layouts.master')
@section('main-content')

  <div class="col-md-12" id="successmsg" style="display: none">
    <div class="alert alert-primary " id="alert-primary">
        <button class="close float-right" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"></span></button>
    </div>
  </div>	

  <div class="col-md-12">
    <h4>Edit Profile</h4>
                
    <div class="card mb-5">
      <div class="card-body">
      	<nav>
          <div class="nav nav-tabs" id="nav-tab" role="tablist">
            	<a class="nav-item nav-link active" id="home-basic-tab" data-toggle="tab" href="#homeBasic" role="tab" aria-controls="homeBasic" aria-selected="true">User Details</a>
              <a class="nav-item nav-link" id="profile-basic-tab" data-toggle="tab" href="#profileBasic" role="tab" aria-controls="profileBasic" aria-selected="false">Password</a>
          </div>                          
        </nav>
      <div class="tab-content" id="myTabContent">
        @php
          $user_id=Auth::id();
        @endphp
        <div class="tab-pane fade show active" id="homeBasic" role="tabpanel" aria-labelledby="home-basic-tab">
          <form id="editform">
                  @csrf
                        <input type="hidden" name="id" value="{{$user_id}}" id="customer_id">

                        <div class="row">
                            <div class="col-md-6 form-group mb-3">
                            <label for="name">Full Name<span style="color: red">*</span></label>
                                <input type="text" name="name" class="form-control" id="name" value="{{$data->name}}" placeholder="Enter name">
                                <span class="error text-danger" id="name_error"></span>
                            </div>
                            <div class="col-md-6 form-group mb-3">
                             <label for="email">Email<span style="color: red">*</span></label>
                                <input type="email" name="email" class="form-control" id="email" value="{{$data->email}}" placeholder="Enter email">
                                <span class="error text-danger" id="email_error"></span>
                            </div>
                        </div>
                       
                        <div class="row">
                           <div class="col-md-6 form-group mb-3">
                            <label for="mobile_number">Phone Number<span style="color: red">*</span></label>
                            
                                <input type="text" minlength="10" maxlength="10" size="10" name="mobile_number" class="form-control" id="mobile_number" placeholder="Enter Phone" value="{{$data->mobile_number}}">
                                <span class="error text-danger err-phone" id="mobile_number_error"></span>
                            </div>

                            <div class="col-md-6 form-group mb-3">
                            <label for="role">Roles<span style="color: red">*</span></label>
                                <input type="hidden" name="role" value="{{$data->role}}">
                                <input type="text" name="" value="{{$data->role_name}}" class="form-control" readonly="">
                                <span class="text-danger" id="role_error">
                                </span>
                            </div>

                        </div>
                        
                         <div class="row">
                            <div class="col-md-6 form-group mb-3">
                            <label for="user_accounts">Account</label>
                                <select name="" class="form-control" disabled="">
                                  @foreach ($accounts as $account)
                                      <option value="{{$account->id}}" {{in_array($account->id, $oldAccounts ?: []) ? "selected": ""}}>{{$account->account_name}}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger" id="account_id_error">
                                </span>
                            </div>

                            <div class="col-md-6 form-group mb-3">
                              <label for="status">User Status</label>
                                <input type="text"  value="{{$data->status}}" class="form-control" readonly="">
                                @if ($data->is_active=='Y')
                                  <input type="hidden" name="user_status" value="{{$data->is_active}}" class="form-control" readonly="">
                                  @else
                                  <input type="hidden" name="user_status" value="" class="form-control" readonly="">
                                @endif
                            </div>
                        </div>

                        <div class="row">
                          <div class="col-md-6 form-group mb-3">
                            <label for="additional_info">Additional Information</label>
                              <textarea id="additional_info" name="additional_info" rows="4" cols="50" class="form-control" maxlength="500">{{$data->additional_info}}</textarea>
                              <span class="error text-danger err-password" id="additional_info_error" >
                              </span>                 
                          </div>
                      </div>
                       
                     <div class="row col-md-12 btn-center">
                           <button type="submit" id="submitButton" class="btn btn-primary btn-rounded mt-1">Submit</button>
                     </div>
              </form>                           
        </div>
                                
        <div class="tab-pane fade" id="profileBasic" role="tabpanel" aria-labelledby="profile-basic-tab">
                
          <div class="card-body">
              <form action="#" id="editPasswordForm" method="post" enctype="multipart/form-data">
             	@csrf
          		<input type="hidden" name="id" value="{{$user_id}}">
                <div class="form-group row">
                    <label for="staticEmail6" class="ul-form__label ul-form--margin col-lg-3 col-form-label ">Enter Old Password:</label>
                    <div class="col-lg-6">
                        <input type="password" class="form-control" id="oldPassword" name="oldPassword" placeholder="Enter Old Password">
                    	<span class="text-danger err-password" id="oldPassword_error">
                		</span>
                    </div>
              	</div>

              	<div class="form-group row">
                    <label for="staticEmail6" class="ul-form__label ul-form--margin col-lg-3 col-form-label ">Enter New Password:</label>
                    <div class="col-lg-6">
                        <input type="password" class="form-control" id="newPassword" name="newPassword" placeholder="Enter New Password">
                        <span class="text-danger err-password" id="newPassword_error">
                        </span>
                    </div>
           		  </div>

           		  <div class="form-group row">
                  <label for="staticEmail6" class="ul-form__label ul-form--margin col-lg-3 col-form-label ">Confirm Password:</label>
                  <div class="col-lg-6">
                      <input type="password" class="form-control" id="confirmPassword" name="confirmPassword" placeholder="Confirm Password">
                  	  <span class="text-danger err-password" id="confirmPassword_error">
                      </span>
                  </div>
               	</div>
           		 <div class="row col-md-12 btn-center">
             		  <button type="button" id="passwordSubmit" class="btn btn-primary btn-rounded mt-3">Submit</button>
       			    </div>
            	</form>
          </div>
        </div>
      </div>
    </div>
    </div>
  </div>
  
@endsection
@section('bottom-js')

<script>
  $( function() {
    $( "#dob" ).datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: "-90:+0",
      dateFormat: 'dd-mm-yy',
      maxDate:0
    });
  } );

$('#editform').validate({
    ignore: "",
    ignore: [],
    rules: {
        name:{
          only_text: true,
          required: true,
          minlength:3,
          maxlength:100,
        },
        email:{
            required: true,
            custom_email: true,
            maxlength:100,
        },
        mobile_number:{
          required: true,
          numeric : true,
          maxlength:10,
        },
        'account_id[]':{
            // required: true,
        },
        role:{
            required: true,
        },
      },
      messages:{
            name:{
              required: "Name is Required"
            },
             email:{
              required: "Email is Required"
            },
             mobile_number:{
              required: "Mobile Number is Required"
            },
             role:{
              required: "Role is Required"
            },
            
          },
    submitHandler:function(){
      $('#submitButton').attr('disabled',true);
      $('.loadscreen').show();
      $.ajax({
          method: "POST",
          headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }, 
          url: "{{url('user/update')}}",
          data:new FormData($("#editform")[0]),
          contentType: false,
          processData: false,
        })
      .done(function( data ) {
            $('.loadscreen').hide();  
            $('#submitButton').removeAttr('disabled',true);
          if(data.status== 'success'){
              $("#alert-primary").text('Profile Updated Successfully !');
              $("#successmsg").show();
              $('html, body').animate({
                scrollTop: $('.main-header').offset().top
            }, 1000);
              setTimeout(function(){ $('#successmsg').fadeOut() }, 3000);
          }
          else if(data.status== 'validation_error'){
              $.each(data.data, function (key, val) {
                $("#"+key+"_error").text(val[0]);
                $("#"+key+"_error").show();
                $(document).on("keyup", "input[name='"+key+"']", function(e){
                      $("#"+key+"_error").hide();
                 });
                 $(document).on("change", "input[name='"+key+"']", function(e) {
                      $("#"+key+"_error").hide();
                 });
            });
          }
          else{
              $("#alert-primary").text('Something Went Wrong !');
              $("#successmsg").show();
              $('html, body').animate({
                scrollTop: $('.main-header').offset().top
              }, 1000);
              setTimeout(function(){ $('#successmsg').fadeOut() }, 3000);
          }
      });
    }
  });
  
  $('#user_accounts').on('change',function(){
      $('#account_id_error').hide();
  })

  $(function(){
      $('#mobile_number').keypress(function(e){
        if(e.which!=8 && e.which!=0 && (e.which<48 || e.which>57))
        {
          return false;
        }
      });
  });

$('#editPasswordForm').validate({
  rules: {
    confirmPassword:{
        required: true,
        minlength:6,
        maxlength:30,
        equalTo: "#newPassword"
      },
      oldPassword:{
       required: true,
       maxlength:30
      }, 
      newPassword:{
        required: true,
        minlength:6,
        maxlength:30,
        notEqualTo:"#oldPassword"
      },
  },
messages:{
    confirmPassword:{
      equalTo:"Confirm Password Should be same as Password"
    },
    newPassword:{
      notEqualTo:"Old password and New password must be different"
    }
  }
});
 $("#passwordSubmit").on('click', function(){	
	var data=$('#editPasswordForm').serialize();
	if($('#editPasswordForm').valid()){
		$.ajax({
	        method: "POST",
	        url : "{{url('user/update-password')}}",
	        data     :data,
	      })
        .done(function( data ) {
	        if(data.status == 'success'){
	        	$("#oldPwdError").html('').show();
	        	$(".alert-primary").html("Password Updated Successfully!");
                $(".alert_notice").show();
                $('html, body').animate({
                    scrollTop: $('.main-header').offset().top
                }, 1000);
                setTimeout(function(){ $('.alert_notice').fadeOut() }, 5000);
                window.location.href = "{{url('/logout')}}";
	        }else if(data.status == 'validation_error'){
              $.each(data.data, function (key, val) {
                alert(key)
                alert(val)
                $("#"+key+"_error").text(val[0]);
                $("#"+key+"_error").show();
                $(document).on("keyup", "input[name='"+key+"']", function(e){
                      $("#"+key+"_error").hide();
                 });
              });

	        }
	        else{
	        	$(".alert-primary").html("Something Went Wrong!");
                $(".alert_notice").show();
                $('html, body').animate({
                    scrollTop: $('.main-header').offset().top
                }, 1000);
                setTimeout(function(){ $('.alert_notice').fadeOut() }, 5000);
	        }
		});
	}
}); 
</script>


@endsection
