
@extends('layouts.master')
@section('main-content')
<div class="row">
                <div class="col-md-12">
                  <h4 class="pb-2">User Details<span class="float-right"><a class="btn btn-primary btn-rounded btn-md" href="{{url('user/list')}}" >Back</a></span></h4>
                   
                    <div class="card mb-5">
                        <div class="card-body">
                           <form action="createrole" class="was-validated" method="post">
                 
                                <div class="row">
                                  <div class="col-md-6 form-group mb-3">
                                  
                                    <label for="name">Full Name</label>
                                        <input type="text" name="name" class="form-control" id="name" value="{{$data->name}}" readonly style="background-color: #f8f9fa">
                                    </div>
                                         <div class="col-md-6 form-group mb-3">
                                              <label for="email"> Email</label>
                                        <input type="email" name="email" class="form-control" id="email" value="{{$data->email}}" readonly style="background-color: #f8f9fa">
                                    </div>
                                </div>
                               <div class="row">
                                  <div class="col-md-6 form-group mb-3">
                                    <label for="phone">Phone Number</label>
                                        <input type="text" name="phone" class="form-control" id="phone" value="{{$data->mobile_number}}" readonly style="background-color: #f8f9fa">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                      <label for="phone"> Role</label>
                                        <select class="form-control"  name="role" disabled="" style="background-color: #f8f9fa">
                                            @foreach ($role as $roles)
                                              <option value="{{$roles->id}}"<?= ($data->role == $roles->id) ? 'selected' : ''?> >{{$roles->name}}</option>
                                              
                                            @endforeach
                                        </select>
                                     </div>
                                </div>
                              
                                 <div class="row">
                                    <div class="col-md-6 form-group mb-3">
                                    <label for="phone">Account</label>
                                        <select class="selectbox form-control" multiple  disabled="" style="background-color: #f8f9fa">
                                            @foreach ($accounts as $account)
                                              <option value="{{$account->id}}"{{in_array($account->id, $oldAccounts ?: []) ? "selected": ""}} >{{$account->account_name}}</option>
                                              
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-md-6 form-group mt-4">
                                      <label for="status">User Status</label>
                                      <label class="switch pr-5 switch-primary mr-3">
                                        <span>{{$data->status}}</span>
                                    
                                             @if($data->status == 'Active')
                                              <input type="checkbox"  checked="checked"  disabled ><span class="slider">
                                              @else
                                             <input type="checkbox" disabled ><span class="slider">
                                             @endif
                                        </span>
                                      </label>
                                  </div>

                                  <div class="col-md-6 form-group mb-3">
                                   <label for="status">Additional Information</label>
                                    <textarea id="additional_info" name="additional_info" rows="4" cols="50" class="form-control" readonly style="background-color: #f8f9fa">{{$data->additional_info}}</textarea>
                                    </span>                 
                                  </div>
                                </div>
                                  
                                </form>
                        </div>
                    </div>
                </div>
            </div>
@endsection
@section('bottom-js')
<script>
  $(document).ready(function(){
  var select = $('.selectbox');
  var single_select = $('.selectbox2');
  function formatSelection(state) {
    return state.text;   
}

function formatResult(state) {
    if (!state.id) return state.text; 
    var id = 'state' + state.id.toLowerCase();
    var label = $('<label></label> ' , { for: id })
            .text(state.text);
    var checkbox = $('<input type="checkbox">' , { id: id });
    
    return checkbox.add(label);   
}

  select.select2({
    closeOnSelect: true,
    allowClear: true,
    formatResult: formatResult,
    formatSelection: formatSelection,
    theme: "bootstrap",
    escapeMarkup: function (m) {
        return m;
    },
    matcher: function(term, text) {

        if (term.length === 0) return true;
        texts = text.split(" ");

        allCombinations = permute(texts, [], []);

        for (i in allCombinations) {
          if (allCombinations[i].join(" ").toUpperCase().indexOf(term.toUpperCase()) === 0) {
            return true;
          }
        }
        return false;
      }

});
});
</script>

@endsection