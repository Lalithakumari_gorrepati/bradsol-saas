@extends('layouts.master')

@section('main-content')
<div class="row">
    <div class="col-md-12">
        <h4 class="pb-2">Account Details<span class="float-right"><a class="btn btn-primary btn-rounded btn-md" href="{{url('account/list')}}" >Back</a></span></h4>
        <div class="card mb-5">
			<div class="card-body">
				<form action="createrole" class="was-validated" method="post">
					<div class="row">
						<div class="col-md-6 form-group mb-3">
                        	<label for="name">Account Name</label>
                            <input type="text" name="name" class="form-control" id="name" value="{{$data->account_name}}" readonly style="background-color: #f8f9fa">
                        </div>

                        <div class="col-md-6 form-group mb-3">
                            <label for="name">Account Domain</label>
                            <input type="text" name="name" class="form-control" id="name" value="{{$data->account_domain}}" readonly style="background-color: #f8f9fa">
                        </div>
					</div>
                    <div class="row">
                        <div class="col-md-6 form-group mb-3">
                            <label for="email"> Contact Name</label>
                            <input type="email" name="email" class="form-control" id="email" value="{{$data->contact_name}}" readonly style="background-color: #f8f9fa">
                        </div>
                          
                        <div class="col-md-6 form-group mb-3">
                        <label for="phone">Contact Email</label>
                            <input type="text" name="phone" class="form-control" id="phone" value="{{$data->contact_email}}" readonly style="background-color: #f8f9fa">
                        </div>
                    </div>
					<div class="row">
                        <div class="col-md-6 form-group mb-3">
                        <label for="phone">Contact Phone</label>
                            <input type="text" name="phone" class="form-control" id="phone" value="{{$data->contact_phone}}" readonly style="background-color: #f8f9fa">
                        </div>

						<div class="col-md-6 form-group mb-3">
							<label for="status">Account Status</label>
							<input type="text" name="phone" class="form-control" id="phone" value="{{$data->status}}" readonly style="background-color: #f8f9fa">
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<label for="Additional">Additional Information</label>
							<textarea id="additional_info" name="additional_info" rows="6" cols="100" class="form-control" readonly style="background-color: #f8f9fa">{{$data->additional_info}}</textarea>
						</div> 
                        <div class="col-md-6 form-group mb-3">
                            <label for="profile">Company Logo</label><br>
                            @if(!empty($data->logo))
                               <img src="{{asset('uploads')}}/{{$data->logo}}" style="height:150px; width:150px;">
                            @else 
                              <h4>No Image found...</h4>
                            @endif
                        </div> 

					</div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
