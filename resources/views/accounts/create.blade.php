@extends('layouts.master')
@section('page-css')
<style>
.pip {
  display: inline-block;
  margin: 10px 10px 0 0;
}
.remove {
  display: block;
 color: black;
  text-align: right;
  cursor: pointer;
}
.remove:hover {
  background: white;
  color: black;
}

.filenameupload {
    width: 70%;
    padding: 3px;
    border-radius: 3px;
    margin-left: 14px;
}

#upload_prev {
  /*border:thin solid #000;*/
  width: 65%;
  padding:0.5em 1em 1.5em 1em;
  margin-left: 17px;
    border-radius: 5px;
}

#upload_prev span {
    display: flex;
  padding: 0 5px;
  font-size:12px;
}

p.close {
    margin: 0 5px 0 5px;
    cursor: pointer;
  padding-bottom:0;
}

</style>
@endsection

@section('main-content')
<div class="col-md-12" id="successmsg" style="display: none">
    <div class="alert alert-primary " id="alert-primary">
      <button class="close float-right" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"></span></button>
    </div>
</div>
<div class="col-md-12">
    <h4 class="pb-2">Add New Account<span class="float-right">
    	<a class="btn btn-primary btn-rounded" href="{{url('account/list')}}">Cancel</a></span>
	</h4>
	<div class="card mb-5">
    	<div class="card-body">
         	<form id="account_form" enctype="multipart/form-data">
                 <input type="hidden" value="{{csrf_token()}}" name="csrf-token" id="token">
             	<div class="row">
	                  <div class="col-md-6 form-group mb-3">
	                    <label for="account_name">Account Name<span style="color: red">*</span></label>
	                    <input type="text" name="account_name" class="form-control" id="account_name" placeholder="Enter Account Name">
	                     <span class="error text-danger err-name" id="account_name_error"></span>
                    </div>

                    <div class="col-md-6 form-group mb-3">
                      <label for="account_domain">Account Domain<span style="color: red">*</span></label>
                      <input type="text" name="account_domain" class="form-control" id="account_domain" placeholder="Enter Account Domain">
                       <span class="error text-danger err-name" id="account_domain_error"></span>
                    </div> 
                </div>
                <div class="row">    
                    <div class="col-md-6 form-group mb-3">
	                    <label for="contact_name">Contact Name
	                    	<span style="color: red">*</span>
	                    </label>
                        <input type="text" name="contact_name" class="form-control" id="contact_name" placeholder="Enter Contact Name">
                        <span class="error text-danger err-contact_name" id="contact_name_error"></span>
                    </div>

                    <div class="col-md-6 form-group mb-3">
                      <label for="contact_email">Contact Email<span style="color: red">*</span></label>
                    
                        <input type="email" name="contact_email" class="form-control" id="contact_email" placeholder="Enter Email"  value="{{ old('email') }}">
                        <span class="error text-danger err-email" id="contact_email_error"></span>
                    </div>

                </div>

                <div class="row">

	                  <div class="col-md-6 form-group mb-3">
	                    <label for="contact_phone">Contact Phone<span style="color: red">*</span></label>
	                    <input type="text" minlength="10" maxlength="10" size="10" name="contact_phone" class="form-control" id="contact_phone" placeholder="Enter Phone" value="{{ old('phone') }}">
                        <span class="error text-danger err-phone" id="contact_phone_error"></span>
                    </div> 

                    <div class="col-md-6 form-group mt-3">
                    <label for="Upload">Upload Company Logo</label><br>
                      <span class="pip mb-3" style="display:none;"> 
                      <span class="remove" >
                        <i class='fa fa-times'></i></span> 
                       <img src="" style="display:none;height:120px; width:150px;" id="OpenImgUpload">
                     </span><br>
                      <input type="file" name="image_path" id="image_path" >
                     </div>
                </div>
               
                <div class="row mb-1">
                   <div class="col-md-6">
                    <label for="additional_info">Additional Information</label>
                        <textarea id="additional_info" name="additional_info" rows="5" cols="100" class="form-control" ></textarea>
                        <span class="error text-danger err-password" id="additional_info_error" >
                       </span>
                    </div>
                 </div><hr>
                 
                <div class="row"> 
                  <div class="col-md-6 form-group mb-1">
                  <label><h5>User Details</h5></label>
                </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group mb-3">
                      <label for="user_name">User Name<span style="color: red">*</span></label>
                      <input type="text" name="user_name" class="form-control" id="user_name" placeholder="Enter User Name">
                       <span class="error text-danger err-name" id="user_name_error"></span>
                    </div>

                    <div class="col-md-6 form-group mb-3">
                      <label for="user_phone">User Phone<span style="color: red">*</span></label>
                      <input type="text" name="user_phone" class="form-control" id="user_phone" minlength="10" maxlength="10" placeholder="Enter User Phone">
                       <span class="error text-danger err-name" id="user_phone_error"></span>
                    </div> 
                </div>

                <div class="row mb-2">
                    <div class="col-md-6 form-group mb-3">
                      <label for="user_email">User Email<span style="color: red">*</span></label>
                      <input type="text" name="user_email" class="form-control" id="user_email" placeholder="Enter User Email">
                       <span class="error text-danger err-name" id="user_email_error"></span>
                    </div>

                    <div class="col-md-6 form-group mb-3">
                     <label for="password">Password<span style="color: red">*</span></label>
                        <input type="password" name="password" class="form-control" id="password" placeholder="Enter Password" value="{{ old('password') }}" autocomplete="new-password">
                        <span class="error text-danger err-password" id="password_error">
                       </span>
                   </div>
                </div>
                
                <div class="row">
                   <div class="col-md-6 form-group mb-3">
                     <label for="confirm_password">Confirm Password<span style="color: red">*</span></label>
                    
                        <input type="password" name="confirmpassword" class="form-control" id="cppassword" placeholder="Enter Confirm Password">
                        <span class="text-danger err-confirmpassword" id="confirmpassword_error">
                        </span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 btn-center">
                      <button type="submit" id="submitButton" class="btn btn-primary btn-rounded m-1">Submit</button> 
                    </div>
                </div>
            </form>
      	</div>
    </div>
</div>
@endsection

@section('page-js')

<script>
  $(document).on('click','.close',function(){
    $(this).parents('span').remove();
  })

  $(function(){
      $('#phone').keypress(function(e){
        if(e.which!=8 && e.which!=0 && (e.which<48 || e.which>57))
        {
          return false;
        }
      });
    });
    $('#OpenImgUpload').click(function(){ 
        $('#image_path').trigger('click');
   });

  var img = $('#uploadPic').val();

  if(img == null || img == '' || img == undefined){
  	   $(".pip").hide();
  }

function readURL(input) {
	  if (input.files && input.files[0]) {
	    var reader = new FileReader();
	    
	    reader.onload = function(e) {
	      $('#OpenImgUpload').show();
	        $('.pip').show();
	        $('#OpenImgUpload').attr('src', e.target.result);


	        $(".remove").click(function(){
	            $('.pip').hide();
	             $('#OpenImgUpload').hide();
	             $('#image_path').val("");
	          });
	    }
	    reader.readAsDataURL(input.files[0]); // convert to base64 string
	  }
	}

$("#image_path").change(function() {
    readURL(this);
});

$.validator.addMethod("only_text", function(value, element) {
  return this.optional(element) || value == value.match(/^[ a-zA-Z][ a-zA-Z_/-]*$/);
  },"Enter valid name");

    $('#account_form').validate({
        ignore: "",
        rules: {
            account_name:{
               required: true,
               only_text: true,
               minlength:3,
               maxlength:100,
            },
            account_domain:{
               required: true,
               minlength:3,
               maxlength:100,
            },
            contact_name:{
               required: true,
               only_text: true,
               minlength:3,
               maxlength:100,
            },
            contact_email:{
                required: true,
                custom_email: true,
                maxlength:100,
            },
            contact_phone:{
              required: true,
	            number : true,
	            maxlength:10,
                
            }, 
            user_name:{
               required: true,
               only_text: true,
               minlength:3,
               maxlength:100,
            },
            user_phone:{
              required: true,
              number : true,
              maxlength:10,
            }, 
            user_email:{
              required: true,
              custom_email: true,
              maxlength:100,
            },
            password:{
                required: true,  
                minlength: 8,  
                maxlength: 30,  
            },
            confirmpassword:{
              required: true, 
              equalTo : "#password",
              minlength: 8,
              maxlength: 30, 
            },
            image_path:{
            	extension:"jpg|jpeg|png|ico|bmp"
            }, 
            additional_info:{
              maxlength:1000,
            }, 
          },
           messages:{
            account_name:{
              required: "Account name is Required"
            },
            account_domain:{
              required: "Account domain is Required"
            },
            contact_name:{
              required: "Contact name is Required"
            },
             contact_email:{
              required: "Contact Email is Required"
            },
             
             contact_phone:{
              required: "Contact Phone is Required"
            },
            
      		image_path:{
       			extension: "Only Images allowed"
      		}
    	},
    	submitHandler:function(){
            $('#submitButton').attr('disabled',true);
            $('.loadscreen').show();
            $.ajax({
                method: "POST",
                headers: {
          			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		      	},	
                url: "{{url('account/store')}}",
                data:new FormData($("#account_form")[0]),
                contentType: false,
           		processData: false,
              })
            .done(function( data ) {
                $('.loadscreen').hide();
                $('#submitButton').removeAttr('disabled',true);
                if(data.status=='success'){
                    $("#account_form")[0].reset();
                    $("#alert-primary").text('Account Created Successfully !');
                    $("#successmsg").show();
                    $('html, body').animate({
	                    scrollTop: $('.main-header').offset().top
	                }, 1000);
                    setTimeout(function(){ $('#successmsg').fadeOut() }, 3000);
                    window.location.href = "{{url('account/list')}}";
                }
                else if(data.status=='validation_error'){
                  $.each(data.data, function (key, val) {
                        $("#"+key+"_error").text(val[0]);
                        $("#"+key+"_error").show();
                        $(document).on("keyup", "input[name='"+key+"']", function(e) {
                              $("#"+key+"_error").hide();
                         });
                    });
                }
              	else{
                   $("#alert-primary").text('Something Went Wrong !');
                    $("#successmsg").show();
                    $('html, body').animate({
                      scrollTop: $('.main-header').offset().top
                  }, 1000);
                    setTimeout(function(){ $('#successmsg').fadeOut() }, 3000);
                }
                
            });
            }

        });
</script>

@endsection

