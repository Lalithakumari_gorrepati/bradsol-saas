@extends('layouts.master')

@section('main-content')
<div class="row">
    <div class="col-md-12">
        @if(session()->has('success'))
        <div class="col-md-12 alert_notice">
            <div class="alert alert-primary " role="alert">
                {{ session()->get('success') }}
                <button class="close float-right" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
        </div>
        @endif
        <div class="card o-hidden mb-4">
            <div class="card-header">
                <h3 class="w-50 float-left card-title m-0">List of Accounts</h3>
                @can('Add Account')
                 <a href="{{url('account/create')}}"  class="btn btn-primary btn-rounded m-1" style="float:right">Add New Account</a>
                @endcan
                
            </div>
            <div class="card-body">
                <table id="accountslist_table" class="table table-bordered  text-center">
                    <thead>
                        <tr>
                            <th scope="col">Account Name</th>
                            <th scope="col">Contact Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Status</th>
                            <th scope="col">Additional Info</th>
                            <th scope="col">Created Date</th>
                            <th width="width: 50px;" scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody id="data"></tbody>
                 </table>                          
            </div>
        </div>
    </div>
</div>

@endsection

@section('bottom-js')
<script>
  $(document).ready(function(){
        var userData = $('#accountslist_table').DataTable({
          "Processing":true,
          "serverSide":true,
          
          "ajax":{
                url : "{{url('account/data-list')}}",
                method :"POST",
                headers: {
			          'X-CSRF-TOKEN': $('#token').val()
			      },
                data : function ( d ) {
                    return $.extend( {}, d, {
                         
                      });
                  },
        },
        "scrollY"       : "500px",
        "scrollCollapse": true,
        "scrollX"       : true,
        "columns": [
         {"data":"account_name"},
         {"data":"contact_name"},
        {"data":"contact_email"},
        {"data":"contact_phone"},
        {"data":"status"},
        {"data":"additional_info"},
        {"data":"created_date"},
        {"data":"action"},
          
        ],
        "aaSorting":[],
        "aLengthMenu": [[10,25, 50, 75,100, -1], 
                [10,25, 50, 75,100, "All"]],
        "columnDefs": [
        { "orderable": false, "targets": 7}
        ],
        dom: 'lBfrtip',
              "buttons": [
               {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5,6]
                }
            },
        ]
    });

});    
</script>
@endsection

