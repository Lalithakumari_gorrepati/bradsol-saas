@extends('layouts.master')
@section('before-css')
<style>
.pip {
  display: inline-block;
  margin: 10px 10px 0 0;
}
.remove {
  display: block;
 color: black;
  text-align: center;
  cursor: pointer;
  color: red;
}
.remove:hover {
  background: white;
  color: black;
}

</style>
@endsection

@section('main-content')
    <div class="col-md-12" id="successmsg" style="display: none">
          <div class="alert alert-primary " id="alert-primary">
              <button class="close float-right" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"></span></button>
          </div>
    </div>
    <div class="row">
      <div class="col-md-12">
          <h4 class="pb-2">Edit Account Details<span class="float-right"><a class="btn btn-primary btn-rounded btn-md" href="{{url('account/list')}}" >Cancel</a></span></h4>
             
          <div class="card mb-5">
              <div class="card-body">
                 <form id="account_form" enctype="multipart/form-data">
                      <div class="row">
                        <input type="hidden" value="{{csrf_token()}}" name="csrf-token" id="token">
                        <input type="hidden" name="id" value="{{$data->id}}" id="account_id">
                        <div class="col-md-6 form-group mb-3">
                          <label for="account_name">Account Name</label>
                              <input type="text" name="account_name" class="form-control" id="account_name" value="{{$data->account_name}}">
                              <span class="error text-danger err-name" id="account_name_error"></span>
                          </div>

                          <div class="col-md-6 form-group mb-3">
                          <label for="account_domain">Account Domain</label>
                              <input type="text" name="account_domain" class="form-control" id="account_domain" value="{{$data->account_domain}}">
                              <span class="error text-danger err-name" id="account_domain_error"></span>
                          </div>
                      </div>
                     <div class="row">
                        <div class="col-md-6 form-group mb-3">
                              <label for="email"> Contact Name</label>
                              <input type="text" name="contact_name" class="form-control" id="contact_name" value="{{$data->contact_name}}">
                              <span class="error text-danger err-contact_name" id="contact_name_error"></span>
                        </div>
                        <div class="col-md-6 form-group mb-3">
                          <label for="phone">Contact Email</label>
                              <input type="email" name="contact_email" class="form-control" id="contact_email" value="{{$data->contact_email}}">
                              <span class="error text-danger err-email" id="contact_email_error"></span>
                          </div>
                      </div>

                       <div class="row">

                        <div class="col-md-6 form-group mb-3">
                          <label for="phone">Contact Phone</label>
                              <input type="text" name="contact_phone" class="form-control" id="contact_phone" value="{{$data->contact_phone}}">
                              <span class="error text-danger err-phone" id="contact_phone_error"></span>
                          </div>

                        <div class="col-md-6 form-group mb-3">
                          <label for="status">Account Status</label>
                           <label class="switch pr-5 switch-primary m-3">
                               <input type="checkbox" name="status" <?=($data->status == 'Active')? "checked" : " " ?> ><span class="slider">
                          </label>
                        </div>
                    </div>
                         
                    <div class="row">
                      <div class="col-md-6">
                      <label for="Additional">Additional Information</label>
                          <textarea id="additional_info" name="additional_info" rows="5" cols="100" class="form-control" value="">{{$data->additional_info}}</textarea>
                          <span class="error text-danger" id="additional_info_error" >
                       </span>
                    </div> 

                    <div class="col-md-6 form-group mb-3">
                        <label for="profile">Company Logo</label><br>
                          @if(!empty($data->logo))
                            <span class="pip">  
                                <img src="{{asset('uploads')}}/{{$data->logo}}" style="height:120px; width:150px;" id="OpenImgUpload"> 
                                <span class="remove">remove</span>
                            </span><br>

                            <input type="file" name="image_path" id="image_path" style="display:none" accept="image/jpgc,image/png,image/jpeg,image/gif"/> 
                            <input type="hidden" name="image_path" value="{{$data->logo}}" id="uploadPic">
                          @else 
                            <span class="pip">  
                                <img  style="display:none;height:120px; width:150px;"id="OpenImgUpload">
                                 <span class="remove">remove</span>
                            </span><br>
                            <span class="upload_logo">
                            <input type="file" name="image_path" id="image_path" accept="image/jpg,image/png,image/jpeg,image/gif"/>
                            </span> 
                          @endif
                        </div>
                   </div>
                   <div class="row col-md-12 btn-center">
                       <button type="submit" id="submitButton" class="btn btn-primary btn-rounded mt-3">Submit</button>
                   </div>
                </form>
              </div>
          </div>
      </div>
  </div>

@endsection


@section('page-js')
<script>
    //code for select image
    $('#OpenImgUpload').click(function(){ 
       $('#image_path').trigger('click');
    });
     
    var img = $('#uploadPic').val();
    if(img == null || img == '' || img == undefined){
       $(".pip").hide();
    }
    // code for preview image
      function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
              $('#OpenImgUpload').show();
                $('.pip').show();
                $('#OpenImgUpload').attr('src', e.target.result);

                $(".remove").click(function(){
                    $('.pip').hide();
                     $('#OpenImgUpload').hide();
                     $('#image_path').val("");
                  });
            }
            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
      }
          
    $("#image_path").change(function() {
        readURL(this);
    });
    $("#uploadPic").change(function() {
        readURL(this);
    });

    $(".remove").click(function(){
            $('.pip').hide();
             $('#OpenImgUpload').hide();
             $('#image_path').val("");
             $('#uploadPic').attr('type', 'file');
             $('#uploadPic').val("");
    });

$(document).ready(function(){
  
  $('#account_form').validate({
          ignore: "",
          rules: {
            account_name:{
               required: true,
               only_text: true,
               minlength:3,
               maxlength:100,
              
            },
            contact_name:{
               required: true,
               only_text: true,
               minlength:3,
               maxlength:100,
            },
            contact_email:{
                required: true,
                custom_email: true,
                maxlength:100,
            },
            contact_phone:{
              required: true,
              number : true,
              maxlength:10,
                
            }, 
            image_path:{
              extension:"jpg|jpeg|png|ico|bmp"
            }, 
            additional_info:{
              maxlength:1000,
            }, 
          },
           messages:{
            account_name:{
              required: "Account name is Required"
            },
            contact_name:{
              required: "Contact name is Required"
            },
             contact_email:{
              required: "Contact Email is Required"
            },
            contact_phone:{
              required: "Contact Phone is Required"
            },
            
            image_path:{
              extension: "Only Images allowed"
            }
      },
    
        submitHandler:function(){
            $('#submitButton').attr('disabled',true);
            $('.loadscreen').show();
            $.ajax({

                method: "POST",
                headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },  
                url: "{{url('account/update')}}",
                data:new FormData($("#account_form")[0]),
                contentType: false,
                processData: false,
              })
            .done(function( data ) {
                $('.loadscreen').hide();
                if(data== 'success'){
                    $("#account_form")[0].reset();
                    $("#alert-primary").text('Account Updated Successfully !');
                    $("#successmsg").show();
                    $('html, body').animate({
                      scrollTop: $('.main-header').offset().top
                  }, 1000);
                    setTimeout(function(){ $('#successmsg').fadeOut() }, 3000);
                    window.location.href = "{{url('account/list')}}";
                }
                else if(data=='fail'){
                    $("#alert-primary").text('Something Went Wrong !');
                    $("#successmsg").show();
                    $('html, body').animate({
                      scrollTop: $('.main-header').offset().top
                  }, 1000);
                    setTimeout(function(){ $('#successmsg').fadeOut() }, 3000);
                    window.location.href = "{{url('account/list')}}";
                }
                else{
                   $('#submitButton').removeAttr('disabled',true);
                    $.each(data.errors, function (key, val) {
                      console.log(key)
                        $("#"+key+"_error").text(val[0]);
                        $("#"+key+"_error").show();
                        $(document).on("keyup", "input[name='"+key+"']", function(e) {
                              $("#"+key+"_error").hide();
                         });
                    });
                }
            });
            }
        });
}) 
</script>
@endsection
