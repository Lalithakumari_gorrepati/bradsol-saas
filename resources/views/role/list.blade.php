 @extends('layouts.master')
@section('main-content')
<div class="row">
	<div class="col-md-12">
		@if(session()->has('success'))
		<div class="col-md-12 alert_notice">
			<div class="alert alert-primary " role="alert">
				{{ session()->get('success') }}
				<button class="close float-right" type="button" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		</div>
		@endif
		<div class="card o-hidden mb-4">
			<div class="card-header">
				<h3 class="w-50 float-left card-title m-0">List of Roles</h3>
				@can('Add Roles') 
					<a href="{{url('role/create')}}" class="btn btn-primary btn-rounded m-1" style="float: right"> Add New Role</a>
				@endcan
			</div>
			<div class="card-body">

				<div class="table-responsive">

					<input type="hidden" value="{{csrf_token()}}" name="_token"
						id="token">
					<table id="userlist_table" class="table table-bordered">
						<thead>
							<tr>
								<!-- <th scope="col">S.No</th> -->
								<th scope="col">Name</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody id="data"></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection 

@section('bottom-js')


<script>
 	setTimeout(function(){
        $('.alert_notice').hide('slow');
    }, 3000);
$('#userlist_table').DataTable({
	 "Processing":true,
	  "serverSide":true,
	  
	"ajax":{
        url : "{{url('role/data-list')}}",
        method :"POST",
        headers: {
	          'X-CSRF-TOKEN': $('#token').val()
	      },
        data : function ( d ) {
            return $.extend( {}, d, {
                 
              });
          },
},
 
"columns": [
{"data":  "name"},

{"data":  "action"},
  
],
"aLengthMenu": [[10,25, 50, 75,100, -1], 
        [10,25, 50, 75,100, "All"]],
"columnDefs": [
{ "orderable": false, "targets": 1 }
],
dom: 'lBfrtip',
      "buttons": [
           {
            extend: 'excelHtml5',
            exportOptions: {
                columns: [ 0 ]
            }
            },
           ]
});


</script>

@endsection

