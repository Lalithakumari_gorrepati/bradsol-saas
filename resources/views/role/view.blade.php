
@extends('layouts.master')
@section('before-css')

@endsection

@section('main-content')


<div class="row">
                <div class="col-md-12">
                        <h4 class="pb-2">View Role<span class="float-right mr-2"> <a class="btn btn-primary btn-rounded " href="{{url('role/list')}}" >Back</a></span></h4>
                   
                    <div class="card mb-5">
                        <div class="card-body">
                           <form action="createrole" class="was-validated" method="post">
                 				<div class="row">
         									<div class="col-md-6 form-group mb-3">
                              <label for="firstName1">Role Name</label>
                             	<input type="name" name="name" class="form-control" id="name" value="{{$role->name}}" readonly>
                          </div>
         							 		<div class="col-md-6 form-group mb-3">
                            <label for="firstName1">Description</label>
                           	<input type="description" name="description" class="form-control" id="description" value="{{$role->description}}" readonly>
                        </div>
                        @role('SuperAdmin')
                        <div class="col-md-6 form-group mb-3">
                        <label for="phone">Account<span style="color: red">*</span></label>
                          <input class="form-control" id="name" value="{{$role->account_name}}" readonly>
                        </div>
                        @endrole
   							      </div>

                    @if ($role->name!='Member') 

                      <div class="row">
                        <div class="col-form-label col-sm-2 pt-0"></div>
                        <div class="col-sm-10"></div>
                        <div class="col-sm-2"></div>
                        <div class="col-sm-8 font-weight-bold pb-2">Dashboard:</div>
                                
                          @foreach ($permissions['dashboard'] as $permission)
                              <div class="col-sm-3"></div>
                          <div class="col-sm-3">
                              <div class="form-group">
                                @if($stored_permissions)
                                
                                <input type="checkbox" class="i-checks" 
                                {{in_array($permission->id,$stored_permissions)?'checked':''}}
                                value="{{$permission->name}}"
                                 name="Permission[]" class="checkbox1" disabled>
                                 <label for="checkbox4" class="permissions">{{$permission->name}}</label> 
                                

                                @else
                                  <input type="checkbox" class="i-checks" value="{{$permission->name}}"
                                 name="Permission[]" class="checkbox1" disabled>
                                <label for="checkbox4" class="permissions">{{$permission->name}}</label> 
                                
                                @endif  

                               </div>        
                        </div>
                          @endforeach
                      </div>

                     <div class="row">
                        <div class="col-form-label col-sm-2 pt-0"></div>
                        <div class="col-sm-10"></div>
										    <div class="col-sm-2"></div>
									      <div class="col-sm-8 font-weight-bold pb-2">User & Role Management:</div>
                                
                          @foreach ($permissions['userRole'] as $permission)
                              <div class="col-sm-3"></div>
                          <div class="col-sm-3">
                              <div class="form-group">
                                @if($stored_permissions)
                                
                                <input type="checkbox" class="i-checks" 
                                {{in_array($permission->id,$stored_permissions)?'checked':''}}
                                value="{{$permission->name}}"
                                 name="Permission[]" class="checkbox1" disabled>
                                 <label for="checkbox4" class="permissions">{{$permission->name}}</label> 
                                

                                @else
                                  <input type="checkbox" class="i-checks" value="{{$permission->name}}"
                                 name="Permission[]" class="checkbox1" disabled>
                                <label for="checkbox4" class="permissions">{{$permission->name}}</label> 
                                
                                @endif  

                               </div>        
                        </div>
                          @endforeach
                      </div>
                      @role('SuperAdmin')
                      <div class="row">
                        <div class="col-form-label col-sm-2 pt-0"></div>
                        <div class="col-sm-10"></div>
                        <div class="col-sm-2"></div>
                        <div class="col-sm-8 font-weight-bold pb-2">Accounts Management:</div>
                                
                          @foreach ($permissions['accountPermissions'] as $permission)
                              <div class="col-sm-3"></div>
                          <div class="col-sm-3">
                              <div class="form-group">
                                @if($stored_permissions)
                                
                                <input type="checkbox" class="i-checks" 
                                {{in_array($permission->id,$stored_permissions)?'checked':''}}
                                value="{{$permission->name}}"
                                 name="Permission[]" class="checkbox1" disabled>
                                 <label for="checkbox4" class="permissions">{{$permission->name}}</label> 
                                

                                @else
                                  <input type="checkbox" class="i-checks" value="{{$permission->name}}"
                                 name="Permission[]" class="checkbox1" disabled>
                                <label for="checkbox4" class="permissions">{{$permission->name}}</label> 
                                
                                @endif  

                               </div>        
                        </div>
                          @endforeach
                      </div>
                      @endrole

                      <div class="row">
                        <div class="col-form-label col-sm-2 pt-0"></div>
                        <div class="col-sm-10"></div>
                        <div class="col-sm-2"></div>
                        <div class="col-sm-8 font-weight-bold pb-2">Member Management:</div>
                                
                          @foreach ($permissions['memberPermissions'] as $permission)
                              <div class="col-sm-3"></div>
                          <div class="col-sm-3">
                              <div class="form-group">
                                @if($stored_permissions)
                                
                                <input type="checkbox" class="i-checks" 
                                {{in_array($permission->id,$stored_permissions)?'checked':''}}
                                value="{{$permission->name}}"
                                 name="Permission[]" class="checkbox1" disabled>
                                 <label for="checkbox4" class="permissions">{{$permission->name}}</label> 
                                

                                @else
                                  <input type="checkbox" class="i-checks" value="{{$permission->name}}"
                                 name="Permission[]" class="checkbox1" disabled>
                                <label for="checkbox4" class="permissions">{{$permission->name}}</label> 
                                
                                @endif  

                               </div>        
                        </div>
                          @endforeach
                      </div>

                      <div class="row">
                        <div class="col-form-label col-sm-2 pt-0"></div>
                        <div class="col-sm-10"></div>
                        <div class="col-sm-2"></div>
                        <div class="col-sm-8 font-weight-bold pb-2">Corpus Fund Setting:</div>
                                
                          @foreach ($permissions['corpusPermissions'] as $permission)
                              <div class="col-sm-3"></div>
                          <div class="col-sm-3">
                              <div class="form-group">
                                @if($stored_permissions)
                                
                                <input type="checkbox" class="i-checks" 
                                {{in_array($permission->id,$stored_permissions)?'checked':''}}
                                value="{{$permission->name}}"
                                 name="Permission[]" class="checkbox1" disabled>
                                 <label for="checkbox4" class="permissions">{{$permission->name}}</label> 
                                

                                @else
                                  <input type="checkbox" class="i-checks" value="{{$permission->name}}"
                                 name="Permission[]" class="checkbox1" disabled>
                                <label for="checkbox4" class="permissions">{{$permission->name}}</label> 
                                
                                @endif  

                               </div>        
                        </div>
                          @endforeach
                      </div>

                      <div class="row">
                        <div class="col-form-label col-sm-2 pt-0"></div>
                        <div class="col-sm-10"></div>
                        <div class="col-sm-2"></div>
                        <div class="col-sm-8 font-weight-bold pb-2">Fraternity Contribution Setting:</div>
                                
                          @foreach ($permissions['FCPermissions'] as $permission)
                              <div class="col-sm-3"></div>
                          <div class="col-sm-3">
                              <div class="form-group">
                                @if($stored_permissions)
                                
                                <input type="checkbox" class="i-checks" 
                                {{in_array($permission->id,$stored_permissions)?'checked':''}}
                                value="{{$permission->name}}"
                                 name="Permission[]" class="checkbox1" disabled>
                                 <label for="checkbox4" class="permissions">{{$permission->name}}</label> 
                                

                                @else
                                  <input type="checkbox" class="i-checks" value="{{$permission->name}}"
                                 name="Permission[]" class="checkbox1" disabled>
                                <label for="checkbox4" class="permissions">{{$permission->name}}</label> 
                                
                                @endif  

                               </div>        
                        </div>
                          @endforeach
                      </div>

                      <div class="row">
                        <div class="col-form-label col-sm-2 pt-0"></div>
                        <div class="col-sm-10"></div>
                        <div class="col-sm-2"></div>
                        <div class="col-sm-8 font-weight-bold pb-2">Payments Management:</div>
                                
                          @foreach ($permissions['PaymentsPermissions'] as $permission)
                              <div class="col-sm-3"></div>
                          <div class="col-sm-3">
                              <div class="form-group">
                                @if($stored_permissions)
                                
                                <input type="checkbox" class="i-checks" 
                                {{in_array($permission->id,$stored_permissions)?'checked':''}}
                                value="{{$permission->name}}"
                                 name="Permission[]" class="checkbox1" disabled>
                                 <label for="checkbox4" class="permissions">{{$permission->name}}</label> 
                                

                                @else
                                  <input type="checkbox" class="i-checks" value="{{$permission->name}}"
                                 name="Permission[]" class="checkbox1" disabled>
                                <label for="checkbox4" class="permissions">{{$permission->name}}</label> 
                                
                                @endif  

                               </div>        
                        </div>
                          @endforeach
                      </div>

                      <div class="row">
                        <div class="col-form-label col-sm-2 pt-0"></div>
                        <div class="col-sm-10"></div>
                        <div class="col-sm-2"></div>
                        <div class="col-sm-8 font-weight-bold pb-2">Reports Management:</div>
                                
                          @foreach ($permissions['ReportsPermissions'] as $permission)
                              <div class="col-sm-3"></div>
                          <div class="col-sm-3">
                              <div class="form-group">
                                @if($stored_permissions)
                                
                                <input type="checkbox" class="i-checks" 
                                {{in_array($permission->id,$stored_permissions)?'checked':''}}
                                value="{{$permission->name}}"
                                 name="Permission[]" class="checkbox1" disabled>
                                 <label for="checkbox4" class="permissions">{{$permission->name}}</label> 
                                

                                @else
                                  <input type="checkbox" class="i-checks" value="{{$permission->name}}"
                                 name="Permission[]" class="checkbox1" disabled>
                                <label for="checkbox4" class="permissions">{{$permission->name}}</label> 
                                
                                @endif  

                               </div>        
                        </div>
                          @endforeach
                      </div>

                      @endif

                      @if ($role->name=='Member')
                      <div class="row">
                        <div class="col-form-label col-sm-2 pt-0"></div>
                        <div class="col-sm-10"></div>
                        <div class="col-sm-2"></div>
                        <div class="col-sm-8 font-weight-bold pb-2">Member Dashboard:</div>
                          @foreach ($permissions['memberDashboard'] as $permission)
                              <div class="col-sm-3"></div>
                          <div class="col-sm-3">
                              <div class="form-group">
                                @if($stored_permissions)
                                
                                <input type="checkbox" class="i-checks" 
                                {{in_array($permission->id,$stored_permissions)?'checked':''}}
                                value="{{$permission->name}}"
                                 name="Permission[]" class="checkbox1" disabled>
                                 <label for="checkbox4" class="permissions">{{$permission->name}}</label> 
                                

                                @else
                                  <input type="checkbox" class="i-checks" value="{{$permission->name}}"
                                 name="Permission[]" class="checkbox1" disabled>
                                <label for="checkbox4" class="permissions">{{$permission->name}}</label> 
                                
                                @endif  

                               </div>        
                        </div>
                          @endforeach
                      </div>
                      @endif
                      
                            </form>
                        </div>
                    </div>
                </div>
            </div>
@endsection

