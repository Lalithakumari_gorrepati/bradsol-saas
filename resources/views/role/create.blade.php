@extends('layouts.master') 

@section('main-content')
<div class="col-md-12">
	<h4 class="pb-2">Add New Role<span class="float-right mr-2"> 
		<a class="btn btn-primary btn-rounded" href="{{url('role/list')}}">Cancel</a></span>
	</h4>
	@php $account_id = Session::get('account_id'); @endphp
	<div class="card mb-5">
		<div class="card-body">
			<form action="{{url('role/store')}}" id="createRoleForm" method="post">
				{{ csrf_field() }}
				<div class="row">
					<div class="col-md-6 form-group mb-3">
						<label for="firstName1">Role Name<span style="color: red">*</span></label>
						<input type="text" id="name" name="name" class="form-control"
							placeholder="Enter Role Name"> <label
							class="error text-danger err-name" id="Role_name_error_msg">
							@if($errors->has('name')) {{ $errors->first('name') }} @endif </label>
					</div>
					<div class="col-md-6 form-group mb-3">
						<label for="firstName1">Description<span style="color: red">*</span></label>
						<input type="text" name="description"
							class="form-control" id="description"
							placeholder="Enter Description"> <label
							class="error text-danger err-description"
							id="description_error_msg"> @if($errors->has('description')) {{
							$errors->first('description') }} @endif </label>
					</div>

				</div>
				@role('SuperAdmin')
				<div class="row">
                  	<div class="col-md-6 form-group mb-3">
                     <label for="confirm_password">Account<span style="color: red">*</span></label>
                      <select name="account" class=" form-control @error('account') is-invalid @enderror" id="accountMain">
                            <option value="" hidden>Please Select</option>
                            @foreach ($accounts as $account)
                              <option value="{{$account->id}}">{{$account->account_name}}</option>
                            @endforeach
                          </select>
                	</div>
                </div>
                @else
                  	<input type="hidden" name="account" value="{{$account_id}}">
                @endrole
				<div class="row">
					<div class="col-form-label font-weight-bold col-sm-2 pt-0">All Permissions</div>
					<div class="col-sm-10"></div>
					<div class="col-sm-2"></div>
					<div class="col-sm-8 font-weight-bold  pb-2">User & Role
						Management:</div>
					@foreach($data['userRole'] as $Permission)
					<div class="col-sm-3"></div>
					<div class="col-sm-3">
						<div class="form-group">
							<tr>
								<input type="checkbox" id="" name="Permissions[]"
									value="{{$Permission['name']}}">
								<td scope="col-md-3">{{$Permission['name']}}</td>
								<br>
							</tr>
						</div>
					</div>
					@endforeach

				</div>
				
				@role('SuperAdmin')
				<div class="row">
					<div class="col-form-label col-sm-2 pt-0"></div>
					<div class="col-sm-10"></div>
					<div class="col-sm-2"></div>
					<div class="col-sm-8 font-weight-bold  pb-2">Accounts Management:</div>
					@foreach($data['accountPermissions'] as $Permission)
					<div class="col-sm-3"></div>
					<div class="col-sm-3">
						<div class="form-group">
							<tr>
								<input type="checkbox" id="" name="Permissions[]"
									value="{{$Permission['name']}}">
								<td scope="col-md-3">{{$Permission['name']}}</td>
								<br>
							</tr>
						</div>
					</div>
					@endforeach
				</div>
				 @endrole
				
				@if( Helpers::getCurrentUserAccountId()  == 2 )
				@role('Admin')
				
				<div class="row">
					<div class="col-form-label col-sm-2 pt-0"></div>
					<div class="col-sm-10"></div>
					<div class="col-sm-2"></div>
					<div class="col-sm-8 font-weight-bold  pb-2">Dashboard:</div>
					@foreach($data['dashboard'] as $Permission)
					<div class="col-sm-3"></div>
					<div class="col-sm-3">
						<div class="form-group">
							<tr>
								<input type="checkbox" id="" name="Permissions[]"
									value="{{$Permission['name']}}">
								<td scope="col-md-3">{{$Permission['name']}}</td>
								<br>
							</tr>
						</div>
					</div>
					@endforeach
				</div>
				
				<div class="row">
					<div class="col-form-label col-sm-2 pt-0"></div>
					<div class="col-sm-10"></div>
					<div class="col-sm-2"></div>
					<div class="col-sm-8 font-weight-bold  pb-2">Member Management:</div>
					@foreach($data['memberPermissions'] as $Permission)
					<div class="col-sm-3"></div>
					<div class="col-sm-3">
						<div class="form-group">
							<tr>
								<input type="checkbox" id="" name="Permissions[]"
									value="{{$Permission['name']}}">
								<td scope="col-md-3">{{$Permission['name']}}</td>
								<br>
							</tr>
						</div>
					</div>
					@endforeach
				</div>
				
				<div class="row">
					<div class="col-form-label col-sm-2 pt-0"></div>
					<div class="col-sm-10"></div>
					<div class="col-sm-2"></div>
					<div class="col-sm-8 font-weight-bold  pb-2">Corpus Fund Setting:</div>
					@foreach($data['corpusPermissions'] as $Permission)
					<div class="col-sm-3"></div>
					<div class="col-sm-3">
						<div class="form-group">
							<tr>
								<input type="checkbox" id="" name="Permissions[]"
									value="{{$Permission['name']}}">
								<td scope="col-md-3">{{$Permission['name']}}</td>
								<br>
							</tr>
						</div>
					</div>
					@endforeach
				</div>
				
				<div class="row">
					<div class="col-form-label col-sm-2 pt-0"></div>
					<div class="col-sm-10"></div>
					<div class="col-sm-2"></div>
					<div class="col-sm-8 font-weight-bold  pb-2">Fraternity Contribution Setting:</div>
					@foreach($data['FCPermissions'] as $Permission)
					<div class="col-sm-3"></div>
					<div class="col-sm-3">
						<div class="form-group">
							<tr>
								<input type="checkbox" id="" name="Permissions[]"
									value="{{$Permission['name']}}">
								<td scope="col-md-3">{{$Permission['name']}}</td>
								<br>
							</tr>
						</div>
					</div>
					@endforeach
				</div>
				
				<div class="row">
					<div class="col-form-label col-sm-2 pt-0"></div>
					<div class="col-sm-10"></div>
					<div class="col-sm-2"></div>
					<div class="col-sm-8 font-weight-bold  pb-2">Payments Management:</div>
					@foreach($data['PaymentsPermissions'] as $Permission)
					<div class="col-sm-3"></div>
					<div class="col-sm-3">
						<div class="form-group">
							<tr>
								<input type="checkbox" id="" name="Permissions[]"
									value="{{$Permission['name']}}">
								<td scope="col-md-3">{{$Permission['name']}}</td>
								<br>
							</tr>
						</div>
					</div>
					@endforeach
				</div>
				
				<div class="row">
					<div class="col-form-label col-sm-2 pt-0"></div>
					<div class="col-sm-10"></div>
					<div class="col-sm-2"></div>
					<div class="col-sm-8 font-weight-bold  pb-2">Reports Management:</div>
					@foreach($data['ReportsPermissions'] as $Permission)
					<div class="col-sm-3"></div>
					<div class="col-sm-3">
						<div class="form-group">
							<tr>
								<input type="checkbox" id="" name="Permissions[]"
									value="{{$Permission['name']}}">
								<td scope="col-md-3">{{$Permission['name']}}</td>
								<br>
							</tr>
						</div>
					</div>
					@endforeach
				</div>
				<div class="row">
					<div class="col-form-label col-sm-2 pt-0"></div>
					<div class="col-sm-10"></div>
					<div class="col-sm-2"></div>
					<div class="col-sm-8 font-weight-bold  pb-2">Member Dashboard:</div>
					@foreach($data['memberDashboard'] as $Permission)
					<div class="col-sm-3"></div>
					<div class="col-sm-3">
						<div class="form-group">
							<tr>
								<input type="checkbox" id="" name="Permissions[]"
									value="{{$Permission['name']}}">
								<td scope="col-md-3">{{$Permission['name']}}</td>
								<br>
							</tr>
						</div>
					</div>
					@endforeach
				</div>

				<br>
				 @endrole
				@endif
				<div class="account-permissions"></div>
				
				
				<br>
				
				</fieldset>

				<div class="form-group row">
					<div class="col-sm-12 btn-center">
						<button type="submit" class="btn btn-primary btn-rounded m-1">Submit</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection

@section('bottom-js')
<script type="text/javascript">
	$('#createRoleForm').validate({
	  rules: {
	      name:{
	         required: true,
	         maxlength:100,
	        
	      },
	      description:{
	    	  required: true,
	          maxlength:100,
	      }
	      
	    },
	     messages:{
	      name:{
	        required: "Role Name is Required",
	        maxlength: "Role Name may not be greater than 100 characters"
	      },
	      description:{
	          required: "Description is Required",
	          maxlength: "Description may not be greater than 100 characters"
	        },
	       
		}
	});

	$('#accountMain').on('change',function() {
		$('.account-permissions').empty();
	      var id = $(this).find(':selected').val();
	      $.ajax({

	        method:"POST",
	        url:"{{ url('role/get-permissions') }}",
	        data:{
	              "_token": "{{ csrf_token() }}",
	              "id"    : id
	          },
	      })
	      .done(function( data ) {
		     console.log(data);
		      var allperms='';
		      $.each( data, function( keys, values ) {
		    	  var permssions='';		    	 
		    	$.each( values, function( key, value ) {
			    	 permssions += '<div class="col-sm-3"></div><div class="col-sm-3"><div class="form-group"><tr><input type="checkbox" id="" name="Permissions[]"value="'+value +'"><td scope="col-md-3">'+ value +'</td><br></tr></div></div>';
			    	});
		    	allperms+= '<div class="row"><div class="col-form-label font-weight-bold col-sm-2 pt-0"></div><div class="col-sm-10"></div><div class="col-sm-2"></div><div class="col-sm-8 font-weight-bold  pb-2 heading-permission">'+ keys+'</div>'+permssions+'</div>';
		    	
		    	});
		      $('.account-permissions').append(allperms);
	        if(data.status=='success'){
               
		         
	        }
	        else if(data.status=='fail'){
	          $("#alert-primary").text('Data not Found !');
	          $("#successmsg").show();
	          $('html, body').animate({
	            scrollTop: $('.main-header').offset().top
	          }, 1000);
	          setTimeout(function(){ $('#successmsg').fadeOut() }, 3000);
	        }
	      })
	  })
		
</script>
@endsection 