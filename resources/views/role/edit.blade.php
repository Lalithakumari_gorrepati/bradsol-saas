@extends('layouts.master')

@section('main-content')
    <div class="col-md-12">
            <h4 class="pb-2">Edit Role<span class="float-right mr-2"> 
              <a class="btn btn-primary btn-rounded" href="{{url('role/list')}}" >Cancel</a></span></h4>
        <div class="card mb-5">
            <div class="card-body">
               <form id="rolesform" action="{{url('role/update')}}" method="post">
                   @csrf
                      <input type="hidden" name="id" id="roleId" value="{{$role->id}}">
                      
                      <div class="row">
					              <div class="col-md-6 form-group mb-3">
                            <label for="firstName1">Role Name</label>
                           	<input type="text" name="name" class="form-control" id="name" value="{{$role->name}}" readonly>
                        </div>
			 		                <div class="col-md-6 form-group mb-3">
                            <label for="firstName1">Description<span style="color: red">*</span></label>
                           	<input type="text" name="description" class="form-control" id="description" value="{{$role->description}}">
                             <label class="error text-danger err-description">
                                @if($errors->has('description'))
                                  {{ $errors->first('description') }}
                                @endif
                           </label>
                        </div>
			                
                      @role('SuperAdmin')    
                      <div class="col-md-6 form-group mb-3">
                        <label for="phone">Account<span style="color: red">*</span></label>
                          <select class="form-control"  name="account_id" id="accountMain">
                            <option value="" hidden>Select Account</option>
                              @foreach ($accounts as $account)
                                <option value="{{$account->id}}"<?= ($role->account_id == $account->id) ? 'selected' : ''?> >{{$account->account_name}}</option>
                              @endforeach
                          </select>
                        <span class="text-danger" id="account_error">
                        </span>
                      </div>
                      @else
                      <input type="hidden" name="account_id" value="{{$role->account_id}}">
                    @endrole    
	 	                </div>
	 	            
	 	                
                    @if ($role->name!='Member')
                      
                    <div class="row">
                        <div class="col-form-label col-sm-2 pt-0"></div>
                        <div class="col-sm-10"></div>
                        <div class="col-sm-2"></div>
                        <div class="col-sm-8 font-weight-bold pb-2">User & Role Management:</div>
                        @foreach ($permissions['userRole'] as $permission)
                            <div class="col-sm-3"></div>
                            <div class="col-sm-3">
                              <div class="form-group">
                                @if($stored_permissions)
                                  <input type="checkbox" class="i-checks" 
                                  {{in_array($permission->id,$stored_permissions)?'checked':''}}
                                  value="{{$permission->id}}"
                                   name="Permission[]" class="checkbox1">
                                   <label for="checkbox4" class="permissions">{{$permission->name}}</label> 
                                @else
                                  <input type="checkbox" class="i-checks" value="{{$permission->id}}"
                                   name="Permission[]" class="checkbox1">
                                    <label for="checkbox4" class="permissions">{{$permission->name}}</label> 
                                @endif  
                              </div>        
                            </div>
                        @endforeach
                    </div>
                     <div class="account-permissions">
                     @if ($role->account_id == 2)  
                    <div class="row">
                        <div class="col-form-label col-sm-2 pt-0"></div>
                        <div class="col-sm-10"></div>
                        <div class="col-sm-2"></div>
				                <div class="col-sm-8 font-weight-bold pb-2">Dashboard:</div>
                        @foreach ($permissions['dashboard'] as $permission)
                            <div class="col-sm-3"></div>
                            <div class="col-sm-3">
                              <div class="form-group">
                                @if($stored_permissions)
                                  <input type="checkbox" class="i-checks" 
                                  {{in_array($permission->id,$stored_permissions)?'checked':''}}
                                  value="{{$permission->id}}"
                                   name="Permission[]" class="checkbox1">
                                   <label for="checkbox4" class="permissions">{{$permission->name}}</label> 
                                @else
                                  <input type="checkbox" class="i-checks" value="{{$permission->id}}"
                                   name="Permission[]" class="checkbox1">
                                    <label for="checkbox4" class="permissions">{{$permission->name}}</label> 
                                @endif  
                              </div>        
                            </div>
                        @endforeach
                    </div>

                    

                    @role('SuperAdmin')    
                    <div class="row">
                        <div class="col-form-label col-sm-2 pt-0"></div>
                        <div class="col-sm-10"></div>
                        <div class="col-sm-2"></div>
                        <div class="col-sm-8 font-weight-bold pb-2">Accounts Management:</div>
                          @foreach ($permissions['accountPermissions'] as $permission)
                            <div class="col-sm-3"></div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                  @if($stored_permissions)
                                  <input type="checkbox" class="i-checks" 
                                  {{in_array($permission->id,$stored_permissions)?'checked':''}}
                                  value="{{$permission->id}}"
                                   name="Permission[]" class="checkbox1">
                                   <label for="checkbox4" class="permissions">{{$permission->name}}</label> 
                                  
                                  @else
                                <input type="checkbox" class="i-checks" value="{{$permission->id}}"
                                   name="Permission[]" class="checkbox1">
                                 
                                <label for="checkbox4" class="permissions">{{$permission->name}}</label> 

                                  @endif  

                                 </div>        
                          </div>
                            @endforeach
                        </div>
                      @endrole
                        
                        <div class="row">
                        <div class="col-form-label col-sm-2 pt-0"></div>
                        <div class="col-sm-10"></div>
                        <div class="col-sm-2"></div>
                        <div class="col-sm-8 font-weight-bold pb-2">Member Management:</div>
                          @foreach ($permissions['memberPermissions'] as $permission)
                            <div class="col-sm-3"></div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                  @if($stored_permissions)
                                  <input type="checkbox" class="i-checks" 
                                  {{in_array($permission->id,$stored_permissions)?'checked':''}}
                                  value="{{$permission->id}}"
                                   name="Permission[]" class="checkbox1">
                                   <label for="checkbox4" class="permissions">{{$permission->name}}</label> 
                                  
                                  @else
                                <input type="checkbox" class="i-checks" value="{{$permission->id}}"
                                   name="Permission[]" class="checkbox1">
                                 
                                <label for="checkbox4" class="permissions">{{$permission->name}}</label> 

                                  @endif  

                                 </div>        
                          </div>
                            @endforeach
                        </div>

                        <div class="row">
                        <div class="col-form-label col-sm-2 pt-0"></div>
                        <div class="col-sm-10"></div>
                        <div class="col-sm-2"></div>
                        <div class="col-sm-8 font-weight-bold pb-2">Corpus Fund Setting:</div>
                          @foreach ($permissions['corpusPermissions'] as $permission)
                            <div class="col-sm-3"></div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                  @if($stored_permissions)
                                  <input type="checkbox" class="i-checks" 
                                  {{in_array($permission->id,$stored_permissions)?'checked':''}}
                                  value="{{$permission->id}}"
                                   name="Permission[]" class="checkbox1">
                                   <label for="checkbox4" class="permissions">{{$permission->name}}</label> 
                                  
                                  @else
                                <input type="checkbox" class="i-checks" value="{{$permission->id}}"
                                   name="Permission[]" class="checkbox1">
                                 
                                <label for="checkbox4" class="permissions">{{$permission->name}}</label> 

                                  @endif  

                                 </div>        
                          </div>
                            @endforeach
                        </div>

                        <div class="row">
                        <div class="col-form-label col-sm-2 pt-0"></div>
                        <div class="col-sm-10"></div>
                        <div class="col-sm-2"></div>
                        <div class="col-sm-8 font-weight-bold pb-2">Fraternity Contribution Setting:</div>
                          @foreach ($permissions['FCPermissions'] as $permission)
                            <div class="col-sm-3"></div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                  @if($stored_permissions)
                                  <input type="checkbox" class="i-checks" 
                                  {{in_array($permission->id,$stored_permissions)?'checked':''}}
                                  value="{{$permission->id}}"
                                   name="Permission[]" class="checkbox1">
                                   <label for="checkbox4" class="permissions">{{$permission->name}}</label> 
                                  
                                  @else
                                <input type="checkbox" class="i-checks" value="{{$permission->id}}"
                                   name="Permission[]" class="checkbox1">
                                 
                                <label for="checkbox4" class="permissions">{{$permission->name}}</label> 

                                  @endif  

                                 </div>        
                          </div>
                            @endforeach
                        </div>

                        <div class="row">
                        <div class="col-form-label col-sm-2 pt-0"></div>
                        <div class="col-sm-10"></div>
                        <div class="col-sm-2"></div>
                        <div class="col-sm-8 font-weight-bold pb-2">Payments Management:</div>
                          @foreach ($permissions['PaymentsPermissions'] as $permission)
                            <div class="col-sm-3"></div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                  @if($stored_permissions)
                                  <input type="checkbox" class="i-checks" 
                                  {{in_array($permission->id,$stored_permissions)?'checked':''}}
                                  value="{{$permission->id}}"
                                   name="Permission[]" class="checkbox1">
                                   <label for="checkbox4" class="permissions">{{$permission->name}}</label> 
                                  
                                  @else
                                <input type="checkbox" class="i-checks" value="{{$permission->id}}"
                                   name="Permission[]" class="checkbox1">
                                 
                                <label for="checkbox4" class="permissions">{{$permission->name}}</label> 

                                  @endif  

                                 </div>        
                          </div>
                            @endforeach
                        </div>

                        <div class="row">
                        <div class="col-form-label col-sm-2 pt-0"></div>
                        <div class="col-sm-10"></div>
                        <div class="col-sm-2"></div>
                        <div class="col-sm-8 font-weight-bold pb-2">Reports Management:</div>
                          @foreach ($permissions['ReportsPermissions'] as $permission)
                            <div class="col-sm-3"></div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                  @if($stored_permissions)
                                  <input type="checkbox" class="i-checks" 
                                  {{in_array($permission->id,$stored_permissions)?'checked':''}}
                                  value="{{$permission->id}}"
                                   name="Permission[]" class="checkbox1">
                                   <label for="checkbox4" class="permissions">{{$permission->name}}</label> 
                                  
                                  @else
                                <input type="checkbox" class="i-checks" value="{{$permission->id}}"
                                   name="Permission[]" class="checkbox1">
                                 
                                <label for="checkbox4" class="permissions">{{$permission->name}}</label> 

                                  @endif  

                                 </div>        
                          </div>
                            @endforeach
                        </div>
                        @endif
						@endif
						
                        @if ($role->name=='Member' && $role->account_id == 2)
                        <div class="row">
                        <div class="col-form-label col-sm-2 pt-0"></div>
                        <div class="col-sm-10"></div>
                        <div class="col-sm-2"></div>
                        <div class="col-sm-8 font-weight-bold pb-2">Member Dashboard:</div>
                          @foreach ($permissions['memberDashboard'] as $permission)
                            <div class="col-sm-3"></div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                  @if($stored_permissions)
                                  <input type="checkbox" class="i-checks" 
                                  {{in_array($permission->id,$stored_permissions)?'checked':''}}
                                  value="{{$permission->id}}"
                                   name="Permission[]" class="checkbox1">
                                   <label for="checkbox4" class="permissions">{{$permission->name}}</label> 
                                  
                                  @else
                                <input type="checkbox" class="i-checks" value="{{$permission->id}}"
                                   name="Permission[]" class="checkbox1">
                                 
                                <label for="checkbox4" class="permissions">{{$permission->name}}</label> 

                                  @endif  

                                 </div>        
                          </div>
                            @endforeach
                        </div>
                        @endif
                        </div>
                       
                    <div class="form-group row">
                        <div class="col-sm-12 btn-center">
                          <button  class="btn btn-primary btn-rounded m-1">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('page-js')

<script>


$('#rolesform').validate({
  rules: {
      name:{
         required: true,
         maxlength:100,
        
      },
      description:{
    	  required: true,
          maxlength:100,
      }
      
    },
     messages:{
      name:{
        required: "Role Name is Required",
        maxlength: "Role Name may not be greater than 100 characters"
      },
      description:{
          required: "Description is Required",
          maxlength: "Description may not be greater than 100 characters"
        },
       
}
  });

$('#accountMain').on('change',function() {
	$('.account-permissions').empty();
	
	var id = $(this).find(':selected').val();
      var roleid=$('#roleId').val();
      $.ajax({

        method:"POST",
        url:"{{ url('role/get-permissions') }}",
        data:{
              "_token": "{{ csrf_token() }}",              
              "id"    : id,
              "roleId"    : roleid
          },
      })
      .done(function( data ) {
	     
	      var allperms='';
	      var allpermssions=data['all_permissions'];
	      var storedpermssions=data['stored_permissions'];
	      console.log(allpermssions);
	      console.log(storedpermssions);
	      $.each( allpermssions, function( keys, values ) {
	    	  var permssions='';		    	 
	    	$.each( values, function( key, value ) {
	    		console.log(storedpermssions.includes(parseInt(key)));
		    	if(storedpermssions.includes(parseInt(key))){
			    	
			    	console.log("inside iff");
		    		permssions += '<div class="col-sm-3"></div><div class="col-sm-3"><div class="form-group"><input type="checkbox" class="i-checks" value="'+key+'"name="Permission[]" class="checkbox1" checked><label for="checkbox4" class="permissions">'+value+'</label></div></div>';
		    	}else{
			    permssions += '<div class="col-sm-3"></div><div class="col-sm-3"><div class="form-group"><input type="checkbox" class="i-checks" value="'+key+'"name="Permission[]" class="checkbox1"><label for="checkbox4" class="permissions">'+value+'</label></div></div>';
		    	}
		    	
	    	});
	    	allperms+= '<div class="row"><div class="col-form-label font-weight-bold col-sm-2 pt-0"></div><div class="col-sm-10"></div><div class="col-sm-2"></div><div class="col-sm-8 font-weight-bold  pb-2 heading-permission">'+ keys+'</div>'+permssions+'</div>';
	    	
	    	});
	      $('.account-permissions').append(allperms);
        if(data.status=='success'){
           
	         
        }
        else if(data.status=='fail'){
          $("#alert-primary").text('Data not Found !');
          $("#successmsg").show();
          $('html, body').animate({
            scrollTop: $('.main-header').offset().top
          }, 1000);
          setTimeout(function(){ $('#successmsg').fadeOut() }, 3000);
        }
      })
  })
</script>

@endsection

