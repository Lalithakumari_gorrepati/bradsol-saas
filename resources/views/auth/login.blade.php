<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>MFFSS</title>
        <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet">
        <link id="gull-theme" rel="stylesheet" href="{{  asset('theme/styles/css/lite-purple.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/custom.css')}}">
    </head>

    <body>
        <div class="auth-layout-wrap">
            <div class="auth-content">
                <div class="card o-hidden">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="p-4">
                                <div class="auth-logo text-center mb-4">
                                    <img src="{{asset('images/logo.png')}}" alt="">
                                </div>
                                <h1 class="text-center mb-3 text-14">Sign in to start your session</h1>
                                <form method="POST" action="{{ route('login') }}">
                                    @csrf
                                    <div class="form-group">
                                        <label for="email">Enter Email address</label>
                                        <input id="email"
                                            class="form-control @error('email') is-invalid @enderror"
                                            name="email" value="{{ old('email') }}" placeholder="Enter Email address" autocomplete="email"
                                            autofocus>
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Enter Password</label>
                                        <input id="password" type="password"
                                            class="form-control @error('password') is-invalid @enderror"
                                            name="password" placeholder="Enter Password" autocomplete="current-password">
                                             @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                   <!--  <div class="form-group ">
                                        <div class="">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                                <label class="form-check-label" for="remember">
                                                    {{ __('Remember Me Next Time') }}
                                                </label>
                                            </div>
                                        </div>
                                    </div>-->
                                    <div class="row col-sm-12 btn-center">
                                    <button class="btn sign-in-btn btn-rounded btn-primary btn-block mt-2">Sign In</button>
                               </div>
                                </form>
                                @if (Route::has('password.request'))
                                <div class="mt-3 text-center">
                                    {{-- <a href="{{ route('password.request') }}" class="text-muted"><u>Forgot Password?</u></a> --}}
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="{{asset('theme/js/common-bundle-script.js')}}"></script>

        <script src="{{asset('theme/js/script.js')}}"></script>
    </body>

</html>