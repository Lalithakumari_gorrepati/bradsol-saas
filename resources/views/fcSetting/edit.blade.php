@extends('layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-12">
      <div class="col-md-12" id="successmsg" style="display: none">
          <div class="alert alert-primary " id="alert-primary">
              
              <button class="close float-right" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"></span></button>
          </div>
        </div> 
    	<div class="card o-hidden mb-4">
	    	<div class="card-header">
	              <h3 class="pb-2">Edit FC Setting
                    <span class="float-right mr-2"><a class="btn btn-primary btn-rounded" href="{{url('fcSetting/view')}}">Cancel</a></span>
                </h3>
	      	</div>
	      	<div class="card-body">
	      		<form id="FcSetting">
                <div class="row">
                    <div class="col-md-6 form-group mb-3">
                        <label for="scheme_name">Scheme Name<span style="color: red">*</span></label>
                          <input type="text" name="Scheme_Name" class="form-control" value="{{$data['scheme_name']}}" maxlength="101">
                          <span class="text-danger" id="Scheme_Name_error"></span>
                      </div>
                      <div class="col-md-6 form-group mb-3">
                        <label for="fc_amount">Fraternity Amount<span style="color: red">*</span></label>
                        <input type="text" name="Fraternity_Amount" class="form-control" value="{{$data['fc_amount']}}" maxlength="8">
                        <span class="text-danger" id="Fraternity_Amount_error"></span>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-6 form-group mb-3">
                          <label for="age_from">Eligibility Age From<span style="color: red">*</span></label>
                            <input type="text" name="Eligibility_Age_From" class="form-control" value="{{$data['age_from']}}" maxlength="2">
                            <span class="text-danger" id="Eligibility_Age_From_error"></span>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                          <label for="age_to">Eligibility Age To<span style="color: red">*</span></label>
                            <input type="text" name="Eligibility_Age_To" class="form-control" id="age_to" value="{{$data['age_to']}}" maxlength="2">
                            <span class="text-danger" id="Eligibility_Age_To_error"></span>
                        </div>
                  </div>

                  <div class="row">
                      <div class="col-md-6 form-group mb-3">
                        <label for="fc_first_year">FC in First Year<span style="color: red">*</span></label>
                          <input type="text" class="form-control" name="FC_First_Year" value="{{$data['fc_first_year']}}">
                          <span class="text-danger" id="FC_First_Year_error"></span>
                      </div>

                      <div class="col-md-6 form-group mb-3">
                        <label for="fc_second_year">FC in Second Year<span style="color: red">*</span></label>
                          <input type="text" name="FC_Second_Year" class="form-control" value="{{$data['fc_second_year']}}">
                          <span class="text-danger" id="FC_Second_Year_error"></span>
                      </div>
                  </div>
                       
                  <div class="row">
                      <div class="col-md-6 form-group mb-3">
                        <label for="advance_amount">Advance Amount Half Yearly<span style="color: red">*</span></label>
                          <input type="text" name="Advance_Amount_Half_Yearly" class="form-control" value="{{$data['advance_amount']}}"
                          maxlength="8">
                          <span class="text-danger" id="Advance_Amount_Half_Yearly_error"></span>
                      </div>

                      <div class="col-md-6 form-group mb-3">
                        <label for="penalty">Due Penalty Per Month<span style="color: red">*</span></label>
                          <input type="text" name="Due_Penalty_Per_Month" class="form-control" value="{{$data['penalty']}}" style="background-color:white" maxlength="6">
                          <span class="text-danger" id="Due_Penalty_Per_Month_error"></span>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6 form-group mb-2">
                        <label for="Additional">Other Terms & Conditions</label>
                        <div class="row">
                           @if(!empty($data['terms_conditions']))
                            @foreach($data['terms_conditions'] as $key=>$value)
                              <div class="col-md-8 form-group" id="{{$key}}">
                                  <a href="{{asset('uploads')}}/{{$value}}"  target = '_blank'>{{$value}}</a>
                                  <span class="remove_customer_images" data-name="age_proof_changed" data-image="age_proof_image" onclick="removeimage({{$key}})">
                                    <i class='fa fa-times'></i>
                                  </span>
                                  <input type="hidden" class="{{$key}}" name="terms_conditions1[]" value="{{$value}}">
                              </div>
                            @endforeach
                          @endif  
                        </div>

                      <div class="row">
                        <div class="col-md-6 form-group">
                          <label>Upload Files<span style="font-size:12px;color:red;"></span></label>
                          <input type="file" class="form-control-file" name="terms_conditions[]" id="terms_conditions" style="border:0px;"  onchange="ValidateSizeadd(this,this.id)">
                          <span class="error text-danger" id="terms_conditions_error">
                          </span>
                        </div>
                        <button type="button" class="btn btn-success" style="height: 30px;margin-top: 25px;" onclick="get_divs();">Add</button>
                      </div>
                      <div id = "here">
                      </div>
                    </div>


                    <div class="col-md-6 form-group">
                        <label for="Additional">All By Laws<span style="color: red">*</span></label>
                        <textarea rows="8" class="form-control" name="All_By_Laws" id="All_By_Laws">{{$data['laws']}}</textarea>
                        <span class="text-danger" id="All_By_Laws_error"></span>
                    </div>
                  </div>
                </div>  	 
                  <div class="row col-md-12 btn-center mb-4">
                     <button type="submit" id="submitButton" class="btn btn-primary btn-rounded mt-3">Save</button>
                 </div>
          	</form>	
	      	</div>
   		</div>
   	</div>
</div>
@endsection   

@section('bottom-js')
<script>
  $('#FcSetting').validate({
    ignore: "",
    rules: {
      Scheme_Name:{
          lettersonly:true,
          required:true,
          minlength:3,
          maxlength:100,
      },
      Eligibility_Age_From:{
          required:true,
          minlength:1,
          maxlength:2,
          numeric : true,
      },
      Eligibility_Age_To:{
          required:true,
          minlength:1,
          maxlength:2,
          numeric : true,
      },
      Fraternity_Amount:{
        numeric : true,
        required:true,
        maxlength:8,
      }, 
      FC_First_Year:{
        required:true,
        maxlength:100,
      },
      FC_Second_Year:{
        numeric : true,
        required:true,
        maxlength:8,
      }, 
      Advance_Amount_Half_Yearly:{
        numeric : true,
        required:true,
        maxlength:8,
      }, 
      Due_Penalty_Per_Month:{
        numeric : true,
        required:true,
        maxlength:6,
      },
       All_By_Laws:{
        required:true,
        maxlength:3000,
      }, 
    },
    messages:{
          Scheme_Name:{
             lettersonly:"Please enter only alphabets and numbers",
          },
    },
      submitHandler:function(){
        $('#submitButton').attr('disabled',true);
        $('.loadscreen').show();
          $.ajax({
              method: "POST",
              headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },  
              url: "{{url('fcSetting/update')}}",
              data:new FormData($("#FcSetting")[0]),
              contentType: false,
              processData: false,
            })
          .done(function( data ) {
            $('.loadscreen').hide();
            $('#submitButton').removeAttr('disabled',true);
            if(data.status== 'success'){
                $("#alert-primary").text('Updated Successfully !');
                $("#successmsg").show();
                $('html, body').animate({
                  scrollTop: $('.main-header').offset().top
              }, 1000);
                setTimeout(function(){ $('#successmsg').fadeOut() }, 3000);
                $('#All_By_Laws_error').hide();
            }
            else if(data.status=='validation_error'){
              $.each(data.data, function (key, val) {
                  $("#"+key+"_error").text(val[0]);
                  $("#"+key+"_error").show();
                  $(document).on("keyup", "input[name='"+key+"']", function(e) {
                        $("#"+key+"_error").hide();
                   });
              });
          }
          else{
              $("#alert-primary").html("Something Went Wrong!");
              $("#successmsg").show();
              $('html, body').animate({
                  scrollTop: $('.main-header').offset().top
              }, 1000);
              setTimeout(function(){ $('#successmsg').fadeOut() }, 3000);
          }
        });
    }
});

  function ValidateSizeadd(file,id,i){

    if(!/(\.jpg|\.jpeg|\.png|\.pdf)$/i.test(file.value))
    { 
      alert("Invalid image file type.");      
        $('#'+id).val('');  
        return false;   
    }   
      var FileSize = file.files[0].size;
      
      if (FileSize > 6000000)
      {
        $('#terms_conditions_error').html('File size should be less than 6 MB').show();
        $('#'+id).val('');  
        return false;
      }
      else{
        $('#terms_conditions_error').hide();
      }
  }

  var s=1;
  function get_divs()
  {
    s++;
    $("#here").append('<div id = "new'+s+'" class="row"> <div class="col-md-6 form-group"><label>Upload File<span style="font-size:12px;color:red;"></span></label><input type="file" class="form-control-file" name="terms_conditions[]" id="img'+s+'" style="border:0px;" accept="image/x-png,image/png,image/gif,image/jpeg,pdf,doc"></div><button type="button" class="btn btn-danger" style="height: 30px;margin-top: 25px;font-size: 12px;line-height: 20px;" onclick="delete_divs('+s+');">remove</button></div>');
  }

  function delete_divs(s)
  {
    $("#new"+s).remove();
    s-1;
  }

  function removeimage(id){
    $('#'+id).remove()
    $('.'+id).val(" ");
    $('.'+id).attr("disabled", "disabled"); 
}
</script>
@endsection	 	