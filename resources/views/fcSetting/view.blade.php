@extends('layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-12">
    	<div class="card o-hidden mb-4">
          	<div class="card-header">
              <h3 class="float-left card-title m-0">Fraternity Contribution Setting</h3>
              @can('Edit FC Setting')
               	<a href="{{url('fcSetting/edit')}}"  class="btn btn-primary btn-rounded m-1" style="float:right">Edit Scheme</a>
              @endcan
          	</div>
          	<div class="card-body">
          		<form>
                  	<div class="row">
                    	<div class="col-md-6 form-group mb-3">
                      		<label for="name">Scheme Name</label>
                          	<input type="text" class="form-control" readonly="" value="{{$data['scheme_name']}}" style="background-color:white">
                      	</div>
                      	<div class="col-md-6 form-group mb-3">
                          <label for="fc_amount">Fraternity Amount</label>
                          <input type="text" class="form-control" readonly="" value="{{$data['fc_amount']}}" style="background-color:white">
                      	</div>
                  	</div>

                 	<div class="row">
                        <div class="col-md-6 form-group mb-3">
                          	<label for="age_from">Eligibility Age From</label>
                              <input type="email" name="contact_email" class="form-control" readonly="" value="{{$data['age_from']}}" style="background-color:white">
                          </div>

                          <div class="col-md-6 form-group mb-3">
                          	<label for="age_to">Eligibility Age To</label>
                              <input type="text" name="contact_phone" class="form-control" id="contact_phone" readonly="" value="{{$data['age_to']}}" style="background-color:white">
                          </div>
                  	</div>

                    <div class="row">
                        <div class="col-md-6 form-group mb-3">
                          <label for="fc_first_year">FC in First Year</label>
                           	<input type="text" class="form-control" readonly="" value="{{$data['fc_first_year']}}" style="background-color:white">
                        </div>

                        <div class="col-md-6 form-group mb-3">
                          <label for="fc_second_year">FC in Second Year</label>
                           	<input type="text" class="form-control" readonly="" value="{{$data['fc_second_year']}}" style="background-color:white">
                        </div>
                    </div>
                         
                      <div class="row">
                      	<div class="col-md-6 form-group mb-3">
                      		<label for="advance_amount">Advance Amount Half Yearly</label>
                          	<input type="text" class="form-control" readonly="" value="{{$data['advance_amount']}}" style="background-color:white">
                    	  </div>

                        <div class="col-md-6 form-group mb-3">
                          <label for="penalty">Due Penalty Per Month</label>
                            <input type="text" class="form-control" readonly="" value="{{$data['penalty']}}" style="background-color:white">
                        </div>
                      </div>

                      <div class="row mb-4">
                    	  <div class="col-md-6 form-group mb-3">
                      		<label for="Additional">Other Terms & Conditions</label><br>
                      		 @if(!empty($data['terms_conditions']))
                          		@foreach($data['terms_conditions'] as $key=>$value)
                              		<div class="col-md-8 form-group" id="{{$key}}">
                                  	<a href="{{asset('uploads')}}/{{$value}}"  target = '_blank'>{{$value}}</a>
                              		</div>
                            	@endforeach
                           	@else
                           	<h5>No Image found...</h5> 	
                          	@endif
                    	  </div>

                        <div class="col-md-6 form-group mb-2">
                          <label for="Additional">All By Laws</label>
                          <textarea rows="8" readonly="" class="form-control" name="" style="background-color:white">{{$data['laws']}}</textarea>
                        </div>
                      </div> 
            	</form>
          	</div>		
        </div>      
   	</div>
</div>
@endsection   