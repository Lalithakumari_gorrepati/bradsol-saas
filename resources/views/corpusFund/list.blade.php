@extends('layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-12">
                    @if(session()->has('success'))
                    <div class="col-md-12 alert_notice">
                        <div class="alert alert-primary " role="alert">
                            {{ session()->get('success') }}
                            <button class="close float-right" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                    </div>
                    @endif
                    <div class="card o-hidden mb-4">
                        <div class="card-header">
                            <h3 class="w-50 float-left card-title m-0">Corpus Fund Table Setting</h3>
                            @can('Add Corpus Fund')
                             <a href="{{url('corpusFund/create')}}"  class="btn btn-primary btn-rounded m-1" style="float:right">Add Corpus Fund</a>
                            @endcan
                            
                        </div>
                        <div class="card-body">
                            <table id="corpus_fund_list" class="table table-bordered  text-center">
                            <thead>
                                <tr>
                                    <th scope="col">Age From</th>
                                    <th scope="col">Age To</th>
                                    <th scope="col">Corpus Fund</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody id="data"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
@endsection

@section('bottom-js')
<script>
  $(document).ready(function(){
        var userData = $('#corpus_fund_list').DataTable({
          "Processing":true,
          "serverSide":true,
          
          "ajax":{
                url : "{{url('corpusFund/data-list')}}",
                method :"POST",
                headers: {
			          'X-CSRF-TOKEN': $('#token').val()
			      },
                data : function ( d ) {
                    return $.extend( {}, d, {
                         
                      });
                  },
        },
        "scrollY"       : "500px",
        "scrollCollapse": true,
       
        "columns": [
         {"data":"age_from"},
         {"data":"age_to"},
         {"data":"corpus_fund"},
         {"data":"action"},
        ],
        "aLengthMenu": [[10,25, 50, 75,100, -1], 
                [10,25, 50, 75,100, "All"]],
        "columnDefs": [
        { "orderable": false, "targets": 3}
        ],
        dom: 'lBfrtip',
            "buttons": [
               {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2]
                }
            },
        ]
        });
});    
</script>
@endsection

