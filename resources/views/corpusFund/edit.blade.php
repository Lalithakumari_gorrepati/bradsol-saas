
@extends('layouts.master')

@section('main-content')
        
        <div class="col-md-12" id="successmsg" style="display: none">
          <div class="alert alert-primary " id="alert-primary">
              
              <button class="close float-right" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"></span></button>
          </div>
        </div>    
                <div class="col-md-12">
                      <h4 class="pb-2">Edit Corpus Fund<span class="float-right mr-2"><a class="btn btn-primary btn-rounded" href="{{url('corpusFund/list')}}" >Back</a></span></h4>
                   
                <div class="card mb-5">
                        <div class="card-body">
                    <form id="editCorpusFund">
                                <input type="hidden" name="id" value="{{$data->id}}" id="fund_id">

                                <div class="row">
                                    <div class="col-md-6 form-group mb-3">
                                    <label for="age_from">Age From<span style="color: red">*</span></label>
                                        <input type="text" name="age_from" class="form-control" id="age_from" value="{{$data->age_from}}" maxlength="2">
                                        <span class="text-danger" id="age_from_error">
                                       
                                        </span>
                                    </div>
                                    <div class="col-md-6 form-group mb-3">
                                     <label for="age_to">Age To<span style="color: red">*</span></label>
                                        <input type="text" name="age_to" class="form-control" id="age_to" value="{{$data->age_to}}" maxlength="2">
                                        <span class="text-danger" id="age_to_error">
                                       
                                        </span>
                                    </div>
                                </div>
                               
                                <div class="row">
                                   <div class="col-md-6 form-group mb-3">
                                    <label for="corpus_fund">Corpus Fund Amount<span style="color: red">*</span></label>
                                    
                                        <input type="text" name="corpus_fund" class="form-control" id="corpus_fund" placeholder="Enter corpus fund" value="{{$data->corpus_fund}}" maxlength="7">
                                        <label class="error text-danger" id="corpus_fund_error">
                                       </label>
                                    </div>
                                </div>
                                
                               <div class="row col-md-12 btn-center">
                                     <button type="submit" id="btnSubmit" class="btn btn-primary btn-rounded mt-1">Submit</button>
                               </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
      @endsection

@section('bottom-js')
<script>


$.validator.addMethod("numeric", function (value, element, param) {
	 return this.optional(element) || /^[-]?[0-9]+$/.test(value);
	}, "Please enter only numbers.");

$('#editCorpusFund').validate({
    ignore: "",
    rules: {
      age_from:{
          required:true,
          minlength:1,
          maxlength:2,
          numeric : true,
      },
      age_to:{
          required:true,
          minlength:1,
          maxlength:2,
          numeric : true,
      },
     corpus_fund:{
    	  numeric : true,
        required:true,
        maxlength:7,
      }, 
    },
    submitHandler:function(){
      $('#btnSubmit').attr('disabled',true);
      $('.loadscreen').show();
            $.ajax({
                method: "POST",
                headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },  
                url: "{{url('corpusFund/update')}}",
                data:new FormData($("#editCorpusFund")[0]),
                contentType: false,
                processData: false,
              })
            .done(function( data ) {
                $('.loadscreen').hide();
                if(data== 'success'){
                    $("#editCorpusFund")[0].reset();
                    $("#alert-primary").text('Corpus Fund Updated Successfully !');
                    $("#successmsg").show();
                    $('html, body').animate({
                      scrollTop: $('.main-header').offset().top
                  }, 1000);
                    setTimeout(function(){ $('#successmsg').fadeOut() }, 3000);
                    window.location.href = "{{url('corpusFund/list')}}";
                }
                else if(data=='fail'){
                    $("#alert-primary").text('Something Went Wrong !');
                    $("#successmsg").show();
                    $('html, body').animate({
                      scrollTop: $('.main-header').offset().top
                  }, 1000);
                    setTimeout(function(){ $('#successmsg').fadeOut() }, 3000);
                }
                else{
                  $('#btnSubmit').removeAttr('disabled',true);
                    $.each(data.errors, function (key, val) {
                        $("#"+key+"_error").text(val[0]);
                        $("#"+key+"_error").show();
                        $(document).on("keyup", "input[name='"+key+"']", function(e) {
                              $("#"+key+"_error").hide();
                         });
                    });
                }
                
            });
            }
});

</script>


@endsection
