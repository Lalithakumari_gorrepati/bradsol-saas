@extends('layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-12">
          <div class="col-md-12" id="successmsg" style="display: none">
            <div class="alert alert-primary " id="alert-primary">
                
                <button class="close float-right" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"></span></button>
            </div>
          </div>
          <div class="card o-hidden mb-4">
              <div class="card-header">
                  <h4 class="pb-2">View Payment<span class="float-right mr-2"> <a class="btn btn-primary btn-rounded " href="{{url('payments/pending/list')}}" >Back</a></span></h4>
              </div>

              <div class="card-body"><br>
                <div class="row pb-3">
                  
                  <table id="members_list_table" class="table table-bordered  text-center">
                    <input type="hidden" id="member_id" value={{$member_id}}>
                    <thead>
                        <tr>
                            <th scope="col">Payment against Expired Member</th>
                            <th scope="col">FC Released</th>
                            <th scope="col">Amount</th>
                            <th scope="col">Total Due</th>
                            <th scope="col">Status</th>
                            <th width="width: 50px;" scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody id="data"></tbody>
                 </table>                          
              </div>
          </div>
      </div>
  </div>

  <!------------------- View  FC Detail Modal Start---------------------->
             
    <div class="modal fade bd-example-modal-lg " id="viewPayment" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" style="margin-top: 60px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle-2">FC Due Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body padding-style">
                <input type="hidden" value="{{csrf_token()}}" name="_token" id="token">
                <div class="form-group row">
                    <div class="col-md-6 form-group " >
                        <div class="row">
                          <div class="col-sm-5">
                              <label for=""><h6 class="labelname">FC released</h6></label>
                          </div>
                          <div class="col-sm-1">:</div>
                          <div class="col-sm-5">
                              <label for="" ><h6 class="value" id="fc_released"></h6></label>
                          </div>
                      </div>
                    </div>

                    <div class="col-md-6 form-group " >
                        <div class="row">
                          <div class="col-sm-5">
                              <label for=""><h6 class="labelname">Total FC Members</h6></label>
                          </div>
                          <div class="col-sm-1">:</div>
                          <div class="col-sm-5">
                              <label for="" ><h6 class="value" id="fc_members"></h6></label>
                               
                          </div>
                        </div>
                    </div>
                   </div> 
                    <div class="form-group row">
                        <div class="col-md-6 form-group ">
                          <div class="row">
                            <div class="col-sm-5">
                                <label for=""><h6 class="labelname">FC Due</h6></label>
                            </div>
                            <div class="col-sm-1">:</div>
                            <div class="col-sm-5">
                                <label for="" ><h6 class="value" id="fc_due"></h6></label>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 form-group ">
                          <div class="row">
                            <div class="col-sm-5">
                                <label for=""><h6 class="labelname">Admin expense</h6></label>
                            </div>
                            <div class="col-sm-1">:</div>
                              <div class="col-sm-5">
                                <label for="">
                                  <h6 class="value" id="admin_expense"></h6></label>
                              </div>
                          </div>
                        </div>
                    </div>
                    
                    <div class="row form-group">
                        <div class="col-md-6 form-group "></div>
                        <div class="col-md-6 form-group ">
                          <div class="row">
                            <div class="col-sm-5">
                                <label for=""><h6 class="labelname"><b>Total Due</b></h6></label>
                            </div>
                            <div class="col-sm-1">:</div>
                              <div class="col-sm-5">
                                <label for="">
                                  <h6 class="value" style="font-weight: bold;" id="total_due"></h6></label>
                              </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-rounded" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
  <!------------------- View  FC Detail  Modal End-------------------------->

@endsection

@section('bottom-js')
<script>
  $(document).ready(function(){
    var membersData = $('#members_list_table').DataTable({
      
      "Processing":true,
      "serverSide":true,
      
      "ajax":{
            url : "{{url('payments/pending/get-data')}}",
            method :"POST",
            headers: {
	          'X-CSRF-TOKEN': $('#token').val()
	        },
            data : function ( d ) {
                return $.extend( {}, d, {
                  "id" : $('#member_id').val(),
                });
              },
    },
    "scrollY"       : "500px",
    "scrollCollapse": true,
   
    "columns": [
    {"data":"expired_member_name"},
    {"data":"fc_released"},
    {"data":"amount_to_pay"},
    {"data":"total_payment"},
    {"data":"has_paid"},
    {"data":"action"},
      
    ],
    "aaSorting": [],
    "aLengthMenu": [[10,25, 50, 75,100, -1], 
            [10,25, 50, 75,100, "All"]],
    "columnDefs": [
    { "orderable": false, "targets": 5}
    ],
    dom: 'lfrtip',
  });


}); 

  function showViewForm(id){
      var totalFC = $('#user_'+id).data("totalfc");
      var totalMembers = $('#user_'+ id).data("totalmembers");
      var fcDue = $('#user_'+ id).data("fcdue");
      var adminExpense = $('#user_'+ id).data("adminexpense");
      var fyAdvance = $('#user_'+ id).data("fyadvance");
      var dueInterest = $('#user_'+ id).data("dueinterest");
      var totalDue = $('#user_'+ id).data("totaldue");
      var duePenalty = $('#user_'+ id).data("duepenalty");
      
      $("#fc_released").text(totalFC);
      $("#fc_members").text(totalMembers);
      $("#fc_due").text(fcDue);
      $("#admin_expense").text(adminExpense);
      $("#fy_advance").text(fyAdvance);
      $("#due_penalty").text(duePenalty);
      $("#interest").text(dueInterest);
      $("#total_due").text(totalDue);

      $('#viewPayment').modal({
          show: true,
          keyboard: false,
          backdrop: 'static'
      });
  };   
</script>
@endsection

