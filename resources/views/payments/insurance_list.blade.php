@extends('layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-12">
          <div class="col-md-12" id="successmsg" style="display: none">
            <div class="alert alert-primary " id="alert-primary">
                
                <button class="close float-right" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"></span></button>
            </div>
          </div>
          <div class="card o-hidden mb-4">
              <div class="card-header">
                  <h3 class="w-50 float-left card-title m-0">Accidental Insurance List</h3>
              </div>

              <div class="card-body">
                <div class="row pb-3">
                  <div class="col-md-12 mb-3 font-size">
                      <a href="#" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                        <b style="color: black">Filters By <i class="fas fa-chevron-circle-down"></i></b> click here
                      </a>
                  </div>
                </div>
                <div class="collapse" id="collapseExample">
                <div class="row pb-3">
                   <div class="col-md-2">Register ID</div>
                     <div class="col-md-3">
                       <input type="text" class="form-control" id="register_id" name="register_id" placeholder="Register ID" autocomplete="off">
                     </div>
                     <div class="col-md-2"></div>

                   <div class="col-md-2">Member Name</div>
                    <div class="col-md-3">
                        <input type="text" class="form-control" id="member_name" name="member_name" placeholder="Member Name" autocomplete="off">
                    </div>
                  </div>

                    <div class="row pb-3">  
                        <div class="col-md-2">Register Date From</div>
                        <div class="col-md-3">
                          <input type="text" class="form-control" id="from" name="from" placeholder="Select From Date" autocomplete="off">
                        </div>
                        <div class="col-md-2"></div>

                        <div class="col-md-2">Register Date To</div>
                        <div class="col-md-3">
                          <input type="text" class="form-control" id="to" name="to" placeholder="Select To Date" autocomplete="off">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">Member Status</div>
                        <div class="col-md-3">
                            <div class="form-group">
                              <select class="form-control" name="member_status" id="member_status">
                                <option value="" hidden="">Please Select</option>
                                  <option value="Active">Active</option>
                                  <option value="Inactive">Inactive</option>
                                  <option value="Probationary">Probationary</option>
                                  <option value="Expired">Expired</option>
                              </select>
                            </div>
                        </div>
                        <div class="col-md-2"></div>

                        <div class="col-md-2">Payment Status</div>
                        <div class="col-md-3">
                            <div class="form-group">
                              <select class="form-control" name="payment_status" id="payment_status">
                                <option value="" hidden="">Please Select</option>
                                  <option value="Y">Paid</option>
                                  <option value="N">Unpaid</option>
                              </select>
                            </div>
                        </div>
                    </div>
                    <div class="row pb-3">
                        <div class="col-6 col-md-1">
                            <button type="button" class="txt-color btn btn-primary btn-rounded" id="filter" name="filter">Filter</button>
                        </div>
                        <div class="col-6 col-md-9">
                            <button type="button" class="txt-color btn btn-primary btn-rounded" id="reset" name="reset">Reset</button>
                        </div>
                    </div>
                </div>
                  <table id="members_list_table" class="table table-bordered  text-center">
                    <thead>
                        <tr>
                            <th scope="col">Register ID</th>
                            <th scope="col">Member Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Age</th>
                            <th scope="col">Member Status</th>
                            <th scope="col">Membership Date</th>
                            <th scope="col">Registered Date</th>
                            <th width="width: 50px;" scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody id="data"></tbody>
                 </table>                          
              </div>
          </div>
      </div>
  </div>

  <!-------------------------Insurance List Modal Start----------------->   

          <div class="modal fade" id="allInusuranceList" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle-2" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                          <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalCenterTitle-2">Member Accidental Insurance</h5>
                              <button type="button" class="close cancelNote" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                    <div class="modal-body padding-style" >
                        <div class="table-responsive mt-4 mb-4">
                            <table id="insurance_list"  class="table table-bordered ">
                                <thead>
                                    <tr>
                                        <th scope="col">Year</th>
                                        <th scope="col">Payment Status</th>
                                        <th scope="col">Payment Date</th>
                                        <th scope="col">Updated By</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                               
                            </table>
                        </div>  
                    </div>
              </div>
              </div>
              </div>
        <!-------------------------Insurance List Modal End-----------------> 

        <!-------------------------Insurance Update Modal Start----------------->   

          <div class="modal fade" id="updateInsurance" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle-2" aria-hidden="true">
              <div class="modal-dialog modal-md">
                  <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle-2">Update Insurance</h5>
                            <button type="button" class="close cancel" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        
                        <div class="modal-body padding-style">
                          <form id="update_member_status">
                            <input type="hidden" name="insurance_id" id="insurance_id" value="">
                            <input type="hidden" value="{{csrf_token()}}" name="_token" id="token">
                            <div class="form-group row">
                              <div class="form-group col-md-3">
                                <label>Insurance Year<span style="color: red">*</span></label>
                              </div>
                              <div class="form-group col-md-8">
                                  <input class="form-control" type="text" name="insurance_year" id="insurance_year" readonly="">
                                  <span class="error text-danger" id="insurance_year_error"></span>
                              </div>
                            </div>

                            <div class="form-group row">
                              <div class="form-group col-md-3">
                                <label>Insurance Status<span style="color: red">*</span></label>
                              </div>
                              <div class="form-group col-md-8">
                                <select name="status" id="status" class="form-control">
                                  <option value="" hidden>Please Select</option>
                                  <option value="Y">Paid</option>
                                  <option value="N">Unpaid</option>
                                </select>
                                <span class="error text-danger" id="status_error"></span>
                              </div>
                            </div>

                            <div class="form-group row" id="payment_date_div" style="display: none">
                              <div class="form-group col-md-3">
                                <label>Payment Date<span style="color: red">*</span></label>
                              </div>
                              <div class="form-group col-md-8">
                                <input class="form-control" type="text" id="payment_date" name="payment_date" placeholder="Select Date">
                                <span class="error text-danger" id="payment_date_error"></span>
                              </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" name="submit" id="submit" class="btn btn-primary btn-rounded">Submit</button> 
                          <button type="button" class="btn btn-primary btn-rounded cancel" data-dismiss="modal">Close</button> 
                        </div>
                      </form>
                  </div>
              </div>
          </div>
        <!-------------------------Insurance Update Modal End-----------------> 
@endsection

@section('bottom-js')
<script>
  $(document).ready(function(){

    $("#from").datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      changeYear: true,
      autoclosed:true,
      maxDate:0,
      onSelect: function (date) {
          var date2 = $('#from').datepicker('getDate');
          $('#to').datepicker('option', 'minDate', date2);
      }
    });
    $('#to').datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      changeYear: true,
      maxDate:0,
      onClose: function () {
          var dt1 = $('#from').datepicker('getDate');
          var dt2 = $('#to').datepicker('getDate');
          if (dt2 <= dt1) {
              var minDate = $('#to').datepicker('option', 'minDate');
              $('#to').datepicker('setDate', minDate);
          }
      }
    });

    
        var membersData = $('#members_list_table').DataTable({
          "Processing":true,
          "serverSide":true,
          
          "ajax":{
                url : "{{url('insurance/data-list')}}",
                method :"POST",
                headers: {
			          'X-CSRF-TOKEN': $('#token').val()
			      },
                data : function ( d ) {
                    return $.extend( {}, d, {
                          "from" : $('#from').val(),
                          "to"   : $('#to').val(),
                          "member_status"   : $('#member_status').val(),
                          "payment_status"   : $('#payment_status').val(),
                         "register_id" : $('#register_id').val(),
                         "member_name" : $('#member_name').val(),
                      });
                  },
        },
        "scrollY"       : "500px",
        "scrollCollapse": true,
        "scrollX"   : true,
        "columns": [
        {"data":"member_id",
            fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                $(nTd).html("<a href='{{ url('member/view') }}/"+oData.id+"' class='anchortag'>"+oData.member_id+"</a>");
              }
        },
        {"data":"name"},
        {"data":"email"},
        {"data":"mobile_number"},
        {"data":"member_age"},
        {"data":"memeber_status"},
        {"data":"join_date"},
        {"data":"created_date"},
        {"data":"action"},
          
        ],
        "aaSorting": [],
        "aLengthMenu": [[10,25, 50, 75,100, -1], 
                [10,25, 50, 75,100, "All"]],
        "columnDefs": [
        { "orderable": false, "targets": [8]}
        ],
        dom: 'lBfrtip',
          buttons: [
            {
              extend: 'excelHtml5',
              exportOptions: {
                  columns: [0, 1, 2, 3,4,5,6,7]
              }
            },
          ]
    });
      $('#filter').click(function(){
        membersData.draw();
      });

      $('#reset').click(function(){
        $('#age_from,#age_to').prop('selectedIndex',0);
        $("#from, #to,#register_id,#member_name,#member_status,#payment_status").val('');
        membersData.draw();
    });
});  


  $( "#payment_date" ).datepicker({
      container:'#myModal',
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd-mm-yy',
      maxDate: 0,
  });

  $('#payment_date').change(function(){
      $('#payment_date-error').hide();       
  })
  function showInsurance(user_id) {
    
    $('#insurance_list').DataTable().destroy();
    
    $("#allInusuranceList").modal({
        show: true,
        keyboard: false,
        backdrop: 'static'
    });

    insuranceList =  $('#insurance_list').DataTable({
        "Processing":true,
        "serverSide":true,
              
         "ajax":{
                  url : "{{url('insurance/member/data-list')}}",
                  type  : "POST",  
                  data : function ( d ) {
                      return $.extend( {}, d, {
                        "user_id" : user_id,
                        "_token"  :$('#token').val(),
                    });
                },
          },
           "scrollY"  : "300px",
            "scrollX" : true, 
           "autoWidth": false,
        "columns": [    
          {"data":  "year"},
          {"data":  "payment_status"},
          {"data":  "payment_date"}, 
          {"data":  "updated_by"}, 
          {"data":  "action"}, 
        ],  
        "aaSorting": [],
      "aoColumnDefs": [
        {
           bSortable: false,
           aTargets: [ -1 ]
        },
      ],
      "order":[[ 0, "asc" ]],
        dom: 'lfrtip',
    });
}


$('#status').change(function(){
    var status=$(this).val()
    if (status=='Y') {
      $('#payment_date_div').show()
      $('#status_error').hide();
    }else if(status=='N'){
      $('#payment_date_div').hide()
    }
})
$(document).on("click",".change_status",function() {
    $('html, body').animate({
      scrollTop: $('.main-header').offset().top
    }, 1000);

    $("#allInusuranceList").modal('hide');

    var id = $(this).data('id');
    var status = $(this).data('status');
    var year = $(this).data('year');
    var date = $(this).data('date');
    
    if (status=='Y') {
      $('#payment_date_div').show();
      $('#status_error').hide();
    }else if(status=='N'){
      $('#payment_date_div').hide()
    }

    $('#insurance_id').val(id)
    $('#insurance_year').val(year)
    $("#status").val(status).change();
    $("#payment_date").val(date);
    $('#payment_date-error').hide()
    $("#updateInsurance").modal({
        show: true,
        keyboard: false,
        backdrop: 'static'
    });
}); 

$(document).on("click","#submit",function() {
    $('#update_member_status').validate({
        ignore: "",
        rules:{
          insurance_year:{
            required:true,
          },
          status:{
            required: true,
          },
          payment_date:{
            required: true,
          },
        }
    });
    if ($('#status').val()=='Y') {
    if($('#update_member_status').valid()){
      $('#submit').attr('disabled',true)
      var data = $('#update_member_status').serialize();
        $.ajax({
            url      : "{{url('insurance/update')}}",
            method   : 'POST',
            data   : data,
            })
          .done(function( data ) {
            $('#submit').removeAttr('disabled',true)
            if(data.status == "success")
            {
              $('#payment_date_error').hide();
              $('#updateInsurance').modal('hide');
              $("#alert-primary").html("Insurance Updated Successfully");
                $('#successmsg').show();
                $('html, body').animate({
                    scrollTop: $('.main-header').offset().top
                }, 1000);
                setTimeout(function(){ 
                  $('#successmsg').fadeOut();
                   $('#insurance_list').DataTable().draw();
                  $('#allInusuranceList').modal('show');
                }, 2000);
            
            }else if(data.status=='validation_error'){
                $('#submit').removeAttr('disabled',true);
                  $.each(data.data, function (key, val) {
                      $("#"+key+"_error").text(val[0]);
                      $("#"+key+"_error").show();
                      $(document).on("keyup", "input[name='"+key+"']", function(e) {
                            $("#"+key+"_error").hide();
                       });
                  });
              }
            else{
                  $("#alert-primary").html("Something Went Wrong!");
                  $('#updateMedicalDetails').removeAttr('disabled',true);
                  $("#successmsg").show();
                  $('html, body').animate({
                      scrollTop: $('.main-header').offset().top
                  }, 1000);
                  setTimeout(function(){ $('#successmsg').fadeOut() }, 3000);
            }
        });
      }else{
        return false;
      }
      }
      else{
        $('#status_error').text('Please change status');
        $('#status_error').show();
        return false;
      }
    });
    
    $('.cancel').on('click', function(){
       $('#status_error').hide();
    });
</script>
@endsection

