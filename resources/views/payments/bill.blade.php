<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>MF-FSS Invoice</title>

<style>
body {
	width: 100%;
    height: 100%;
	font-family: 'DejaVuSans';
	font-size: 12px;
}
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
 #list{
	color: black;
	background: white;
	font-size: 12px;
}

li{
    list-style: none;
}

li::before{
	content: "\00BB";
}
</style>
</head>
<body>
	<div style="padding: 5px;">
		<div style="margin-top: -5px;text-align: center;font-weight: bold;font-size: 14px">
			<span>MAHESH FOUNDATION</span><br>
			<span>712 & 713, 7th Floor, Raghava Ratna Towers,Chirag Ali Lane,</span><br>
			<span>Abids, Hyd-500001 Cell No.9381737965:E-Mail:maheshfoundation@yahoo.in</span><br>
		</div>
		<div style="margin-top: 5px;margin-left: 180px;text-align: center; font-weight: bold">
			<span>Contribution Period : {{$data['contribution_date1']}} to {{$data['contribution_date2']}}</span><br>
			<span>Bill No.{{$data['bill_number']}} :  Date : {{$data['bill_date']}}</span><br>
			<span>Invoice No:{{$invoice_no}}</span><br>
		</div>

		<div style="margin-top: 5px;text-align: left;font-weight: bold;width: 300px">
			<span>{{$data['member_id']}} - {{$data['name']}}</span><br>
			<span>{{$data['address']}}</span><br>
			<span>{{$data['mobile_number']}} </span><br>
		</div><br>

		<div>
			<table style="width: 100%;">
				<thead>
					<tr>
						 <th class="vl" style="width:10%;padding: 10px;text-align: center;background-color:rgb(240, 240, 240)">S.No</th>
						<th class="vl" style="width:70%;padding: 10px;text-align: center;background-color: rgb(240, 240, 240)">Particulars</th>
						<th style="width:20%;padding: 10px;text-align: center;background-color: rgb(240, 240, 240)">Amount</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($data as $key=>$element)
						@if (gettype($element) =='object')
						<tr>
							<td style="width:10%;text-align:center;padding: 5px;border: none">
								@if ($key==0)
									<span>1</span>
								@else
									<span></span>
								@endif
							</td>
							<td style="width:70%;text-align:left;padding: 5px">
								<span>{{$key+1}}) Fraternity Claim towards death of FSS Member {{$element['expired_member_name']}} ({{$element['expired_member_id']}}) on {{$element['expired_date']}} for Rs.{{$element['fc_amount']}}/- divided by {{$element['total_members']}} FSS Members</span><br>
							</td>
							<td style="width:20%;text-align:center;">
								<span>{{$element['amount_to_pay']}}</span>
								
							</td>
						</tr>
						@endif
					@endforeach

					<tr>
						<td style="width:10%;text-align:center;padding: 5px">
							<span>2</span>
						</td>
						<td style="width:70%;text-align:left;padding: 5px">
							<span>Administrative Charges (for {{$data['total_fc']}} death cases)</span><br>
						</td>
						<td style="width:20%;text-align:center;">
							<span>{{$data['total_admin_expense']}}</span><br>
						</td>
					</tr>

					<tr>
						<td style="width:10%;text-align:center;padding: 5px">
							<span>3</span>
						</td>
						<td style="width:70%;text-align:left;padding: 5px">
							<span>Penalty Charges for Old Invoice - {{$data['old_invoice_no']}}</span><br>
						</td>
						<td style="width:20%;text-align:center;">
							<span>{{$data['total_penalty']}}</span><br>
						</td>
					</tr>

					<tr >
						<td style="width:10%;text-align:center;padding: 5px">
							<span></span>
						</td>
						<td style="width:70%;text-align:left;padding: 5px">
							<span>Total :(Rupees {{$data['amount_in_word']}})</span><br>
						</td>
						<td style="width:20%;text-align:center;">
							<span>{{$data['amount']}}</span><br>
						</td>
					</tr>
				</tbody>
			</table>
		</div><br>

		<div>
			<table style="width: 100%; border: none; font-weight: bold">
				<tr>
					<td style="width:1%;text-align:left;padding: 5px; margin-top: -2px;border: none">
						<span>>></span>
					</td>
					<td style="width:95%;text-align:left;padding: 5px;border: none">
						Penalty of Rs.100/- per month will be charged subject to a maximum of Rs.250/- and interest @ 15% p.a. shall be separately levied for the period under default.
					</td>
				</tr>
				<tr>
					<td style="width:1%;text-align:left;padding: 5px;border: none">
						<span>>></span>
					</td>
					<td style="width:95%;text-align:left;padding: 5px;border: none">
						In case default exceeds 12 months from 1-10-2020, Membership would stand automatically terminated and as a consequence thereof his/her Nominee will/shall not be eligible for claim/be entitled to the benefit under the Scheme. For e.g.: 1st October 2020 payment  will  have to be  cleared by 31st October 2021 but not later than 30th September 2021 (12months) with arrears of interest and penalty.
					</td>
				</tr>
				<tr>
					<td style="width:1%;text-align:left;padding: 5px;border: none">
						<span>>></span>
					</td>
					<td style="width:95%;text-align:left;padding: 5px;border: none">
						Failure to clear the Fraternity Claim will result in termination of the Member from the Scheme and advance paid by the Member will be adjusted and account closed.
					</td>
				</tr>
				<tr>
					<td style="width:1%;text-align:left;padding: 5px;border: none">
						<span>>></span>
					</td>
					<td style="width:95%;text-align:left;padding: 5px;border: none">
						In case of death during the period under default beyond 30 days and before termination the beneficiary will be entitled to receive only 50% of the FSS Contribution subject to adjustment of all dues payable to MF-FSS.
					</td>
				</tr>
				<tr>
					<td style="width:1%;text-align:left;padding: 5px;border: none">
						<span>>></span>
					</td>
					<td style="width:95%;text-align:left;padding: 5px;margin-top: -2px;border: none">
						In case, Notice is not received by the Member due to the lapse of Postal Authorities/Courier or on account of any other reason, the Member shall obtain the details of the Contribution Amount payable by contacting/corresponding with Mahesh Foundation office at the address mentioned above and effect the payment.
					</td>
				</tr>

				<tr>
					<td style="width:1%;text-align:left;padding: 5px;border: none;">
						<span>>></span>
					</td>
					<td style="width:95%;text-align:left;padding: 5px;border: none;font-size: 13px">
						The payment may be made in any one of the following modes and intimation of the remittance be sent by e-mail to Mahesh Foundation at maheshfoundation.yahoo.in 
					</td>
				</tr>
			</table>

			<table style="margin-left: 20px; border: none; font-weight: bold">
				<tr>
					<td style="width:1%;text-align:left;padding: 5px;border: none">
						<span>•</span>
					</td>
					<td style="width:95%;text-align:left;padding: 5px;border: none">
						Cheque drawn in the name of “Mahesh Foundation A/c MF FSS” may be handed over in the office of Mahesh Foundation at the address mentioned above, or  
					</td>
				</tr>
				<tr>
					<td style="width:1%;text-align:left;padding: 5px;  border: none">
						<span>•</span>
					</td>
					<td style="width:95%;text-align:left;padding: 5px;border: none">
						The cheque may be deposited in any branch of HDFC Bank for crediting our SB A/c No.00211330014536 at HDFC Bank, Lakdikapul Branch, Hyderabad, or  
					</td>
				</tr>
				<tr>
					<td style="width:1%;text-align:left;padding: 5px;border: none">
						<span>•</span>
					</td>
					<td style="width:95%;text-align:left;padding: 5px;border: none">
						The amount may be remitted directly into our account by RTGS/NEFT. The details of our account are as follows: (PLEASE MENTION FSS MEMBER’S NAME IN RTGS/NEFT)  
					</td>
				</tr>
			</table>
			<table style="margin-left: 45px; border: none; font-weight: bold">	
				<tr>
					<td style="width:35%;text-align:left;border: none">
						<span>Name of Account</span>
					</td>
					<td style="width:1%;text-align:left;border: none">
						<span>:</span>
					</td>
					<td style="width:55%;text-align:left;border: none">
						Mahesh Foundation A/c MF FSS  
					</td>
				</tr>

				<tr>
					<td style="width:35%;text-align:left;border: none">
						<span>Name of Branch of HDFC Bank</span>
					</td>
					<td style="width:1%;text-align:left;border: none">
						<span>:</span>
					</td>
					<td style="width:55%;text-align:left;border: none">
						Lakdikapul, Hyderabad
					</td>
				</tr>

				<tr>
					<td style="width:35%;text-align:left;border: none">
						<span>Account No</span>
					</td>
					<td style="width:1%;text-align:left;border: none">
						<span>:</span>
					</td>
					<td style="width:55%;text-align:left;border: none">
						00211330014536
					</td>
				</tr>

				<tr>
					<td style="width:35%;text-align:left;border: none">
						<span>IFSC NO</span>
					</td>
					<td style="width:1%;text-align:left;border: none">
						<span>:</span>
					</td>
					<td style="width:55%;text-align:left;border: none">
						HDFC0000021
					</td>
				</tr>
			</table>
		</div><br>
		<div>
			<span style="font-weight: bold">
				Pl.send photo of the counter foil of cheque deposit slip or Rtgs/Neft details to us on Whatsapp without fail.
			</span> <br><br>
			<span style="font-weight:bold">FOR MAHESH FOUNDATION</span><br><br>
			<span style="font-weight:bold">(SANJAY  LAHOTI)</span><br>
			<span style="font-weight:bold">SECRETARY</span><br>
			<span style="font-weight:bold">Note: This is a computer generated letter and hence no signature has been affixed</span>
		</div>
		</div><br>
	</div>		
	
</body>
</html>

