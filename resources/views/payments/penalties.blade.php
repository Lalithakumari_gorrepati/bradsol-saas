@extends('layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-12">
          <div class="col-md-12" id="successmsg" style="display: none">
            <div class="alert alert-primary " id="alert-primary">
                
                <button class="close float-right" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"></span></button>
            </div>
          </div>
          <div class="card o-hidden mb-4">
              <div class="card-header">
                  <h3 class="w-50 float-left card-title m-0">Due Penalties
                  </h3>
              </div>

              <div class="card-body">
                  <table id="penalties_list_table" class="table table-bordered  text-center">
                    <thead>
                        <tr>
                            <th scope="col">Register ID</th>
                            <th scope="col">Member Name</th>
                            <th scope="col">Invoice No</th>
                            <th scope="col">Penalty Amount</th>
                            <th scope="col">Due Date</th>
                            <th scope="col">Overdue By(Days)</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody id="data"></tbody>
                 </table>                          
              </div>
          </div>
      </div>
  </div>

   <!------------------- Add new Patment Modal Start---------------------->
             
    <div class="modal fade" id="viewPayment" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle-2" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle-2">Add New Payment</h5>
              <button class="close cancelPayment" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
              </div>
                <div class="modal-body">
                  <form name="store_payment" id="store_payment">
                    
                      <input type="hidden" value="{{csrf_token()}}" name="_token" id="token">
                      <input type="hidden" value="" name="invoice_id" id="invoice_id">
                      <input type="hidden" value="" name="member_id" id="member_id">
                      <input type="hidden" value="" name="penalty_id" id="penalty_id">
                        <div class="form-group row">
                          <div class="form-group col-md-3">
                            <label>Payment Mode<span style="color: red">*</span></label>
                          </div>
                          <div class="form-group col-md-8">
                             <select class="form-control" name="payment_mode" id="payment_mode">
                              <option value="" hidden>Please Select</option>
                                @foreach($dropdowns['payment_modes'] as $row)
                                  <option value="{{ $row['dropdown_value'] }}">{{  $row['dropdown_value']  }}</option>
                                @endforeach
                                </select>
                                <span class="error text-danger" id="payment_mode_error"></span>
                          </div>
                        </div>

                        <div class="form-group row">
                          <div class="form-group col-md-3">
                            <label>Payment Date<span style="color: red">*</span></label>
                          </div>
                          <div class="form-group col-md-8">
                             <input type="text" class="form-control" id="payment_date" name="payment_date" autocomplete="off" readonly="" style="background-color: white" placeholder="Select Payment Date">
                             <span class="error text-danger" id="payment_date_error"></span>
                          </div>
                        </div>

                        <div class="form-group row">
                          <div class="form-group col-md-3">
                            <label>Amount<span style="color: red">*</span></label>
                          </div>
                          <div class="form-group col-md-8">
                             <input type="text" class="form-control" id="amount" name="amount" autocomplete="off" readonly="">
                             <span class="error text-danger" id="amount_error"></span>
                          </div>
                        </div>
                      </div>
                    <div class="modal-footer">
                        <button type="button" name="submit" id="submitButton" class="btn btn-primary btn-rounded">Submit</button> 
                        <button type="button" class="btn btn-primary btn-rounded cancelPayment" data-dismiss="modal">Close</button> 
                    </div>
                  </form>
                </div>
            </div>
        </div>
    </div> 
  <!------------------- Add new Patment Modal End-------------------------->

@endsection

@section('bottom-js')
<script>
  $(document).ready(function(){
      var penaltiesData = $('#penalties_list_table').DataTable({
      "Processing":true,
      "serverSide":true,
      
      "ajax":{
            url : "{{url('payments/penalties/data-list')}}",
            method :"POST",
            headers: {
            'X-CSRF-TOKEN': $('#token').val()
          },
            data : function ( d ) {
                return $.extend( {}, d, {
                 
                });
              },
      },
      "scrollY"       : "500px",
      "scrollCollapse": true,
      // "scrollX"   : true,
      "columns": [
      {"data":"register_id",
        fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
            $(nTd).html("<a href='{{ url('member/view') }}/"+oData.user_id+"' class='anchortag'>"+oData.register_id+"</a>");
          }
        },
      {"data":"name"},
      {"data":"invoice_no"},
      {"data":"amount_to_pay"},
      {"data":"due_date"},
      {"data":"overdue"},
      {"data":"action"},
        
      ],
      "aaSorting": [],
      "aLengthMenu": [[10,25, 50, 75,100, -1], 
              [10,25, 50, 75,100, "All"]],
      "columnDefs": [
      { "orderable": false, "targets": 6}
      ],
      dom: 'lBfrtip',
      buttons: [
            {
              extend: 'excelHtml5',
              exportOptions: {
                  columns: [0, 1, 2, 3,4,5]
              }
            },
          ]
    });

  });

  function showPaymentForm(id){

      var duePenalty = $('#user_'+ id).data("duepenalty");
      var memberId = $('#user_'+ id).data("memberid");
      var penaltyId = $('#user_'+ id).data("penaltyid");
      $("#amount").val(duePenalty);
      $("#invoice_id").val(id);
      $("#member_id").val(memberId);
      $("#penalty_id").val(penaltyId);

      $('#viewPayment').modal({
          show: true,
          keyboard: false,
          backdrop: 'static'
      });
  }  


  $( "#payment_date" ).datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd-mm-yy',
        maxDate: 0,
    });
  $('#payment_date').change(function(){
      $('#payment_date-error').hide();       
    })
$(document).on("click","#submitButton",function() {
  
    $('#store_payment').validate({
        ignore: "",
        rules:{
          payment_mode:{
            required: true,
          },
          payment_date:{
            required: true,
          },
          amount:{
            required: true,
          },
        }
    });
    if($('#store_payment').valid()){
      var data = $('#store_payment').serialize();
      $('#submitButton').attr('disabled',true);
        $.ajax({
            url      : "{{url('payment/storePenalty')}}",
            method   : 'POST',
            data   : data,
            }).done(function( data ) {
            $('#submitButton').removeAttr('disabled',true);
            console.log(data)
            if(data.status == "success")
            {
              $('#viewPayment').modal('hide')
              $('#penalties_list_table').DataTable().draw();
              $("#alert-primary").html("Payment Submitted Successfully");
                $('#successmsg').show();
                $('html, body').animate({
                    scrollTop: $('.main-header').offset().top
                }, 1000);
                setTimeout(function(){ $('#successmsg').fadeOut() }, 5000);
            }else if(data.status == "validation_error"){
                $.each(data.data, function (key, val) {
                    $("#"+key+"_error").text(val[0]);
                    $("#"+key+"_error").show();
                    $(document).on("keyup", "input[name='"+key+"']", function(e) {
                          $("#"+key+"_error").hide();
                     });
                });              
            }
            else{
                $("#alert-primary").text('Something Went Wrong !');
                $('#submitButton').removeAttr('disabled',true);
                $("#successmsg").show();
                $('html, body').animate({
                  scrollTop: $('.main-header').offset().top
              }, 1000);
                setTimeout(function(){ $('#successmsg').fadeOut() }, 3000);
            }
        });
      }
      else{
        return false;
      }
    });

  $('.cancelPayment').on('click', function(){
       document.store_payment.reset();
       var $form = $('#store_payment');
       $form.validate().resetForm();
       $form.find('.error').removeClass('error');
       $('.text-danger').hide();
    });
</script>
@endsection

