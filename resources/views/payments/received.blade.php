@extends('layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-12">
          <div class="col-md-12" id="successmsg" style="display: none">
            <div class="alert alert-primary " id="alert-primary">
                
                <button class="close float-right" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"></span></button>
            </div>
          </div>
          <div class="card o-hidden mb-4">
              <div class="card-header">
                  <h3 class="w-50 float-left card-title m-0">Received Payments</h3>
                  @can('Add New Payment')
                    <a href="#" onclick="showPaymentForm()" class="btn btn-primary btn-rounded m-1" style="float:right">Add New Payment</a>
                  @endcan
              </div>

              <div class="card-body">
                <div class="row pb-3">
                  <div class="col-md-12 mb-3 font-size">
                      <a href="#" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                        <b style="color: black">Filters By <i class="fas fa-chevron-circle-down"></i></b> click here
                      </a>
                  </div>
                </div>

                <div class="collapse" id="collapseExample">
                <div class="row pb-3">
                  </div>
                  <div class="row pb-3">  
                        <div class="col-md-2">Age From</div>
                        <div class="col-md-3">
                          <select class="form-control" name="age_from" id="age_from">
                            <option value="" hidden="">Please Select</option>
                            @foreach ($data as $value)
                              <option value="{{$value->age_from}}">{{$value->age_from}}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="col-md-2"></div>

                        <div class="col-md-2">Age To</div>
                        <div class="col-md-3">
                          <select class="form-control" name="age_to" id="age_to">
                            <option value="" hidden="">Please Select</option>
                            @foreach ($data as $value)
                              <option value="{{$value->age_to}}">{{$value->age_to}}</option>
                            @endforeach
                          </select>
                          <span id="AgeError" hidden="" class="text-danger">Age To should be greater than Age from</span>
                        </div>
                    </div>

                    <div class="row pb-3">  
                        <div class="col-md-2">Payment Date From</div>
                        <div class="col-md-3">
                          <input type="text" class="form-control" id="from" name="from" placeholder="Select From Date" autocomplete="off">
                        </div>
                        <div class="col-md-2"></div>

                        <div class="col-md-2">Payment Date To</div>
                        <div class="col-md-3">
                          <input type="text" class="form-control" id="to" name="to" placeholder="Select To Date" autocomplete="off">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">Register ID</div>
                          <div class="col-md-3">
                            <input type="text" class="form-control" id="register_id" name="register_id" placeholder="Register ID" autocomplete="off">
                          </div>
                          <div class="col-md-2"></div>

                        <div class="col-md-2">Expired Members</div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <select class="form-control" name="expired_id" id="expired_id">
                                  <option value="" hidden="">Please Select</option>
                                  @foreach ($expiredMembers as $value)
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                  @endforeach
                                </select>
                            </div>
                        </div>
                      <div class="col-md-2"></div>
                    </div>

                    <div class="row">
                      <div class="col-md-2">Members</div>
                        <div class="col-md-3">
                            <div class="form-group">
                              <select class="form-control" name="member_id" id="member_id">
                                <option value="" hidden="">Please Select</option>
                                @foreach ($activeMembers as $value)
                                  <option value="{{$value->id}}">{{$value->name}}</option>
                                @endforeach
                              </select>
                            </div>
                        </div>

                        <div class="col-md-2"></div>
                        <div class="col-md-2">Payment Type</div>
                        <div class="col-md-3">
                            <div class="form-group">
                              <select class="form-control" name="payment_type" id="payment_type">
                                <option value="" hidden="">Please Select</option>
                                  <option value="fc_amount">FC Amount</option>
                                  <option value="due_penalty">Due Penalty</option>
                              </select>
                            </div>
                        </div>
                    </div>
                    <div class="row pb-3">
                        <div class="col-6 col-md-1">
                            <button type="button" class="txt-color btn btn-primary btn-rounded" id="filter" name="filter">Filter</button>
                        </div>
                        <div class="col-6 col-md-9">
                            <button type="button" class="txt-color btn btn-primary btn-rounded" id="reset" name="reset">Reset</button>
                        </div>
                    </div>
                </div>
                  <table id="members_list_table" class="table table-bordered text-center">
                    <thead>
                        <tr>
                            <th scope="col">Register ID</th>
                            <th scope="col">Transaction ID</th>
                            <th scope="col">Member Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Expired Member</th>
                            <th scope="col">Amount</th>
                            <th scope="col">Payment Date</th>
                            <th scope="col">Payment Type</th>
                            <th scope="col">Payment Mode</th>
                        </tr>
                    </thead>
                    <tbody id="data"></tbody>
                 </table>                          
              </div>
          </div>
      </div>
  </div>

  <!------------------- Add new Patment Modal Start---------------------->
             
    <div class="modal fade" id="viewPayment" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle-2" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle-2">Add New Payment</h5>
              <button class="close cancelPayment" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
              </div>
                <div class="modal-body">
                  <form name="store_payment" id="store_payment">
                    @if (count($activeMembers)>0)
                      <input type="hidden" name="account_id" id="account_id" value="{{$activeMembers[0]->account_id}}">
                    @endif
                      <input type="hidden" value="{{csrf_token()}}" name="_token" id="token">
                      <div class="form-group row" hidden>
                        <div class="form-group col-md-3">
                          <label>Payment Type<span style="color: red">*</span></label>
                        </div>
                        <div class="form-group col-md-8">
                            <select class="form-control payment_type" name="type_of_payment" id="" readonly>
                              <option value="" hidden>Please Select</option>
                              <option value="fc_amount" selected="">FC Payment</option>  
                          </select>
                          <span class="error text-danger" id="payment_type_error">
                        </span>
                        </div>
                      </div>
                      <div id="extra_fields_div">
                        <div class="form-group row" id="expired_fields_div">
                          <div class="form-group col-md-3">
                            <label>Expired Member<span style="color: red">*</span></label>
                          </div>
                          <div class="form-group col-md-8">
                            <select class="form-control" name="expired_member" id="expired_member">
                              <option value="" hidden="">Please Select</option>
                                @foreach ($expiredMembers as $value)
                                  @if ($value->total_members>0)
                                      <option value="{{$value->id}}">{{$value->name}}</option>
                                  @endif
                                @endforeach  
                            </select>
                            <span class="error text-danger" id="expired_member_error">
                        </span>
                          </div>
                        </div>

                        <div class="form-group row" id="fc_member_field" hidden="">
                          <div class="form-group col-md-3">
                            <label>Member<span style="color: red">*</span></label>
                          </div>
                          <div class="form-group col-md-8">
                            <select class="form-control member_name" name="member_name" id="member_name">
                              <option value="" >Please Select</option>
                            </select>
                            <span class="error text-danger" id="member_name_error">
                          </span>
                          </div>
                        </div>

                        <div class="form-group row">
                          <div class="form-group col-md-3">
                            <label>Payment Mode<span style="color: red">*</span></label>
                          </div>
                          <div class="form-group col-md-8">
                             <select class="form-control" name="payment_mode" id="payment_mode" placeholder="Select Payment Mode"> 
                              <option value="" hidden>Please Select</option>
                                @foreach($dropdowns['payment_modes'] as $row)
                                  <option value="{{ $row['dropdown_value'] }}">{{  $row['dropdown_value']  }}</option>
                                @endforeach
                                </select>
                                <span class="error text-danger" id="payment_mode_error"></span>
                          </div>
                        </div>

                        <div class="form-group row">
                          <div class="form-group col-md-3">
                            <label>Payment Date<span style="color: red">*</span></label>
                          </div>
                          <div class="form-group col-md-8">
                             <input type="text" class="form-control" id="payment_date" name="payment_date" autocomplete="off" readonly="" style="background-color: white" placeholder="Select Payment Date">
                             <span class="error text-danger" id="payment_date_error"></span>
                          </div>
                        </div>

                        <div class="form-group row">
                          <div class="form-group col-md-3">
                            <label>Amount<span style="color: red">*</span></label>
                          </div>
                          <div class="form-group col-md-8">
                             <input type="text" class="form-control" id="amount" name="amount" autocomplete="off" readonly="" placeholder="Amount">
                             <span class="error text-danger" id="amount_error"></span>
                          </div>
                        </div>

                        <div class="form-group row">
                          <div class="form-group col-md-3">
                            <label>Remarks</label>
                          </div>
                          <div class="form-group col-md-8">
                              <textarea class="form-control" name="payment_note" id="payment_note" rows="5" placeholder="Remarks"></textarea>
                              <span class="error text-danger" id="payment_note_error"></span>
                          </div>
                        </div>
                      </div>
                    <div class="modal-footer">
                        <button type="button" name="submit" id="submitButton" class="btn btn-primary btn-rounded">Submit</button> 
                        <button type="button" class="btn btn-primary btn-rounded cancelPayment" data-dismiss="modal">Close</button> 
                    </div>
                  </form>
                </div>
            </div>
        </div>
    </div> 
  <!------------------- Add new Patment Modal End-------------------------->

@endsection

@section('bottom-js')
<script>
  $(document).ready(function(){
    $("#from").datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      changeYear: true,
      autoclosed:true,
      maxDate:0,
      onSelect: function (date) {
          var date2 = $('#from').datepicker('getDate');
          $('#to').datepicker('option', 'minDate', date2);
      }
    });
    $('#to').datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      changeYear: true,
      maxDate:0,
      onClose: function () {
          var dt1 = $('#from').datepicker('getDate');
          var dt2 = $('#to').datepicker('getDate');
          if (dt2 <= dt1) {
              var minDate = $('#to').datepicker('option', 'minDate');
              $('#to').datepicker('setDate', minDate);
          }
      }
    });

    function checkage(age){
      var age1=$("#age_from option:selected").text();
      var age2=$("#age_to option:selected").text();
      if(age1==age2 || age2<age1){
          $('#AgeError').removeAttr('hidden',true)
          $('#age_to option').prop('selected', function() {
          return this.defaultSelected;
      });
      }
       else{
          $('#AgeError').attr('hidden',true)
      }
   }
    $("#age_to").on("change", checkage);

    var membersData = $('#members_list_table').DataTable({
      "Processing":true,
      "serverSide":true,
      "ajax":{
            url : "{{url('payments/received/data-list')}}",
            method :"POST",
            headers: {
	          'X-CSRF-TOKEN': $('#token').val()
	      },
            data : function ( d ) {
                return $.extend( {}, d, {
                      "from" : $('#from').val(),
                      "to"   : $('#to').val(),
                      "expired_id"   : $('#expired_id').val(),
                      "age_from"   : $('#age_from').val(),
                      "age_to"   : $('#age_to').val(),
                     "register_id" : $('#register_id').val(),
                     "member_id" : $('#member_id').val(),
                     "payment_type" : $('#payment_type').val(),
                  });
              },
    },
    "scrollY"       : "500px",
    "scrollCollapse": true,
    "scrollX"       : true,
    "columns": [
    {"data":"member_id",
        fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
            $(nTd).html("<a href='{{ url('member/view') }}/"+oData.id+"' class='anchortag'>"+oData.member_id+"</a>");
          }
    },
    {"data":"transaction_id"},
    {"data":"name"},
    {"data":"email"},
    {"data":"mobile_number"},
    {"data":"expired_name"},
    {"data":"amount"},
    {"data":"payment_date"},
    {"data":"payment_type"},
    {"data":"mode_of_payment"},
      
    ],
    "aaSorting": [],
    "aLengthMenu": [[10,25, 50, 75,100, -1], 
            [10,25, 50, 75,100, "All"]],
    "columnDefs": [
    
    ],
    "dom": 'lBfrtip',
    buttons: [
      {
        extend: 'excelHtml5',
        exportOptions: {
            columns: [0, 1, 2, 3,4,5,6,7,8,9]
        }
      },
    ]
  });
  $('#filter').click(function(){
    membersData.draw();
  });

  $('#reset').click(function(){
    $('#age_from,#age_to').prop('selectedIndex',0);
    $("#from, #to,#register_id,#member_id,#expired_id,#payment_type").val('');
    membersData.draw();
  });

  $( "#payment_date" ).datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd-mm-yy',
        maxDate: 0,
    });

$(document).on("click","#submitButton",function() {
  
    $('#store_payment').validate({
        ignore: "",
        rules:{
          payment_note:{
            maxlength:1000,
          },
          
          payment_mode:{
            required: true,
          },
          payment_date:{
            required: true,
          },
          amount:{
            required: true,
          },
          type_of_payment:{
            required: true,
          },
          expired_member:{
            required: true,
          },
          member_name:{
            required: true,
          },
        }
    });
    if($('#store_payment').valid()){
      var data = $('#store_payment').serialize();
      $('#submitButton').attr('disabled',true);
        $.ajax({
            url      : "{{url('payment/storePayment')}}",
            method   : 'POST',
            data   : data,
            }).done(function( data ) {
            $('#submitButton').removeAttr('disabled',true);
            if(data.status == "success")
            {
              $('#viewPayment').modal('hide')
              $('#members_list_table').DataTable().draw();
              resetModal();
              $("#alert-primary").html("Payment Submitted Successfully");
                $('#successmsg').show();
                $('html, body').animate({
                    scrollTop: $('.main-header').offset().top
                }, 1000);
                setTimeout(function(){ $('#successmsg').fadeOut() }, 5000);
            }else if(data.status == "validation_error"){
                $.each(data.data, function (key, val) {
                    $("#"+key+"_error").text(val[0]);
                    $("#"+key+"_error").show();
                    $(document).on("keyup", "input[name='"+key+"']", function(e) {
                          $("#"+key+"_error").hide();
                     });
                });              
            }
            else{
                $("#alert-primary").text('Something Went Wrong !');
                $('#submitButton').removeAttr('disabled',true);
                $("#successmsg").show();
                $('html, body').animate({
                  scrollTop: $('.main-header').offset().top
              }, 1000);
                setTimeout(function(){ $('#successmsg').fadeOut() }, 3000);
            }
        });
      }
      else{
        return false;
      }
    });
    
    $('.member_name').change(function(){
       var id = $(this).attr('id');
      $('#amount').val('');
      $('#member_name_error').hide();
      $.ajax({
            method: "POST",
            url: "{{url('member/getDueAmount')}}",
            data:{
                  "_token": "{{ csrf_token() }}",
                  "expired_id"  : $('#expired_member').val(),
                  "id"    : $('#'+id).val()
            },
          }).done(function(data) {
          if(data){
            $('#amount').val(data)
            $('#amount_error').hide();
            $('#amount-error').hide();
          };
        })         
    })
    $('#expired_member').change(function(){
      $('#fc_member_field').removeAttr('hidden',true);
      $('#member_name').prop('selectedIndex',0);
      $('#amount').val('');
      $('#expired_member_error').hide(); 
      
      $.ajax({
            method: "POST",
            url: "{{url('member/getMembers')}}",
            data:{
                  "_token": "{{ csrf_token() }}",
                  "expired_id"  : $('#expired_member').val(),
            },
          }).done(function(data) {
          if(data){
            $('#member_name').empty();
            $('#member_name').append($("<option value='' hidden=''>Please Select</option>"))
              $.each(data, function(key, value) { 
                $('#member_name').append($("<option></option>")
                    .attr("value", value.user_id)
                    .text(value.name));
                $('#fc_member_field').removeAttr('hidden',true);
              });
          };
      })

    })
    $('#payment_mode').change(function(){
      $('#payment_mode_error').hide();       
    })
    $('#payment_date').change(function(){
      $('#payment_date-error').hide();       
    })
    
    function resetModal(){
      document.store_payment.reset();
      $('#member_name').empty();
      $('#fc_member_field').attr('hidden',true);
      $('.error').text('')
    }

    $('.cancelPayment').on('click', function(){
       resetModal();
       var $form = $('#store_payment');
       $form.validate().resetForm();
       $form.find('.error').removeClass('error');
    });
});
function showPaymentForm(){
  $('#viewPayment').modal({
      show: true,
      keyboard: false,
      backdrop: 'static'
  });
  $('.payment_type').attr('required',true);
}    
</script>
@endsection

