@extends('layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-12">
          <div class="col-md-12" id="successmsg" style="display: none">
            <div class="alert alert-primary " id="alert-primary">
                
                <button class="close float-right" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"></span></button>
            </div>
          </div>
          <div class="card o-hidden mb-4">
              <div class="card-header">
                  <h3 class="w-50 float-left card-title m-0">Pending Bills
                  </h3>
              </div>

              <div class="card-body">
                <div class="row pb-3">
                  <div class="col-md-12 mb-3 font-size">
                    <a href="#" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                      <b style="color: black">Filters By <i class="fas fa-chevron-circle-down"></i></b> click here
                    </a>
                      
                  </div>
                  </div>
                  <div class="collapse" id="collapseExample">
                  <div class="row pb-3">  
                        <div class="col-md-2">Age From</div>
                        <div class="col-md-3">
                          <select class="form-control" name="age_from" id="age_from">
                            <option value="" hidden="">Please Select</option>
                            @foreach ($data as $value)
                              <option value="{{$value->age_from}}">{{$value->age_from}}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="col-md-2"></div>

                        <div class="col-md-2">Age To</div>
                        <div class="col-md-3">
                          <select class="form-control" name="age_to" id="age_to">
                            <option value="" hidden="">Please Select</option>
                            @foreach ($data as $value)
                              <option value="{{$value->age_to}}">{{$value->age_to}}</option>
                            @endforeach
                          </select>
                          <span id="AgeError" hidden="" class="text-danger">Age To should be greater than Age from</span>
                        </div>
                    </div>

                    <div class="row pb-3">  
                        <div class="col-md-2">Due Date From</div>
                        <div class="col-md-3">
                          <input type="text" class="form-control" id="from" name="from" placeholder="Select From Date" autocomplete="off">
                        </div>
                        <div class="col-md-2"></div>

                        <div class="col-md-2">Due Date To</div>
                        <div class="col-md-3">
                          <input type="text" class="form-control" id="to" name="to" placeholder="Select To Date" autocomplete="off">
                        </div>
                    </div>
                    <div class="row pb-1">
                        <div class="col-md-2">Register ID</div>
                          <div class="col-md-3">
                            <input type="text" class="form-control" id="register_id" name="register_id" placeholder="Register ID" autocomplete="off">
                          </div>
                          <div class="col-md-2"></div>

                        <div class="col-md-2">Members</div>
                        <div class="col-md-3">
                            <div class="form-group">
                              <select class="form-control" name="member_id" id="member_id">
                                <option value="" hidden="">Please Select</option>
                                @foreach ($activeMembers as $value)
                                  <option value="{{$value->id}}">{{$value->name}}</option>
                                @endforeach
                              </select>
                            </div>
                        </div>
                      <div class="col-md-2"></div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">Invoice Number</div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control" id="invoice_no" name="invoice_no" placeholder="Invoice Number" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="row pb-3">
                        <div class="col-6 col-md-1">
                            <button type="button" class="txt-color btn btn-primary btn-rounded" id="filter" name="filter">Filter</button>
                        </div>
                        <div class="col-6 col-md-9">
                            <button type="button" class="txt-color btn btn-primary btn-rounded" id="reset" name="reset">Reset</button>
                        </div>
                    </div>
                    </div>
                  <table id="members_list_table" class="table table-bordered  text-center">
                    <thead>
                        <tr>
                            <th scope="col">Invoice No</th>
                            <th scope="col">Register ID</th>
                            <th scope="col">Member Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Total FC's</th>
                            <th scope="col">Bill Amount</th>
                            <th scope="col">Bill Date</th>
                            <th scope="col">Due Date</th>
                            <th scope="col">Over due By(Days)</th>
                            <th scope="col">Penality</th>
                            <th scope="col">Total Due</th>
                            <th scope="col">Pending Due</th>
                            <th width="width: 50px;" scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody id="data"></tbody>
                 </table>                          
              </div>
          </div>
      </div>
  </div>

@endsection

@section('bottom-js')
<script>
  $(document).ready(function(){

    $("#from").datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      changeYear: true,
      autoclosed:true,
      maxDate:0,
      onSelect: function (date) {
          var date2 = $('#from').datepicker('getDate');
          $('#to').datepicker('option', 'minDate', date2);
      }
    });
    $('#to').datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      changeYear: true,
      maxDate:0,
      onClose: function () {
          var dt1 = $('#from').datepicker('getDate');
          var dt2 = $('#to').datepicker('getDate');
          if (dt2 <= dt1) {
              var minDate = $('#to').datepicker('option', 'minDate');
              $('#to').datepicker('setDate', minDate);
          }
      }
    });

    function checkage(age){
      var age1=$("#age_from option:selected").text();
      var age2=$("#age_to option:selected").text();
      
      if(age1==age2 || age2<age1){
          $('#AgeError').removeAttr('hidden',true)
         $('#age_to option').prop('selected', function() {
          return this.defaultSelected;
      });
      }
       else{
          $('#AgeError').attr('hidden',true)
      }
   }
    $("#age_to").on("change", checkage);

    var membersData = $('#members_list_table').DataTable({
      dom: 'Bfrtip',
      buttons: [
        'copy', 'csv', 'excel', 'pdf', 'print'
      ],
      "Processing":true,
      "serverSide":true,
      "scrollX"   : true,
      "ajax":{
            url : "{{url('payments/pending/data-list')}}",
            method :"POST",
            headers: {
	          'X-CSRF-TOKEN': $('#token').val()
	      },
            data : function ( d ) {
                return $.extend( {}, d, {
                      "from" : $('#from').val(),
                      "to"   : $('#to').val(),
                      "expired_id"   : $('#expired_id').val(),
                      "age_from"   : $('#age_from').val(),
                      "age_to"   : $('#age_to').val(),
                     "register_id" : $('#register_id').val(),
                     "member_id" : $('#member_id').val(),
                     "invoice_no" : $('#invoice_no').val(),
                  });
              },
    },
    "scrollY"       : "500px",
    "scrollCollapse": true,
   
    "columns": [
    {"data":"invoice_no"},
    {"data":"member_id",
        fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
            $(nTd).html("<a href='{{ url('member/view') }}/"+oData.user_id+"' class='anchortag'>"+oData.member_id+"</a>");
          }
    },
    {"data":"name"},
    {"data":"email"},
    {"data":"mobile_number"},
    {"data":"total_fcs"},
    {"data":"bill_amount"},
    {"data":"bill_date"},
    {"data":"bill_due_date"},
    {"data":"overdue"},
    {"data":"penalty_amount"},
    {"data":"total_due_amount"},
    {"data":"pending_amount"},
    {"data":"action"},
      
    ],
    "aaSorting": [],
    "aLengthMenu": [[10,25, 50, 75,100, -1], 
            [10,25, 50, 75,100, "All"]],
    "columnDefs": [
    { "orderable": false, "targets": 13}
    ],
    dom: 'lBfrtip',
     buttons: [
      {
        extend: 'excelHtml5',
        exportOptions: {
            columns: [0, 1, 2, 3,4,5,6,7,8,9,10,11,12]
        }
      },
    ]
});
  $('#filter').click(function(){
    membersData.draw();
  });

  $('#reset').click(function(){
    $('#age_from,#age_to').prop('selectedIndex',0);
    $("#from, #to,#register_id,#member_id,#expired_id,#invoice_no").val('');
    membersData.draw();
});
});  
</script>
@endsection

