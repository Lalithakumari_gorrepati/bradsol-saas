@extends('layouts.master')
@section('page-css')

<style type="text/css">
#member_id{
    text-transform: uppercase;
}
#member_id::placeholder {
    text-transform: none;
}
 </style>
@endsection
@section('main-content')
<div class="row">
      <div class="col-md-12" id="successmsg" style="display: none">
          <div class="alert alert-primary " id="alert-primary">
              <button class="close float-right" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"></span></button>
          </div>
      </div>

    <div class="col-md-12">
    	<div class="card mt-2 mb-4">
		    <div class="card-header bg-transparent">
	        <h4 class="p-2">Add New Member<span class="float-right">
	        	<a class="btn btn-primary btn-rounded" href="{{url('member/list')}}">Cancel</a></span>
	    	  </h4>
    	 </div>

    	<div class="card-body">
             <nav>
            	<div class="row nav nav-tabs" id="nav-tab" role="tablist">
	                <a href="#nav-personal" class="nav-item nav-link active show" id="nav-personal-tab" data-toggle="tab"  role="tab" aria-controls="nav-personal" aria-selected="true">Personal Details</a>

	                <a href="#nav-medical" class="nav-item nav-link"  id="nav-medical-tab" data-toggle="tab"  role="tab" aria-controls="nav-medical" aria-selected="false">Medical details</a>

	                <a href="#nav-nominee" class="nav-item nav-link"  id="nav-nominee-tab" data-toggle="tab" role="tab" aria-controls="nav-nominee" aria-selected="false">Nominee Details</a>

	                <a  href="#nav-payment" class="nav-item nav-link" id="nav-payment-tab" data-toggle="tab"  role="tab" aria-controls="nav-payment" aria-selected="false">Payment Details</a>

	                <a href="#nav-member_access" class="nav-item nav-link"  id="nav-member_access-tab" data-toggle="tab"  role="tab" aria-controls="nav-member_access" aria-selected="false">Member Access</a>
	            </div> 
         	</nav>

         	<form id="memberDetails"  enctype="multipart/form-data">
              	<input type="hidden" value="{{csrf_token()}}" name="_token" id="token">
                <input type="hidden" id="age_from" value="{{$setting['age_from']}}">
                <input type="hidden" id="age_to" value="{{$setting['age_to']}}">
              	<div class="tab-content ul-tab__content" id="nav-tabContent">
            	<!--------------Member Personal details tab start------------->  	
                	<div class="tab-pane fade active show" id="nav-personal" role="tabpanel" aria-labelledby="nav-personal-tab">

                		<div class="form-group row">
                    		<div class="col-md-6 form-group mb-3">
                            	<label for="name">Member Id<span style="color: red">*</span></label>
                        		<input type="text" class="form-control" id="member_id" name="member_id" placeholder="Enter Member Id" maxlength="10">
                                <span class="error text-danger err-name" id="member_id_error" style="display: none">
                                  Member Id already exists
                                </span>
                                
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="name">Member Name<span style="color: red">*</span></label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Enter Member Name" maxlength="101" autocomplete="nope">
                                <span class="error text-danger" id="name_error"></span>
                            </div>
                        </div>
                                
                        <div class="form-group row">
                        	  <div class="col-md-6 form-group mb-3">
                                <label for="mobile_number" class="">Member Mobile Number<span style="color: red">*</span></label>
                                	<input type="text" class="form-control was-validated" id="mobile_number" name="mobile_number" placeholder="Enter Member Mobile Number" maxlength="10">
                                <span class="error text-danger err-phone" id="mobile_number_error" style="display: none">Mobile Number already exists</span>	
                            </div>
                                    
							              <div class="col-md-6 form-group mb-3">
								              <label for="alternative_number" class="">Alternative Mobile Number</label>
                               	<input type="text" class="form-control was-validated" id="alternative_number" name="alternative_number" placeholder="Enter Alternative Mobile Number" maxlength="10"> 

                               <span class="error text-danger err-phone" id="alternative_number_error"></span>
							              </div>
                    	  </div>
                            
                        	<div class="form-group row">
                      <div class="col-md-6 form-group mb-3">
                        <label for="inputEmail" class="">Profile Email<span style="color: red">*</span></label>
                          <input type="email" class="form-control" id="email" name="email" placeholder="Enter Member Email"  value="{{old('email')}}" autocomplete="nope">
                          <span class="error text-danger" id="email_error" style="display: none">Email already exists</span>
                      </div>

                  	<div class="col-md-6 form-group mb-3">
                        <label for="alternative_email" class="">Alternative Email</label>
                            <input type="email" class="form-control" id="alternative_email" name="alternative_email" placeholder="Enter Alternative Email" autocomplete="nope">
                           <span class="error text-danger" id="alternative_email_error"></span>
                      </div>
                  </div>

                  <div class="form-group row">
                      <div class="col-md-6 form-group mb-3">
                          <label for="martialStatus" class="">Marital Status<span style="color: red">*</span></label>
                            <select class="form-control" name="marital_status" id="marital_status">
                              <option value="" hidden="">Please Select</option>
                              <option value="Married">Married</option>
                              <option value="Single">Single</option>
                            </select>
                            <span class="error text-danger" id="marital_status_error"></span>
                        </div>

                        <div class="col-md-6 form-group mb-3" id="gender">
                            <label for="gender" class="">Gender<span style="color: red">*</span></label>
                              <select class="form-control " name="gender" id="genderfield">
                                  <option value="" hidden>Please Select</option>
                                  <option value="Male">Male</option>
                                  <option value="Female">Female</option>  
                              </select>
                              <span class="error text-danger" id="gender_error"></span>
                          </div>  
                  	</div>
                      
                      <div class="form-group row">

				                <div class="col-md-6 form-group mb-3" >
                          <label for="country" class="">Country<span style="color: red">*</span></label>
                              <select class="selectbox2 form-control" name="country" id="country_id">
                              <option value="" hidden="">Please Select</option>
                                @if(!empty($data['dropdowns']['country_data']))
                                  @foreach($data['dropdowns']['country_data'] as $row)
                                  <option value="{{ $row->name }}" data-id="{{ $row->id }}">{{ $row->name }}</option>
                                  @endforeach
                                @endif
                              </select>
                          </div>
                         <div class="col-md-6 form-group mb-3" >
                          	<label for="state" class="">State
                          	<span class="location_state_city" style="color: red">*</span>
                          	</label>
                              <select class="selectbox2 form-control" name="state" id="state_id">
                                  
                              </select>
                              <span class="error text-danger" id="state_error"></span>
                          </div>
			                </div>

			              <div class="form-group row">
		                    <div class="col-md-6 form-group mb-3" >
                          <label for="city" class="">City<span class="location_state_city" style="color: red">*</span></label>
                          <select class="selectbox2 form-control" name="city" id="city_id">
                            <option value="">Please Select</option>
                          </select>
                          <span class="error text-danger" id="city_error"></span>
                        </div>

                        <div class="col-md-6 form-group mb-3" >
                          <label for="dob" class="">DOB<span style="color: red">*</span></label>
                          <input type="text" class="form-control dob" id="member_dob"  name="dob" placeholder="Select Member DOB" autocomplete="nope" readonly="" style="background-color: white"> 
                          <span class="error text-danger" id="dob_error"></span>   
                        </div>
				            </div>
                       
                       <div class="form-group row">
                        	<div class="col-md-6 form-group mb-3">
                          	<label for="customer_id_type" class="">ID Proof<span style="color: red">*</span></label>
                              <select class="form-control" name="customer_id_type" id="customer_id_type">
                              <option value="">Please Select</option>
                                @if(!empty($data['dropdowns']['customer_id_proofs']))
                                  @foreach($data['dropdowns']['customer_id_proofs'] as $row)
                                  <option value="{{ $row['dropdown_value'] }}">{{  $row['dropdown_value']  }}</option>
                                  @endforeach
                                @endif
                            </select>
                            <span class="error text-danger" id="customer_id_type_error"></span>
                          </div>

                            <div id="customer_id_proofs" class="row col-md-6">
                              <div class="col-md-6 form-group mb-3" >
                                <label for="customer_id_front" class="">Id Proof File Front</label>
                                 <input class="customer_id_images" type="file" id="customer_id_front" name="customer_id_front" accept="image/x-png,image/png,image/gif,image/jpeg,application/pdf">
                                 <label class="text-danger" id="customer_id_front_error" style="display: none;"></label>
                              </div>
                              <div class="col-md-6 form-group mb-3" >
                                <label for="customer_id_back" class="">Id Proof File Back</label>
                                  <input class="customer_id_images"  type="file" id="customer_id_back" name="customer_id_back" accept="image/x-png,image/png,image/gif,image/jpeg,application/pdf">
                                  <label class="text-danger" id="customer_id_back_error" style="display: none;" ></label>
                              </div>
                          </div>
                       </div>

                      <div class="form-group row">
                        	<div class="col-md-6 form-group mb-3">
                          	<label for="member_address" class="mr-3">Member Address</label>
                              <textarea id="member_address" name="member_address" rows="5" class="form-control"></textarea>
                          </div>

                          <div class="col-md-6 form-group mb-3">
                            <label for="customer_id_type" class="mr-3">Upload Age Proof</label><br>
                              <input class="customer_id_images"  type="file" id="age_proof" name="age_proof" accept="image/x-png,image/png,image/gif,image/jpeg,application/pdf"><br>
                              <span class="error text-danger" id="age_proof_error"></span>
                          </div>

                          <div class="col-md-6 form-group">
                            <div class="row">
                              <div class="col-md-6 form-group">
                              <label>Upload Scanned Application<span style="font-size:12px;color:red;">*</span></label>
                              <input type="file" class="form-control-file" name="application_form[]" id="application_form" accept="image/x-png,image/png,image/gif,image/jpeg,application/pdf,doc" style="border:0px;"  onchange="ValidateSizeadd(this,this.id)">
                              <span class="error text-danger" id="application_form_error"></span>
                            </div>
                              <button type="button" class="btn btn-success" style="height: 30px;margin-top: 25px;" onclick="add_divs();">Add</button>
                            </div>
                            <div id="new_file">
                            </div>
                          </div>
                       </div> 
                       <div class="form-group row">
                            <div class="col-sm-12 d-flex justify-content-center">
                            <a class="txt-color btn btn-primary btn-rounded m-1" id="changeTabMedical" style="float:right; color: white">Next</a>
                            </div>
                        </div> 
                    </div>
            <!--------------Member Personal details tab end------------->
            
            <!--------------Member Medical details tab start------------->  	
            	<div class="tab-pane fade" id="nav-medical" role="tabpanel" aria-labelledby="nav-medical-tab">
                    <div class="form-group row">  
                        <div class="col-md-6 form-group mb-3" >
                          	<label for="physical_disability" class="">Physical Disability<span style="color: red">*</span></label>
                          	<select class="form-control" name="physical_disability" id="physical_disability">
                            	<option value="" hidden>Please Select</option>
                                 <option value="Y">Yes</option>
                                 <option value="N" selected="">No</option>
                            </select>
                            <span class="error text-danger" id="physical_disability_error"></span>
                        </div>
                        <div class="col-md-6 form-group mb-3" id="physical_disability_type_div">
                          	<label for="physical_disability_type" class="">Physical Disability Type<span style="color: red">*</span></label>
                            <input type="text" class="form-control" id="physical_disability_type" name="physical_disability_type" placeholder="Enter Physical disability Type" >
                            <span class="error text-danger" id="physical_disability_type_error"></span>
                        </div>
                     
                        <div class="col-md-6 form-group mb-3" >
                          	<label for="previous_illness" class="">History Of Previous Illness<span style="color: red">*</span></label>
                          	<select class="form-control" name="previous_illness" id="previous_illness">
                            	<option value="" hidden>Please Select</option>
                                 <option value="Y">Yes</option>
                                 <option value="N">No</option>
                            </select>
                            <span class="error text-danger" id="previous_illness_error"></span>
                        </div>

                        <div class="col-md-6 form-group mb-3" id="Illness_type_div">
                          	<label for="Illness_type" class="">Illness Type<span style="color: red">*</span></label>
                            <select class="form-control" id="illness_type" name="illness_type">
                            	<option value="">Please Select</option>
                                @foreach($data['dropdowns']['Illness_types'] as $row)
                                  <option value="{{ $row['dropdown_value'] }}">{{  $row['dropdown_value']  }}</option>
                                @endforeach
                            </select>
                            <span class="error text-danger" id="illness_type_error"></span>
                        </div>
                     </div>

                     <div class="form-group row">  
                        <div class="col-md-6 form-group">
                            <div class="row">
                              <div class="col-md-6 form-group">
                              <label>Upload Reports If any<span id="report_label" style="font-size:12px;color:red;"></span></label>
                              <input type="file" class="form-control-file" name="reports[]" id="reports" accept="image/x-png,image/png,image/gif,image/jpeg,pdf,doc" style="border:0px;"  onchange="ValidateSizeadd(this,this.id)">
                              <span class="error text-danger" id="reports_error"></span>
                            </div>
                              <button type="button" class="btn btn-success" style="height: 30px;margin-top: 25px;" onclick="get_divs();">Add</button>
                            </div>
                            <div id = "here">
                            </div>
                        </div>
                        
                        <div class="col-md-6 form-group mb-3" >
                            <label for="previous_illness" class="mb-3">Final Health Declaration<span style="color: red">*</span></label><br>
                            <input type="checkbox" checked name="health_declaration" id="health_declaration">&nbsp;Hereby, I declare my present health condition is good and certified by medical examiner <br>

                             <span class="error text-danger" id="health_declaration_error"></span>
                        </div>
                     </div>

                    <div class="form-group row">
                        <div class="col-sm-12 d-flex justify-content-center">
                        	<a class="txt-color btn btn-primary btn-rounded m-1" id="previousTab1" style="float:right;color: white">Previous</a>
                            <a class="txt-color btn btn-primary btn-rounded m-1" id="changeTabNominee" style="float:right; color: white">Next</a>
                        </div>
                    </div>
                </div>
            <!--------------Member Medical details tab end------------->

            <!--------------Member Nominee details tab start------------->  	
            	<div class="tab-pane fade" id="nav-nominee" role="tabpanel" aria-labelledby="nav-nominee-tab">
                	<div class="row">
                        <div class="col-md-6 form-group mb-3">
                            <label for="nominee_name" class="">Nominee Name<span style="color: red">*</span></label>
                          
                          	<input type="text" class="form-control" id="NomineeDetails_1_nominee_name"  name="NomineeDetails[1][nominee_name]" placeholder="Enter Nominee Name">
                            <span class="error text-danger" id="NomineeDetails_1_nominee_name_error"></span>
                       </div>

                       <div class="col-md-6 form-group mb-3">
                            <label for="nominee_fcp" class="">FC Amount Percentage<span style="color: red">*</span></label>
                          
                            <input type="text" class="form-control fc_percentage" id="NomineeDetails_1_nominee_fcp"  name="NomineeDetails[1][nominee_fcp]" placeholder="Enter FC Amount %" value="100" onchange="getId('1')">
                            <span class="error text-danger" id="NomineeDetails_1_nominee_fcp_error"></span>
                       </div>
                  </div>

                    <div class="row">
                       <div class="col-md-6 form-group mb-3">
                            <label for="nominee_phone" class="">Nominee Phone<span style="color: red">*</span></label>
                          
                              <input type="text" class="form-control" id="NomineeDetails_1_nominee_phone"  name="NomineeDetails[1][nominee_phone]" placeholder="Enter Nominee Phone" maxlength="10">
                              <span class="error text-danger" id="NomineeDetails_1_nominee_phone_error"></span>
                       </div>
                    
                        <div class="col-md-6 form-group mb-3">
                            <label for="nominee_email" class="">Nominee Email<span style="color: red">*</span></label>
                          
                          	<input type="email" class="form-control" id="NomineeDetails_1_nominee_email"  name="NomineeDetails[1][nominee_email]" placeholder="Enter Nominee Email">
                            <span class="error text-danger" id="NomineeDetails_1_nominee_email_error"></span>
                       </div>
                    </div>

                    <div class="row">
                       <div class="col-md-6 form-group mb-3">
                            <label for="nominee_gender" class="">Nominee Gender<span style="color: red">*</span></label>
                          	<select class="form-control " name="NomineeDetails[1][nominee_gender]" id="NomineeDetails_1_nominee_gender">
	                            <option value="">Please Select</option>
	                            <option value="Male">Male</option>
	                            <option value="Female">Female</option>  
                        	</select>
                          <span class="error text-danger" id="NomineeDetails_1_nominee_gender_error"></span>
                       </div>
                    
                        <div class="col-md-6 form-group mb-3">
                            <label for="nominee_dob" class="">Nominee DOB<span style="color: red">*</span></label>
                          
                          	<input type="text" class="form-control nominee_dob" id="NomineeDetails_1_nominee_dob"  name="NomineeDetails[1][nominee_dob]" placeholder="Select Nominee DOB" autocomplete="nope" readonly="" style="background-color: white">
                            <span class="error text-danger" id="NomineeDetails_1_nominee_dob_error"></span>
                       </div>
                    </div>

                    <div class="row">
                       <div class="col-md-6 form-group mb-3">
                            <label for="nominee_relation" class="">Nominee Relation<span style="color: red">*</span></label>
                          	<input type="text" class="form-control" id="NomineeDetails_1_nominee_relation"  name="NomineeDetails[1][nominee_relation]" placeholder="Enter Relation Name" autocomplete="nope">
                            <span class="error text-danger" id="NomineeDetails_1_nominee_relation_error"></span>
                       </div>
                    
                        <div class="col-md-6 form-group mb-3">
                            <label for="nominee_address" class="">Nominee Address<span style="color: red">*</span></label>
							             <textarea class="form-control" id="NomineeDetails_1_nominee_address" name="NomineeDetails[1][nominee_address]" rows="3" placeholder="Enter Nominee Address"></textarea>
                           <span class="error text-danger" id="NomineeDetails_1_nominee_address_error"></span>
                        </div>
                    </div>

                    <div class="row"> 
                       <div class="col-md-6 form-group mb-3" style="margin-top:-15px;margin-left:-10px">
                        <div class="col-md-6 form-group mb-2">
                          <label for="profile_photo">Nominee Photo</label>
                        </div>
                        <div class="col-md-6 form-group mb-3">
                            <input type="file" name="NomineeDetails[1][nominee_photo]"  id="NomineeDetails_1_nominee_photo" accept="image/x-png,image/png,image/gif,image/jpeg" onchange="ValidateSizeadd(this,this.id)">
                            <span class="error text-danger" id="NomineeDetails_1_nominee_photo_error">
                           </span>
                        </div>
                        </div>   
                    </div> 
                    <div class="form-group  new_nominee_div">
                    </div>
                    
                    <div class="row d-flex justify-content-end">
                     <a href="javascript:void(0);"  class="btn btn-success" id="add_nominee">Add Another Nominee+</a>
                    </div> <hr> 
                        <div class="form-group row">
                            <div class="col-sm-12 d-flex justify-content-center">
                            	<a class="txt-color btn btn-primary btn-rounded m-1" id="previousTab2" style="float:right;color: white">Previous</a>
                                <a class="txt-color btn btn-primary btn-rounded m-1" id="changeTabPayment" style="float:right; color: white">Next</a>
                            </div>
                        </div>
                </div>
            <!--------------Member Nominee details tab end------------->

            <!--------------Member Payment details tab start------------->  	
            	<div class="tab-pane fade" id="nav-payment" role="tabpanel" aria-labelledby="nav-payment-tab">
                	<div class="row">
                        <div class="col-md-6 form-group mb-3">
                            <label for="member_age" class="">Member Age<span style="color: red">*</span></label>
                          
                          	<input type="text" class="form-control" id="member_age"  name="member_age" placeholder="Enter Memebr Age" readonly="">
                            <span class="error text-danger" id="member_age_error"></span>
                       </div>

                       <div class="col-md-6 form-group mb-3">
                            <label for="corpus_amount" class="">Corpus Amount<span style="color: red">*</span></label>
                          
                              <input type="text" class="form-control" id="corpus_amount"  name="corpus_amount" placeholder="Enter Corpus Amount" readonly="">
                              <span class="error text-danger" id="corpus_amount_error"></span>
                       </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 form-group mb-3">
                            <label for="payment_mode" class="">Payment Mode<span style="color: red">*</span></label>
                          
                          	<select class="form-control" name="payment_mode" id="payment_mode">
                          		<option value="" hidden>Please Select</option>
                          		  @foreach($data['dropdowns']['payment_modes'] as $row)
                                  <option value="{{ $row['dropdown_value'] }}">{{  $row['dropdown_value']  }}</option>
                                @endforeach 
                          	</select>
                            <span class="error text-danger" id="payment_mode_error"></span>
                       </div>

                       <div class="col-md-6 form-group mb-3">
                            <label for="payment_date" class="">Payment Date<span style="color: red">*</span></label>
                          
                              <input type="text" class="form-control pdate" id="payment_date"  name="payment_date" placeholder="Select Payment Date" readonly="" style="background-color: white">
                              <span class="error text-danger" id="payment_date_error"></span>
                       </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 form-group mb-3">
                            <label for="note" class="">Payment Note</label>
							               <textarea class="form-control" id="note" name="note" rows="3" placeholder="Enter payment note"></textarea>
                             <span class="error text-danger" id="note_error"></span>
                       </div>

                       <div class="col-md-6 form-group mb-3">
                            <label>Is Member Intersted in Other schemes ?<span style="color: red">*</span></label><br>
                             <input type="radio" value="Y" name="interested"> <label class="form-check-label">Yes</label>
                             <input type="radio" value="N" name="interested"> <label class="form-check-label">No</label><br>
                             <span class="error text-danger" id="interested_error"></span>
                       </div>

                    </div>
                    <hr>
                    <div class="row">
                      <div class="col-md-6 form-group mb-3">
                            <label for="corpus_amount" class="">Advance Amount<span style="color: red">*</span></label>
                          
                              <input type="text" class="form-control" id="advance_amount"  name="advance_amount" placeholder="Enter Corpus Amount" readonly="" value="{{$setting['advance_amount']}}">
                              <span class="error text-danger" id="advance_amount_error"></span>
                       </div>

                        <div class="col-md-6 form-group mb-3">
                            <label for="payment_mode" class="">Advance Payment Mode<span style="color: red">*</span></label>
                          
                            <select class="form-control" name="advance_payment_mode" id="advance_payment_mode">
                              <option value="" hidden>Please Select</option>
                                @foreach($data['dropdowns']['payment_modes'] as $row)
                                  <option value="{{ $row['dropdown_value'] }}">{{  $row['dropdown_value']  }}</option>
                                @endforeach 
                            </select>
                            <span class="error text-danger" id="advance_payment_mode_error"></span>
                       </div>

                       <div class="col-md-6 form-group mb-3">
                            <label for="payment_date" class="">Advance Payment Date<span style="color: red">*</span></label>
                          
                              <input type="text" class="form-control pdate" id="advance_payment_date"  name="advance_payment_date" placeholder="Select Payment Date" readonly="" style="background-color: white">
                              <span class="error text-danger" id="advance_payment_date_error"></span>
                       </div>
                    </div>
                        <div class="form-group row">
                            <div class="col-sm-12 d-flex justify-content-center">
                            	<a class="txt-color btn btn-primary btn-rounded m-1" id="previousTab3" style="float:right;color: white">Previous</a>
                              <a class="txt-color btn btn-primary btn-rounded m-1" id="changeTabMemberAcess" style="float:right; color: white">Next</a>
                            </div>
                        </div>
                </div>
            <!--------------Member Payment details tab end------------->

            <!--------------Member Access tab start------------->  	
            	<div class="tab-pane fade" id="nav-member_access" role="tabpanel" aria-labelledby="nav-member_access-tab">
                	<div class="row">
                      <div class="col-md-6 form-group mb-3">
                     <label for="password">Password<span style="color: red">*</span></label>
                        <input type="password" name="password" class="form-control" id="password" placeholder="Enter Password"  autocomplete="new-password">
                        <span class="error text-danger err-password" id="password_error">
                       </span>
                   </div>
                   <div class="col-md-6 form-group mb-3">
                     <label for="confirm_password">Confirm Password<span style="color: red">*</span></label>
                    
                        <input type="password" name="confirmpassword" class="form-control" id="cppassword" placeholder="Enter Confirm Password">
                        <span class="text-danger err-confirmpassword" id="confirmpassword_error"></span>
                    </div>
                </div>

                <div class="row">
                   <div class="col-md-6 form-group mb-3">
                     <label for="member_status">Member Status
                     	<span style="color: red">*</span>
                     </label>
                    	<select class="form-control" name="member_status" id="member_status">
                    		<option value="" hidden>Please Select</option>
                      		<option value="Active">Active</option>
                          <option value="Probationary" selected="">Probationary</option>
                      		<option value="InActive">InActive</option>
                    	</select>
                      <span class="text-danger err-confirmpassword" id="member_status_error"></span>
                    </div>

                    <div class="col-md-6 form-group mb-3">
                        <label for="membership_date">Membership Date<span style="color: red">*</span></label>
                        <input class="form-control pdate" type="text" name="membership_date"  id="membership_date" placeholder="Select Membership Date" style="background-color: white" readonly="">
                        <span class="error text-danger" id="membership_date_error"></span>
                    </div>
                  </div>

                  <div class="row">  
                    <div class="col-md-6 form-group mb-3">
                        <label for="additional_info" class="">Additional Information</label>
						            <textarea class="form-control" id="additional_info" name="additional_info" rows="4" placeholder="Enter Additional Information"></textarea>
                        <span class="error text-danger" id="additional_info_error">
                       </span>
                   </div>

                   <div class="col-md-6 form-group mb-3">
                      <div class="col-md-6 form-group mb-2">
                          <label for="profile_photo">Upload Profile Photo</label>
                        </div>
                      <div class="col-md-6 form-group mb-2">
                        <input type="file" class="customer_id_images" name="profile_photo"  id="profile_photo" accept="image/x-png,image/png,image/gif,image/jpeg">
                        <span class="error text-danger" id="profile_photo_error">
                       </span>
                      </div>
                    </div>
                </div>
                    <div class="form-group row">
                        <div class="col-sm-12 d-flex justify-content-center">
                        	<a class="txt-color btn btn-primary btn-rounded m-1" id="previousTab4" style="float:right;color: white">Previous</a>
                        	<button type="button" class="btn btn-primary btn-rounded m-1" id="submitButton">Submit</button> 
                            
                        </div>
                    </div>
                </div>
                
          <!---------------------Member Access tab end----------------->        
        </div> 
	   </div>    	
	</div>    	
</div>    	
@endsection
@section('page-js')
<script>
$(document).ready(function(){
  var age_from=$('#age_from').val();
  var age_to=$('#age_to').val();
  
  $('#customer_id_proofs').hide();
  $('#Illness_type_div').hide();
  $('#physical_disability_type_div').hide();

  $('#physical_disability').on('change', function() {
      if($(this).val() == 'Y'){
        $('#physical_disability_type_div').show();
        $('#physical_disability_type').attr('required',true);
      }else{
        $('#physical_disability_type_div').hide();
        $('#physical_disability_type').val('').attr('required',false);;
      }
  });

  $('#customer_id_type').on('change', function(){
      if($(this).val() != ''){
        $('#customer_id_proofs').show();
        $('#customer_id_front').val("");
        $('#customer_id_back').val("");
        $('#customer_id_back_error').hide();
        $('#customer_id_front_error').hide();
      }else{
        $('#customer_id_proofs').hide();
        $('#customer_id_back_error').hide();
      $('#customer_id_front_error').hide();
      }
  });

  $('#previous_illness').on('change', function(){
      if($(this).val() == 'Y'){
        $('#Illness_type_div').show();
        $('#illness_type').attr('required',true);
      }else{
        $('#Illness_type_div').hide();
        $('#illness_type').val('').attr('required',false);
      }
  });
  
$( function() {
    $( ".dob" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd-mm-yy',
      yearRange: "-"+age_to+":+"+age_from+"",
      maxDate:"-"+age_from+"Y",
    });

    $('body').on('focus',".nominee_dob", function(){
        if( $(this).hasClass('hasDatepicker') === false )  {
          $(this).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd-mm-yy',
            yearRange: "-65:+1",
            maxDate:"-1Y",
          });
        }
    });

    $( ".pdate" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd-mm-yy',
      maxDate: 0,
    });
  });
$('#changeTabMedical').on('click', function() {

    $('#nav-tab a[href="#nav-medical"]').tab('show');
});

//code for previous tab
$('#previousTab1').on('click', function() {
   $('#nav-tab a[href="#nav-personal"]').tab('show');
    
});

$('#changeTabNominee').on('click', function() {

   $('#nav-tab a[href="#nav-nominee"]').tab('show');
});

$('#previousTab2').on('click', function() {
  $('#nav-tab a[href="#nav-medical"]').tab('show');
});


$('#changeTabPayment').click(function(e){
  	$('#nav-tab a[href="#nav-payment"]').tab('show');
});

$('#previousTab3').on('click', function() {
   	$('#nav-tab a[href="#nav-nominee"]').tab('show');
});

$('#changeTabMemberAcess').click(function(e){
      $('#nav-tab a[href="#nav-member_access"]').tab('show');
});

$('#previousTab4').on('click', function() {
    $('#nav-tab a[href="#nav-payment"]').tab('show');
});

$('.customer_id_images').on('change', function() {
  if(this.files[0] && this.files[0].size/1024/1024 > 6){
    $("#"+$(this).attr("id")+'_error').html('File size should be less than 6 MB').show();
    $('#'+$(this).attr("id")).val("");
  }else {
    $("#"+$(this).attr("id")+'_error').html('').hide();
  }
});

$('#country_id').change(function(){
        var id = $(this).find(':selected').data('id');

        $('#state_id').empty();
         $('#city_id').empty();
         $('#state_id').append('<option value="" hidden="">Please Select</option>');
        
        $('#city_id').append('<option value="" hidden="">Please Select</option>');
        $('#city_id').trigger('change');
        if(id != '' && id != 'undefined'&& id != 'Select Country') {
            $.ajax({
                url      : "{{url('/getStatesofCountry')}}/"+id,
                method   : 'GET',
                dataType : 'json',
                success: function(data) {
                    if(data.status == 'success'){
                        var data = data.data;
                        var options = '';
                        for (var item in data) {
                            var res = data[ item ];
                            options += "<option value= '"+res.name+"' data-id='"+res.id+"'>"+res.name+"</option>";
                        }
                        if(!options) {
                            $('#state_id').empty();
                        } else {
                            
                            $('#state_id').append(options);
                        }
                    }else{
                      $('#state_id').empty();
                      $('#state_id').append('<option></option>');
                    }
                    $("#state_id").trigger('change');
                },
                error:function() {
                    alert('Something went wrong');
                }
            });
        }
    });

    $('#country_id').val('India').trigger('change');

    $('#state_id').change(function(){
        var id = $(this).find(':selected').data('id');
        $('#city_id').empty();
        $('#city_id').append('<option value="" hidden="">Please Select</option>');
        
        if(id != '' && id != 'undefined'&& id != 'Please Select' && typeof id != "undefined") {
            $.ajax({
                url      : "{{url('/getCitiesofState')}}/"+id,
                method   : 'GET',
                dataType : 'json',
                success: function(data) {
                  
                  if(data.status == 'success'){
                    var data = data.data;
                        var options = '';
                        for (var item in data) {
                            var res = data[ item ];
                            options += "<option value= '"+res.name+"' data-id='"+res.id+"'>"+res.name+"</option>";
                        }
                        if(options != '') {
                           $('#city_id').append(options);
                        }
                  }
                  $('#city_id').trigger('change');
                },
                error:function() {
                    alert('Something went wrong');
                }
            });
        }
    });

    $('#memberDetails').validate({
          ignore: "",
          rules: {
            member_id:{
               required: true,
               alpha_numeric: true,
               minlength:3,
               maxlength:100,
            }, 
            name:{
               required: true,
               only_text: true,
               minlength:3,
               maxlength:100,
            },
            "NomineeDetails[1][nominee_name]":{
               required: true,
               only_text: true,
               minlength:3,
               maxlength:100,
            },
            email:{
                required: true,
                custom_email: true,
                maxlength:100,
            },
            alternative_email:{
                custom_email: true,
                maxlength:100,
                notEqualTo:"#email",
            },
            "NomineeDetails[1][nominee_email]":{
                required:true,
                custom_email: true,
                maxlength:100,
            },
            "NomineeDetails[1][nominee_address]":{
               required:true,
                maxlength:1000,
            },
            note:{
                maxlength:1000,
            },
            additional_info:{
                maxlength:1000,
            },
            mobile_number:{
              required: true,
              number : true,
              minlength:10,
              maxlength:10,
            },
            alternative_number:{
                notEqualTo:"#mobile_number",
                number : true,
                minlength:10,
                maxlength:10,
            },
            "NomineeDetails[1][nominee_phone]":{
                required:true,
                number : true,
                minlength:10,
                maxlength:10,
            },
            "NomineeDetails[1][nominee_fcp]":{
                required:true,
                number:true,
                minlength:1,
                maxlength:3,
                max:100
            },
            marital_status:{
              required:true
            },
            gender:{
              required:true
            },
            "application_form[]":{
              required:true
            },
            "NomineeDetails[1][nominee_gender]":{
              required:true
            },
            country:{
              required:true
            },
            state:{
              required:true
            }, 
            city:{
              required:true
            }, 
            dob:{
              required:true
            }, 
            member_address:{
              maxlength:1000
            }, 
            "NomineeDetails[1][nominee_dob]":{
              required:true
            },
            "NomineeDetails[1][nominee_relation]":{
              required:true,
              only_text: true,
              maxlength:100,
            }, 
            customer_id_type:{
              required:true
            }, 
            
             physical_disability:{
              required:true
            },
             
            previous_illness:{
              required:true
            },
            
            health_declaration:{
              required:true
            },
            payment_mode:{
              required:true
            },
            payment_date:{
              required:true
            },
            interested:{
              required:true
            },
            advance_payment_mode:{
              required:true
            },
            advance_payment_date:{
              required:true
            },
            membership_date:{
              required:true
            }, 
            physical_disability_type:{
              only_text: true,
              maxlength:100,
            }, 
            password:{
                required: true,  
                minlength: 6,  
                maxlength: 30,  
            },
            confirmpassword:{
              required: true, 
              equalTo : "#password",
              minlength: 6,
              maxlength: 30, 
            },
            member_status:{
              required: true, 
            },
            profile_photo:{
              // required: true, 
            }, 
          },
        messages:{
            member_id:{
              alpha_numeric: "Enter valid Member Id"
            },
            name:{
              required: "This field is Required"
            },
             email:{
              required: "This field is Required"
            },
            password:{
              required: "This field is Required"
            },
            confirmpassword:{
              equalTo: "Confirm Password should be same as Password"
            },
            member_address:{
              maxlength: "This field can not exceed 1000 characters"
            },
            note:{
              maxlength: "This field can not exceed 1000 characters"
            },
            additional_info:{
              maxlength: "This field can not exceed 1000 characters"
            },
            "NomineeDetails[1][nominee_address]":{
              maxlength: "This field can not exceed 1000 characters"
            },
            mobile_number:{
              required: "This field is Required"
            },
            alternative_email:{
              notEqualTo:"Email and alternative email should be different"
            },
            alternative_number:{
              notEqualTo:"Mobile Number and alternative Mobile Number should be different"
            },
            physical_disability_type:{
              only_text:"Only alphabets are allowed",
              maxlength: "This field can not exceed 100 characters"
            },
            "NomineeDetails[1][nominee_relation]":{
              only_text:"Only alphabets are allowed",
              maxlength: "This field can not exceed 100 characters"
            },
            "NomineeDetails[1][nominee_fcp]":{
              max:"Maximum Percentage allowed 100 %",
            },
        },
        errorPlacement: function(error, element) {
            if (element.attr("type") == "radio") {
                error.insertAfter('#interested_error');
            } else {
                error.insertAfter(element);
            }
        }
      });
      $('#submitButton').click(function () {
          if($("#memberDetails").valid()){
          alert("Form validated successfully!");
          if (confirm("Are you sure want to submit form!")){
              $('#submitButton').attr('disabled',true);
              $('.loadscreen').show();
              $.ajax({
                  method: "POST",
                  headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },  
                  url: "{{url('member/store')}}",
                  data:new FormData($("#memberDetails")[0]),
                  contentType: false,
                  processData: false,
                }).done(function( data ) {
                  $('.loadscreen').hide();
                  if(data.status== 'success'){
                      $("#memberDetails")[0].reset();
                      $("#alert-primary").text('Member Created Successfully !');
                      $("#successmsg").show();
                      $('html, body').animate({
                        scrollTop: $('.main-header').offset().top
                    }, 1000);
                      setTimeout(function(){ $('#successmsg').fadeOut() }, 3000);
                      window.location.href = "{{url('member/list')}}";
                  }
                  else if(data.status=='validation_error'){

                      $('#submitButton').removeAttr('disabled',true);
                      var messages = '';
                      $.each(data.data, function (index, value) {
                        messages += index.charAt(0).toUpperCase() + index.slice(1) + ' : ' + value+'\n';
                      });
                      swal("Please fill mandatory field",messages);

                      $.each(data.data, function (key, val) {
                          var key1=key.replace(/\./g, '_')
                          $("#"+key1+"_error").text(val[0]);
                          $("#"+key1+"_error").show();

                          $(document).on("keyup", "input[id='"+key1+"']", function(e) {
                                $("#"+key1+"_error").hide();
                          });
                          $(document).on("change", "input[id='"+key1+"']", function(e) {
                                $("#"+key1+"_error").hide();
                          });
                      });
                  }
                  else{
                      $("#alert-primary").text('Something Went Wrong !');
                      $('#submitButton').removeAttr('disabled',true);
                      $("#successmsg").show();
                      $('html, body').animate({
                        scrollTop: $('.main-header').offset().top
                    }, 1000);
                      setTimeout(function(){ $('#successmsg').fadeOut() }, 3000);
                  }
              });
            }   
            else{
                return false;
            }
        }  
        else{
          var validator = $("#memberDetails").validate();
          var messages = '';
          $.each(validator.errorMap, function (index, value) {
            messages += index.charAt(0).toUpperCase() + index.slice(1) + ' : ' + value+'\n';
            });
          swal("Please fill mandatory field",messages);
        }
            
    });
  });
  
  $('#member_dob').change(function(){
    $('#dob_error').hide();
    if($(this).val() != ''){
      var dateString=$(this).val();
      
      var dateAr = dateString.split('-');
      var newDate = dateAr[1] + '-' + dateAr[0] + '-' + dateAr[2].slice(-2);

      var today = new Date();
      var birthDate = new Date(newDate);
    
      var age = today.getFullYear() - birthDate.getFullYear();
    
      var m = today.getMonth() - birthDate.getMonth();
    
      if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
      }
      $('#member_age').val(age)
      if(age != ''|| age !=NaN){
        getCorpusAmount(age)

        if(age>=50){
          $('#reports').attr('required',true)
          $('#report_label').text('*')
        }
        else{
          $('#reports').removeAttr('required',true)
          $('#report_label').text('')
          $('#reports-error').val("").hide();
          $('#reports_error').val("").hide();
        }
      }
    }
  });
  function getCorpusAmount(age){
      $.ajax({
              method: "POST",
                url: "{{url('member/getCorpusAmount')}}",
                data:{
                    "_token": "{{ csrf_token() }}",
                    "age" : age
                  },
              }).done(function( data ) {
              if(data.status=='success'){
                  $('#corpus_amount').val(data.amount)
              }
              else if(data.status=='fail'){
                  $('#dob_error').show();
                  $('#member_age').val('')
                  $('#corpus_amount').val('')
                  $('#dob_error').text(data.amount)
              }
          })
      }

  function ValidateSizeadd(file,id,i){
    if(!/(\.jpg|\.jpeg|\.png|\.pdf)$/i.test(file.value))
    { 
      alert("Invalid image /pdf file type.");      
        $('#'+id).val('');  
        return false;   
    }   
      var FileSize = file.files[0].size;
      
      if (FileSize > 6000000)
      {
        $("#"+id+'_error').html('File size should be less than 6 MB').show();
        $('#'+id).val('');  
        return false;
      }else{
        $("#"+id+'_error').hide();
      }
  }

  var s=1;
  function get_divs()
  {
    s++;
    $("#here").append('<div id = "new'+s+'" class="row"> <div class="col-md-6 form-group"><label>Upload File<span style="font-size:12px;color:red;"></span></label><input type="file" class="form-control-file" name="reports[]" accept="image/x-png,image/png,image/gif,image/jpeg,pdf,doc" id="img'+s+'" style="border:0px;" onchange="ValidateSizeadd(this,this.id)"><span class="error text-danger" id="img'+s+'_error"></span></div><button type="button" class="btn btn-danger" style="height: 30px;margin-top: 25px;font-size: 12px;line-height: 20px;" onclick="delete_divs('+s+');">remove</button></div>');
  }

  function delete_divs(s)
  {
    $("#new"+s).remove();
    s-1;
  }   

  //script to add Paternal details
var j=2


$('#add_nominee').on('click',function(){
  
  addNominee='<div id="add_nominee_div'+j+'"><hr><div class="row d-flex justify-content-end"><a href="javascript:void(0);"  class="btn btn-danger" onclick="removeNominee('+j+')">remove-</a></div><div class="row"><div class="col-md-6 form-group mb-3"><label for="nominee_name" class="">Nominee Name<span style="color: red">*</span></label><input type="text" class="form-control" id="NomineeDetails_'+j+'_nominee_name"  name="NomineeDetails['+j+'][nominee_name]" placeholder="Enter Nominee Name"><span class="error text-danger" id="NomineeDetails_'+j+'_nominee_name_error"></span></div><div class="col-md-6 form-group mb-3"><label for="nominee_name" class="">FC Amount Percentage<span style="color: red">*</span></label><input type="text" class="form-control fc_percentage" id="NomineeDetails_'+j+'_nominee_fcp"  name="NomineeDetails['+j+'][nominee_fcp]" placeholder="Enter FC amount %" onchange="getId('+j+')"><span class="error text-danger" id="NomineeDetails_'+j+'_nominee_fcp_error"></span></div></div><div class="row"><div class="col-md-6 form-group mb-3"><label for="nominee_phone" class="">Nominee Phone<span style="color: red">*</span></label><input type="text" class="form-control" id="NomineeDetails_'+j+'_nominee_phone"  name="NomineeDetails['+j+'][nominee_phone]" placeholder="Enter Nominee Phone" maxlength="10"><span class="error text-danger" id="NomineeDetails_'+j+'_nominee_phone_error"></span></div><div class="col-md-6 form-group mb-3"><label for="nominee_email" class="">Nominee Email<span style="color: red">*</span></label><input type="email" class="form-control" id="NomineeDetails_'+j+'_nominee_email"  name="NomineeDetails['+j+'][nominee_email]" placeholder="Enter Nominee Email"><span class="error text-danger" id="NomineeDetails_'+j+'_nominee_email_error"></span></div></div><div class="row"><div class="col-md-6 form-group mb-3"><label for="nominee_gender" class="">Nominee Gender<span style="color: red">*</span></label><select class="form-control " name="NomineeDetails['+j+'][nominee_gender]" id="NomineeDetails_'+j+'_nominee_gender"><option value="">Please Select</option><option value="Male">Male</option><option value="Female">Female</option></select><span class="error text-danger" id="NomineeDetails_'+j+'_nominee_gender_error"></span></div><div class="col-md-6 form-group mb-3"><label for="nominee_dob" class="">Nominee DOB<span style="color: red">*</span></label><input type="text" class="form-control nominee_dob" id="NomineeDetails_'+j+'_nominee_dob"  name="NomineeDetails['+j+'][nominee_dob]" placeholder="Select Nominee DOB" autocomplete="nope" readonly="" style="background-color: white"><span class="error text-danger" id="NomineeDetails_'+j+'_nominee_dob_error"></span></div></div><div class="row"><div class="col-md-6 form-group mb-3"><label for="nominee_relation" class="">Nominee Relation<span style="color: red">*</span></label><input type="text" class="form-control" id="NomineeDetails_'+j+'_nominee_relation"  name="NomineeDetails['+j+'][nominee_relation]" placeholder="Enter Relation Name" autocomplete="nope"><span class="error text-danger" id="NomineeDetails_'+j+'_nominee_relation_error"></span></div><div class="col-md-6 form-group mb-3"><label for="nominee_address" class="">Nominee Address<span style="color: red">*</span></label><textarea class="form-control" id="NomineeDetails_'+j+'_nominee_address" name="NomineeDetails['+j+'][nominee_address]" rows="3" placeholder="Enter Nominee Address"></textarea><span class="error text-danger" id="NomineeDetails_'+j+'_nominee_address_error"></span></div></div><div class="row"><div class="col-md-6 form-group mb-3" style="margin-top:-15px;margin-left:-10px"><div class="col-md-6 form-group mb-2"><label for="profile_photo">Nominee Photo</label></div><div class="col-md-6 form-group mb-3"><input type="file" name="NomineeDetails['+j+'][nominee_photo]"  id="NomineeDetails_'+j+'_nominee_photo" accept="image/x-png,image/png,image/gif,image/jpeg" class="customer_id_images" onchange="ValidateSizeadd(this,this.id)"><span class="error text-danger" id="NomineeDetails_'+j+'_nominee_photo_error"></span></div></div></div></div>';
  
    $(".new_nominee_div").show().append(addNominee);
    $("#NomineeDetails_"+j+"_nominee_name").rules("add", {required:true, only_text:true, minlength:3,maxlength:100 });
    $("#NomineeDetails_"+j+"_nominee_address").rules("add", {required:true, maxlength:1000 });
    $("#NomineeDetails_"+j+"_nominee_dob").rules("add", { required:true});
    $("#NomineeDetails_"+j+"_nominee_gender").rules("add", { required:true});
    $("#NomineeDetails_"+j+"_nominee_relation").rules("add", { required:true,only_text:true, minlength:3,maxlength:100});
    $("#NomineeDetails_"+j+"_nominee_email").rules("add", { required:true,maxlength:100});
    $("#NomineeDetails_"+j+"_nominee_phone").rules("add", { required:true,number : true,minlength:10,maxlength:11 });
    
    $("#NomineeDetails_"+j+"_nominee_fcp").rules("add", { required:true,number : true,minlength:1,maxlength:3,max:100 });

    var percent= calculatePercentage()
    
    if(percent>0){
      var percent_share=100-parseInt(percent);
    }else{
      var percent_share=parseInt(percent);
    }
  
    $('#NomineeDetails_'+j+'_nominee_fcp').val(percent_share)

    $('html, body').animate({
        scrollTop: $('#add_nominee_div'+j).offset().top
    }, 1000);
    j++;
  })
  
  
  function getId(id){
    calculatePercentage();
  }

  function calculatePercentage(){
      var sum = 0;
      $(".fc_percentage").each(function(){
      sum += +$(this).val();
      if(sum>100){
        alert('Total FC Percentage should not exceed 100%')
        $(this).val('')
      }
    });
    return sum;
  }
    function removeNominee(value){
      j--;
      $('#add_nominee_div'+value).empty()
      $('#add_nominee_div'+value).remove()
      $('#add_nominee_div'+value).hide()
      $('#add_nominee').removeAttr('hidden');
    } 

    var s=1;
    function add_divs()
    {
      s++;
      $("#new_file").append('<div id = "new'+s+'" class="row"> <div class="col-md-6 form-group"><label>Upload File<span style="font-size:12px;color:red;"></span></label><input type="file" class="form-control-file" name="application_form[]" accept="image/x-png,image/png,image/gif,image/jpeg,pdf,doc" id="img'+s+'" style="border:0px;" onchange="ValidateSizeadd(this,this.id)"><span class="error text-danger" id="img'+s+'_error"></span></div><button type="button" class="btn btn-danger" style="height: 30px;margin-top: 25px;font-size: 12px;line-height: 20px;" onclick="delete_div('+s+');">remove</button></div>');
    }
    function delete_div(s)
    {
      $("#new"+s).remove();
      s-1;
    }

    $("#member_id").change(function(){
        $.ajax({
              method: "POST",
              url : "{{url('member/checkDuplicates')}}",
              data :{
                "_token": "{{ csrf_token() }}",
                "member_id": $('#member_id').val(),
              },
            })
            .done(function( data ) {
              if(data.message.member_id== 'The member id has already been taken.'){
                    $('#nav-tab a[href="#nav-personal"]').tab('show');
                    $('#member_id_error').show()
              }else if(data.message ==''){
                $('#member_id_error').hide()
              }
            });
      }); 
    $('#member_id').on('keyup',function(){
        $('#member_id_error').hide();
      })
    $("#mobile_number").change(function(){
      $.ajax({
            method: "POST",
            url : "{{url('member/checkDuplicates')}}",
            data :{
              "_token": "{{ csrf_token() }}",
              "mobile_number": $('#mobile_number').val(),
            },
          })
          .done(function( data ) {
            if(data.message.mobile_number =='The mobile number has already been taken.'){
                  $('#nav-tab a[href="#nav-home"]').tab('show');
                  $('#mobile_number_error').show()
              }
              else if(data.message ==''){
                $('#mobile_number_error').hide()
              }
          });
    });

      $('#mobile_number').on('keyup',function(){
        $('#mobile_number_error').hide();
      })

    $("#email").change(function(){
      $.ajax({
            method: "POST",
            url : "{{url('member/checkDuplicates')}}",
            data :{
              "_token": "{{ csrf_token() }}",
              "email": $('#email').val(),
            },
          })
          .done(function( data ) {
              if(data.message.email =='The email has already been taken.'){
                  $('#nav-tab a[href="#nav-home"]').tab('show');
                  $('#email_error').show()
              }
              else if(data.message ==''){
                $('#email_error').hide()
              }
          });
   }); 

    $('#email').on('keyup',function(){
        $('#email_error').hide();
      })
</script>

@endsection