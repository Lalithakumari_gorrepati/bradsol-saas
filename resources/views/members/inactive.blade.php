@extends('layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-12">
          <div class="col-md-12" id="successmsg" style="display: none">
            <div class="alert alert-primary " id="alert-primary">
                
                <button class="close float-right" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"></span></button>
            </div>
          </div>
          <div class="card o-hidden mb-4">
              <div class="card-header">
                <h3 class="w-50 float-left card-title m-0">List of InActive Members</h3>
              </div>

              <div class="card-body">

                <div class="row pb-3">
                    <div class="col-md-12 mb-3 font-size">
                        <a href="#" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                          <b style="color: black">Filters By <i class="fas fa-chevron-circle-down"></i></b> click here
                        </a>
                    </div>
                </div>

                <div class="collapse" id="collapseExample">
                  <div class="row pb-3">
                   <div class="col-md-2">Register ID</div>
                     <div class="col-md-3">
                       <input type="text" class="form-control" id="register_id" name="register_id" placeholder="Register ID" autocomplete="off">
                     </div>
                     <div class="col-md-2"></div>

                   <div class="col-md-2">Member Name</div>
                    <div class="col-md-3">
                        <input type="text" class="form-control" id="member_name" name="member_name" placeholder="Member Name" autocomplete="off">
                    </div>
                  </div>
                  <div class="row pb-3">  
                        <div class="col-md-2">Age From</div>
                        <div class="col-md-3">
                          <select class="form-control" name="age_from" id="age_from">
                            <option value="" hidden="">Please Select</option>
                            @foreach ($data as $value)
                              <option value="{{$value->age_from}}">{{$value->age_from}}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="col-md-2"></div>

                        <div class="col-md-2">Age To</div>
                        <div class="col-md-3">
                          <select class="form-control" name="age_to" id="age_to">
                            <option value="" hidden="">Please Select</option>
                            @foreach ($data as $value)
                              <option value="{{$value->age_to}}">{{$value->age_to}}</option>
                            @endforeach
                          </select>
                        </div>
                    </div>

                    <div class="row pb-3">  
                        <div class="col-md-2">Updated Date From</div>
                        <div class="col-md-3">
                          <input type="text" class="form-control" id="from" name="from" placeholder="Select From Date" autocomplete="off">
                        </div>
                        <div class="col-md-2"></div>

                        <div class="col-md-2">Updated Date To</div>
                        <div class="col-md-3">
                          <input type="text" class="form-control" id="to" name="to" placeholder="Select To Date" autocomplete="off">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">Phone</div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control" id="member_phone" name="member_phone" placeholder="Member Phone" autocomplete="off">
                            </div>
                        </div>
                      <div class="col-md-2"></div>
                      <div class="col-md-2">Email</div>
                      <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control" id="member_email" name="member_email" placeholder="Member Email" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="row pb-3">
                        <div class="col-6 col-md-1">
                            <button type="button" class="txt-color btn btn-primary btn-rounded" id="filter" name="filter">Filter</button>
                        </div>
                        <div class="col-6 col-md-9">
                            <button type="button" class="txt-color btn btn-primary btn-rounded" id="reset" name="reset">Reset</button>
                        </div>
                    </div>
                </div>
                  <table id="members_list_table" class="table table-bordered  text-center">
                    <thead>
                        <tr>
                            <th scope="col">Register ID</th>
                            <th scope="col">Member Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Age</th>
                            <th scope="col">Corpus Amount</th>
                            <th scope="col">Payment Date</th>
                            <th scope="col">Membership Date</th>
                            <th scope="col">Updated Date</th>
                            <th width="width: 50px;" scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody id="data"></tbody>
                 </table>                          
              </div>
          </div>
      </div>
  </div>
@endsection

@section('bottom-js')
<script>
  $(document).ready(function(){

    $("#from").datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      changeYear: true,
      autoclosed:true,
      maxDate:0,
      onSelect: function (date) {
          var date2 = $('#from').datepicker('getDate');
          $('#to').datepicker('option', 'minDate', date2);
      }
    });
    $('#to').datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      changeYear: true,
      maxDate:0,
      onClose: function () {
          var dt1 = $('#from').datepicker('getDate');
          var dt2 = $('#to').datepicker('getDate');
          if (dt2 <= dt1) {
              var minDate = $('#to').datepicker('option', 'minDate');
              $('#to').datepicker('setDate', minDate);
          }
      }
    });

      var membersData = $('#members_list_table').DataTable({
          "Processing":true,
          "serverSide":true,
          "ajax":{
                url : "{{url('member/inactive/data-list')}}",
                method :"POST",
                headers: {
			          'X-CSRF-TOKEN': $('#token').val()
			      },
            data : function ( d ) {
                return $.extend( {}, d, {
                      "from" : $('#from').val(),
                      "to"   : $('#to').val(),
                      "member_phone"   : $('#member_phone').val(),
                      "member_email"   : $('#member_email').val(),
                      "age_from"   : $('#age_from').val(),
                      "age_to"   : $('#age_to').val(),
                     "register_id" : $('#register_id').val(),
                     "member_name" : $('#member_name').val(),
                  });
            },
        },
        "scrollY"       : "500px",
        "scrollCollapse": true,
        "scrollX"       : true,
        "columns": [
        {"data":"member_id",
            fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                $(nTd).html("<a href='{{ url('member/view') }}/"+oData.id+"' class='anchortag'>"+oData.member_id+"</a>");
              }
        },
        {"data":"name"},
        {"data":"email"},
        {"data":"mobile_number"},
        {"data":"age"},
        {"data":"amount"},
        {"data":"payment_date"},
        {"data":"join_date"},
        {"data":"created_date"},
        {"data":"action"},
          
        ],
        "aaSorting": [],
        "aLengthMenu": [[10,25, 50, 75,100, -1], 
                [10,25, 50, 75,100, "All"]],
        "columnDefs": [
        { "orderable": false, "targets": 9}
        ],
        dom: 'lBfrtip',
        buttons: [
            {
              extend: 'excelHtml5',
              exportOptions: {
                  columns: [0, 1, 2, 3,4,5,6,7,8]
              }
            },
          ]
    });
      $('#filter').click(function(){
        membersData.draw();
      });

      $('#reset').click(function(){
        $('#age_from,#age_to').prop('selectedIndex',0);
        $("#from, #to,#register_id,#member_phone,#member_email,#member_name").val('');
        membersData.draw();
    });

  $(document).on("click",".change_status_new",function() {
    
    var id = $(this).data('id');
    var status = $(this).data('status');
    $.ajax({
            url      : "{{url('member/update/MemberStatus')}}",
            method   : 'POST',
            headers: {
                'X-CSRF-TOKEN': $('#token').val()
            },
            data     : {
                        "id":id,
                        "member_status":status
                      },
            success: function(data) {
            if(data.status == "success")
            {
              membersData.draw();
              $("#alert-primary").html("Member Status changeed successfully");
                $('#successmsg').show();
                $('html, body').animate({
                    scrollTop: $('.main-header').offset().top
                }, 1000);
                setTimeout(function(){ $('#successmsg').fadeOut() }, 5000);
            }else{
              $('#myModal').modal('hide')
              $("#alert-primary").html("Something went wrong");
              $('#successmsg').show();
              $('html, body').animate({
                    scrollTop: $('.main-header').offset().top
                }, 1000);
              setTimeout(function(){ $('#successmsg').fadeOut() }, 5000);
            }
            },
            error:function() {
              $('#myModal').modal('hide')
              $("#alert-primary").html("Something went wrong");
              $('#successmsg').show();
              $('html, body').animate({
                    scrollTop: $('.main-header').offset().top
                }, 1000);
              setTimeout(function(){ $('#successmsg').fadeOut() }, 5000);
            }
        });

  });
});
</script>
@endsection

