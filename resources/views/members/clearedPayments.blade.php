@extends('layouts.master')
@section('main-content')
<div class="row">
    <div class="col-md-12">
          <div class="col-md-12" id="successmsg" style="display: none">
            <div class="alert alert-primary " id="alert-primary">
                
                <button class="close float-right" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"></span></button>
            </div>
          </div>
          <div class="card o-hidden mb-4">
              <div class="card-header">
                  <h3 class=" float-left card-title m-0">Cleared Payments</h3>
              </div>

              <div class="card-body">
                {{-- <div class="row pb-3">
                  <div class="col-md-12 mb-3 font-size"><b>Filters By:</b></div>
                  </div>
                    <div class="row pb-3">  
                        <div class="col-md-2">Paid Date From</div>
                        <div class="col-md-3">
                          <input type="text" class="form-control" id="from" name="from" placeholder="Select From Date" autocomplete="off">
                        </div>
                        <div class="col-md-2"></div>

                        <div class="col-md-2">Paid Date To</div>
                        <div class="col-md-3">
                          <input type="text" class="form-control" id="to" name="to" placeholder="Select To Date" autocomplete="off">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">Expired Members</div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <select class="form-control" name="expired_id" id="expired_id">
                                  <option value="" hidden="">Please Select</option>
                                  @foreach ($expiredMembers as $value)
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                  @endforeach
                                </select>
                            </div>
                        </div>
                      
                        <div class="col-md-2"></div>
                        <div class="col-md-2">Transaction Id </div>
                        <div class="col-md-3">
                            <div class="form-group">
                              <input type="text" class="form-control" name="transaction_id" id="transaction_id" placeholder=" Transaction Id">
                            </div>
                        </div>
                    </div>
                    <div class="row pb-3">
                        <div class="col-6 col-md-1">
                            <button type="button" class="txt-color btn btn-primary btn-rounded" id="filter" name="filter">Filter</button>
                        </div>
                        <div class="col-6 col-md-9">
                            <button type="button" class="txt-color btn btn-primary btn-rounded" id="reset" name="reset">Reset</button>
                        </div>
                    </div> --}}
                  <div class="table-responsive">
                  <table id="payments_list_table" class="table table-bordered text-center">
                    <input type="hidden" id="member_id" value={{$member_id}}>
                    <thead>
                        <tr>
                            <th scope="col">Transaction Id</th>
                            <th scope="col">Payment against Expired Member</th>
                            <th scope="col">Expired date</th>
                            <th scope="col">FC Released</th>
                            <th scope="col">Total Due</th>
                            <th scope="col">Payment Date</th>
                            <th scope="col">Payment Type</th>
                            <th scope="col">Payment Mode</th>
                        </tr>
                    </thead>
                    <tbody id="data"></tbody>
                 </table>                          
                </div>
              </div>
          </div>
      </div>
  </div>

  <!------------------- View  Payment Details Modal Start---------------------->
             
    <div class="modal fade bd-example-modal-lg " id="viewPayment" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" style="margin-top: 60px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle-2">Payment Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body padding-style">
                <input type="hidden" value="{{csrf_token()}}" name="_token" id="token">
                <div class="form-group row">
                    <div class="col-md-6 form-group " >
                        <div class="row">
                          <div class="col-sm-5">
                              <label for=""><h6 class="labelname">FC released</h6></label>
                          </div>
                          <div class="col-sm-1">:</div>
                          <div class="col-sm-5">
                              <label for="" ><h6 class="value" id="fc_released"></h6></label>
                          </div>
                      </div>
                    </div>

                    <div class="col-md-6 form-group " >
                        <div class="row">
                          <div class="col-sm-5">
                              <label for=""><h6 class="labelname">Total FC Members</h6></label>
                          </div>
                          <div class="col-sm-1">:</div>
                          <div class="col-sm-5">
                              <label for="" ><h6 class="value" id="fc_members"></h6></label>
                               
                          </div>
                        </div>
                    </div>
                   </div> 
                    <div class="form-group row">
                        <div class="col-md-6 form-group ">
                          <div class="row">
                            <div class="col-sm-5">
                                <label for=""><h6 class="labelname">FC Due</h6></label>
                            </div>
                            <div class="col-sm-1">:</div>
                            <div class="col-sm-5">
                                <label for="" ><h6 class="value" id="fc_due"></h6></label>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 form-group ">
                          <div class="row">
                            <div class="col-sm-5">
                                <label for=""><h6 class="labelname">Admin expense</h6></label>
                            </div>
                            <div class="col-sm-1">:</div>
                              <div class="col-sm-5">
                                <label for="">
                                  <h6 class="value" id="admin_expense"></h6></label>
                              </div>
                          </div>
                        </div>
                    </div>
                    
                    <div class="row form-group">
                        <div class="col-md-6 form-group ">
                          <div class="row">
                            <div class="col-sm-5">
                                <label for=""><h6 class="labelname">Due Penalty</h6></label>
                            </div>
                            <div class="col-sm-1">:</div>
                              <div class="col-sm-5">
                                <label for="">
                                  <h6 class="value" id="due_penalty"></h6></label>
                              </div>
                          </div>
                        </div>

                        <div class="col-md-6 form-group ">
                          <div class="row">
                            <div class="col-sm-5">
                                <label for=""><h6 class="labelname">15% Due Interest</h6></label>
                            </div>
                            <div class="col-sm-1">:</div>
                              <div class="col-sm-5">
                                <label for="">
                                  <h6 class="value" id="interest"></h6></label>
                              </div>
                          </div>
                        </div>
                     </div>
                        
                    <div class="row form-group">
                        <div class="col-md-6 form-group "></div>
                        <div class="col-md-6 form-group ">
                          <div class="row">
                            <div class="col-sm-5">
                                <label for=""><h6 class="labelname"><b>Total Due</b></h6></label>
                            </div>
                            <div class="col-sm-1">:</div>
                              <div class="col-sm-5">
                                <label for="">
                                  <h6 class="value" style="font-weight: bold;" id="total_due"></h6></label>
                              </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-rounded" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
  <!-------------------View  Payment Details  Modal End---------------------->
@endsection

@section('bottom-js')
<script>
  $(document).ready(function(){
    $("#from").datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      changeYear: true,
      autoclosed:true,
      onSelect: function (date) {
          var date2 = $('#from').datepicker('getDate');
          $('#to').datepicker('option', 'minDate', date2);
      }
    });
    $('#to').datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      changeYear: true,
      onClose: function () {
          var dt1 = $('#from').datepicker('getDate');
          var dt2 = $('#to').datepicker('getDate');
          if (dt2 <= dt1) {
              var minDate = $('#to').datepicker('option', 'minDate');
              $('#to').datepicker('setDate', minDate);
          }
      }
    });

    function checkage(age){
      var age1=$("#age_from option:selected").text();
      var age2=$("#age_to option:selected").text();
      if(age1==age2 || age2<age1){
          $('#AgeError').removeAttr('hidden',true)
          $('#age_to option').prop('selected', function() {
          return this.defaultSelected;
      });
      }
       else{
          $('#AgeError').attr('hidden',true)
      }
   }
    $("#age_to").on("change", checkage);

    var paymentsData =$('#payments_list_table').DataTable({
      "Processing":true,
      "serverSide":true,
      "ajax":{
            url : "{{url('payments/cleared/data-list')}}",
            method :"POST",
            headers: {
	          'X-CSRF-TOKEN': $('#token').val()
	      },
        data : function ( d ) {
              return $.extend( {}, d, {
                "id" : $('#member_id').val(),
                "from" : $('#from').val(),
                "to"   : $('#to').val(),
                "expired_id"   : $('#expired_id').val(),
                "transaction_id" : $('#transaction_id').val(),
              });
          },
    },
    "scrollY"       : "500px",
    "scrollCollapse": true,
    "scrollX"       : true,
    "columns": [
    
    {"data":"transaction_id"},
    {"data":"expired_name"},
    {"data":"expired_date"},
    {"data":"fc_released"},
    {"data":"amount"},
    {"data":"payment_date"},
    {"data":"payment_type"},
    {"data":"mode_of_payment"},
      
    ],
    "aaSorting": [],
    "aLengthMenu": [[10,25, 50, 75,100, -1], 
            [10,25, 50, 75,100, "All"]],
    "columnDefs": [
    ],
    "dom": 'lfrtip',
  });
  $('#filter').click(function(){
    paymentsData.draw();
  });

  $('#reset').click(function(){
    
    $("#from, #to,#expired_id,#transaction_id").val('');
    paymentsData.draw();
  });

});
 function showViewForm(id){
      var totalFC = $('#user_'+id).data("totalfc");
      var totalMembers = $('#user_'+ id).data("totalmembers");
      var fcDue = $('#user_'+ id).data("fcdue");
      var adminExpense = $('#user_'+ id).data("adminexpense");
      var fyAdvance = $('#user_'+ id).data("fyadvance");
      var dueInterest = $('#user_'+ id).data("dueinterest");
      var totalDue = $('#user_'+ id).data("totaldue");
      var duePenalty = $('#user_'+ id).data("duepenalty");
      
      $("#fc_released").text(totalFC);
      $("#fc_members").text(totalMembers);
      $("#fc_due").text(fcDue);
      $("#admin_expense").text(adminExpense);
      $("#fy_advance").text(fyAdvance);
      $("#due_penalty").text(duePenalty);
      $("#interest").text(dueInterest);
      $("#total_due").text(totalDue);

      $('#viewPayment').modal({
          show: true,
          keyboard: false,
          backdrop: 'static'
      });
  };  
</script>
@endsection

