<!---------------Change Member Status Modal Start----------------------->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle-2" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document" 
        style="margin-top: -5px;">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle-2">Member Status</h5>
              <button class="close Cancel" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
              </div>
                <div class="modal-body">
                  <form id="update_member_status">
                      <input type="hidden" name="id" id="user_id" value="">
                      <input type="hidden" value="{{csrf_token()}}" name="_token" id="token">
                      <div class="form-group row">
                        <div class="form-group col-md-3">
                          <label>Member Status<span style="color: red">*</span></label>
                        </div>
                        <div class="form-group col-md-8">
                            <select class="form-control" name="member_status" id="member_status">
                              <option value="" hidden>Please Select</option>
                              <option value="Active">Active</option>
                              <option value="Probationary">Probationary</option>
                              <option value="Inactive">Inactive</option>  
                              <option value="Expired">Expired</option>  
                          </select>
                        </div>
                      </div>
                      <div id="expired_fields_div" style="display:none">
                        <div class="form-group row">
                          <div class="form-group col-md-3">
                            <label>Expired Date<span style="color: red">*</span></label>
                          </div>
                          <div class="form-group col-md-8">
                             <input type="text" class="form-control" id="confirm_date" name="confirm_date" autocomplete="off" readonly="" style="background-color: white" placeholder="Select Expired Date">
                          </div>
                        </div>

                        <div class="form-group row">
                          <div class="form-group col-md-3">
                            <label>Eligible  FC Amount<span style="color: red">*</span></label>
                          </div>
                          <div class="form-group col-md-8">
                             <input type="text" class="form-control" id="fc_amount" name="fc_amount" autocomplete="off" placeholder="FC Amount">
                          </div>
                        </div>

                        <div class="form-group row mb-3">
                        <div class="form-group col-md-3">
                          <label>Total Pending Due<span style="color: red">*</span></label>
                        </div>
                        <div class="form-group col-md-8">
                           <input type="text" class="form-control" id="pending_due" name="pending_due" readonly="" placeholder="Pending Due">
                        </div>
                      </div>
                      
                      <div class="form-group row mb-3">
                        <div class="form-group col-md-3">
                          <label>Advance Amount paid<span style="color: red">*</span></label>
                        </div>
                        <div class="form-group col-md-8">
                           <input type="text" class="form-control" id="advance_amount" name="advance_amount"  value="{{$advance['advance_amount']}}" readonly="">
                        </div>
                      </div>

                      <div class="form-group row mb-3">
                        <div class="form-group col-md-3">
                          <label>Released FC Amount<span style="color: red">*</span></label>
                        </div>
                        <div class="form-group col-md-8">
                           <input type="text" class="form-control" id="final_fc_amount" name="final_fc_amount" placeholder="Released FC Amount">
                        </div>
                      </div>

                        <div class="form-group row">
                          <div class="form-group col-md-3">
                            <label>Note</label>
                          </div>
                          <div class="form-group col-md-8">
                              <textarea class="form-control" name="fc_note" id="fc_note" rows="5" placeholder="Note" style="resize: none;"></textarea>
                          </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" name="submit" id="submit" class="btn btn-primary btn-rounded">Submit</button> 
                        <button type="button" class="btn btn-primary btn-rounded Cancel" data-dismiss="modal">Close</button> 
                    </div>
                  </form>
                </div>
            </div>
        </div>
    </div>   
  <!---------------Change Member Status Modal End----------------------->