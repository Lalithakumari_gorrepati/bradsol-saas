@extends('layouts.master')
@section('page-css')
<style type="text/css">
    .labelname{
    color: #663399;
    font-weight: bold;
}
.value{
    color: #663399;
     width: 250px 
}
</style>
@endsection
@section('main-content')
<div class="row">
    <div class="col-md-12">
        <div class="card mt-2 mb-4">
        <div class="col-md-12" id="successmsg" style="display: none">
          <div class="alert alert-primary " id="alert-primary">
              
              <button class="close float-right" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"></span></button>
          </div>
        </div>
        
        <div class="card-header bg-transparent">
            <h4 class="p-2">View Member Details<span class="float-right">
                <a class="btn btn-primary btn-rounded" href="{{url()->previous()}}">Back</a></span>
            </h4>
        </div>

        <div class="card-body">
             <nav>
                <div class="row nav nav-tabs" id="nav-tab" role="tablist">
                    <a href="#nav-personal" class="nav-item nav-link active show" id="nav-personal-tab" data-toggle="tab"  role="tab" aria-controls="nav-personal" aria-selected="true">Personal Details</a>

                    <a href="#nav-medical" class="nav-item nav-link"  id="nav-medical-tab" data-toggle="tab"  role="tab" aria-controls="nav-medical" aria-selected="false">Medical details</a>

                    <a href="#nav-nominee" class="nav-item nav-link"  id="nav-nominee-tab" data-toggle="tab" role="tab" aria-controls="nav-nominee" aria-selected="false">Nominee Details</a>

                    <a  href="#nav-payment" class="nav-item nav-link" id="nav-payment-tab" data-toggle="tab"  role="tab" aria-controls="nav-payment" aria-selected="false">Payment Details</a>

                    <a href="#nav-member_access" class="nav-item nav-link"  id="nav-member_access-tab" data-toggle="tab"  role="tab" aria-controls="nav-member_access" aria-selected="false">Member Access</a>

                    <a href="#nav-insurance" class="nav-item nav-link"  id="nav-insurance-tab" data-toggle="tab"  role="tab" aria-controls="nav-insurance" aria-selected="false">Accidental Insurance Tracking</a>

                    <a href="#nav-bill" class="nav-item nav-link"  id="nav-bill-tab" data-toggle="tab"  role="tab" aria-controls="nav-bill" aria-selected="false">Bill Tracking</a>
                </div> 
            </nav>

            <form id="memberDetails"  enctype="multipart/form-data">
                <div class="tab-content ul-tab__content" id="nav-tabContent">
                <!--------------Member Personal details tab start------------->
                    <div class="tab-pane fade active show" id="nav-personal" role="tabpanel" aria-labelledby="nav-personal-tab">
                    <div class="form-group row mt-2">
                        <div class="col-md-6 form-group mb-3">
                            <div class="row">
                                <div class="col-4">
                                    <label for="">
                                        <h6 class="labelname">Member Id</h6>
                                    </label>
                                </div>
                                <div class="col-1">:</div>
                                <div class="col-6" >
                                    <label for=""><h6 class="value">{{$data['user_member_id']}}</h6></label>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                            <div class="row">
                                <div class="col-4">
                                    <label for="">
                                        <h6 class="labelname">Member Name</h6>
                                    </label>
                                </div>
                                <div class="col-1">:</div>
                                <div class="col-6" >
                                    <label for="">
                                        <h6 class="value">{{$data['name']}}</h6>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                                
                        <div class="form-group row">
                            <div class="col-md-6 form-group mb-3">
                                <div class="row">
                                    <div class="col-4">
                                        <label for="">
                                            <h6 class="labelname">Member Mobile Number</h6>
                                        </label>
                                    </div>
                                    <div class="col-1">:</div>
                                    <div class="col-6" >
                                        <label for=""><h6 class="value">{{$data['mobile_number']}}</h6>
                                        </label>
                                    </div>
                                </div>    
                            </div>
                                    
                            <div class="col-md-6 form-group mb-3">
                                <div class="row">
                                    <div class="col-4">
                                        <label for="">
                                            <h6 class="labelname">Alternative Mobile Number</h6>
                                        </label>
                                    </div>
                                    <div class="col-1">:</div>
                                    <div class="col-6" >
                                        <label for=""><h6 class="value">{{$data['alternative_phone']}}</h6>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                            
                        <div class="form-group row">
                            <div class="col-md-6 form-group mb-3">
                                <div class="row">
                                    <div class="col-4">
                                        <label for="">
                                            <h6 class="labelname">Profile Email</h6>
                                        </label>
                                    </div>
                                    <div class="col-1">:</div>
                                    <div class="col-6" >
                                        <label for=""><h6 class="value">{{$data['email']}}</h6>
                                        </label>
                                    </div>
                                </div> 
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <div class="row">
                                    <div class="col-4">
                                        <label for="">
                                            <h6 class="labelname">Alternative Email</h6>
                                        </label>
                                    </div>
                                    <div class="col-1">:</div>
                                    <div class="col-6" >
                                        <label for=""><h6 class="value">{{$data['alternative_email']}}</h6>
                                        </label>
                                    </div>
                                </div> 
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 form-group mb-3">
                            <div class="row">
                                <div class="col-4">
                                    <label for="">
                                        <h6 class="labelname">Marital Status</h6>
                                    </label>
                                </div>
                                <div class="col-1">:</div>
                                <div class="col-6" >
                                    <label for=""><h6 class="value">{{$data['martial_status']}}</h6>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 form-group mb-3" id="gender">
                            <div class="row">
                                <div class="col-4">
                                    <label for="">
                                        <h6 class="labelname">Gender</h6>
                                    </label>
                                </div>
                                <div class="col-1">:</div>
                                <div class="col-6" >
                                    <label for=""><h6 class="value">{{$data['gender']}}</h6>
                                    </label>
                                </div>
                            </div>
                          </div>  
                    </div>
                      
                      <div class="form-group row">
                            <div class="col-md-6 form-group mb-3">
                                <div class="row">
                                    <div class="col-4">
                                        <label for="">
                                            <h6 class="labelname">Country</h6>
                                        </label>
                                    </div>
                                    <div class="col-1">:</div>
                                    <div class="col-6" >
                                        <label for=""><h6 class="value">{{$data['country']}}</h6>
                                        </label>
                                    </div>
                                </div>
                          </div>
                         <div class="col-md-6 form-group mb-3">
                            <div class="row">
                                <div class="col-4">
                                    <label for="">
                                        <h6 class="labelname">State</h6>
                                    </label>
                                </div>
                                <div class="col-1">:</div>
                                <div class="col-6" >
                                    <label for=""><h6 class="value">{{$data['state']}}</h6>
                                    </label>
                                </div>
                            </div>
                          </div>
                        </div>

                          <div class="form-group row">
                            <div class="col-md-6 form-group mb-3" >
                            <div class="row">
                                <div class="col-4">
                                    <label for="">
                                        <h6 class="labelname">City</h6>
                                    </label>
                                </div>
                                <div class="col-1">:</div>
                                <div class="col-6" >
                                    <label for=""><h6 class="value">{{$data['city']}}</h6>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 form-group mb-3" >
                          <div class="row">
                                <div class="col-4">
                                    <label for="">
                                        <h6 class="labelname">DOB</h6>
                                    </label>
                                </div>
                                <div class="col-1">:</div>
                                <div class="col-6" >
                                    <label for=""><h6 class="value">{{$data['dob']}}</h6>
                                    </label>
                                </div>
                            </div>   
                        </div>
                        </div>
                       
                       <div class="form-group row">
                            <div class="col-md-6 form-group mb-3">
                              <div class="row">
                                    <div class="col-4">
                                        <label for="">
                                            <h6 class="labelname">Member Address</h6>
                                        </label>
                                    </div>
                                    <div class="col-1">:</div>
                                    <div class="col-6" >
                                        <label for=""><h6 class="value">{{$data['address']}}</h6>
                                        </label>
                                    </div>
                                </div>
                          </div>

                            <div class="col-md-6 form-group mb-3">
                                <div class="row">
                                    <div class="col-4">
                                        <label for="">
                                            <h6 class="labelname">ID Proof</h6>
                                        </label>
                                    </div>
                                    <div class="col-1">:</div>
                                    <div class="col-6" >
                                        <label for=""><h6 class="value">{{$data['id_proofs']['customer_id_type']}}</h6>
                                        </label>
                                    </div>
                                    <input type="hidden" disabled class="form-control" id="customer_id_type" name="customer_id_type" placeholder="" value="{{$data['id_proofs']['customer_id_type']}}">
                                </div>
                          </div>
                        </div>  
                            <div class="form-group row">
                              <div class="col-md-6 form-group mb-3" >
                                 <div class="row">
                                    <div class="col-4">
                                        <label for="">
                                            <h6 class="labelname">Id Proof File Front</h6>
                                        </label>
                                    </div>
                                    <div class="col-1">:</div>
                                    <div class="col-6" >
                                        @if(!empty($data['id_proofs']['customer_id_front']))
                                        @if (Str::endsWith($data['id_proofs']['customer_id_front'],'pdf'))
                                        <a href="{{asset('uploads')}}/{{$data['id_proofs']['customer_id_front']}}"  target = '_blank'>{{$data['id_proofs']['customer_id_front']}}</a>
                                        @else 
                                        <a href="{{asset('uploads')}}/{{$data['id_proofs']['customer_id_front']}}"  target = '_blank'>
                                        <img src="{{asset('uploads')}}/{{$data['id_proofs']['customer_id_front']}}" style="height:150px; width:150px;"></a>
                                        @endif
                                        @else 
                                            <h4>No Image found...</h4>
                                        @endif
                                    </div>
                                </div>
                              </div>
                              <div class="col-md-6 form-group mb-3" >
                                <div class="row">
                                    <div class="col-4">
                                        <label for="">
                                            <h6 class="labelname">Id Proof File Back</h6>
                                        </label>
                                    </div>
                                    <div class="col-1">:</div>
                                    <div class="col-6">
                                        @if(!empty($data['id_proofs']['customer_id_back']))
                                        @if (Str::endsWith($data['id_proofs']['customer_id_back'],'pdf'))
                                        <a href="{{asset('uploads')}}/{{$data['id_proofs']['customer_id_back']}}"  target = '_blank'>{{$data['id_proofs']['customer_id_back']}}</a>
                                        @else 
                                        <a href="{{asset('uploads')}}/{{$data['id_proofs']['customer_id_back']}}"  target = '_blank'><img src="{{asset('uploads')}}/{{$data['id_proofs']['customer_id_back']}}" style="height:150px; width:150px;"></a>
                                        @endif
                                        @else 
                                            <h4>No Image found...</h4>
                                        @endif
                                    </div>
                                </div>
                              </div>
                       </div>

                       <div class="form-group row">
                            <div class="col-md-6 form-group mb-3">
                                <div class="row">
                                    <div class="col-4">
                                        <label for="">
                                            <h6 class="labelname">Age Proof</h6>
                                        </label>
                                    </div>
                                    <div class="col-1">:</div>
                                    <div class="col-6" >
                                        @if(!empty($data->age_proof))
                                            @if (Str::endsWith($data->age_proof,'pdf'))
                                            <a href="{{asset('uploads')}}/{{$data->age_proof}}"  target = '_blank'>{{$data->age_proof}}</a>
                                            @else
                                            <a href="{{asset('uploads')}}/{{$data->age_proof}}"  target = '_blank'><img src="{{asset('uploads')}}/{{$data->age_proof}}" style="height:150px; width:150px;"></a>
                                        @endif
                                        @else 
                                            <h4>No Image found...</h4>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 form-group mb-1" >
                            <div class="row">
                                <div class="col-4">
                                    <label for="">
                                        <h6 class="labelname">Scanned Application</h6>
                                    </label>
                                </div>
                                <div class="col-1">:</div>
                                <div class="col-6">
                                    <label for=""><h6 class="value"></h6>
                                        @if(!empty($data['application']))
                                        @foreach($data['application'] as $key=>$value)
                                        <a href="{{asset('uploads')}}/{{$value}}"  target = '_blank'>{{$value}}</a><br>
                                        @endforeach
                                        @else 
                                            <h4>No Image found...</h4>
                                        @endif   
                                    </label>
                                </div>
                            </div>
                          </div>
                       </div> 
                       <div class="form-group row">
                            <div class="col-sm-12 d-flex justify-content-center">
                            <a class="txt-color btn btn-primary btn-rounded m-1" id="changeTabMedical" style="float:right; color: white">Next</a>
                            </div>
                        </div> 
                    </div>
            <!--------------Member Personal details tab end------------->
            
            <!--------------Member Medical details tab start------------->      
                <div class="tab-pane fade" id="nav-medical" role="tabpanel" aria-labelledby="nav-medical-tab">
                    <div class="form-group row">  
                        <div class="col-md-6 form-group mb-3">
                            <div class="row">
                                <div class="col-4">
                                    <label for="">
                                        <h6 class="labelname">Physical Disability</h6>
                                    </label>
                                </div>
                                <div class="col-1">:</div>
                                <div class="col-6" >
                                    <label for=""><h6 class="value">{{$data['physical_disability']}}</h6>
                                    </label>
                                </div>
                                <input type="hidden" disabled class="form-control" id="physical_disability" name="physical_disability" placeholder="" value="{{$data['physical_disability']}}">
                            </div>
                        </div>
                        <div class="col-md-6 form-group mb-3" id="physical_disability_type_div">
                            <div class="row">
                                <div class="col-4">
                                    <label for="">
                                        <h6 class="labelname">Physical Disability Type</h6>
                                    </label>
                                </div>
                                <div class="col-1">:</div>
                                <div class="col-6" >
                                    <label for=""><h6 class="value">{{$data['physical_disability_type']}}</h6>
                                    </label>
                                </div>
                            </div>
                        </div>
                     
                        <div class="col-md-6 form-group mb-3">
                            <div class="row">
                                <div class="col-4">
                                    <label for="">
                                        <h6 class="labelname">History Of Previous Illness</h6>
                                    </label>
                                </div>
                                <div class="col-1">:</div>
                                <div class="col-6" >
                                    <label for=""><h6 class="value">{{$data['previous_illness']}}</h6>
                                    </label>
                                </div>
                                <input type="hidden" disabled class="form-control" id="previous_illness" name="previous_illness" placeholder="" value="{{$data['previous_illness']}}">
                            </div>
                        </div>

                        <div class="col-md-6 form-group mb-3" id="Illness_type_div">
                            <div class="row">
                                <div class="col-4">
                                    <label for="">
                                        <h6 class="labelname">Illness Type</h6>
                                    </label>
                                </div>
                                <div class="col-1">:</div>
                                <div class="col-6" >
                                    <label for=""><h6 class="value">{{$data['illness_type']}}</h6>
                                    </label>
                                </div>
                            </div>
                        </div>
                     </div>

                     <div class="form-group row">  
                        <div class="col-md-6 form-group mb-3" >
                            <div class="row">
                                <div class="col-4">
                                    <label for="">
                                        <h6 class="labelname">Uploaded Reports</h6>
                                    </label>
                                </div>
                                <div class="col-1">:</div>
                                <div class="col-6" >
                                    <label for=""><h6 class="value"></h6>
                                        @if(!empty($data['reports']))
                                        @foreach($data['reports'] as $key=>$value)
                                        <a href="{{asset('uploads')}}/{{$value}}"  target = '_blank'>{{$value}}</a><br><br>
                                        @endforeach
                                        @else 
                                            <h4>No Image found...</h4>
                                        @endif
                                    </label>
                                </div>
                            </div>
                          </div>
                        
                        <div class="col-md-6 form-group mb-3">
                            <div class="row">
                                <div class="col-4">
                                    <label for="">
                                        <h6 class="labelname">Final Health Declaration</h6>
                                    </label>
                                </div>
                                <div class="col-1">:</div>
                                <div class="col-6" >
                                    <label for=""><h6 class="value">{{$data['declaration']}}</h6>

                                    </label>
                                </div>
                            </div>
                        </div>
                     </div>

                    <div class="form-group row">
                        <div class="col-sm-12 d-flex justify-content-center">
                            <a class="txt-color btn btn-primary btn-rounded m-1" id="previousTab1" style="float:right;color: white">Previous</a>
                            <a class="txt-color btn btn-primary btn-rounded m-1" id="changeTabNominee" style="float:right; color: white">Next</a>
                        </div>
                    </div>
                </div>
            <!--------------Member Medical details tab end------------->

            <!--------------Member Nominee details tab start------------->      
                <div class="tab-pane fade" id="nav-nominee" role="tabpanel" aria-labelledby="nav-nominee-tab">
                    @foreach ($data['nominees'] as $value)
                     
                    <div class="row mt-2">
                        <div class="col-md-6 form-group mb-3">
                            <div class="row">
                                <div class="col-4">
                                    <label for="">
                                        <h6 class="labelname">Nominee Name</h6>
                                    </label>
                                </div>
                                <div class="col-1">:</div>
                                <div class="col-6" >
                                    <label for=""><h6 class="value">{{$value['nominee_name']}}</h6>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                            <div class="row">
                                <div class="col-4">
                                    <label for="">
                                        <h6 class="labelname">FC Amount Percentage</h6>
                                    </label>
                                </div>
                                <div class="col-1">:</div>
                                <div class="col-6" >
                                    <label for=""><h6 class="value">{{$value['fc_percentage']}}</h6>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-2">
                       <div class="col-md-6 form-group mb-3">
                            <div class="row">
                                <div class="col-4">
                                    <label for="">
                                        <h6 class="labelname">Nominee Phone</h6>
                                    </label>
                                </div>
                                <div class="col-1">:</div>
                                <div class="col-6" >
                                    <label for=""><h6 class="value">{{$value['nominee_phone']}}</h6>
                                    </label>
                                </div>
                            </div>
                       </div>
                    
                        <div class="col-md-6 form-group mb-3">
                            <div class="row">
                                <div class="col-4">
                                    <label for="">
                                        <h6 class="labelname">Nominee Email</h6>
                                    </label>
                                </div>
                                <div class="col-1">:</div>
                                <div class="col-6" >
                                    <label for=""><h6 class="value">{{$value['nominee_email']}}</h6>
                                    </label>
                                </div>
                            </div>
                       </div>
                    </div>

                    <div class="row">   
                       <div class="col-md-6 form-group mb-3">
                            <div class="row">
                                <div class="col-4">
                                    <label for="">
                                        <h6 class="labelname">Nominee Gender</h6>
                                    </label>
                                </div>
                                <div class="col-1">:</div>
                                <div class="col-6" >
                                    <label for=""><h6 class="value">{{$value['nominee_gender']}}</h6>
                                    </label>
                                </div>
                            </div>
                       </div>
                    
                        <div class="col-md-6 form-group mb-3">
                            <div class="row">
                                <div class="col-4">
                                    <label for="">
                                        <h6 class="labelname">Nominee DOB</h6>
                                    </label>
                                </div>
                                <div class="col-1">:</div>
                                <div class="col-6" >
                                    <label for=""><h6 class="value">{{$value['nominee_dob']}}</h6>
                                    </label>
                                </div>
                            </div>
                       </div>
                    </div>

                    <div class="row">
                       <div class="col-md-6 form-group mb-3">
                            <div class="row">
                                <div class="col-4">
                                    <label for="">
                                        <h6 class="labelname">Nominee Relation</h6>
                                    </label>
                                </div>
                                <div class="col-1">:</div>
                                <div class="col-6" >
                                    <label for=""><h6 class="value">{{$value['nominee_relationship']}}</h6>
                                    </label>
                                </div>
                            </div>
                       </div>
                    
                        <div class="col-md-6 form-group mb-3">
                            <div class="row">
                                <div class="col-4">
                                    <label for="">
                                        <h6 class="labelname">Nominee Address</h6>
                                    </label>
                                </div>
                                <div class="col-1">:</div>
                                <div class="col-6" >
                                    <label for=""><h6 class="value">{{$value['nominee_address']}}</h6>
                                    </label>
                                </div>
                            </div>
                       </div>
                    </div>

                    <div class="row">
                       <div class="col-md-6 form-group mb-3">
                            <div class="row">
                                <div class="col-4">
                                    <label for="">
                                        <h6 class="labelname">Nominee Photo</h6>
                                    </label>
                                </div>
                                <div class="col-1">:</div>
                                <div class="col-6" >
                                    @if(!empty($value->nominee_image))
                                    <a href="{{asset('uploads')}}/{{$value->nominee_image}}"  target = '_blank'><img src="{{asset('uploads')}}/{{$value->nominee_image}}" style="height:150px; width:150px;"></a>
                                       
                                    @else 
                                      <h4>No Image found...</h4>
                                    @endif
                                    
                                </div>
                            </div>
                        </div>   
                    </div> 
                    <hr>
                    @endforeach  
                        <div class="form-group row">
                            <div class="col-sm-12 d-flex justify-content-center">
                                <a class="txt-color btn btn-primary btn-rounded m-1" id="previousTab2" style="float:right;color: white">Previous</a>
                                <a class="txt-color btn btn-primary btn-rounded m-1" id="changeTabPayment" style="float:right; color: white">Next</a>
                            </div>
                        </div>
                </div>
            <!--------------Member Nominee details tab end------------->

            <!--------------Member Payment details tab start------------->      
                <div class="tab-pane fade" id="nav-payment" role="tabpanel" aria-labelledby="nav-payment-tab">
                    <div class="row mt-2">
                        <div class="col-md-6 form-group mb-3">
                            <div class="row">
                                <div class="col-4">
                                    <label for="">
                                        <h6 class="labelname">Member Age</h6>
                                    </label>
                                </div>
                                <div class="col-1">:</div>
                                <div class="col-6" >
                                    <label for=""><h6 class="value">{{$data['member_age']}}</h6>
                                    </label>
                                </div>
                            </div>
                       </div>

                       <div class="col-md-6 form-group mb-3">
                            <div class="row">
                                <div class="col-4">
                                    <label for="">
                                        <h6 class="labelname">Corpus Amount</h6>
                                    </label>
                                </div>
                                <div class="col-1">:</div>
                                <div class="col-6" >
                                    <label for=""><h6 class="value">{{$data['corpus_amount']}}</h6>
                                    </label>
                                </div>
                            </div>
                       </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 form-group mb-3">
                            <div class="row">
                                <div class="col-4">
                                    <label for="">
                                        <h6 class="labelname">Payment Mode</h6>
                                    </label>
                                </div>
                                <div class="col-1">:</div>
                                <div class="col-6" >
                                    <label for=""><h6 class="value">{{$data['mode_of_payment']}}</h6>
                                    </label>
                                </div>
                            </div>
                       </div>

                       <div class="col-md-6 form-group mb-3">
                            <div class="row">
                                <div class="col-4">
                                    <label for="">
                                        <h6 class="labelname">Payment Date</h6>
                                    </label>
                                </div>
                                <div class="col-1">:</div>
                                <div class="col-6" >
                                    <label for=""><h6 class="value">{{$data['payment_date']}}</h6>
                                    </label>
                                </div>
                            </div>
                       </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 form-group mb-3">
                            <div class="row">
                                <div class="col-4">
                                    <label for="">
                                        <h6 class="labelname">Payment Note</h6>
                                    </label>
                                </div>
                                <div class="col-1">:</div>
                                <div class="col-6" >
                                    <label for=""><h6 class="value">{{$data['remarks']}}</h6>
                                    </label>
                                </div>
                            </div>
                       </div>

                       <div class="col-md-6 form-group mb-3">
                            <div class="row">
                                <div class="col-4">
                                    <label for="">
                                        <h6 class="labelname">Is Member Intersted in Other schemes ?</h6>
                                    </label>
                                </div>
                                <div class="col-1">:</div>
                                <div class="col-6" >
                                    <label for=""><h6 class="value">{{$data['interested_in']}}</h6>
                                    </label>
                                </div>
                            </div>
                       </div>
                    </div><hr>

                    <div class="row mt-2">
                        <div class="col-md-6 form-group mb-3">
                            <div class="row">
                                <div class="col-4">
                                    <label for="">
                                        <h6 class="labelname">Advance Amount</h6>
                                    </label>
                                </div>
                                <div class="col-1">:</div>
                                <div class="col-6" >
                                    <label for=""><h6 class="value">{{$data['advance_payment']['advance_amount']}}</h6>
                                    </label>
                                </div>
                            </div>
                       </div>

                       <div class="col-md-6 form-group mb-3">
                            <div class="row">
                                <div class="col-4">
                                    <label for="">
                                        <h6 class="labelname">Advance Payment Mode</h6>
                                    </label>
                                </div>
                                <div class="col-1">:</div>
                                <div class="col-6" >
                                    <label for=""><h6 class="value">{{$data['advance_payment']['advance_mode_of_payment']}}</h6>
                                    </label>
                                </div>
                            </div>
                       </div>

                       <div class="col-md-6 form-group mb-3">
                            <div class="row">
                                <div class="col-4">
                                    <label for="">
                                        <h6 class="labelname">Advance Payment Date</h6>
                                    </label>
                                </div>
                                <div class="col-1">:</div>
                                <div class="col-6" >
                                    <label for=""><h6 class="value">{{$data['advance_payment']['advance_payment_date']}}</h6>
                                    </label>
                                </div>
                            </div>
                       </div>
                    </div>


                        <div class="form-group row">
                            <div class="col-sm-12 d-flex justify-content-center">
                                <a class="txt-color btn btn-primary btn-rounded m-1" id="previousTab3" style="float:right;color: white">Previous</a>
                                <a class="txt-color btn btn-primary btn-rounded m-1" id="changeTabMemberAcess" style="float:right; color: white">Next</a>
                            </div>
                        </div>
                </div>
            <!--------------Member Payment details tab end------------->

            <!--------------Member Access tab start------------->   
                <div class="tab-pane fade" id="nav-member_access" role="tabpanel" aria-labelledby="nav-member_access-tab">
                <div class="row">    
                    <div class="col-md-6 form-group mb-3">
                        <div class="row">
                            <div class="col-4">
                              <label for="">
                                  <h6 class="labelname">Membership Date</h6>
                              </label>
                            </div>
                            <div class="col-1">:</div>
                            <div class="col-6" >
                              <label for=""><h6 class="value">{{$data['join_date']}}</h6>
                              </label>
                            </div>
                        </div>    
                    </div>    

                    <div class="col-md-6 form-group mb-3">
                        <div class="row">
                          <div class="col-4">
                              <label for="">
                                  <h6 class="labelname">Member Status</h6>
                              </label>
                          </div>
                          <div class="col-1">:</div>
                          <div class="col-6" >
                              <label for=""><h6 class="value">{{$data['member_status']}}</h6>
                              </label>
                          </div>
                       </div>
                   </div>
                </div>

                <div class="row">
                    <div class="col-md-6 form-group mb-3">
                         <div class="row">
                           <div class="col-4">
                               <label for="">
                                   <h6 class="labelname">Additional Information</h6>
                               </label>
                           </div>
                           <div class="col-1">:</div>
                           <div class="col-6" >
                               <label for=""><h6 class="value">{{$data['additional_info']}}</h6>
                               </label>
                           </div>
                        </div>
                       </span>
                   </div>

                   <div class="col-md-6 form-group mb-3">
                        <div class="row">
                          <div class="col-4">
                              <label for="">
                                  <h6 class="labelname">Profile Photo</h6>
                              </label>
                          </div>
                          <div class="col-1">:</div>
                          <div class="col-6">
                            @if(!empty($data->image))
                            <a href="{{asset('uploads')}}/{{$data->image}}"  target = '_blank'>
                               <img src="{{asset('uploads')}}/{{$data->image}}" style="height:150px; width:150px;"></a>
                            @else 
                              <h4>No Image found...</h4>
                            @endif
                          </div>
                        </div>
                    </div>
                </div>
                    <div class="form-group row">
                        <div class="col-sm-12 d-flex justify-content-center">
                            <a class="txt-color btn btn-primary btn-rounded m-1" id="previousTab4" style="float:right;color: white">Previous</a>
                            <a class="txt-color btn btn-primary btn-rounded m-1" id="changeTabInsurance" style="float:right; color: white">Next</a>
                        </div>
                    </div>
                </div>
          <!---------------------Member Access tab end-----------------> 

          <!--------------Member Insurance tab start------------->   
                <div class="tab-pane fade" id="nav-insurance" role="tabpanel" aria-labelledby="nav-insurance-tab">
                    <input type="hidden" id="user_id" name="id" value="{{ $data['user_id'] }}" >
                        <div class="table-responsive mt-4">
                            <table id="insurance_list"  class="table table-bordered ">
                                <thead>
                                    <tr>
                                        <th scope="col">Year</th>
                                        <th scope="col">Payment Status</th>
                                        <th scope="col">Payment Date</th>
                                        <th scope="col">Updated By</th>
                                    </tr>
                                </thead>
                               
                            </table>
                        </div>  

                
                    <div class="form-group row">
                        <div class="col-sm-12 d-flex justify-content-center">
                            <a class="txt-color btn btn-primary btn-rounded m-1" id="previousTab5" style="float:right;color: white">Previous</a>
                            <button type="button" class="btn btn-primary btn-rounded m-1" id="changeTabBill">Next</button> 
                            
                        </div>
                    </div>
                </div>
          <!---------------------Member Insurance tab end----------------->

          <!--------------Member Bill tab start---------------------->   
                <div class="tab-pane fade" id="nav-bill" role="tabpanel" aria-labelledby="nav-bill-tab">
                    <input type="hidden" id="user_id" name="id" value="{{ $data['user_id'] }}" >
                        <div class="table-responsive mt-4">
                            <table id="bill_list"  class="table table-bordered ">
                                <thead>
                                    <tr>
                                        <th scope="col">No of FC's</th>
                                        <th scope="col">Total Due</th>
                                        <th scope="col">Bill Date</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                               
                            </table>
                        </div>  

                
                    <div class="form-group row">
                        <div class="col-sm-12 d-flex justify-content-center">
                            <a class="txt-color btn btn-primary btn-rounded m-1" id="previousTab6" style="float:right;color: white">Previous</a>
                        </div>
                    </div>
                </div>
          <!---------------------Member Bill tab end-----------------------> 
        </div> 
       </div>       
    </div>      
</div>      
@endsection
@section('page-js')
<script>
$(document).ready(function(){
 
 var physical_disability=$('#physical_disability').val();
 var previous_illness=$('#previous_illness').val();
 var customer_id_type=$('#customer_id_type').val();
 
  $('#customer_id_proofs').hide();
  $('#Illness_type_div').hide();
  $('#physical_disability_type_div').hide();

      if(physical_disability == 'Yes'){
        $('#physical_disability_type_div').show();
        $('#physical_disability_type').attr('required',true);
      }else{
        $('#physical_disability_type_div').hide();
        $('#physical_disability_type').val('').attr('required',false);;
      }

      if(customer_id_type != ''){
        $('#customer_id_proofs').show();
        $('#customer_id_front').val("");
        $('#customer_id_back').val("");
      }else{
        $('#customer_id_proofs').hide();
      }
      
  if(previous_illness == 'Yes'){
    $('#Illness_type_div').show();
  }else{
    $('#Illness_type_div').hide();
  }

$('#changeTabMedical').on('click', function() {
    
    $('#nav-tab a[href="#nav-medical"]').tab('show');
});

//code for previous tab
$('#previousTab1').on('click', function() {
   $('#nav-tab a[href="#nav-personal"]').tab('show');
    
});

$('#changeTabNominee').on('click', function() {

   $('#nav-tab a[href="#nav-nominee"]').tab('show');
});

$('#previousTab2').on('click', function() {
  $('#nav-tab a[href="#nav-medical"]').tab('show');
});


$('#changeTabPayment').click(function(e){
    $('#nav-tab a[href="#nav-payment"]').tab('show');
});

$('#previousTab3').on('click', function() {
    $('#nav-tab a[href="#nav-nominee"]').tab('show');
});

$('#changeTabMemberAcess').click(function(e){
      $('#nav-tab a[href="#nav-member_access"]').tab('show');
});

$('#previousTab4').on('click', function() {
    $('#nav-tab a[href="#nav-payment"]').tab('show');
});

$('#changeTabInsurance').on('click', function() {
    $('#nav-tab a[href="#nav-insurance"]').tab('show');
});

$('#changeTabBill').on('click', function() {
    $('#nav-tab a[href="#nav-bill"]').tab('show');
});

$('#previousTab5').on('click', function() {
    $('#nav-tab a[href="#nav-member_access"]').tab('show');
});

$('#previousTab6').on('click', function() {
    $('#nav-tab a[href="#nav-insurance"]').tab('show');
});
});
var user_id=$('#user_id').val();
insuranceList =  $('#insurance_list').DataTable({
            "Processing":true,
            "serverSide":true,
                  
             "ajax":{
                     url : "{{url('insurance/member/data-list')}}",
                     type  : "POST",  
                      data : function ( d ) {
                          return $.extend( {}, d, {
                               "user_id" : user_id,
                               "_token":$('#token').val(),
                            });
                        },
              },
                "autoWidth": false,
          "columns": [    
            {"data":  "year"},
            {"data":  "payment_status"},
            {"data":  "payment_date"}, 
            {"data":  "updated_by"}, 
        ],  
        "aaSorting": [],
      "aoColumnDefs": [
        {
           bSortable: false,
           // aTargets: [ -1 ]
        },
      ],
        "aLengthMenu": [[10,25, 50, 75,100, -1], 
            [10,25, 50, 75,100, "All"]],
    dom: 'lfrtip',
    });

billList =  $('#bill_list').DataTable({
            "Processing":true,
            "serverSide":true,
                  
             "ajax":{
                     url : "{{url('member/invoice/data-list')}}",
                     type  : "POST",  
                      data : function ( d ) {
                          return $.extend( {}, d, {
                               "user_id" : user_id,
                               "_token":$('#token').val(),
                            });
                        },
              },
                "autoWidth": false,
          "columns": [    
            {"data":  "total_fc"},
            {"data":  "total_due"},
            {"data":  "bill_date"}, 
            {"data":  "action"}, 
        ],  
        "aaSorting": [],
      "aoColumnDefs": [
        {
           bSortable: false,
           aTargets: [ -1 ]
        },
      ],
        "aLengthMenu": [[10,25, 50, 75,100, -1], 
            [10,25, 50, 75,100, "All"]],
    dom: 'lfrtip',
    });

</script>
@endsection