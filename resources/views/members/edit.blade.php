@extends('layouts.master')
@section('before-css')
<style>
.pip {
  display: inline-block;
  margin: 10px 10px 0 0;
}
.remove {
  display: block;
 color: black;
  text-align: center;
  cursor: pointer;
  color: red;
}
.remove:hover {
  background: white;
  color: black;
}

</style>
@endsection
@section('main-content')
<div class="row">
    <div class="col-md-12" id="successmsg" style="display: none">
      <div class="alert alert-primary " id="alert-primary">
          
          <button class="close float-right" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"></span></button>
      </div>
    </div>
    <div class="col-md-12">
        <div class="card mt-2 mb-4">
        
        <div class="card-header bg-transparent">
            <h4 class="p-2">Edit Member Details<span class="float-right">
                <a class="btn btn-primary btn-rounded" href="{{url()->previous()}}">Cancel</a></span>
            </h4>
        </div>

        <div class="card-body">
             <nav>
                <div class="row nav nav-tabs" id="nav-tab" role="tablist">
                    <a href="#nav-personal" class="nav-item nav-link active show" id="nav-personal-tab" data-toggle="tab"  role="tab" aria-controls="nav-personal" aria-selected="true">Personal Details</a>

                    <a href="#nav-medical" class="nav-item nav-link"  id="nav-medical-tab" data-toggle="tab"  role="tab" aria-controls="nav-medical" aria-selected="false">Medical details</a>

                    <a href="#nav-nominee" class="nav-item nav-link"  id="nav-nominee-tab" data-toggle="tab" role="tab" aria-controls="nav-nominee" aria-selected="false">Nominee Details</a>

                    <a  href="#nav-payment" class="nav-item nav-link" id="nav-payment-tab" data-toggle="tab"  role="tab" aria-controls="nav-payment" aria-selected="false">Payment Details</a>

                    <a href="#nav-member_access" class="nav-item nav-link"  id="nav-member_access-tab" data-toggle="tab"  role="tab" aria-controls="nav-member_access" aria-selected="false">Member Access</a>
                </div> 
            </nav>

            
                <div class="tab-content ul-tab__content" id="nav-tabContent">
                <!--------------Member Personal details tab start------------->     
                    <div class="tab-pane fade active show" id="nav-personal" role="tabpanel" aria-labelledby="nav-personal-tab">
                      <input type="hidden" id="age_from" value="{{$setting['age_from']}}">
                      <input type="hidden" id="age_to" value="{{$setting['age_to']}}">
                      <form id="personalDetails"  enctype="multipart/form-data">
                      <input type="hidden" value="{{csrf_token()}}" name="_token" id="token">
                      <input type="hidden" id="id" name="id" value="{{ $data['user_id'] }}" >
                      <input type="hidden" id="tabName_personal" name="tabName" value="personalTab">
                        <div class="form-group row">
                            <div class="col-md-6 form-group mb-3">
                                <label for="name">Member Id<span style="color: red">*</span></label>
                                <input type="text" value="{{$data['user_member_id']}}" class="form-control" id="member_id" name="member_id" placeholder="Enter Member Id" maxlength="10" readonly="">
                                <span class="error text-danger err-name" id="member_id_error"></span>
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="name">Member Name<span style="color: red">*</span></label>
                                <input type="text" value="{{$data['name']}}" class="form-control" id="name" name="name" placeholder="Enter Member Name" maxlength="101" autocomplete="nope">
                                <span class="error text-danger" id="name_error"></span>
                            </div>
                        </div>
                                
                        <div class="form-group row">
                              <div class="col-md-6 form-group mb-3">
                                <label for="mobile_number" class="">Member Mobile Number<span style="color: red">*</span></label>
                                    <input type="text" value="{{$data['mobile_number']}}" class="form-control was-validated" id="mobile_number" name="mobile_number" placeholder="Enter Member Mobile Number" maxlength="10">
                                <span class="error text-danger err-phone" id="mobile_number_error" style="display: none">Mobile Number already exists</span>  
                            </div>
                                    
                          <div class="col-md-6 form-group mb-3">
                              <label for="alternative_number" class="">Alternative Mobile Number</label>
                                <input type="text" value="{{$data['alternative_phone']}}" class="form-control was-validated" id="alternative_number" name="alternative_number" placeholder="Enter Alternative Mobile Number" maxlength="10"> 

                               <span class="error text-danger err-phone" id="alternative_number_error"></span>
                                          </div>
                          </div>
                            
                            <div class="form-group row">
                      <div class="col-md-6 form-group mb-3">
                        <label for="inputEmail" class="">Profile Email<span style="color: red">*</span></label>
                          <input type="email" class="form-control" id="email" name="email" placeholder="Enter Member Email"  value="{{$data['email']}}" autocomplete="nope">
                          <span class="error text-danger" id="email_error" style="display: none">Email already exists</span>
                      </div>

                    <div class="col-md-6 form-group mb-3">
                        <label for="alternative_email" class="">Alternative Email</label>
                            <input type="email" value="{{$data['alternative_email']}}" class="form-control" id="alternative_email" name="alternative_email" placeholder="Enter Alternative Email" autocomplete="nope">
                           <span class="error text-danger" id="alternative_email_error"></span>
                      </div>
                  </div>

                  <div class="form-group row">
                      <div class="col-md-6 form-group mb-3">
                          <label for="martialStatus" class="">Marital Status<span style="color: red">*</span></label>
                            <select class="form-control" name="marital_status" id="marital_status">
                              <option value="" hidden="">Please Select</option>
                              <option value="Married" <?= (!empty($data['martial_status']) && $data['martial_status'] == 'Married' ? 'selected':'')?>>Married</option>
                                <option value="Single" <?= (!empty($data['martial_status']) && $data['martial_status'] == 'Single' ? 'selected':'')?>>Single</option>
                            </select>
                            <span class="error text-danger" id="marital_status_error"></span>
                        </div>

                        <div class="col-md-6 form-group mb-3" id="gender">
                            <label for="gender" class="">Gender<span style="color: red">*</span></label>
                              <select class="form-control " name="gender" id="genderfield">
                                <option value="" hidden>Please Select</option>
                                <option value="Male" <?= (!empty($data['gender']) && $data['gender'] == 'Male' ? 'selected':'')?>>Male</option>
                                <option value="Female" <?= (!empty($data['gender']) && $data['gender'] == 'Female' ? 'selected':'')?>>Female</option>
                              </select>
                              <span class="error text-danger" id="gender_error"></span>
                          </div>  
                    </div>
                      
                      <div class="form-group row">

                        <div class="col-md-6 form-group mb-3" >
                          <label for="country" class="">Country<span style="color: red">*</span></label>
                              <select class="selectbox2 form-control" name="country" id="country_id">
                              <option value="" hidden="">Please Select</option>
                                @if(!empty($data['dropdowns']['country_data']))
                                  @foreach($data['dropdowns']['country_data'] as $row)
                                  <option value="{{ $row->name }}" data-id="{{ $row->id }}">{{ $row->name }}</option>
                                  @endforeach
                                @endif
                              </select>
                          </div>
                         <div class="col-md-6 form-group mb-3" >
                            <label for="state" class="">State
                            <span class="location_state_city" style="color: red">*</span>
                            </label>
                              <select class="selectbox2 form-control" name="state" id="state_id">
                                  
                              </select>
                              <span class="error text-danger" id="state_error"></span>
                          </div>
                            </div>

                          <div class="form-group row">
                            <div class="col-md-6 form-group mb-3" >
                          <label for="city" class="">City<span class="location_state_city" style="color: red">*</span></label>
                          <select class="selectbox2 form-control" name="city" id="city_id">
                            <option value="">Please Select</option>
                          </select>
                          <span class="error text-danger" id="city_error"></span>
                        </div>

                        <div class="col-md-6 form-group mb-3" >
                          <label for="dob" class="">DOB<span style="color: red">*</span></label>
                          <input type="text" value="{{$data['dob']}}" class="form-control dob" id="member_dob"  name="dob" placeholder="Select Member DOB" autocomplete="nope" readonly="" style="background-color: white"> 
                          <span class="error text-danger" id="dob_error"></span>   
                        </div>
                            </div>
                       
                       <div class="form-group row">
                            <div class="col-md-6 form-group mb-3">
                            <label for="customer_id_type" class="">ID Proof<span style="color: red">*</span></label>
                              <select class="form-control" name="customer_id_type" id="customer_id_type">
                              <option value="" hidden>Please Select</option>
                              @if(!empty($data['dropdowns']['customer_id_proofs']))
                                  @foreach($data['dropdowns']['customer_id_proofs'] as $row)
                                  <option value="{{ $row['dropdown_value'] }}" <?= (!empty($data['id_proofs']['customer_id_type']) && $row['dropdown_value'] == $data['id_proofs']['customer_id_type'] ? 'selected':'')?>>{{  $row['dropdown_value']  }}</option>
                                  @endforeach
                              @endif
                            </select>
                            <span class="error text-danger" id="customer_id_type_error"></span>
                          </div>

                            <div id="customer_id_proofs" class="row col-md-6">
                              <div class="col-md-6 form-group mb-3" >
                                <label for="customer_id_front" class="">Id Proof File Front</label>
                                <input type="hidden" name="customer_id_front_changed" id="customer_id_front_changed">
                                <input class="customer_id_images" type="file" id="customer_id_front" name="customer_id_front" accept="image/x-png,image/png,image/gif,image/jpeg,application/pdf">
                                 
                                 <label class="text-danger" id="customer_id_front_error" style="display: none;"></label>
                                  @if(!empty($data['id_proofs']['customer_id_front']))
                                      <input type="hidden" name="customer_front_old" id="customer_front_old" value="{{$data['id_proofs']['customer_id_front']}}">
                                      <span id= "customer_id_front_image" ><a href="{{asset('uploads')}}/{{$data['id_proofs']['customer_id_front'] }}"  target = '_blank'>{{$data['id_proofs']['customer_id_front']}}</a>
                                      <span class="remove_customer_images" data-name="customer_id_front_changed" data-image="customer_id_front_image">
                                      <i class='fa fa-times'></i>
                                      </span>
                                  </span>
                                  @endif
                              </div>
                              <div class="col-md-6 form-group mb-3" >
                                <label for="customer_id_back" class="">Id Proof File Back</label>
                                <input type="hidden" name="customer_id_back_changed" id="customer_id_back_changed">
                                  <input class="customer_id_images"  type="file" id="customer_id_back" name="customer_id_back" accept="image/x-png,image/png,image/gif,image/jpeg,application/pdf">

                                  @if(!empty($data['id_proofs']['customer_id_back']))
                                      <input type="hidden" name="customer_back_old" id="customer_back_old" value="{{$data['id_proofs']['customer_id_back']}}">
                                      <span id= "customer_id_back_image" ><a href="{{asset('uploads')}}/{{$data['id_proofs']['customer_id_back'] }}"  target = '_blank'>{{$data['id_proofs']['customer_id_back']}}</a>
                                      <span class="remove_customer_images" data-name="customer_id_back_changed" data-image="customer_id_back_image">
                                      <i class='fa fa-times'></i>
                                      </span>
                                  </span>
                                  @endif

                                  <label class="text-danger" id="customer_id_back_error" style="display: none;" ></label>
                              </div>
                          </div>
                       </div>

                      <div class="form-group row">
                            <div class="col-md-6 form-group mb-3">
                            <label for="member_address" class="mr-3">Member Address</label>
                              <textarea id="member_address" name="member_address" rows="5" class="form-control">{{$data['address']}}</textarea>
                              <span class="error text-danger" id="member_address_error"></span>
                          </div>

                          <div class="col-md-6 form-group mb-3">
                            <label for="customer_age_proof" class="mr-3">Age Proof</label><br>
                              <input type="hidden" name="age_proof_changed" id="age_proof_changed">
                              <input class="customer_id_images"  type="file" id="age_proof" name="age_proof" accept="image/x-png,image/png,image/gif,image/jpeg,application/pdf"><br>
                                  @if(!empty($data->age_proof))
                                    <input type="hidden" name="age_proof_old" id="age_proof_old" value="{{$data['age_proof']}}">
                                    <span id= "age_proof_image">
                                      <a href="{{asset('uploads')}}/{{$data['age_proof']}}"  target = '_blank'>{{$data['age_proof']}}</a>
                                      <span class="remove_customer_images" data-name="age_proof_changed" data-image="age_proof_image">
                                        <i class='fa fa-times'></i>
                                      </span>
                                    </span>
                                  @endif
                              
                              <span class="error text-danger" id="age_proof_error"></span>
                          </div>
                       </div> 

                       <div class="form-group row">
                         <div class="col-md-6 form-group mb-3" >
                            <label for="application" class="mb-3">Uploaded Scanned Application</label><br>
                            <div class="row">
                               @if(!empty($data['application']))
                                @foreach($data['application'] as $key=>$value)
                                  @if (!empty($value))
                                  <div class="col-md-8 form-group" id="{{$key}}">
                                      <a href="{{asset('uploads')}}/{{$value}}"  target = '_blank'>{{$value}}</a>
                                      <span class="remove_customer_images" data-name="age_proof_changed" data-image="age_proof_image" onclick="removeimage({{$key}})">
                                        <i class='fa fa-times'></i>
                                      </span>
                                      <input type="hidden" class="{{$key}}" name="application1[]" value="{{$value}}">
                                  </div>
                                  @endif
                                @endforeach
                                @else
                                <input type="hidden" name="application1[]" value="">
                              @endif  
                          </div>

                          <div class="row">
                              <div class="col-md-6 form-group">
                                <label>Upload File<span style="font-size:12px;color:red;">*</span></label>
                                <input type="file" class="form-control-file" name="application_form[]" id="application_form" accept="image/x-png,image/png,image/gif,image/jpeg,application/pdf,doc" style="border:0px;"  onchange="ValidateSizeadd(this,this.id)" >
                                <span class="error text-danger" id="application_form_error"></span>
                              </div>
                                <button type="button" class="btn btn-success" style="height: 30px;margin-top: 25px;" onclick="add_divs();">Add</button>
                            </div>
                            <div id ="new_file">
                            </div>

                          </div>
                       </div>
                       <div class="form-group row">
                            <div class="col-sm-12 d-flex justify-content-center">
                              <button type="button" id="updatePersonalDetails" class="txt-color btn btn-primary btn-rounded m-1">Save</button>
                            </div>
                        </div> 
                    </form>  
                </div>
            <!--------------Member Personal details tab end------------->
            
            <!--------------Member Medical details tab start-------------> 
                <div class="tab-pane fade" id="nav-medical" role="tabpanel" aria-labelledby="nav-medical-tab">
                  <form id="medicalDetails" enctype="multipart/form-data">
                  <input type="hidden" value="{{csrf_token()}}" name="_token" id="token">
                  <input type="hidden" id="id" name="id" value="{{ $data['user_id'] }}">
                  <input type="hidden" id="members_id" name="members_id" value="{{$data['user_member_id']}}">
                  <input type="hidden" id="members_age" value="{{$data['member_age']}}" name="member_age">
                    <div class="form-group row">  
                        <div class="col-md-6 form-group mb-3" >
                            <label for="physical_disability" class="">Physical Disability<span style="color: red">*</span></label>
                            <select class="form-control" name="physical_disability" id="physical_disability">
                                <option value="" hidden>Please Select</option>
                                <option value="Y" <?= (!empty($data['physical_disability']) && $data['physical_disability'] == 'Yes' ? 'selected':'')?>>Yes</option>
                                <option value="N" <?= (!empty($data['physical_disability']) && $data['physical_disability'] == 'No' ? 'selected':'')?>>No</option>
                            </select>
                            <span class="error text-danger" id="physical_disability_error"></span>

                        </div>
                        <div class="col-md-6 form-group mb-3" id="physical_disability_type_div">
                            <label for="physical_disability_type" class="">Physical Disability Type<span style="color: red">*</span></label>
                            <input type="text" value="{{$data['physical_disability_type']}}" class="form-control" id="physical_disability_type" name="physical_disability_type" placeholder="Enter Physical disability Type" >
                            <span class="error text-danger" id="physical_disability_type_error"></span>
                        </div>
                     
                        <div class="col-md-6 form-group mb-3" >
                            <label for="previous_illness" class="">History Of Previous Illness<span style="color: red">*</span></label>
                            <select class="form-control" name="previous_illness" id="previous_illness">
                                <option value="" hidden>Please Select</option>
                                 <option value="Y" <?= (!empty($data['previous_illness']) && $data['previous_illness'] == 'Yes' ? 'selected':'')?>>Yes</option>
                                <option value="N" <?= (!empty($data['previous_illness']) && $data['previous_illness'] == 'No' ? 'selected':'')?>>No</option>
                            </select>
                            <span class="error text-danger" id="previous_illness_error"></span>
                        </div>

                        <div class="col-md-6 form-group mb-3" id="Illness_type_div">
                            <label for="Illness_type" class="">Illness Type<span style="color: red">*</span></label>
                            <select class="form-control" id="illness_type" name="illness_type">
                                <option value="">Please Select</option>
                                @foreach($data['dropdowns']['Illness_types'] as $row)
                                  <option value="{{ $row['dropdown_value'] }}" <?= (!empty($data['illness_type']) && $row['dropdown_value'] == $data['illness_type'] ? 'selected':'')?>>{{  $row['dropdown_value']  }}</option>
                                @endforeach
                            </select>
                            <span class="error text-danger" id="illness_type_error"></span>
                        </div>
                     </div>

                     <div class="form-group row">  
                          <div class="col-md-6 form-group mb-3" >
                            <label for="reports" class="mb-3">Uploaded Reports<span id="report_label" style="font-size:12px;color:red;"></span></label><br>
                            <div class="row">
                               @if(!empty($data['reports']))
                                @foreach($data['reports'] as $key=>$value)
                                  <div class="col-md-8 form-group" id="{{$key}}">
                                      <a href="{{asset('uploads')}}/{{$value}}"  target = '_blank'>{{$value}}</a>
                                      <span class="remove_customer_images" data-name="age_proof_changed" data-image="age_proof_image" onclick="removeimage({{$key}})">
                                        <i class='fa fa-times'></i>
                                      </span>
                                      <input type="hidden" class="{{$key}}" name="reports1[]" value="{{$value}}">
                                  </div>
                                @endforeach
                              @endif  
                          </div>

                          <div class="row">
                              <div class="col-md-6 form-group">
                              <label>Upload File<span style="font-size:12px;color:red;"></span></label>
                              <input type="file" class="form-control-file" name="reports[]" id="reports" accept="image/x-png,image/png,image/gif,image/jpeg,application/pdf,doc" style="border:0px;"  onchange="ValidateSizeadd(this,this.id)">
                              <span class="error text-danger" id="reports_error"></span>
                            </div>
                              <button type="button" class="btn btn-success" style="height: 30px;margin-top: 25px;" onclick="get_divs();">Add</button>
                            </div>
                            <div id = "here">
                            </div>

                          </div>
                        
                        <div class="col-md-6 form-group mb-3" >
                            <label for="health_declaration" class="mb-3">Final Health Declaration<span style="color: red">*</span></label><br>
                            <input type="checkbox" name="health_declaration" id="health_declaration">&nbsp;Hereby, I declare my present health condition is good and certified by medical examiner <br>
                            <input type="hidden" id="health_declaration_value" value="{{$data['health_declaration']}}">
                             <span class="error text-danger" id="health_declaration_error"></span>
                        </div>
                     </div>

                    <div class="form-group row">
                        <div class="col-sm-12 d-flex justify-content-center">
                          <button type="button" id="updateMedicalDetails" class="txt-color btn btn-primary btn-rounded m-1">Save</button>
                        </div>
                    </div>
                  </form>
                </div>
            <!--------------Member Medical details tab end------------->

            <!--------------Member Nominee details tab start------------->      
              <div class="tab-pane fade" id="nav-nominee" role="tabpanel" aria-labelledby="nav-nominee-tab">
                <form id="nomineeDetails"  enctype="multipart/form-data">
                  @php $count1=!empty($data['nominees'])?count($data['nominees']):0;  @endphp
                @foreach ($data['nominees'] as $key=>$value)
                  <input type="hidden" value="{{csrf_token()}}" name="_token" id="token">
                  <input type="hidden" id="id" name="id" value="{{ $data['user_id'] }}"> 
                  <input type="hidden" id="members_id" name="members_id" value="{{$data['user_member_id']}}">

                  <div id="add_nominee_div{{$key+1}}">
                    @if ($key+1!='1')
                    <hr>
                      <div class="row d-flex justify-content-end"><a href="javascript:void(0);"  class="btn btn-danger" onclick="removeNominee({{$key+1}},{{ $value['nominee_id'] }})" >remove-</a></div>
                    @endif
                    <input type="hidden" id="NomineeDetails{{$key+1}}_nominee_id" name="NomineeDetails[{{$key+1}}][nominee_id]" value="{{ $value['nominee_id'] }}"> 
                    <div class="row">
                        <div class="col-md-6 form-group mb-3">
                            <label for="nominee_name" class="">Nominee Name<span style="color: red">*</span></label>
                          
                            <input type="text" value="{{$value['nominee_name']}}" class="form-control" id="NomineeDetails_{{$key+1}}_nominee_name"  name="NomineeDetails[{{$key+1}}][nominee_name]" placeholder="Enter Nominee Name">
                            <span class="error text-danger" id="NomineeDetails_{{$key+1}}_nominee_name_error"></span>
                        </div>

                        <div class="col-md-6 form-group mb-3">
                            <label for="nominee_fcp" class="">FC Amount Percentage<span style="color: red">*</span></label>
                          
                            <input type="text" value="{{$value['fc_percentage']}}" class="form-control fc_percentage" id="NomineeDetails_{{$key+1}}_nominee_fcp"  name="NomineeDetails[{{$key+1}}][nominee_fcp]" placeholder="Enter FC Amount %" onchange="getId({{$key+1}})">
                            <span class="error text-danger" id="NomineeDetails_{{$key+1}}_nominee_fcp_error"></span>
                       </div>
                    </div>

                    <div class="row"> 
                        <div class="col-md-6 form-group mb-3">
                            <label for="nominee_phone" class="">Nominee Phone<span style="color: red">*</span></label>
                          
                              <input type="text" value="{{$value['nominee_phone']}}" class="form-control" id="NomineeDetails_{{$key+1}}_nominee_phone"  name="NomineeDetails[{{$key+1}}][nominee_phone]" placeholder="Enter Nominee Phone" maxlength="10">
                              <span class="error text-danger" id="NomineeDetails_{{$key+1}}_nominee_phone_error"></span>
                        </div>
                    
                        <div class="col-md-6 form-group mb-3">
                            <label for="nominee_email" class="">Nominee Email<span style="color: red">*</span></label>
                          
                            <input type="email" value="{{$value['nominee_email']}}" class="form-control" id="NomineeDetails_{{$key+1}}_nominee_email"  name="NomineeDetails[{{$key+1}}][nominee_email]" placeholder="Enter Nominee Email">
                            <span class="error text-danger" id="NomineeDetails_{{$key+1}}_nominee_email_error"></span>
                       </div>
                    </div>
                    
                    <div class="row">
                       <div class="col-md-6 form-group mb-3">
                            <label for="nominee_gender" class="">Nominee Gender<span style="color: red">*</span></label>
                            <select class="form-control " name="NomineeDetails[{{$key+1}}][nominee_gender]" id="NomineeDetails_{{$key+1}}_nominee_gender">
                                <option value="" hidden="">Please Select</option>
                                <option value="Male" <?= (!empty($value['nominee_gender']) && $value['nominee_gender'] == 'Male' ? 'selected':'')?>>Male</option>
                                <option value="Female" <?= (!empty($value['nominee_gender']) && $value['nominee_gender'] == 'Female' ? 'selected':'')?>>Female</option>
                            </select>
                          <span class="error text-danger" id="NomineeDetails_{{$key+1}}_nominee_gender_error"></span>
                       </div>
                    
                        <div class="col-md-6 form-group mb-3">
                            <label for="nominee_dob" class="">Nominee DOB<span style="color: red">*</span></label>
                          
                            <input type="text" value="{{$value['nominee_dob']}}" class="form-control nominee_dob" id="NomineeDetails_{{$key+1}}_nominee_dob"  name="NomineeDetails[{{$key+1}}][nominee_dob]" placeholder="Select Nominee DOB" autocomplete="nope" readonly="" style="background-color: white">
                            <span class="error text-danger" id="NomineeDetails_{{$key+1}}_nominee_dob_error"></span>
                       </div>
                    </div> 

                    <div class="row">
                       <div class="col-md-6 form-group mb-3">
                            <label for="nominee_relation" class="">Nominee Relation<span style="color: red">*</span></label>
                            <input type="text" value="{{$value['nominee_relationship']}}" class="form-control" id="NomineeDetails_{{$key+1}}_nominee_relation"  name="NomineeDetails[{{$key+1}}][nominee_relation]" placeholder="Enter Relation Name" autocomplete="nope">
                            <span class="error text-danger" id="NomineeDetails_{{$key+1}}_nominee_relation_error"></span>
                       </div>
                    
                        <div class="col-md-6 form-group mb-3">
                            <label for="nominee_address" class="">Nominee Address<span style="color: red">*</span></label>
                             <textarea class="form-control" id="NomineeDetails_{{$key+1}}_nominee_address" name="NomineeDetails[{{$key+1}}][nominee_address]" rows="5" placeholder="Enter Nominee Address">{{$value['nominee_address']}}</textarea>
                           <span class="error text-danger" id="NomineeDetails_{{$key+1}}_nominee_address_error"></span>                           
                       </div>
                    </div>

                    <div class="row">   
                       <div class="col-md-6 form-group mb-3" style="margin-top:-15px;">
                          <label for="profile_photo">Nominee Photo</label><br>
                          <input type="hidden" name="NomineeDetails[{{$key+1}}][nominee_image_changed]" id="NomineeDetails_{{$key+1}}_nominee_photo_changed">

                          <input class="customer_id_images"  type="file" id="NomineeDetails_{{$key+1}}_nominee_image" name="NomineeDetails[{{$key+1}}][nominee_image]" accept="image/x-png,image/png,image/gif,image/jpeg"><br>
                        
                          @if(!empty($value['nominee_image']))
                            <input type="hidden" name="NomineeDetails[{{$key+1}}][nominee_image_old]" id="NomineeDetails_{{$key+1}}_nominee_image_old" value="{{$value['nominee_image']}}">

                              <span id="NomineeDetails_{{$key+1}}_nominee_photo"> 
                                <a href="{{asset('uploads')}}/{{$value['nominee_image']}}"  target = '_blank'>{{$value['nominee_image']}}</a>

                                 <span class="remove_customer_images" data-name="NomineeDetails_{{$key+1}}_nominee_photo_changed" data-image="NomineeDetails_{{$key+1}}_nominee_photo">
                                 <i class='fa fa-times'></i>
                                 </span>
                              </span>
                          @endif
                          <span class="error text-danger" id="NomineeDetails_{{$key+1}}_nominee_image_error">
                         </span>
                        </div>   
                      </div>
                      </div> 
                      @endforeach 
                      <div class="form-group  new_nominee_div">
                      </div>
                      <input type="hidden" id="testinput" name="" value="{{$count1+1}}">
                      <div class="row d-flex justify-content-end">
                          <a href="javascript:void(0);" class="btn btn-success" onclick="addNomineeData({{$count1}})">Add Another Nominee+</a>
                      </div><hr> 
                        <div class="form-group row">
                            <div class="col-sm-12 d-flex justify-content-center">
                              <button type="button" id="updateNomineeDetails" class="txt-color btn btn-primary btn-rounded m-1">Save</button>
                                
                            </div>
                        </div>
                  </form>   
                </div>
            <!--------------Member Nominee details tab end------------->

            <!--------------Member Payment details tab start------------->      
                <div class="tab-pane fade" id="nav-payment" role="tabpanel" aria-labelledby="nav-payment-tab">
                  <form id="paymentDetails"  enctype="multipart/form-data">
                  <input type="hidden" value="{{csrf_token()}}" name="_token" id="token">
                  <input type="hidden" id="id" name="id" value="{{ $data['user_id'] }}">
                  <input type="hidden" id="members_id" name="members_id" value="{{$data['user_member_id']}}">
                  <input type="hidden" id="payment_id" name="payment_id" value="{{$data['payment_id']}}">
                  <input type="hidden" id="advance_pay_id" name="advance_pay_id" value="{{$data['advance_payment']['advance_payment_id']}}">
                    <div class="row">
                        <div class="col-md-6 form-group mb-3">
                            <label for="member_age" class="">Member Age<span style="color: red">*</span></label>
                          
                            <input type="text" value="{{$data['member_age']}}" class="form-control" id="member_age"  name="member_age" placeholder="Enter Memebr Age" readonly="">
                            <span class="error text-danger" id="member_age_error"></span> 
                       </div>

                       <div class="col-md-6 form-group mb-3">
                            <label for="corpus_amount" class="">Corpus Amount<span style="color: red">*</span></label>
                          
                              <input type="text" value="{{$data['corpus_amount']}}" class="form-control" id="corpus_amount"  name="corpus_amount" placeholder="Enter Corpus Amount" readonly="">
                              <span class="error text-danger" id="corpus_amount_error"></span>
                       </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 form-group mb-3">
                            <label for="payment_mode" class="">Payment Mode<span style="color: red">*</span></label>
                          
                            <select class="form-control" name="payment_mode" id="payment_mode">
                                <option value="" hidden>Please Select</option>
                                @foreach($data['dropdowns']['payment_modes'] as $row)
                                  <option value="{{ $row['dropdown_value'] }}" <?= (!empty($data['mode_of_payment']) && $row['dropdown_value'] == $data['mode_of_payment'] ? 'selected':'')?>>{{  $row['dropdown_value']  }}</option>
                                @endforeach
                            </select>
                            <span class="error text-danger" id="payment_mode_error"></span>
                       </div>

                       <div class="col-md-6 form-group mb-3">
                            <label for="payment_date" class="">Payment Date<span style="color: red">*</span></label>
                          
                              <input type="text" value="{{$data['payment_date']}}" class="form-control pdate" id="payment_date"  name="payment_date" placeholder="Select Payment Date" readonly="" style="background-color: white">
                              <span class="error text-danger" id="payment_date_error"></span>
                       </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 form-group mb-3">
                            <label for="note" class="">Payment Note</label>
                               <textarea class="form-control" id="note" name="note" rows="5" placeholder="Enter payment note">{{$data['remarks']}}</textarea>
                             <span class="error text-danger" id="note_error"></span>
                       </div>

                       <div class="col-md-6 form-group mb-3">
                            <label for="note" class="">Is Member Intersted in Other schemes ?</label><br>
                            
                            <input type="radio" value="Y" {{ ($data['interested_in_other_scheme']=="Y")? "checked" : "" }} name="interested"> <label class="form-check-label">Yes</label>
                             <input type="radio" value="N" {{ ($data['interested_in_other_scheme']=="N")? "checked" : "" }} name="interested"> <label class="form-check-label">No</label><br>
                             <span class="error text-danger" id="interested_error"></span>
                       </div>

                    </div><hr>

                    <div class="row">
                      <div class="col-md-6 form-group mb-3">
                            <label for="corpus_amount" class="">Advance Amount<span style="color: red">*</span></label>
                          
                              <input type="text" class="form-control" id="advance_amount"  name="advance_amount" placeholder="Enter Corpus Amount" readonly="" value="{{$data['advance_payment']['advance_amount']}}">
                              <span class="error text-danger" id="advance_amount_error"></span>
                       </div>

                        <div class="col-md-6 form-group mb-3">
                            <label for="payment_mode" class="">Advance Payment Mode<span style="color: red">*</span></label>
                          
                            <select class="form-control" name="advance_payment_mode" id="advance_payment_mode">
                              <option value="" hidden>Please Select</option>
                              @foreach($data['dropdowns']['payment_modes'] as $row)
                                <option value="{{ $row['dropdown_value'] }}" <?= (!empty($data['advance_payment']['advance_mode_of_payment']) && $row['dropdown_value'] == $data['advance_payment']['advance_mode_of_payment'] ? 'selected':'')?>>{{  $row['dropdown_value']  }}</option> 
                                @endforeach
                            </select>
                            <span class="error text-danger" id="advance_payment_mode_error"></span>
                       </div>

                       <div class="col-md-6 form-group mb-3">
                            <label for="payment_date" class="">Advance Payment Date<span style="color: red">*</span></label>
                          
                              <input type="text" class="form-control pdate" id="advance_payment_date"  name="advance_payment_date" placeholder="Select Payment Date" readonly="" style="background-color: white" value="{{$data['advance_payment']['advance_payment_date']}}">
                              <span class="error text-danger" id="advance_payment_date_error"></span>
                       </div>
                    </div>
                        <div class="form-group row">
                            <div class="col-sm-12 d-flex justify-content-center">
                               <button type="button" id="updatePaymentDetails" class="txt-color btn btn-primary btn-rounded m-1">Save</button>

                            </div>
                        </div>
                    </form>    
                </div>
            <!--------------Member Payment details tab end------------->

            <!--------------Member Access tab start------------->   
                <div class="tab-pane fade" id="nav-member_access" role="tabpanel" aria-labelledby="nav-member_access-tab">
                <form id="memberDetails"  enctype="multipart/form-data">
                  <input type="hidden" value="{{csrf_token()}}" name="_token" id="token">
                  <input type="hidden" id="id" name="id" value="{{ $data['user_id'] }}">
                  <input type="hidden" id="members_id" name="members_id" value="{{$data['user_member_id']}}">
                    <div class="row">
                      <div class="col-md-4 form-group mb-3">
                      <label for="password">Password<span style="color: red">*</span></label>
                        <input type="password" name="old_password" class="form-control" id="old_password" placeholder="Enter Password"  autocomplete="nope" value="{{$data['password']}}" readonly="">
                        <span class="error text-danger err-password" id="password_error">
                       </span>
                      </div>
                      <div class="col-md-2 form-group mt-4">

                        <input type="button" class="btn btn-primary btn-rounded" onclick="changePassowrd()" value="Change Password">
                          
                      </div>

                   <div class="col-md-4 form-group mb-3">
                    <label for="member_status">Member Status</label>
                        <input type="text" id="status" name="member_status" class="form-control" value="{{$data->member_status}}" readonly="">
                    </div>
                    @if($data->member_status =='Active' || $data->member_status =='Probationary')
                      <div class="col-md-2 form-group mt-4">
                        <input type="button" class="btn btn-primary btn-rounded" onclick="changeStatus()" value="Change Status">
                      </div>
                    @endif
                </div>
                
                <div class="row">
                    <div class="col-md-6 form-group mb-3">
                        <label for="membership_date">Membership Date<span style="color: red">*</span></label>
                        <input class="form-control pdate" type="text" name="membership_date" value="{{$data['join_date']}}" id="membership_date" readonly="" style="background-color: white">
                        <span class="error text-danger" id="membership_date_error"></span>
                    </div>

                    <div class="col-md-6 form-group mb-3">
                        <label for="profile_photo">Upload Profile Photo</label><br>
                        <input type="hidden" name="profile_photo_changed" id="profile_photo_changed">
                        <input class="customer_id_images"  type="file" id="profile_photo" name="profile_photo" accept="image/x-png,image/png,image/gif,image/jpeg"><br>
                          @if(!empty($data->image))
                              <input type="hidden" name="profile_photo_old" id="profile_photo_old" value="{{$data['image']}}">
                              <span id= "profile_photo_image">
                                <a href="{{asset('uploads')}}/{{$data['image']}}"  target = '_blank'>{{$data['image']}}</a>
                                <span class="remove_customer_images" data-name="profile_photo_changed" data-image="profile_photo_image">
                                  <i class='fa fa-times'></i>
                                </span>
                              </span>
                            @endif
                          <span class="error text-danger" id="profile_photo_error"></span>
                      </div>

                       <div class="col-md-6 form-group mb-3">
                           <label for="additional_info" class="">Additional Information</label>
                               <textarea class="form-control" id="additional_info" name="additional_info" rows="5" placeholder="Enter additional information">{{$data['additional_info']}}</textarea>
                           <span class="error text-danger" id="additional_info_error">
                          </span>
                      </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-12 d-flex justify-content-center">
                            <button type="button" class="btn btn-primary btn-rounded m-1" id="updateMemberDetails">Save</button> 
                            
                        </div>
                    </div>
                </form>
              </div>
              <input type="hidden" name="id" id="users_id" value="{{ $data['user_id'] }}">

          <!---------------------Change Password Popup start----------------->
              <div class="modal fade" id="changePassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle-2" aria-hidden="true">
                <div class="modal-dialog modal-md" style="margin-top: 60px">
                    <div class="modal-content">
                      <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalCenterTitle-2">Change Password</h5>
                          <button type="button" class="close closePopup" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                      </div>

                    <div class="modal-body padding-style">
                        <form id="passowrd_change">
                            <input type="hidden" value="{{csrf_token()}}" name="_token" id="token">
                            <div class="row form-group">
                            <div class="col-md-4"  style="padding-top: 6px;">Password<span style="color: red">*</span></div>
                            <div class="col-md-6">
                              <input type="password" class="form-control" name="newPassword" id="new_password">
                              <span class="text-danger err-password" id="newPassword">
                               </span>  
                            </div>
                            </div>
                            <div class="row form-group">
                            <div class="col-md-4"  style="padding-top: 6px;">Confirm Password<span style="color: red">*</span></div>
                            <div class="col-md-6">
                                <input type="password" class="form-control" name="confirmPassword" id="confirm_passowrd">
                                <span class="text-danger err-password" id="confirmPassword" >
                                 
                               </span>
                            </div>
                            </div>
                        </form>
                        </div>
                        <div class="modal-footer">
                        <button type="button" id="savePassword" class="btn btn-primary btn-rounded">Submit</button>
                            <button type="button" class="btn btn-primary btn-rounded closePopup" data-dismiss="modal">Close</button>
                        </div>
                    </div>
              </div>
              </div> 
          <!---------------------Change Password Popup end----------------->

          <!---------------------Member Access tab end----------------->

    <!------------------- Change Member Status Modal Start-------------------------->
              
                @include('members.statusPopup')
                
    <!------------------- Change Member Status Modal End--------------------------->          
        </div> 
       </div>       
    </div>      
</div>      
@endsection
@section('page-js')
<script src="{{  asset('js/statusPopup.script.js')}}"></script>
<script>

$(document).ready(function(){
   var age_from=$('#age_from').val();
  var age_to=$('#age_to').val();
  var health_declaration=$('#health_declaration_value').val();
  if (health_declaration =='Y') {
    $('#health_declaration').attr('checked','checked');
  }else{

  $('#health_declaration').removeAttr('checked');
  }
 
  <?php if(!empty($data['id_proofs']['customer_id_type']) && $data['id_proofs']['customer_id_type'] != ''){?>
    $('#customer_id_proofs').show();
  <?php }?>
  $('#customer_id_type').on('change', function(){
      toggleCustomerIds();
  });

  togglePhysicalDisabilityType();
  togglePreviousIllness();

  function togglePhysicalDisabilityType(){
    
  if($('#physical_disability').val() =='Y'){
    $('#physical_disability_type_div').show();
    $('#physical_disability_type').attr('required',true);
  }else{
    $('#physical_disability_type_div').hide();
    $('#physical_disability_type').val('').attr('required',false);
  }
}

  $('#physical_disability').on('change', function() {
      if($(this).val() == 'Y'){
        $('#physical_disability_type_div').show();
        $('#physical_disability_type').attr('required',true);
      }else{
        $('#physical_disability_type_div').hide();
        $('#physical_disability_type').val('').attr('required',false);;
      }
  });
$('#expired_fields_div').hide();
function toggleCustomerIds(){
  $('#customer_id_front_changed').val('D');
  $('#customer_id_back_changed').val('D');
  $('#age_proof_changed').val('D');
  $('#nominee_image_changed').val('D');
  $('#profile_photo_changed').val('D');
  $('#reports_changed').val('D');
  $('#customer_id_front_image').hide();
  $('#customer_id_front_image').hide();
  
  if($('#customer_id_type').val() != ''){
    $('#customer_id_proofs').show();
    $('#customer_id_front').val("");
    $('#customer_id_back').val("");
    $('#customer_id_back_error').hide();
    $('#customer_id_front_error').hide();
  }else{
    $('#customer_id_proofs').hide();
    $('#customer_id_back_error').hide();
    $('#customer_id_front_error').hide();
  }
}

$('.customer_id_images').on('change', function() {

    if(this.files[0] && this.files[0].size/1024/1024 > 6){
      $("#"+$(this).attr("id")+'_error').html('File size should be less than 6 MB').show();
      $('#'+$(this).attr("id")).val("");
      $('#'+$(this).attr("id")+'_changed').val('N');
    }else {
      console.log($('#'+$(this).attr("id")))
      $('#'+$(this).attr("id")+'_changed').val('Y');
      $("#"+$(this).attr("id")+'_error').html('').hide();
    }
  });

  $('.remove_customer_images').on('click', function(){
   
    $('#'+$(this).data('name')).val('D');
    $('#'+$(this).data('image')).hide();
  });

  //code for select image
    $('#OpenImgUpload').click(function(){ 
       $('#nominee_image').trigger('click');
    });
     
    var img = $('#uploadPic').val();
    if(img == null || img == '' || img == undefined){
       $(".pip").hide();
    }
    // code for preview image
      function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
              $('#OpenImgUpload').show();
                $('.pip').show();
                $('#OpenImgUpload').attr('src', e.target.result);

                $(".remove").click(function(){
                    $('.pip').hide();
                     $('#OpenImgUpload').hide();
                     $('#nominee_image').val("");
                  });
            }
            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
      }
          
    $("#nominee_image").change(function() {
        readURL(this);
    });
    $("#uploadPic").change(function() {
        readURL(this);
    });

    $(".remove").click(function(){
        $('.pip').hide();
        $('#OpenImgUpload').hide();
        $('#nominee_image').val("");
        $('#uploadPic').attr('type', 'file');
        $('#uploadPic').val("");
    });

function togglePreviousIllness(){
    
  if($('#previous_illness').val() =='Y'){
    $('#Illness_type_div').show();
    $('#illness_type').attr('required',true);
  }else{
    $('#Illness_type_div').hide();
    $('#illness_type').val('').attr('required',false);
  }
}

  $('#previous_illness').on('change', function(){
      if($(this).val() == 'Y'){
        $('#Illness_type_div').show();
        $('#illness_type').attr('required',true);
      }else{
        $('#Illness_type_div').hide();
        $('#illness_type').val('').removeAttr('required',true);
      }
  });
$( function() {
    $( ".dob" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd-mm-yy',
      yearRange: "-"+age_to+":+"+age_from+"",
      maxDate:"-"+age_from+"Y",
    });

    $('body').on('focus',".nominee_dob", function(){
        if( $(this).hasClass('hasDatepicker') === false )  {
          $(this).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd-mm-yy',
            yearRange: "-75:+1",
            maxDate:"-1Y",
          });
        }
    });

    $( ".pdate" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd-mm-yy',
      maxDate: 0,
    });
  });

var is_country_first = true;
$('#country_id').change(function(){

        var id = $(this).find(':selected').data('id');
        var val = $(this).find(':selected').val();
       
        $('#state_id').empty();
        $('#city_id').empty();
        $('#state_id').select2('data','');
        $('#state_id').trigger('change');
       
        // $('#city_id').append('<option value="" hidden>Please Select</option>');
        $('#city_id').trigger('change');
        if (val=='') {
           
        } 

        if(id != '' && id != 'undefined'&& id != 'Select Country' && id !=null && typeof id != "undefined") {
          $('#country_error').hide();
            $.ajax({
                url      : "{{url('/getStatesofCountry')}}/"+id,
                method   : 'GET',
                dataType : 'json',
                success: function(data) {
                    if(data.status == 'success'){
                        var data = data.data;
                        var options = '';
                         options += '<option value="" hidden="">Please Select</option>';
                        var checkstate = [];
                        for (var item in data) {
                            var res = data[ item ];
                            options += "<option value= '"+res.name+"' data-id='"+res.id+"'>"+res.name+"</option>";
                            checkstate.push(res.name);
                        }
                        if(!options) {
                            $('#state_id').empty();
                        } else {
                            $('#state_id').append(options);
                            if(checkstate.indexOf('<?= $data['state'] ?>') != -1 && is_country_first){
                              $('#state_id').val('<?= $data['state'] ?>').trigger('change');
                              is_country_first = false;
                            }else{
                               $('#state_id').val('').trigger('change');
                               $('#city_id').val('').trigger('change');
                            }
                        }
                    }else{
                      $('#state_id').empty();
                    }
                },
                error:function() {
                    alert('Something went wrong');
                }
            });
        }
    });
   <?php if(!empty($data['country'])){?>
    $('#country_id').val('<?= $data['country'] ?>').trigger('change');
    <?php }else {?>
    $('#country_id').val('India').trigger('change');
    <?php }?>

    var is_state_first = true;
    $('#state_id').change(function(){
        var id = $(this).find(':selected').data('id');
        var val = $(this).find(':selected').val();
        
        if (val=='') {
      
        } 
        else{
            $('#state_error').hide();
        }

        $('#city_id').empty();
        $('#city_id').append('<option value="" hidden="">Please Select</option>');
         $('#city_id').trigger('change');

        if(id !='' && id !='undefined' && id !='Please Select' && typeof id != "undefined") {
         
            $.ajax({
                url      : "{{url('/getCitiesofState')}}/"+id,
                method   : 'GET',
                dataType : 'json',
                success: function(data) {
                  if(data.status == 'success'){
                    var data = data.data;
                    $('#city_id').empty();
                        var options = '';
                        options += '<option value="" hidden>Please Select</option>';
                        var checkcity = [];
                        for (var item in data) {
                            var res = data[ item ];
                            options += "<option value= '"+res.name+"' data-id='"+res.id+"'>"+res.name+"</option>";
                            checkcity.push(res.name);
                        }
                        if(options != '') {
                           $('#city_id').append(options);
                           if(checkcity.indexOf('<?= $data['city'] ?>') != -1 && is_state_first){
                                 $('#city_id').val('<?= $data['city'] ?>').trigger('change');
                                 is_state_first =false;
                             }
                        }
                  }
                },
                error:function() {
                    alert('Something went wrong');
                }
            });
        }
    });

    $('#personalDetails').validate({
          ignore: "",
        rules: {
            member_id:{
               required: true,
               alpha_numeric: true,
               minlength:3,
               maxlength:100,
            }, 
            name:{
               required: true,
               only_text: true,
               minlength:3,
               maxlength:100,
            },
            
            email:{
                required: true,
                custom_email: true,
                maxlength:100,
            },
            alternative_email:{
                custom_email: true,
                maxlength:100,
                notEqualTo:"#email",
            },

            note:{
                maxlength:500,
            },
            mobile_number:{
              required: true,
              number : true,
              minlength:10,
              maxlength:10,
            },
            alternative_number:{
                notEqualTo:"#mobile_number",
                number : true,
                minlength:10,
                maxlength:10,
            },
            marital_status:{
              required:true
            },
            gender:{
              required:true
            },
            country:{
              required:true
            },
            state:{
              required:true
            }, 
            city:{
              required:true
            }, 
            dob:{
              required:true
            },

            customer_id_type:{
              required:true
            }, 
            member_status:{
              required: true, 
            },
            member_address:{
              minlength: 3, 
              maxlength: 1000, 
            }, 
          },
        messages:{
            name:{
              required: "This field is Required"
            },
             email:{
              required: "This field is Required"
            },
            
            mobile_number:{
              required: "This field is Required"
            },
            alternative_email:{
              notEqualTo:"Email and alternative email should be different"
            },
            alternative_number:{
              notEqualTo:"Mobile Number and alternative Mobile Number should be different"
            },
            member_address:{
              maxlength: "This field can not exceed 1000 characters"
            },
        }
      });

      $('#updatePersonalDetails').click(function () {
          if($("#personalDetails").valid()){
            $('#updatePersonalDetails').attr('disabled',true)
            var data=$('#personalDetails').serialize();
              $.ajax({ 
                  method: "POST",
                  headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },  
                  url: "{{url('member/update/perosnalDetails')}}",
                  data:new FormData($("#personalDetails")[0]),
                  contentType: false,
                  processData: false,
                }).done(function( data ) {

                  $('.loadscreen').hide();
                  if(data.status== 'success'){
                    $('#updatePersonalDetails').removeAttr('disabled',true);
                    $('#application_form_error').hide();
                      $("#alert-primary").text('Personal Details Updated Successfully !');
                      $("#successmsg").show();
                      $('html, body').animate({
                        scrollTop: $('.main-header').offset().top
                      }, 1000);
                      setTimeout(function(){ $('#successmsg').fadeOut() 
                        $('#nav-tab a[href="#nav-medical"]').tab('show')
                      }, 3000);
                      
                  }
                  else if(data.status=='validation_error'){
                    $('#updatePersonalDetails').removeAttr('disabled',true);
                      $.each(data.data, function (key, val) {
                          $("#"+key+"_error").text(val[0]);
                          $("#"+key+"_error").show();
                          $(document).on("keyup", "input[name='"+key+"']", function(e) {
                                $("#"+key+"_error").hide();
                           });
                      });
                  }
                  else{
                    $('#updatePersonalDetails').removeAttr('disabled',true);
                      $("#alert-primary").text('Something Went Wrong !');
                      $("#successmsg").show();
                      $('html, body').animate({
                        scrollTop: $('.main-header').offset().top
                    }, 1000);
                      setTimeout(function(){ $('#successmsg').fadeOut() }, 3000);
                  }
              });
        }
        else{
          var validator = $("#personalDetails").validate();
          var messages = '';
          $.each(validator.errorMap, function (index, value) {
            messages += index.charAt(0).toUpperCase() + index.slice(1) + ' : ' + value+'\n';
            });
          swal("Please fill mandatory field",messages);
        }
            
    });
  
  $('#member_dob').change(function(){
    if($(this).val() != ''){
      $('#dob_error').hide();
      var dateString=$(this).val();
      
      var dateAr = dateString.split('-');
      var newDate = dateAr[1] + '-' + dateAr[0] + '-' + dateAr[2].slice(-2);

      var today = new Date();
      var birthDate = new Date(newDate);
    
      var age = today.getFullYear() - birthDate.getFullYear();
    
      var m = today.getMonth() - birthDate.getMonth();
    
      if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
      }
      $('#member_age').val(age)
      $('#members_age').val(age)
      if(age != ''|| age !=NaN){
        getCorpusAmount(age)

        if(age>=50){
          $('#reports').attr('required',true)
          $('#report_label').text('*')
        }
        else{
          $('#reports').removeAttr('required',true)
          $('#report_label').text('')
          $('#reports-error').val("").hide();
          $('#reports_error').val("").hide();
        }
      }
    }
  });
    function getCorpusAmount(age){
      $.ajax({
              method: "POST",
                url: "{{url('member/getCorpusAmount')}}",
                data:{
                    "_token": "{{ csrf_token() }}",
                    "age" : age
                  },
              }).done(function( data ) {
              if(data.status=='success'){
                  $('#corpus_amount').val(data.amount)
              }
              else if(data.status=='fail'){
                  $('#dob_error').show();
                  $('#member_age').val('')
                  $('#corpus_amount').val('')
                  $('#dob_error').text(data.amount)
              };
          })
      }

    $("#nomineeDetails").validate({
        rules: {
          "NomineeDetails[1][nominee_name]":{
               required: true,
               only_text: true,
               minlength:3,
               maxlength:100,
            },
            "NomineeDetails[1][nominee_email]":{
                required: true,
                custom_email: true,
                maxlength:100,
            },
            "NomineeDetails[1][nominee_address]":{
                required:true,
                maxlength:1000,
            },
            "NomineeDetails[1][nominee_phone]":{
                required: true,
                number : true,
                minlength:10,
                maxlength:11,
            }, 
            "NomineeDetails[1][nominee_dob]":{
              required:true
            },
             "NomineeDetails[1][nominee_relation]":{
              required:true,
              only_text: true,
              minlength:3,
              maxlength:100
            },
            "NomineeDetails[1][nominee_gender]":{
              required:true
            },
            "NomineeDetails[1][nominee_fcp]":{
                required:true,
                number:true,
                minlength:1,
                maxlength:3,
                max:100
            },
         },
         messages:{
          "NomineeDetails[1][nominee_relation]":{
              only_text:"Only alphabets are allowed",
              maxlength: "This field can not exceed 100 characters"
            },
         }
      });

    $("#updateNomineeDetails").on('click', function(){ 
    
        if($('#nomineeDetails').valid()){
          $('#updateNomineeDetails').attr('disabled',true)
          $.ajax({
            method: "POST",
            url : "{{url('member/update/nomineeDetails')}}",
            data:new FormData($("#nomineeDetails")[0]),
            contentType: false,
            processData: false,
          })
          .done(function( data ) {
            
            if(data.status == 'success'){
              $("#alert-primary").html("Nominee Details Updated Successfully!");
              $('#updateNomineeDetails').removeAttr('disabled',true);
              $("#successmsg").show();
              $('html, body').animate({
                  scrollTop: $('.main-header').offset().top
              }, 1000);
              setTimeout(function(){ $('#successmsg').fadeOut();
              $('#nav-tab a[href="#nav-payment"]').tab('show'); }, 3000);
            }
            else if(data.status=='validation_error'){
                $('#updateNomineeDetails').removeAttr('disabled',true);
                $("#alert-primary").html("Validation Errors!");
                $("#successmsg").show();
                $('html, body').animate({
                    scrollTop: $('.main-header').offset().top
                }, 1000);
                setTimeout(function(){ $('#successmsg').fadeOut() }, 3000);
                
                $.each(data.data, function (key, val) {
                    var key1=key.replace(/\./g, '_')
                    $("#"+key1+"_error").text(val[0]);
                    $("#"+key1+"_error").show();
                    
                    $(document).on("keyup", "input[id='"+key+"']", function(e) {
                          $("#"+key+"_error").hide();
                     });
                    $(document).on("change", "input[id='"+key1+"']", function(e) {
                      $("#"+key1+"_error").hide();
                    });
                });
              }
              else{
                  $("#alert-primary").html("Something Went Wrong!");
                  $('#updateNomineeDetails').removeAttr('disabled');
                  $("#successmsg").show();
                  $('html, body').animate({
                      scrollTop: $('.main-header').offset().top
                  }, 1000);
                  setTimeout(function(){ $('#successmsg').fadeOut() }, 3000);
            }
        });
        }else{
          var validator = $("#nomineeDetails").validate()
          var messages = '';
          $.each(validator.errorMap, function (index, value) {
            messages += index.charAt(0).toUpperCase() + index.slice(1) + ' : ' + value+'\n';
            });
            swal("Please fill mandatory field",messages);
       }
  });

    $("#paymentDetails").validate({
        rules: {
          note:{
              maxlength:1000,
            },
            payment_mode:{
              required:true
            },
            payment_date:{
              required:true
            },
         },
         messages:{
            note:{
              maxlength: "This field can not exceed 1000 characters"
            },
        }
    });

    $("#updatePaymentDetails").on('click', function(){  
      var data=$('#paymentDetails').serialize();
        if($('#paymentDetails').valid()){
          $('#updatePaymentDetails').attr('disabled',true);
          $.ajax({
            method: "POST",
            url : "{{url('member/update/paymentDetails')}}",
            data     :data,
          })
          .done(function( data ) {
            
            if(data.status == 'success'){
              $("#alert-primary").html("Payment Details Updated Successfully!");
              $('#updatePaymentDetails').removeAttr('disabled',true);
                  $("#successmsg").show();
                  $('html, body').animate({
                      scrollTop: $('.main-header').offset().top
                  }, 1000);
                  setTimeout(function(){ $('#successmsg').fadeOut();
                  $('#nav-tab a[href="#nav-member_access"]').tab('show'); }, 3000);
            }
            else if(data.status=='validation_error'){
                $('#updatePaymentDetails').removeAttr('disabled',true);
                  $.each(data.data, function (key, val) {
                      $("#"+key+"_error").text(val[0]);
                      $("#"+key+"_error").show();
                      $(document).on("keyup", "input[name='"+key+"']", function(e) {
                            $("#"+key+"_error").hide();
                       });
                  });
              }
              else{
                  $("#alert-primary").html("Something Went Wrong!");
                  $('#updatePaymentDetails').removeAttr('disabled');
                  $("#successmsg").show();
                  $('html, body').animate({
                      scrollTop: $('.main-header').offset().top
                  }, 1000);
                  setTimeout(function(){ $('#successmsg').fadeOut() }, 3000);
            }
        });
        }else{
        var validator = $("#paymentDetails").validate()
        var messages = '';
          $.each(validator.errorMap, function (index, value) {
            messages += index.charAt(0).toUpperCase() + index.slice(1) + ' : ' + value+'\n';
            });
            swal("Please fill mandatory field",messages);
       }
    });


    $("#medicalDetails").validate({
        ignore: "",
        rules: {
           physical_disability:{
              required:true
            },
            previous_illness:{
              required:true
            },
            physical_disability_type:{
              only_text:true,
              maxlength:100
            },
        },
        messages:{
            physical_disability_type:{
              only_text: "Only alphabets are allowed",
              maxlength: "This field can not exceed 100 characters"
            },
        }
    });

    $("#updateMedicalDetails").on('click', function(){  
      var data=$('#medicalDetails').serialize();
     
        if($('#medicalDetails').valid()){
          
          $('#updateMedicalDetails').attr('disabled',true);
          $.ajax({
            method: "POST",
            url : "{{url('member/update/medicalDetails')}}",
            data:new FormData($("#medicalDetails")[0]),
            contentType: false,
            processData: false,
            
          })
          .done(function( data ) {
            
            if(data.status == 'success'){
              $("#health_declaration_error").hide();
              $("#alert-primary").html("Medical Details Updated Successfully!");
              $('#updateMedicalDetails').removeAttr('disabled',true);
              $('#reports_error').hide();
                  $("#successmsg").show();
                  $('html, body').animate({
                      scrollTop: $('.main-header').offset().top
                  }, 1000);
                  setTimeout(function(){ 
                    $('#successmsg').fadeOut();
                    $('#nav-tab a[href="#nav-nominee"]').tab('show');
                  }, 3000);
            }
            else if(data.status=='validation_error'){
                $('#updateMedicalDetails').removeAttr('disabled',true);
                  $.each(data.data, function (key, val) {
                      $("#"+key+"_error").text(val[0]);
                      $("#"+key+"_error").show();
                      $(document).on("keyup", "input[name='"+key+"']", function(e) {
                            $("#"+key+"_error").hide();
                       });
                  });
              }
              else{
                  $("#alert-primary").html("Something Went Wrong!");
                  $('#updateMedicalDetails').removeAttr('disabled',true);
                  $("#successmsg").show();
                  $('html, body').animate({
                      scrollTop: $('.main-header').offset().top
                  }, 1000);
                  setTimeout(function(){ $('#successmsg').fadeOut() }, 3000);
            }
        });
        }else{
        var validator = $("#medicalDetails").validate()
        var messages = '';
          $.each(validator.errorMap, function (index, value) {
            messages += index.charAt(0).toUpperCase() + index.slice(1) + ' : ' + value+'\n';
            });
            swal("Please fill mandatory field",messages);
       }
    });

    $("#memberDetails").validate({
        ignore: "",
        rules: {
           additional_info:{
              maxlength:1000
            },
            membership_date:{
              required:true
            },
         },
         messages:{
            additional_info:{
              maxlength: "This field can not exceed 1000 characters"
            },
        }
    });

    $("#updateMemberDetails").on('click', function(){  
      var data=$('#memberDetails').serialize();
        if($('#memberDetails').valid()){
          $('#updateMemberDetails').attr('disabled',true);
          $.ajax({
            method: "POST",
            url : "{{url('member/update/memberDetails')}}",
            data:new FormData($("#memberDetails")[0]),
            contentType: false,
            processData: false,
          })
          .done(function( data ) {
            
            if(data.status == 'success'){
              $("#alert-primary").html("Member access updated successfully!");
              $('#updateMemberDetails').removeAttr('disabled',true);
                  $("#successmsg").show();
                  $('html, body').animate({
                      scrollTop: $('.main-header').offset().top
                  }, 1000);
                  setTimeout(function(){ $('#successmsg').fadeOut() }, 3000);
                  window.location.href = "{{url()->previous()}}"; 
            }
            else if(data.status=='validation_error'){
                $('#updateMemberDetails').removeAttr('disabled',true);
                  $.each(data.data, function (key, val) {
                      $("#"+key+"_error").text(val[0]);
                      $("#"+key+"_error").show();
                      $(document).on("keyup", "input[name='"+key+"']", function(e) {
                            $("#"+key+"_error").hide();
                       });
                  });
              }
              else{
                  $("#alert-primary").html("Something Went Wrong!");
                  $('#updateMemberDetails').removeAttr('disabled');
                  $("#successmsg").show();
                  $('html, body').animate({
                      scrollTop: $('.main-header').offset().top
                  }, 1000);
                  setTimeout(function(){ $('#successmsg').fadeOut() }, 3000);
            }
        });
        }else{
        var validator = $("#memberDetails").validate()
        var messages = '';
          $.each(validator.errorMap, function (index, value) {
            messages += index.charAt(0).toUpperCase() + index.slice(1) + ' : ' + value+'\n';
            });
            swal("Please fill mandatory field",messages);
       }
    });
  });
  
  function changeStatus(){
   $("#confirm_date" ).datepicker('destroy');
   $('#myModal').modal({
        show: true,
        keyboard: false,
        backdrop: 'static'
    })
    var id = $('#users_id').val();
    var status = $('#status').val();

    var joining_date=$('#membership_date').val();
    $("#member_status").val(status).change();
    $('#user_id').val(id);
    $('#status').val(status);

    $( "#confirm_date" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd-mm-yy',
      minDate:joining_date,
      maxDate: 0,
    });
  }

  $(document).on("click","#submit",function() {
    $('#update_member_status').validate({
        ignore: "",
        rules:{
          fc_note:{
            maxlength:1000,
          },
          member_status:{
            required: true,
          },
        }
    });
    if($('#update_member_status').valid()){
      $('#submit').attr('disabled',true)
      var data = $('#update_member_status').serialize();
      
        $.ajax({
            url      : "{{url('member/update/MemberStatus')}}",
            method   : 'POST',
            data   : data,
            success: function(data) {
              $('#submit').removeAttr('disabled',true)
            if(data.status == "success")
            {
              $("#alert-primary").html("Member Status changed successfully");
              $('#myModal').modal('hide');
              location.reload();
                $('#successmsg').show();
                $('html, body').animate({
                    scrollTop: $('.main-header').offset().top
                }, 1000);
                setTimeout(function(){ $('#successmsg').fadeOut() }, 5000);
            }else{
              $('#myModal').modal('hide');
              $("#alert-primary").html("Something went wrong");
              $('#successmsg').show();
              $('html, body').animate({
                    scrollTop: $('.main-header').offset().top
                }, 1000);
              setTimeout(function(){ $('#successmsg').fadeOut() }, 5000);
            }
            },
            error:function() {
              $('#myModal').modal('hide');
              $("#alert-primary").html("Something went wrong");
              $('#successmsg').show();
              $('html, body').animate({
                    scrollTop: $('.main-header').offset().top
                }, 1000);
              setTimeout(function(){ $('#successmsg').fadeOut() }, 5000);
            }
        });
      }
      else{
        return false;
      }
    });

  //Muliple reports upload code
    function ValidateSizeadd(file,id,i){
    if(!/(\.jpg|\.jpeg|\.png|\.pdf)$/i.test(file.value))
    { 
      alert("Invalid image file type.");      
        $('#'+id).val('');  
        return false;   
    }   
      var FileSize = file.files[0].size;
      
      if (FileSize > 6000000)
      {
        $("#"+id+'_error').html('File size should be less than 6 MB').show();
        $('#'+id).val('');  
        return false;
      }else{
        $("#"+id+'_error').hide();
      }
  }

  var s=1;
  function get_divs()
  {
    s++;
    $("#here").append('<div id = "new'+s+'" class="row"> <div class="col-md-6 form-group"><label>Upload File<span style="font-size:12px;color:red;"></span></label><input type="file" class="form-control-file" name="reports[]" id="img'+s+'" style="border:0px;" accept="image/x-png,image/png,image/gif,image/jpeg,application/pdf,doc" onchange="ValidateSizeadd(this,this.id)"><span class="error text-danger" id="img'+s+'_error"></span></div><button type="button" class="btn btn-danger" style="height: 30px;margin-top: 25px;font-size: 12px;line-height: 20px;" onclick="delete_divs('+s+');">remove</button></div>');
  }

  function delete_divs(s)
  {
    $("#new"+s).remove();
    s-1;
  }

  function removeimage(id){
    $('#'+id).remove()
    $('.'+id).val(" ");
    $('.'+id).attr("disabled", "disabled"); 
}  

//script to add Paternal details
function addNomineeData(count){
  var count=$('#testinput').val();
  var j=count;
  addNominee='<div id="add_nominee_div'+j+'"><hr><div class="row d-flex justify-content-end"><a href="javascript:void(0);"  class="btn btn-danger" onclick="removeNominee('+j+')">remove-</a></div><div class="row"><div class="col-md-6 form-group mb-3"><label for="nominee_name" class="">Nominee Name<span style="color: red">*</span></label><input type="text" class="form-control" id="NomineeDetails_'+j+'_nominee_name"  name="NomineeDetails['+j+'][nominee_name]" placeholder="Enter Nominee Name"><span class="error text-danger" id="NomineeDetails_'+j+'_nominee_name_error"></span></div><div class="col-md-6 form-group mb-3"><label for="nominee_name" >FC Amount Percentage<span style="color: red">*</span></label><input type="text" class="form-control fc_percentage" id="NomineeDetails_'+j+'_nominee_fcp"  name="NomineeDetails['+j+'][nominee_fcp]" placeholder="Enter FC amount %" onchange="getId('+j+')"><span class="error text-danger" id="NomineeDetails_'+j+'_nominee_fcp_error"></span></div></div><div class="row"><div class="col-md-6 form-group mb-3"><label for="nominee_phone" class="">Nominee Phone<span style="color: red">*</span></label><input type="text" class="form-control" id="NomineeDetails_'+j+'_nominee_phone"  name="NomineeDetails['+j+'][nominee_phone]" placeholder="Enter Nominee Phone" maxlength="10"><span class="error text-danger" id="NomineeDetails_'+j+'_nominee_phone_error"></span></div><div class="col-md-6 form-group mb-3"><label for="nominee_email" class="">Nominee Email<span style="color: red">*</span></label><input type="email" class="form-control" id="NomineeDetails_'+j+'_nominee_email"  name="NomineeDetails['+j+'][nominee_email]" placeholder="Enter Nominee Email"><span class="error text-danger" id="NomineeDetails_'+j+'_nominee_email_error"></span></div></div><div class="row"><div class="col-md-6 form-group mb-3"><label for="nominee_gender" class="">Nominee Gender<span style="color: red">*</span></label><select class="form-control " name="NomineeDetails['+j+'][nominee_gender]" id="NomineeDetails_'+j+'_nominee_gender"><option value="">Please Select</option><option value="Male">Male</option><option value="Female">Female</option></select><span class="error text-danger" id="NomineeDetails_'+j+'_nominee_gender_error"></span></div><div class="col-md-6 form-group mb-3"><label for="nominee_dob" class="">Nominee DOB<span style="color: red">*</span></label><input type="text" class="form-control nominee_dob" id="NomineeDetails_'+j+'_nominee_dob"  name="NomineeDetails['+j+'][nominee_dob]" placeholder="Select Nominee DOB" autocomplete="nope" readonly="" style="background-color: white"><span class="error text-danger" id="NomineeDetails_'+j+'_nominee_dob_error"></span></div></div><div class="row"><div class="col-md-6 form-group mb-3"><label for="nominee_relation" class="">Nominee Relation<span style="color: red">*</span></label><input type="text" class="form-control" id="NomineeDetails_'+j+'_nominee_relation"  name="NomineeDetails['+j+'][nominee_relation]" placeholder="Enter Relation Name" autocomplete="nope"><span class="error text-danger" id="NomineeDetails_'+j+'_nominee_relation_error"></span></div><div class="col-md-6 form-group mb-3"><label for="nominee_address" class="">Nominee Address<span style="color: red">*</span></label><textarea class="form-control" id="NomineeDetails_'+j+'_nominee_address" name="NomineeDetails['+j+'][nominee_address]" rows="3" placeholder="Enter Nominee Address"></textarea><span class="error text-danger" id="NomineeDetails_'+j+'_nominee_address_error"></span></div></div><div class="row"><div class="col-md-6 form-group mb-3" style="margin-top:-15px;margin-left:-10px"><div class="col-md-6 form-group mb-2"><label for="profile_photo">Nominee Photo</label></div><div class="col-md-6 form-group mb-3"><input type="file" name="NomineeDetails['+j+'][nominee_photo]"  id="NomineeDetails_'+j+'_nominee_photo" accept="image/x-png,image/png,image/gif,image/jpeg" class="customer_id_images" onchange="ValidateSizeadd(this,this.id)"><span class="error text-danger" id="NomineeDetails_'+j+'_nominee_photo_error"></span></div></div></div></div>'; 

    $(".new_nominee_div").show().append(addNominee);
    $("#NomineeDetails_"+j+"_nominee_name").rules("add", {required:true, only_text:true, minlength:3,maxlength:100 });
    $("#NomineeDetails_"+j+"_nominee_address").rules("add", {required:true, maxlength:1000 });
    $("#NomineeDetails_"+j+"_nominee_dob").rules("add", { required:true});
    $("#NomineeDetails_"+j+"_nominee_gender").rules("add", { required:true});
    $("#NomineeDetails_"+j+"_nominee_relation").rules("add", { required:true,only_text:true, minlength:3,maxlength:100});
    $("#NomineeDetails_"+j+"_nominee_email").rules("add", { required:true,maxlength:100});
    $("#NomineeDetails_"+j+"_nominee_phone").rules("add", { required:true,minlength:10 });
    $("#NomineeDetails_"+j+"_nominee_fcp").rules("add", { required:true,number : true,minlength:1,maxlength:3,max:100 });

    var percent= calculatePercentage(j)
    
    if(percent>0){
      var percent_share=100-parseInt(percent);
    }else{
      var percent_share=parseInt(percent);
    }

    $('#NomineeDetails_'+j+'_nominee_fcp').val(percent_share)
    $('html, body').animate({
        scrollTop: $('#add_nominee_div'+j).offset().top
    }, 1000);
    j++;
    count=parseInt(count)+1;
    $('#testinput').empty()
    $('#testinput').val(count)
  }

  function getId(id){

    calculatePercentage(id);
  }

  function calculatePercentage(id){
    
      var sum = 0;
      $(".fc_percentage").each(function(){
      sum += +$(this).val();
      if(sum>100){
        alert('Total FC Percentage should not exceed 100%')
        $('#NomineeDetails_'+id+'_nominee_fcp').val('')
      }
    });
    return sum;
  }

  function removeNominee(value){
      var count=$('#testinput').val();
      count=parseInt(count)-1;

      var id="NomineeDetails"+value+"_nominee_id";
      var nominee_id=$('#'+id).val()

      $('#add_nominee_div'+value).empty()
      $('#add_nominee_div'+value).remove()
      $('#add_nominee_div'+value).hide()
      $('#add_nominee').removeAttr('hidden');
      $('#testinput').val(count)

      $.ajax({
        url      : "{{url('/member/delete/nomineeDetails/')}}/"+nominee_id,
          method   : 'GET',
          }).done(function( data ) {
            if(data.status == 'success'){
              $("#alert-primary").html("Nominee Details Deleted Successfully");
                $('#successmsg').show();
                $('html, body').animate({
                    scrollTop: $('.main-header').offset().top
                }, 1000);
                setTimeout(function(){ $('#successmsg').fadeOut() }, 5000);
            }
            else{
                $("#alert-primary").html("Something Went Wrong!");
                $("#successmsg").show();
                $('html, body').animate({
                    scrollTop: $('.main-header').offset().top
                }, 1000);
                setTimeout(function(){ $('.alert_notice').fadeOut() }, 5000);
            }
      });
  }

  var k=1;
    function add_divs()
    {
      k++;
      $("#new_file").append('<div id = "new'+k+'" class="row"> <div class="col-md-6 form-group"><label>Upload File<span style="font-size:12px;color:red;"></span></label><input type="file" class="form-control-file" name="application_form[]" accept="image/x-png,image/png,image/gif,image/jpeg,application/pdf,doc" id="img'+k+'" style="border:0px;" ><span class="error text-danger" id="img'+s+'_error"></div><button type="button" class="btn btn-danger" style="height: 30px;margin-top: 25px;font-size: 12px;line-height: 20px;" onclick="delete_div('+k+');">remove</button></div>');
    }
    function delete_div(k)
    {
      $("#new"+k).remove();
      k-1;
    }

  $('.Cancel').on("click",function() {
    var $form = $('#update_member_status');
    $form.validate().resetForm();
    $form.find('.error').removeClass('error');
  })  

  function changePassowrd(){
   $('#changePassword').modal({
        show: true,
        keyboard: false,
        backdrop: 'static'
    })
  }

  $("#savePassword").on('click', function(){
        $('#passowrd_change').validate({
            rules: {
                newPassword:{
                    required:true,
                    minlength:6,
                    maxlength:30
                },
                confirmPassword:{
                    required:true,
                    minlength:6,
                    maxlength:30,
                    equalTo :"#new_password"
                },
            },
            messages:{
                confirmPassword:{
                    equalTo:"Password and Confirm Password should be same"
                  },
            }
        });

        if($('#passowrd_change').valid()){ 
          $('#savePassword').attr('disabled',true)
          $('.loadscreen').show();
          $("#changePassword").modal("hide");
            $.ajax({
                method: "POST",
                url: "{{url('user/update-password')}}",
                data:{
                        "_token": $('#token').val(),
                        "id": $('#users_id').val(),
                        "newPassword":$('#new_password').val() ,
                        "oldPassword":$('#old_password').val() ,
                        "confirmPassword":$('#confirm_passowrd').val(),
                      },
                })
            .done(function( data ) {
              $('.loadscreen').hide();
              $('#savePassword').removeAttr('disabled',true)
                if(data.status == 'success'){
                    $("#old_password").val(data.data);
                    $("#changePassword").modal("hide");
                    $('#password,#confirm_passowrd').val('')
                    $(".alert-primary").html('Password Changed Successfully');
                    $("#successmsg").show();
                    $('html, body').animate({
                        scrollTop: $('.main-header').offset().top
                    }, 1000);
                    setTimeout(function(){ $('#successmsg').fadeOut() }, 3000);
                }
                else if(data.status == 'validation_error'){
                  $("#changePassword").modal("show");
                    $.each(data.data, function (key, val) {
                        $("#"+key).text(val[0]);
                        $(document).on("keyup", "input[name='"+key+"']", function(e) {
                              $("#"+key).hide();
                         });
                    });
                }
                else{
                    $("#changePassword").modal("hide");
                    $(".alert-primary").html("Something Went Wrong!");
                    $("#successmsg").show();
                    $('html, body').animate({
                        scrollTop: $('.main-header').offset().top
                    }, 1000);
                    setTimeout(function(){ $('#successmsg').fadeOut() }, 3000);
                }
            });
            }
        });

    $('.closePopup').on('click', function(){
         $('#new_password,#confirm_passowrd').val('')
         var $model = $('#changePassword');
         $model.validate().resetForm();
         $model.find('.error').removeClass('error');
   })


    $("#mobile_number").change(function(){
      $.ajax({
            method: "POST",
            url : "{{url('member/checkDuplicates')}}",
            data :{
              "_token": "{{ csrf_token() }}",
              "mobile_number": $('#mobile_number').val(),
              "id": $('#users_id').val(),
            },
          })
          .done(function( data ) {
            if(data.message.mobile_number =='The mobile number has already been taken.'){
                  $('#nav-tab a[href="#nav-home"]').tab('show');
                  $('#mobile_number_error').show()
              }
              else if(data.message ==''){
                $('#mobile_number_error').hide()
              }
          });
    });

    $('#mobile_number').on('keyup',function(){
        $('#mobile_number_error').hide();
      })

    $("#email").change(function(){
      $.ajax({
            method: "POST",
            url : "{{url('member/checkDuplicates')}}",
            data :{
              "_token": "{{ csrf_token() }}",
              "email": $('#email').val(),
              "id": $('#users_id').val(),
            },
          })
          .done(function( data ) {
              if(data.message.email =='The email has already been taken.'){
                  $('#nav-tab a[href="#nav-home"]').tab('show');
                  $('#email_error').show()
              }
              else if(data.message ==''){
                $('#email_error').hide()
              }
          });
   }); 

    $('#email').on('keyup',function(){
        $('#email_error').hide();
      })
</script>

@endsection