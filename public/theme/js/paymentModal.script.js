
function generateLink(){

	var plan_type=$('#plan_type').val();
	
	if (plan_type=='Personalized') {
		var plan_type=$('#personalized_plan_type').val();
	}
    $.ajax({
    	url : 'generatePaymentLink',
      data:{
          'customer_id':$('#customer_id').val(),
           'plan_name':plan_type,
        '_token':$('#token').val(),
          },
      method:"post",
      success:function(res){
          if(res.status == 'success'){
          	$('#link_error').attr('hidden',true)
            $('#generated_link').removeAttr('hidden',true)
            $('#payment_link').val(res.data)
            $('#link_div').html('<h6 class="info" id="pay_link">'+res.data+'<a href="#" onclick="return copy()"><i class="fa fa-copy ml-2 text-muted"></i></a></h6>')
           }
      }
    })
}

 function copy(){
 	$('#copy_link_div').removeAttr('hidden')
  var copyText = document.getElementById("payment_link");
  copyText.select();
  copyText.setSelectionRange(0, 99999)
  document.execCommand("copy");
  $('#copy_link_div').attr('hidden',true)
  // alert("Copied the text: " + copyText.value)
}

 
 function resetModal(){
	 document.newPaymentForm.reset();
     //$("#newPaymentForm").data('validator').resetForm();
     $("#offline_payment").attr('hidden',true);
     $("#remarks_div").attr('hidden',true);
     $("#online_payment").attr('hidden',true);
     $('#personalized_div').attr('hidden',true)
     $('#mode_of_payment').removeAttr('readonly',true)
     $('#generated_link').attr('hidden',true);
     $('#popup_note').attr('hidden',true);
     $('#bank').attr('hidden',true);
     $("#date_of_payment").val('');
     $("#transaction_id_div").attr('hidden',true);
     $('#personalized_plan_type_div').attr('hidden',true);
     $('#date_of_payment_div').attr('hidden',true);
 }
$(document).ready(function () {
    
	
	$("#date_of_payment").datepicker({
	    dateFormat: "dd-mm-yy",
	    autoclosed:true,
	    changeMonth: true,
	    changeYear: true,
	    maxDate: 0
	    
	});

	  $.validator.addMethod("numeric", function (value, element, param) {
	       return this.optional(element) || /^[-]?[0-9]+$/.test(value);
	    }, "Enter only numbers.");

	  $.validator.addMethod("alpha_numeric", function (value, element, param) {
	       return this.optional(element) || /^[-]?[a-zA-Z0-9]+$/.test(value);
	    }, "Enter only alphabets and numbers");

	  
	   $('#mode_of_payment').change(function(){
	        var name = $("#mode_of_payment option:selected").text();
	        
	        if (name=='Offline') {
	          $('#online_payment').attr('hidden',true);
	          $('#popup_note').attr('hidden',true);
	          $('#offline_payment').removeAttr('hidden',true);
	          $('#remarks_div').removeAttr('hidden',true);
	          $('#payment_link').val('');
	          $('#generated_link').attr('hidden',true);
	          $('#transaction_id_div').removeAttr('hidden',true);
	          $('#date_of_payment_div').removeAttr('hidden',true);
	          $('#date_of_payment').val('');
	          $('#date_of_payment').attr('required',true);
	          $('#link_error').attr('hidden',true)
	        }
	        if (name=='Online') {
	          $('#offline_payment').attr('hidden',true);
	          $('#remarks_div').attr('hidden',true);
	          $('#online_payment').removeAttr('hidden',true);
	          $('#popup_note').removeAttr('hidden',true);

	          $('#offline_payment_mode').removeAttr('required',true);
	          $('#offline_payment_mode').prop('selectedIndex',0);
	          $('#remark').removeAttr('required',true);
	          $('#remark').val('');
	          $('.offline_payment').closest('.form-group').find('label.error').remove();
	          $('.remarks_div').closest('.form-group').find('label.error').remove();

	          $('#transaction_id').val('');
	          $('#transaction_id_div').attr('hidden',true);
	          $('#bank').attr('hidden',true);
	          $('#date_of_payment_div').attr('hidden',true);
	          $('#date_of_payment').val('');
	          $('#date_of_payment').removeAttr('required',true);
	        }
	        if(name=='Please Select'){
	          $('#popup_note').attr('hidden',true);
	          $('#online_payment').attr('hidden',true);
	          $('#offline_payment').attr('hidden',true);
	          $('#remarks_div').attr('hidden',true);
	          $('#payment_link').val('');
	          $('#offline_payment_mode').removeAttr('required',true);
	          $('#offline_payment_mode').prop('selectedIndex',0);
	          $('#remark').removeAttr('required',true);
	          $('.offline_payment').closest('.form-group').find('label.error').remove();
	          $('.remarks_div').closest('.form-group').find('label.error').remove();

	          $('#transaction_id_div').attr('hidden',true);
	          $('#transaction_id').val('');
	          $('#date_of_payment').val('');
	          $('#date_of_payment_div').attr('hidden',true);
	          $('#link_error').attr('hidden',true)
	        }
	    })

	   $('#payment_type').change(function(){
	        var name = $("#payment_type option:selected").text();
	        console.log(name);
	        var active_plan=$('#customer_active_plan').val();
	        
	         
	         if (active_plan=='Personalized') {
	        	$('#personalized_plan_type_div').removeAttr('hidden',true);
	         }
	         $('#personalized_div').attr('hidden',true);
	         $("#remarks_div").attr('hidden',true);
	         $("#offline_payment").attr('hidden',true);
	         $("#online_payment").attr('hidden',true);
	         $('#mode_of_payment').prop('selectedIndex',0);
	         $('#mode_of_payment').removeAttr('readonly',true)
	         $("select option[value*='Online']").prop('disabled',false);

	        if (name=='Renewal') {
	          $.ajax({
	                url:'getPlanDetails',
	                data:{
	                    'plan_name':active_plan,
	                    '_token':$('#token').val(),
	                    },
	                method:"post",
	                success:function(res){
	                  
	                  if (res.status=='success') {
	                    $('#amount').val(res.amount).attr('readonly',true)
	                    $('#validity_months').val(res.validity).attr('readonly',true)
	                    $('#personalized_div').removeAttr('hidden',true)
	                  }
	                  if (res.status=='fail') {
	                     $('#amount').val('').removeAttr('readonly',true)
	                     $('#validity_months').val('').removeAttr('readonly',true)
	                      $('#personalized_div').attr('hidden',true)
	                  }
	            }
	          })
	          $("#plan_type").val(active_plan).attr("selected","selected");
	          $("#plan_type").attr("readonly",true);
	          $('#plan_type').each(function(){
	              $('#plan_type option').each(function() {
	                if(!this.selected) {
	                  $(this).attr('disabled', true);
	                }
	              });
	            });
	          
	        }
	        else{
	        	$('#personalized_plan_type_div').attr('hidden',true);
	              $('#amount').val('').removeAttr('readonly',true)
	              $('#validity_months').val('').removeAttr('readonly',true)
	              $('#personalized_div').attr('hidden',true)
	              $('#plan_type').each(function(){
	              $('#plan_type option').each(function() {
	                if(!this.selected) {
	                  $("#plan_type").removeAttr("readonly",true);
	                  $('#plan_type').prop('selectedIndex',0);
	                  $(this).removeAttr('selected', true);
	                  $(this).removeAttr('disabled', true);
	                }
	              });
	            });
	        }
	    })
	    
	    
	    $('#offline_payment_mode').change(function(){
	    	var payment_mode = $("#offline_payment_mode option:selected").text();
	    	if(payment_mode == 'Bank'){
	    		$('#bank').removeAttr('hidden',true);
	    	}else{
	    		$('#bank').attr('hidden',true);
	    	}
	    });

	   $('#plan_type').change(function(){
	        var name = $("#plan_type option:selected").text();
	        
	       	$.ajax({
	        url:'getPlanDetails',
	        data:{
	            'plan_name':$("#plan_type option:selected").text(),
	            '_token':$('#token').val(),
	            },
	        method:"post",
	        success:function(res){
	           console.log(res)
	           if (res.status=='success') {
	             $('#amount').val(res.amount).attr('readonly',true)
	             $('#validity_months').val(res.validity).attr('readonly',true)
	             $('#personalized_div').removeAttr('hidden',true)
	          }
	          if (res.status=='fail') {
	             $('#amount').val('').removeAttr('readonly',true)
	             $('#validity_months').val('').removeAttr('readonly',true)
	              $('#personalized_div').attr('hidden',true)
	          }

	        }
	      })

	        if (name=='Personalized') {

	          $('#personalized_plan_type_div').removeAttr('hidden',true);
	          $('#personalized_plan_type').prop('selectedIndex',0);
	          // $('#personalized_div').removeAttr('hidden',true);
	          $('#offline_payment').removeAttr('hidden',true);
	          $('#remarks_div').removeAttr('hidden',true);
	          $('#mode_of_payment').removeAttr('required',true);
	          $('#online_payment').attr('hidden',true);
	          $('#generated_link').attr('hidden',true);
	          $('#transaction_id_div').removeAttr('hidden',true);
	        }
	        else{
	            $('#amount').val('');
	            $('#validity_months').val('');
	            $('#personalized_plan_type_div').attr('hidden',true);
	            $('#personalized_div').attr('hidden',true);
	            $('#offline_payment').attr('hidden',true);
	            $('#remarks_div').attr('hidden',true);
	            $('.personalized_div').closest('.form-group').find('label.error').remove();
	            $('#mode_of_payment').prop('selectedIndex',0);
	            $("select option[value*='Online']").prop('disabled',false);
	            $("select option[value*='Please Select']").prop('disabled',false);
	            $("#mode_of_payment").attr("readonly",false);
	            $('#mode_of_payment').attr('required',true);
	            $('#transaction_id_div').attr('hidden',true);
	        }
	    })  
	    
	    $('#personalized_plan_type').change(function(){
	        var name = $("#personalized_plan_type option:selected").text();
	        
	        if(name=='Please Select'){
	        	 $('#amount').val('').removeAttr('readonly',true)
	             $('#validity_months').val('').removeAttr('readonly',true)
		         $('#personalized_div').attr('hidden',true);
		         
		          		          
		    }else{
		    	$.ajax({
		        	url: 'getPlanDetails',
			        data:{
			            'plan_name':$("#personalized_plan_type option:selected").text(),
			            '_token':$('#token').val(),
			            },
			        method:"post",
			        success:function(res){
			           $('#personalized_div').removeAttr('hidden',true);
			           if (res.status=='success') {
			             $('#amount').val(res.amount).attr('readonly',true)
			             $('#validity_months').val(res.validity).attr('readonly',true)
			          }
			          if (res.status=='fail') {
			             $('#amount').val('').removeAttr('readonly',true)
			             $('#validity_months').val('').removeAttr('readonly',true)
			          }
			        }
			      })
		    }
	    }) 
	    
            $('#newPaymentForm').validate({
              rules:{
                employee_id:{
                  required:true,
                },
                date_of_payment:{
                    required:true,
                  },
                bank_list:{
                	  required:true,
                },
                plan_type:{
                  required:true,
                },
                 mode_of_payment:{
                  required:true,
                },
                 offline_payment_mode:{
                  required:true,
                }, 
                transaction_id:{
                  maxlength:20,
                },
                payment_remark:{
                  required:true,
                  maxlength:1000
                },
                validity_months:{
                  numeric:true,
                  required:true,
                },
                amount:{
                  numeric:true,
                  required:true,
                },
                payment_type:{
                  required:true,
                },
                personalized_plan_type:{
                  required:true,
                },
              },

            });
	   
     $('.cancelPayment').on('click', function(){
    	 resetModal();
    	 var $alertas = $('#newPaymentForm');
		 $alertas.validate().resetForm();
		 $alertas.find('.error').removeClass('error');
		 $('#link_error').attr('hidden',true)
        })

     $("#makePayment").on('click', function(){

     	var name = $("#mode_of_payment option:selected").text();
     	
     	if (name=='Online') {
     		var link=$('#payment_link').val();
     		if (link === null || link === ''|| link ===undefined ) {
     			$('#link_error').removeAttr('hidden',true)
     			e.preventDefault();
     		}
     	}
     });
});