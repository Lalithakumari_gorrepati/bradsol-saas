//Displaying email template on modal
function display_email_template(id)
{
  $.ajax({
    url : 'View_Email_Template/'+id,
    dataType : 'json',
    success : function(data){
      $('#template_name').html(data.result.template_name);
      $('#subject').html(data.result.subject);
      $('#body').html(data.result.body);
      $('#myModal').modal('show');
    }
  });
}

setTimeout(function(){
    $('.alert_notice').hide('slow');
}, 3000);

//Displaying SMS template on modal
function display_sms_template(id)
{
  $.ajax({
    url : 'View_SMS_Template/'+id,
    dataType : 'json',
    success : function(data){
      $('#template_name').html(data.result.template_name);
      $('#subject').html(data.result.subject);
      $('#body').html(data.result.body);
      $('#myModal').modal('show');
    }
  });
}

//Displaying Whatsapp template on modal
function display_whatsapp_template(id)
{
    //$('#myModal').modal('show');
    $.ajax({
      url : 'View_Whatsapp_Template/'+id,
      dataType : 'json',
      success : function(data){
        $('#template_name').html(data.result.template_name);
        $('#subject').html(data.result.subject);
        $('#body').html(data.result.body);
        $('#myModal').modal('show');
      }
    });
}


//open modal for confirmation about delete or not
function is_religion_deleted(id)
{
  $('#id').val(id);
  $('#isDeleted').modal('show');
  return false;
}

function delete_religion(id)
{
  var id = $('#id').val();
  window.location = 'deleteReligion/'+id
}

//Delete religion
function addReligionValidation()
{
  var rel_name = $('#religion_name').val();
  if(rel_name == '')
  {
    $('#err-religion').text('Name is required');
    return false;
  }
  else
  {
    return true;
  }
}

function EditDropdwonValidation()
{
  var dropdown_name = $('#dropdown_name').val();
  if(dropdown_name == '')
  {
    $('.err-dropdown').text('Name is required');
    return false;
  }
  else
  {
    return true;
  }
}

function dropdownValidation()
{
  var dropdown = $('#dropdown_value').val();
  var dd_name = $('#dd_name').val();
  if(dropdown == '')
  {
    $('.errDropdown').text(dd_name+' is required');
    return false;
  }
  else
  {
    return true;
  }

}



