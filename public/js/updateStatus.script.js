$(document).on("click",".change_status_new",function() {
    $("#confirm_date").datepicker("destroy");
    $('html, body').animate({
      scrollTop: $('.main-header').offset().top
    }, 1000);
    $('#myModal').modal({
        show: true,
        keyboard: false,
        backdrop: 'static'
    });
    var id = $(this).data('id');
    var status = $(this).data('status');
    
    var joining_date = $(this).data('joining_date');
    
    $("#member_status").val(status).change();

    $('#user_id').val(id);
    $('#status').val(status); 

    $( "#confirm_date" ).datepicker({
        container:'#myModal',
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd-mm-yy',
        minDate:joining_date,
        maxDate: 0,
    });
  }); 

$(document).on("click","#submit",function() {

    $('#update_member_status').validate({
        ignore: "",
        rules:{
          fc_note:{
            maxlength:1000,
          },
          member_status:{
            required: true,
          },
        }
    });
    if($('#update_member_status').valid()){
      $('#submit').attr('disabled',true)
      var data = $('#update_member_status').serialize();
        $.ajax({
            url      : '/member/update/MemberStatus',
            method   : 'POST',
            data   : data,
            success: function(data) {
              $('#submit').removeAttr('disabled',true)
            if(data.status == "success")
            {
              $('#myModal').modal('hide')
              $('#members_list_table').DataTable().draw();
              $("#alert-primary").html("Member Status Updated Successfully");
                $('#successmsg').show();
                $('html, body').animate({
                    scrollTop: $('.main-header').offset().top
                }, 1000);
                setTimeout(function(){ $('#successmsg').fadeOut() }, 5000);
            }else{
              
              $("#alert-primary").html("Something went wrong");
              $('#successmsg').show();
              $('html, body').animate({
                    scrollTop: $('.main-header').offset().top
                }, 1000);
              setTimeout(function(){ $('#successmsg').fadeOut() }, 5000);
            }
            },
            error:function() {
              // $('#myModal').modal('hide')
              $("#alert-primary").html("Something went wrong");
              $('#successmsg').show();
              $('html, body').animate({
                    scrollTop: $('.main-header').offset().top
                }, 1000);
              setTimeout(function(){ $('#successmsg').fadeOut() }, 5000);
            }
        });
      }
      else{
        return false;
      }
  }); 

$('.Cancel').on("click",function() {
    var $form = $('#update_member_status');
    $form.validate().resetForm();
    $form.find('.error').removeClass('error');
})