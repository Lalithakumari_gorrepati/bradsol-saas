$('#member_status').change(function(){

    if($(this).val() =='Expired'){
        $('#expired_fields_div').show();
        $('#confirm_date').attr('required',true)
        $('#fc_amount').attr('required',true)
        $('#final_fc_amount').attr('required',true)
    }else{

      $('#expired_fields_div').hide();
      $('#confirm_date').val("");
      $('#fc_note').val("");
      $('#fc_amount').val("");
      $('#pending_due').val("");
      $('#final_fc_amount').val("");
      $('#confirm_date').removeAttr('required')
      $('#fc_amount').removeAttr('required')
      $('#final_fc_amount').removeAttr('required')
    }
  })
   
  $('#confirm_date').change(function(){
    $('#confirm_date-error').hide()
      getFCAmount($(this).val())
  })


  function getFCAmount(date){

      $.ajax({
            url: '/member/getFCAmount',
            data:{
                  "date"  : date,
                  "id"    : $('#user_id').val(),
                  '_token':$('#token').val(),
            },
            method: "POST",
          }).done(function(data) {
          if(data){
              $('#fc_amount-error').hide()
              $('#fc_amount').val(data)
              getPendingDue(data);
          };
      })
      
  }

  function getPendingDue(fc_amount){
      $.ajax({
            method: "POST",
            url: '/member/getPendingDue',
            data:{
                  "eligible_fc" : fc_amount,
                  "advance" : $('#advance_amount').val(),
                  "id"    : $('#user_id').val(),
                  '_token':$('#token').val(),
            },
          }).done(function(data) {
          if(data){
            $('#final_fc_amount-error').hide()
              $('#pending_due').val(data.due);
              $('#final_fc_amount').val(data.released_fc);
          };
      })
  }